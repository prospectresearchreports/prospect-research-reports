<?php
require '../vendor/autoload.php';
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\IOFactory;


const COLUMNS = array(
	'id',
	'rep_title',
	'rep_descrip',
	'rep_url'
);
const EXCEL_HEADERS = array(
    'Keyword',
    'Report Title',
    'Report Description',
    'Company Name',
    'Report Segmentation',
    'Sample URL',
    'Customization URL',
    'Enquire Before Purchase URL',
    'Discount URL',
    'Checkout URL'
);
#DEVELOPMENT
// $db_server    = "localhost";
// $db_user       ="root";
// $db_pass       ="";
// $db_name      ="market_research";
// @$link = new mysqli($db_server, $db_user, $db_pass,$db_name);
// $base_url = "http://localhost/prospect-research-reports/";

#PRODUCTION
$db_server    ="localhost";
$db_user       ="prospect_re2020";
$db_pass       ="prospect_re2020";
$db_name      ="prospect_research_reports";
@$link = new mysqli($db_server, $db_user, $db_pass,$db_name);
$base_url = "https://prospectresearchreports.com/";

// Get JSON input from AJAX request
$input = file_get_contents('php://input');
// $input = '{"selected_ids": ["202","201"]}';
$data = json_decode($input, true);
// echo "<pre>";var_dump($link);exit;


// Validate input
if (!isset($data['selected_ids']) || empty($data['selected_ids'])) {
    http_response_code(400);
    echo json_encode(['error' => 'No data selected']);
    exit;
}

$selectedIds = $data['selected_ids'];
// Example: Tabular data (replace with actual database query)
$columns = implode(",", COLUMNS);
$report_list_query="select $columns from mr_report where id in (".implode(",", @$selectedIds).")";
// echo $report_list_query;
$report_list_result = $link->query($report_list_query);
$report_list_result_data = array();   

if($report_list_result && $report_list_result->num_rows > 0){
    while($row=$report_list_result->fetch_assoc()){
        $rep_title_arr = explode('Market',$row['rep_title']);
        $rep_id = $row['id'];
        $report_url = $base_url.'report/'.$rep_id;

        $sample_url = $report_url.'?type=request_sample';
        $ebp_url = $report_url.'?type=enquire_before_purchase';
        $discount_url = $report_url.'?type=request_discount';
        $customization_url = $report_url.'?type=request_customization';
        $checkout_url = $base_url.'reports/'.$rep_id.'/'.$row['rep_url'];
        $report_list_result_data[] = array(
            'keyword' => $rep_title_arr[0],
            'rep_title' => $row['rep_title'],
            'rep_descrip' => $row['rep_descrip'],
            'company_name' => '',
            'report_segmentation' => '',
            'sample_url' => $sample_url,
            'customization_url' => $customization_url,
            'ebp_url' => $ebp_url,
            'discount_url' => $discount_url,
            'checkout_url' => $checkout_url
 
        );
    }
    array_unshift($report_list_result_data, EXCEL_HEADERS);
    // echo "<pre>";print_r(EXCEL_HEADERS);exit;

    $spreadsheet = new Spreadsheet();
    // Set document properties
    $spreadsheet->getProperties()->setCreator('ProspectResearch Reports')
        ->setLastModifiedBy('Admin')
        ->setTitle('Report Metadata')
        ->setSubject('Report Metadata')
        ->setDescription('Report Metadata')
        ->setKeywords('Report Metadata')
        ->setCategory('Report Metadata');
    
    // Populate spreadsheet with filtered data
    $rowIndex = 1;
    // echo "<pre>";print_r($report_list_result_data);exit;
    foreach ($report_list_result_data as $row) {
        $colIndex = 'A';
        foreach ($row as $cell) {
            if($rowIndex == 1){
                $cell = ucwords(str_ireplace("_"," ", $cell));
            }
            // echo $cell.'<br/>';
            $spreadsheet->setActiveSheetIndex(0)->setCellValue("{$colIndex}{$rowIndex}", $cell);
            $colIndex++;
        }
        $rowIndex++;
    }
    $spreadsheet->getActiveSheet()->setTitle('Report Metadata');
    $spreadsheet->setActiveSheetIndex(0);


    // Redirect output to a client’s web browser (Xlsx)
    header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
    header('Content-Disposition: attachment;filename="ReportMetadata.xlsx"');
    header('Cache-Control: max-age=0');

    $writer = IOFactory::createWriter($spreadsheet, 'Xlsx');
    $writer->save('php://output');
    exit;
}else{
    echo json_encode(['error' => 'No reports found']);
    exit;
}
?>
