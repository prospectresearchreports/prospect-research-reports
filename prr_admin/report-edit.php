
<html>
<head>
<title>Edit  | Prospect Research Reports</title>



 <!--*****************************  HEADER  ************************************** -->

<?php 
    require_once 'header.php';
    require_once 'auth.php';
?>

<!--*****************************  HEADER  ************************************** -->

<?php 
$id=isset($_GET["id"]) ? $_GET['id'] : (isset($_POST['id']) ? $_POST['id'] : '');
$getReportDetails=$link->query("select * from mr_report left join mr_report_metadata on mr_report.id=mr_report_metadata.meta_rep_id  where mr_report.id='$id' ");
$row=$getReportDetails->fetch_assoc();

?>

<?php
    if(isset($_POST["submit"])){
		// echo "<pre>";print_r($_POST["content"]);exit;

        $rep_id = $_POST['id'];
        $category=$_POST["category"];
        $publisher=$_POST["publisher"];
        $title=addslashes($_POST["title"]);
        // $url=$_POST["url"];
        // $url=generateReportURL($url, $link);
        $pages=$_POST["page"];
        $content=$_POST["content"];
        $description=$_POST["description"];
        $toc=$_POST["toc"];
        $tof=$_POST["tof"];

        $status=$_POST["status"];
        $archive_status=$_POST["archive_status"];

        $published_date=$_POST["date"];
        $date=date("Y-m-d H:i:s");

		// REPORT LICENSES		
		$li_key1="Single User License";
		$li_key2="Enterprise User License";
		$li_key3="Corporate User License";
		$li_value1=$_POST["single_user_license"];
		$li_value2=null;
		$li_value3=null;
		if(isset($_POST['enterprise_user_license'])){			
			$li_value2=$_POST["enterprise_user_license"];
		}
		if(isset($_POST['corporate_user_license'])){			
			$li_value3=$_POST["corporate_user_license"];
		}
		$is_manual = true;
		$update_stmt = $link->prepare("update mr_report set is_manual = ?, rep_title = ?, rep_sub_cat_1_id = ?,publisher_id= ? ,rep_descrip = ?,rep_page = ?,rep_date = ?,rep_toc_fig = ?,rep_entry_date = ? where id = ? ");
		$update_stmt->bind_param('sssssssssd',$is_manual, $title, $category, $publisher, $description, $pages, $date, $tof, $published_date, $rep_id);
		if($update_stmt->execute()){
			$update_stmt->close();
			// UPDATE REPORT METADATA TABLE DATA 
			$update_meta_stmt = $link->prepare("update mr_report_metadata set rep_contents = ?, rep_table_of_contents = ? where meta_rep_id = ? ");
			$update_meta_stmt->bind_param('ssd', $content, $toc, $rep_id);
			$update_meta_stmt->execute();
			$update_meta_stmt->close();
			// UPDATE REPORT LICENSE TABLE DATA 
			if($li_value1 > 0){
				update_licenses($li_key1, $li_value1, $rep_id, $link);
			}
			if($li_value2 > 0){
				update_licenses($li_key2, $li_value2, $rep_id, $link);                            
			}
			if($li_value3 > 0){
				update_licenses($li_key3, $li_value3, $rep_id, $link);                            
			}
        
			$success='<div class="alert alert-success alert-form"><span class="fa fa-check-circle"></span>&nbsp;Report Updated successfully !&nbsp;&nbsp; <mark>Redirecting......</mark></div> 
			<meta http-equiv="refresh" content="2,url='.BASE_URL.'report-list "/>'; 

		}else{
			$error='<div class="alert alert-danger alert-form"><span class="fa fa-times-circle"></span>&nbsp;Error Updating Report !'.(ENVIRONMENT == 'development' ? mysqli_error($link) : '').'</div>';
		}  
    }
    ?>

    <h1 class="stats"><span class="far fa-edit"></span> Edit Report</h1><br/><br/><br/>

    <button  class='btn btn-primary helper-item' onclick="window.history.back()">
        <a href="#" ><i class="fa fa-arrow-left"></i> Back</a> 
    </button>
	<div class="form-container">
	<?php 
		if(isset($success)){
			echo "<br/>".$success;
		}else{
			if(isset($error)){
				echo "<br/>".$error;
			}
	?>    
	    <form class="form-horizontal" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method='post' name="report_edit" id="report_edit" enctype='application/x-www-form-urlencoded'>
	    	
			<input type="hidden" name="id" value="<?=@$id?>" />
			<div class="form-group">
	    		<div class="col-md-10">
		    		<label class="control-label">Report Category<span class="star">*</span> :</label>
		    		<select class="form-control" name="category" id="category" title="Report Category"  required>
                        <option value="">Select Category </option>		
						<?php
	                        $ReportCategory = "select * from mr_sub_cat_1 order by sc1_name";
	                        $ReportCategoryQuery = $link->query($ReportCategory);
	                        if ($ReportCategoryQuery->num_rows > 0){
	                            while ($report_cat = $ReportCategoryQuery->fetch_array()){
									$category_id = $report_cat['id'];  
									$selected =  (isset($category)) ? (@$category == $category_id  ? 'selected' : '' ) :  (@$category_id == @$row['rep_sub_cat_1_id'] ? 'selected' : '') ;                     
									echo "<option value='".$category_id."' ".@$selected."  >".$report_cat["sc1_name"]."</option>";                                
								}
	                        }                        
                        ?>
                    </select>
		    	</div>
	    	</div>
	    	<div class="form-group">
	    		<div class="col-md-10">
		    		<label class="control-label">Publisher<span class="star">*</span> :</label>
		    		<select class="form-control" name="publisher" id="publisher" title="Report publisher"  required>
                        <option value="">Select Publisher </option>	
                        <?php
	                        $ReportPublisher = "select * from mr_publisher order by publisher_name";
	                        $ReportPublisherQuery = $link->query($ReportPublisher);
	                        if ($ReportPublisherQuery->num_rows > 0){
	                            while ($report_pub = $ReportPublisherQuery->fetch_array()){ 
									$publisher_id = $report_pub['id'];  
									$selected =  (isset($publisher)) ? (@$publisher == $publisher_id  ? 'selected' : '' ) :  (@$publisher_id == @$row['publisher_id'] ? 'selected' : '') ;                     
	                                echo '<option value="'. @$publisher_id.'"  '.@$selected.'>'.$report_pub["publisher_name"].'</option>';                                
	                            }
	                        }
                        ?>
                    </select>
		    	</div>
	    	</div>
	    	<div class="form-group">
	    		<div class="col-md-10">
		    		<label class="control-label">Report Title<span class="star">*</span> :</label>
		    		<input  name="title" class="form-control" required="" value="<?=isset($title) ? @$title :  ($row["rep_title"] ? $row["rep_title"] : ''); ?>" />
		    	</div>
	    	</div>

	    	<!-- <div class="form-group">
	    		<div class="col-md-10">
		    		<label class="control-label">Report URL<span class="star">*</span> :</label>
		    		<input  name="url" class="form-control" required="" value="<?//=isset($url) ? @$url :  ($row["rep_url"] ? $row["rep_url"] : ''); ?>" />
		    	</div>
	    	</div> -->
	    	
	    	<div class="form-group">
	    		<div class="col-md-10">
		    		<label class="control-label">Report Description<span class="star">*</span> :</label>
		    		<?php
                        require_once 'ckeditor_old/ckeditor.php';
                        $sBasePath = "ckeditor/";
                        $CKEditor = new CKEditor();
                        $CKEditor->editor("description", isset($description) ? @$description :  $row["rep_descrip"]);
                    ?>  
		    	</div>
	    	</div>
	    	
	    	<div class="form-group">
	    		<div class="col-md-10">
		    		<label class="control-label">Report Contents<span class="star">*</span> :</label>
		    		<?php
                        require_once 'ckeditor_old/ckeditor.php';
                        $sBasePath = "ckeditor/";
                        $CKEditor = new CKEditor();
                        $CKEditor->editor("content", isset($content) ? @$content :  $row["rep_contents"]);
                    ?>  
		    	</div>
	    	</div>
	    	
	    	<div class="form-group">
	    		<div class="col-md-10">
		    		<label class="control-label">Report Table Of Contents :</label>
		    		<?php
                        require_once 'ckeditor_old/ckeditor.php';
                        $sBasePath = "ckeditor/";
                        $CKEditor = new CKEditor();
                        $CKEditor->editor("toc", isset($toc) ? @$toc :  $row["rep_table_of_contents"]);
                    ?>  
		    	</div>
	    	</div>
	    	
	    	<div class="form-group">
	    		<div class="col-md-10">
		    		<label class="control-label">Report Table Of Figures:</label>
		    		<?php
                        require_once 'ckeditor_old/ckeditor.php';
                        $sBasePath = "ckeditor/";
                        $CKEditor = new CKEditor();
                        $CKEditor->editor("tof", isset($tof) ? @$tof :  $row["rep_toc_fig"]);
                    ?>  
		    	</div>
	    	</div>

	    	<div class="form-group">
	    		<div class="col-md-10">
		    		<label class="control-label">Report Pages<span class="star">*</span> :</label>
		    		<input  name="page" class="form-control" required="" value="<?=isset($pages) ? @$pages :  ($row["rep_page"] ? $row["rep_page"] : ''); ?>"/>
		    	</div>
	    	</div>

	    	<?php
	    	$getReportLicense=$link->query("select * from mr_report_license where li_rep_id='$id' ");
			if($getReportLicense->num_rows > 0){
	    	while($result=$getReportLicense->fetch_assoc()) { ?>
				<div class="form-group">
					<div class="col-md-10">
						<label class="control-label"><?php echo $result["li_key"]; ?> :</label>
						<input  name="<?php echo strtolower(str_replace(" ","_" , $result["li_key"] )); ?>" class="form-control" value="<?php echo $result["li_value"]; ?>" />
					</div>
				</div>
	    	<?php }}else{ ?>
				<div class="form-group">
					<div class="col-md-10">
						<label class="control-label">Single User License<span class="star">*</span> :</label>
						<input  name="single_user_license" class="form-control" value="<?=@$li_value1?>" required="" />
					</div>
				</div>	
				<div class="form-group">
					<div class="col-md-10">
						<label class="control-label">Enterprise User License :</label>
						<input  name="enterprise_user_license" value="<?=@$li_value2?>" class="form-control" />
					</div>
				</div>
				<div class="form-group">
					<div class="col-md-10">
						<label class="control-label">Corporate User License :</label>
						<input  name="corporate_user_license" value="<?=@$li_value3?>" class="form-control" />
					</div>
				</div>
			<?php } ?>  	    	

	    	<div class="form-group">
	    		<div class="col-md-10">
		    		<label class="control-label">Published Date<span class="star">*</span> :</label>
		    		<input  name="date" class="form-control datepicker" autocomplete="off" required="" value="<?=isset($published_date) ? @$published_date :  ($row["rep_entry_date"] ? $row["rep_entry_date"] : ''); ?>"/>
		    	</div>
	    	</div>


	    	<div class="form-group">
	    		<div class="col-md-10">
		    		<label class="control-label">STATUS <span class="star">*</span> :</label>
		    		<label class="radio-inline"><input type="radio" name="status" value="1" required=""   <?=isset($status) ? (@$status == '1' ? 'checked' : '') : ($row["status"]=='1' ? 'checked' : ''); ?> />Active</label>
		    		<label class="radio-inline"><input type="radio" name="status" value="0" required=""   <?=isset($status) ? (@$status == '0' ? 'checked' : '' ): ($row["status"]=='0' ? 'checked' : ''); ?>/>Inactive</label>
		    		
		    	</div>
	    	</div>

	    	<div class="form-group">
	    		<div class="col-md-10">
		    		<label class="control-label">Archive Status <span class="star">*</span> :</label>
		    		<label class="radio-inline"><input type="radio" name="archive_status" value="1" required=""  <?= isset($archive_status) ? (@$archive_status == '1' ? 'checked' : '' ): ($row["archive_status"]=='1' ? 'checked' : ''); ?> />Archive</label>
		    		<label class="radio-inline"><input type="radio" name="archive_status" value="0" required=""  <?= isset($archive_status) ? (@$archive_status == '0' ? 'checked' : '' ): ($row["archive_status"]=='0' ? 'checked' : ''); ?>/>Unarchive</label>
		    		
		    	</div>
	    	</div>

	    	<div>
		    	<button type="submit" name="submit" class="btn btn-upload btn-update"><span class="fa fa-check-square"></span> Update</button>
		    	<button type="button" name="cancel" class="btn btn-upload" onclick="window.history.back()"><span class="fas fa-times"></span> Cancel</button>
		    </div>
	    </form>
		<?php } ?>
	</div>



    <!--*****************************  FOOTER  ************************************** -->

    <?php require_once 'footer.php'; ?>

    <!--*****************************  FOOTER  ************************************** -->