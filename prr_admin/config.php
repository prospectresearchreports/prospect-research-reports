
<?php 
require_once 'helper.php';

if(!session_id()){
	session_start();
}
ini_set('date_default_timezone_set','Asia/Kolkata');

//****************************  DATABASE CONNECTION   *************************************//

$host = 'prospectresearchreports.com';
if(isset($_SERVER['HTTP_HOST'])){
	$host=$_SERVER['HTTP_HOST'];
}
$LOCAL_ARRAY = array('192.168.1.197', '127.0.0.1' , 'localhost');

$SHOW = false;

	
//****************************  DEVELOPMENT   *************************************//

if(in_array($host,$LOCAL_ARRAY)){	
	error_reporting(-1);
	ini_set('display_errors', 1);

	//****************************  BASE URL   *************************************//

	define('BASE_URL','http://localhost/prospect-research-reports/prr_admin/');	
	define('CRM_URL','http://localhost/prospect-research-reports/crm/');

	define('ENVIRONMENT','development');
	define('SHOW_DEBUG_BACKTRACE',true);

	$db_server    = "localhost";
	$db_user       ="root";
	$db_pass       ="";
	$db_name      ="market_research";
	@$link = new mysqli($db_server, $db_user, $db_pass,$db_name);

	if(@$link->connect_error){
		$SHOW = true;
	} 

}else{	
	
//****************************  PRODUCTION   *************************************//

	ini_set('display_errors', 0);
	error_reporting(E_ALL & ~E_NOTICE & ~E_DEPRECATED & ~E_STRICT & ~E_USER_NOTICE & ~E_USER_DEPRECATED);
		
	// error_reporting(-1);
	// ini_set('display_errors', 1);

	//****************************  BASE URL   *************************************//

	define('BASE_URL','https://prospectresearchreports.com/prr_admin/');
	define('CRM_URL','https://prospectresearchreports.com/crm/'); 
	define('ENVIRONMENT','production');
	define('SHOW_DEBUG_BACKTRACE',false);

	
	$db_server    ="localhost";
	$db_user       ="prospect_re2020";
	$db_pass       ="prospect_re2020";
	$db_name      ="prospect_research_reports";
	@$link = new mysqli($db_server, $db_user, $db_pass,$db_name);
	if(@$link->connect_error){
		$SHOW = true;
	} 
}
 
if($SHOW){

	$error_code = 0;
	if($link->connect_error){
		$error_code = $link->connect_errno;
	}
	echo 'PHP - '.$error_code;die;
	if(ENVIRONMENT === 'development') {
?>

	<div style="border:1px solid #990000;padding-left:20px;margin:0 0 10px 0;">

		<h4>A PHP Error was encountered</h4>

		<?php if (defined('SHOW_DEBUG_BACKTRACE') && SHOW_DEBUG_BACKTRACE === TRUE): ?>

			<p>Backtrace:</p>

			<?php foreach (debug_backtrace() as $error): ?>

				<?php if (isset($error['file'])): ?>

					<p style="margin-left:10px">
					File: <?php echo $error['file'] ?><br />
					Line: <?php echo $error['line'] ?><br />
					Function: <?php echo $error['function'] ?>
					</p>

				<?php endif; ?>
			<?php endforeach; ?>
		<?php endif;
		exit; ?>

	</div>
<?php }elseif(ENVIRONMENT === 'production') { ?>

<!DOCTYPE html>
<html lang="en">
<head>
<title><?=@$error_code == '2002' ? '500 | Internal Server Error' : '404 | Page Not Found'?></title>
</head>

<style type="text/css">
    .wrap{
        text-align: center;
        background-color: rgb(0, 0, 0, 0.6);
    }
    .wrap > h3{
        color: #f51904;
        background-color: #efefef;
        line-height: 1.8;
        padding: 20px;
        font-size: 1.6em;
    }
    .wrap > h2{
        color: #f51904;
        line-height: 1.8;
        padding: 20px;
        font-size: 2em;
    }
    .wrap h1{
        font-size: 60px;
        color: #f51904;
        text-transform: capitalize;
        font-weight: 900;
        line-height: 2;
    }
    @media screen and (min-width:767px){
        body{
            min-height: 600px;
            width: 100%;
        }
        .wrap{
            margin-top: 12%;
        }
    }
    @media screen and (max-width:767px){
        body{
            min-height: 400px;
        }
    }
    body{
        overflow: hidden;
        background: url('../assets/images/track-bg.jpg') center center no-repeat #090d2b;
        background-attachment: fixed;
        width: 100%;
        font-family: 'calibri';
    }
    
</style>


<!---******************************  ERROR MESSAGE  **************************** -->

<body>
<div class="jumbotron">
    <?php if(@$error_code == '2002') { ?>
    <div class="container wrap">
        <h1>Internal Server Error</h1>
        <h2>Oops ! Something went wrong ! This is not YOU . This is Us</h2>
        <h3>We are working on it to server you better !</h3>
    </div>
    <?php exit; }else{  header("location:".BASE_URL."404"); } ?>
</div>
</body>

<!---******************************  ERROR MESSAGE  **************************** -->

<?php } }?>