<?php require_once 'config.php';

  //************************************  LOGIN & REGISTER   **************************//

    if(isset($_POST['register'])){
        $error_msg = '';
        $success_msg = '';

        $name=mysqli_real_escape_string($link,$_POST["name"]);
        $email=mysqli_real_escape_string($link,$_POST["email"]);
        $password=md5(mysqli_real_escape_string($link,$_POST["password"]));
        $orig_password=mysqli_real_escape_string($link,$_POST["password"]);
        $user_type=mysqli_real_escape_string($link,$_POST["user_type"]);
        $date=date("Y-m-d H:i:s");
        if(!empty(@$name) && !empty(@$email) && !empty(@$orig_password) && !empty(@$user_type) ){

            $getRegistrationDetails=$link->query("select * from mr_login where email='".$email."' and user_type='".$user_type."' ");
            if(@$getRegistrationDetails && @$getRegistrationDetails->num_rows > 0){            
                $getRegistrationResult=$getRegistrationDetails->fetch_assoc();

                $registered_email = $getRegistrationResult['email'];
                $status = $getRegistrationResult['status'];
                $registered_password = $getRegistrationResult['password'];

                if($registered_email == $email && in_array($status , array('1','2'))){
                    $error_msg = " Sorry ! This Email Address already exists !";
                }else{
                    $error_msg = " We have received your registration request. Please wait till we verify the details ! ";
                }
            }else{
                $insertData=$link->query("insert ignore into mr_login(name, email, password, orig_password, user_type, created_at) values('".$name."','".$email."','".$password."','".$orig_password."','".$user_type."','".$date."') ");
                if($insertData){
                    $success_msg = " Registration Successful.";
                }else{
                    $error_msg = " Error While Processing Your Request. ".(ENVIRONMENT == 'development') ? mysqli_error($link) : '';
                }
            }            
        }else{
            $error_msg = " All fields are required.";
        }  
        if(!empty($error_msg)){
            echo "<h4 class='alert alert-danger text-center'>".@$error_msg."</h4>";
        } 
        if(!empty($success_msg)){
            echo "<h4 class='alert alert-success text-center'>".@$success_msg."</h4>";
        }
    }


    if(isset($_POST['login'])){
        // $user_type=mysqli_real_escape_string($link,$_POST["user_type"]);
        // echo '<pre>';print_r($_POST);die;
        $error_msg = '';
        $success_msg = '';

        $email=mysqli_real_escape_string($link,$_POST["email"]);
        $password=md5(mysqli_real_escape_string($link,$_POST["password"]));
        if(!empty(@$email) && !empty(@$password)){

            $getLoginDetails=$link->query("select * from mr_login where password='".$password."' and email='".$email."'");        

            if(@$getLoginDetails && @$getLoginDetails->num_rows > 0){
                while($row=$getLoginDetails->fetch_assoc()){
                    $login_email=$row["email"];
                    $login_password=$row["password"];
                    $login_name=$row["name"];                
                    $user_type=$row["user_type"];
                    $status=$row["status"];
                    if($status == '1'){
                        $_SESSION["email"]=$login_email;
                        $_SESSION["name"]=$login_name;
                        $_SESSION["user_type"]=$user_type;

                        $success_msg = " Login Successful.";
                    }else{
                        $error_msg = " Your registration request is pending. Please wait till we verify the details !"; 
                    }
                }

            }else{
                $error_msg = " Invalid Email Address or Password.";
            }
        }else{
            $error_msg = " All fields are required.";
        }  
        if(!empty($error_msg)){
            echo "<h4 class='alert alert-danger text-center'>".@$error_msg."</h4>";
        } 
        if(!empty($success_msg)){
            echo "<h4 class='alert alert-success text-center'>".@$success_msg."</h4>";
        }
    }


// ***********************************  STATUS      ******************************************    
      

// ***********************************  STATUS TOGGLE  ***********************************    

    if(isset($_POST["table"]) && isset($_POST['status_value']) && isset($_POST['id']) && isset($_POST['to_be_changed']))
    {
        $table = mysqli_real_escape_string($link,$_POST["table"]);
        $id = mysqli_real_escape_string($link,$_POST["id"]);
        $to_be_changed = mysqli_real_escape_string($link,$_POST["to_be_changed"]);
        $status_value = mysqli_real_escape_string($link,$_POST["status_value"]);
                    
        $update_qyr=mysqli_query($link,"update ".@$STATUS_TABLES[$table]." set ".@$STATUS_TYPES[$to_be_changed]."=$status_value where id=$id");        
        if($update_qyr){
            echo "Success";
        }else{
            echo "Error";
        }  
    }


//*************************************   DELETE    **********************************************//


if(isset($_POST["type"]) && $_POST['type'] == 'delete' && isset($_POST["table"]) &&  isset($_POST['id']))
{
    $table = mysqli_real_escape_string($link,$_POST["table"]);
    $id = mysqli_real_escape_string($link,$_POST["id"]);
    $where = " where id = $id";
    if($table == '8'){
        $where = " where contact_id = $id";
    }
    $query=mysqli_query($link,"delete from ".@$STATUS_TABLES[$table].@$where);
    if($query){
        echo "Success";
    }else{
        echo 'Error';
    }    
}



//*************************** PROFILE PICTURE UPLOAD *********************************//



if(isset($_FILES["file"]["type"]))
{
    $validextensions = array("jpeg", "jpg", "png");
    $temporary = explode(".", $_FILES["file"]["name"]);
    $file_extension = end($temporary);
    if ((($_FILES["file"]["type"] == "image/png") || ($_FILES["file"]["type"] == "image/jpg") || ($_FILES["file"]["type"] == "image/jpeg")
    ) && ($_FILES["file"]["size"] < 1000000)//Approx. 100kb files can be uploaded.
    && in_array($file_extension, $validextensions)) {
        if ($_FILES["file"]["error"] > 0)
        {
        echo "Return Code: " . $_FILES["file"]["error"] . "<br/><br/>";
        }
        else
        {
            // if (file_exists("assets/images/Profile/" . $_FILES["file"]["name"])) {
            //     echo $_FILES["file"]["name"] . " <span class='text-danger'><b>already exists.</b></span> ";
            // }
            // else
            // {
                $sourcePath = $_FILES['file']['tmp_name']; // Storing source path of the file in a variable
                $file=$_FILES["file"]["name"];
                $id=$_POST["id"];
                $targetPath ="assets/images/Profile/".$_FILES['file']['name']; // Target path where file is to be stored
                if(move_uploaded_file($sourcePath,$targetPath) ){ // Moving Uploaded file

                    $sql=mysqli_query($link,"update mr_login set profile_image='".$file."' where id='$id' ");
            
                    if($sql===TRUE )
                    {
                        echo "<span class='text-success'>Profile Picture Uploaded Successfully...!!</span><meta http-equiv='refresh' content='1,url=".BASE_URL."profile '/>";
                    }
                }else{
                    echo "<span class='text-danger'>Error while updating profile pricture.<span>";

                }
                
            // }
        }
    }
    else
    {
    echo "<span class='text-danger'>Invalid file Size or Type<span>";
    }
}

?>

