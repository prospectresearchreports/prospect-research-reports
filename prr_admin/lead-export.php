<?php
require '../vendor/autoload.php';
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\IOFactory;


const EXCEL_HEADERS = array(
	'contact_id',
	'contact_rep_title',
	'contact_form_type',
	'contact_person',
	'contact_company',
	'contact_job_role',
	'contact_phone',
	'contact_email',
	'contact_zip',
	'contact_country',
	'contact_real_country',
	'contact_exact_region',
	'contact_city',
	'contact_msg',
	'contact_datetime'
);
$FORM_TYPES = array(
	'1' => 'Contact Query', 
	'2' => 'Custom Research Query',
	'3' => 'Search Query', 
	'4' => 'Request Sample Query', 
	'5' => 'Request Customization Query', 
	'6' => 'Enquire Before Purchase Query', 
	'7' => 'Request Discount Query', 
	'8' => 'Checkout Query',	 
	'9' => 'Report Details Query',	
);
#DEVELOPMENT
$db_server    = "localhost";
$db_user       ="root";
$db_pass       ="";
$db_name      ="market_research";
@$link = new mysqli($db_server, $db_user, $db_pass,$db_name);

#PRODUCTION
// $db_server    ="localhost";
// $db_user       ="prospect_re2020";
// $db_pass       ="prospect_re2020";
// $db_name      ="prospect_research_reports";
// @$link = new mysqli($db_server, $db_user, $db_pass,$db_name);

// Get JSON input from AJAX request
$input = file_get_contents('php://input');
// $input = '{"selected_ids": ["202","201"]}';
$data = json_decode($input, true);
// echo "<pre>";var_dump($link);exit;


// Validate input
if (!isset($data['selected_ids']) || empty($data['selected_ids'])) {
    http_response_code(400);
    echo json_encode(['error' => 'No data selected']);
    exit;
}

$selectedIds = $data['selected_ids'];
// Example: Tabular data (replace with actual database query)
$columns = implode(",", EXCEL_HEADERS);
$lead_list_query="select $columns from mr_form_contact where contact_id in (".implode(",", @$selectedIds).")";
// echo $lead_list_query;
$lead_list_result = $link->query($lead_list_query);
$lead_list_result_data = array();   
$headers = EXCEL_HEADERS; 

if($lead_list_result && $lead_list_result->num_rows > 0){
    while($row=$lead_list_result->fetch_assoc()){
        if(array_key_exists('contact_form_type',$row) ){
            $row['contact_form_type'] = $FORM_TYPES[$row['contact_form_type']];
        }
        array_push($lead_list_result_data, array_values($row));
        // if(empty($headers)){
        //     $headers = array_keys($row);
        // }
    }
    // echo "<pre>";print_r($lead_list_result_data);exit;

    // Create a new spreadsheet
    if(array_search('contact_rep_title', $headers)){
        $key = array_search('contact_rep_title', $headers);
        $headers[$key] = 'contact_report_title';
    }
    array_unshift($lead_list_result_data, $headers);
    $spreadsheet = new Spreadsheet();
    // Set document properties
    $spreadsheet->getProperties()->setCreator('ProspectResearch Reports')
        ->setLastModifiedBy('Admin')
        ->setTitle('Leads')
        ->setSubject('Leads')
        ->setDescription('Exported Leads')
        ->setKeywords('Leads')
        ->setCategory('Leads');
    
    // Populate spreadsheet with filtered data
    $rowIndex = 1;
    // echo "<pre>";print_r($lead_list_result_data);exit;
    foreach ($lead_list_result_data as $row) {
        $colIndex = 'A';
        foreach ($row as $cell) {
            if($rowIndex == 1){
                $cell = ucwords(str_ireplace("_"," ", $cell));
            }
            // echo $cell.'<br/>';
            $spreadsheet->setActiveSheetIndex(0)->setCellValue("{$colIndex}{$rowIndex}", $cell);
            $colIndex++;
        }
        $rowIndex++;
    }
    $spreadsheet->getActiveSheet()->setTitle('Leads');
    $spreadsheet->setActiveSheetIndex(0);


    // Redirect output to a client’s web browser (Xlsx)
    header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
    header('Content-Disposition: attachment;filename="ExportedLeads.xlsx"');
    header('Cache-Control: max-age=0');
    // If you're serving to IE 9, then the following may be needed
    // header('Cache-Control: max-age=1');

    // If you're serving to IE over SSL, then the following may be needed
    // header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
    // header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
    // header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
    // header('Pragma: public'); // HTTP/1.0

    $writer = IOFactory::createWriter($spreadsheet, 'Xlsx');
    $writer->save('php://output');
    exit;
}else{
    echo json_encode(['error' => 'No leads found']);
    exit;
}
?>
