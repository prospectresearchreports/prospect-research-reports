
<html>
<head>
<title>Edit  | Prospect Research Reports</title>



 <!--*****************************  HEADER  ************************************** -->


<?php 
    require_once 'header.php';
    require_once 'auth.php';
?>
<!--*****************************  HEADER  ************************************** -->


<!-- *****************   BOOTSTRAP SELECT          ******************  -->
<link href="<?php echo BASE_URL;?>assets/bootstrap-select/css/bootstrap-select.min.css" rel='stylesheet' />
<script src="<?php echo BASE_URL;?>assets/bootstrap-select/js/bootstrap-select.min.js"></script>

<script type="text/javascript">
    $(document).ready(function(){
        $(".selectpicker").selectpicker();
		$(".datepicker1").datepicker({dateFormat:"yy-mm-dd",
            onSelect: function(datetext) {
                var d = new Date(); // for now

                var h = d.getHours();
                h = (h < 10) ? ("0" + h) : h ;

                var m = d.getMinutes();
                m = (m < 10) ? ("0" + m) : m ;

                var s = d.getSeconds();
                s = (s < 10) ? ("0" + s) : s ;

                datetext = datetext + " " + h + ":" + m + ":" + s;
             $('.datepicker1').val(datetext)   
            }
        });
});
</script>

<?php 
$id=isset($_GET["id"]) ? $_GET['id'] : (isset($_POST['id']) ? $_POST['id'] : '');
$getGlobalCheckoutDetails=$link->query("select * from mr_global_checkout where id='$id'");
$row=$getGlobalCheckoutDetails->fetch_assoc();
?>
<?php
    if(isset($_POST["submit"])){
		ini_set('date.timezone','Asia/Kolkata');
		$error = '';
        $checkout_id = $_POST['id'];
        $name=$_POST["name"];
        $email=$_POST["email"];
        $phone=$_POST["phone"];
        $company=$_POST["company"];
        $address=$_POST["address"];
		$currency=$_POST["currency"];
        $price=$_POST["price"];
        $date=$_POST["date"];
		$old_expiry_date = $_POST['old_expiry_date'];
		$current_datetime = date("Y-m-d H:i:s");
		$time_after_1hour = date('Y-m-d H:i:s', strtotime( $current_datetime . ' +1 hour' ));
        $expiry_date=($_POST["expiry_date"] && !empty($_POST['expiry_date'])) ? $_POST['expiry_date'] : null;
		$link_status = '';
		if($expiry_date === $old_expiry_date){
		    
		}else{		
		    if($expiry_date != $old_expiry_date && $expiry_date > $old_expiry_date && $expiry_date > $time_after_1hour){
    			$link_status = ", status = 1";
    		}else{
    			if($expiry_date < $old_expiry_date){
    				$error='<div class="alert alert-danger alert-form"><span class="fa fa-times-circle"></span>&nbsp;Link Expiry Date can\'t be less than "'.$old_expiry_date.'" </div>';
    			}else{
    				$error='<div class="alert alert-danger alert-form"><span class="fa fa-times-circle"></span>&nbsp;Link Expiry Date can\'t be less than "'.$time_after_1hour.'" </div>';
    			}
    		}
		}
		if(empty(@$error)){
			$license=$_POST["license"];
			$title=$_POST["title"];
			$country=$_POST["country"];
			$zip=$_POST["zip"];        
			
			$sql=mysqli_query($link,"update mr_global_checkout set name='".$name."',email='".$email."',title='".$title."', currency='".$currency."' ,price='".$price."',company='".$company."',phone='".$phone."',license='".$license."',address='".$address."',date='".$date."',expiry_date='".$expiry_date."',country='".$country."',zip='".$zip."' ".$link_status."  where id='$checkout_id' ");
			
			if($sql===TRUE ){
				$success='<div class="alert alert-success alert-form"><span class="fa fa-check-circle"></span>&nbsp;Global Checkout Updated successfully !&nbsp;&nbsp; <mark>Redirecting......</mark></div> 
				<meta http-equiv="refresh" content="2,url='.BASE_URL.'global-checkout-list "/>'; 
			}else{
				$error='<div class="alert alert-danger alert-form"><span class="fa fa-times-circle"></span>&nbsp;Error Updating Global Checkout !'.(ENVIRONMENT == 'development' ? mysqli_error($link) : '').'</div>';
			}
		}
    }
    
    ?>
    <h1 class="stats"><span class="far fa-edit"></span> Edit Global Checkout</h1><br/><br/><br/>

    <button  class='btn btn-primary helper-item' onclick="window.history.back()">
        <a href="#"><i class="fa fa-arrow-left"></i> Back</a> 
    </button>
	<div class="form-container">
	<?php 
		if(isset($success)){
			echo "<br/>".$success;
		}else{
			if(isset($error)){
				echo "<br/>".$error;
			}
	?>    
	    <form class="form-horizontal" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method='post' name="checkout_edit" id="checkout_edit" enctype="application/x-www-form-urlencoded" >

			<input type="hidden" name="id" value="<?=@$id?>" />	    	
	    	<div class="form-group">
	    		<div class="col-md-10">
		    		<label class="control-label">Name<span class="star">*</span> :</label>
		    		<input  name="name" class="form-control" required=""   value="<?= @$row["name"]?>"/>
		    	</div>
	    	</div>

	    	<div class="form-group">
	    		<div class="col-md-10">
		    		<label class="control-label">Email<span class="star">*</span> :</label>
		    		<input type="email" name="email" class="form-control" required=""  value="<?= @$row["email"] ?>" />
		    	</div>
	    	</div>

	    	<div class="form-group">
	    		<div class="col-md-10">
		    		<label class="control-label">Phone<span class="star">*</span> :</label>
		    		<input  name="phone" class="form-control" required=""   value="<?= @$row["phone"] ?>"/>
		    	</div>
	    	</div>

	    	<div class="form-group">
	    		<div class="col-md-10">
		    		<label class="control-label">Company<span class="star">*</span> :</label>
		    		<input  name="company" class="form-control" required=""  value="<?= @$row["company"]?>" />
		    	</div>
	    	</div>

	    	<div class="form-group">
	    		<div class="col-md-10">
		    		<label class="control-label">Address<span class="star">*</span> :</label>
		    		<input  name="address" class="form-control" required=""  value="<?= @$row["address"]  ?>" />
		    	</div>
	    	</div>


	    	<div class="form-group">
	    		<div class="col-md-10">
		    		<label class="control-label">Zip<span class="star">*</span> :</label>
		    		<input  name="zip" class="form-control" required="" value="<?=@$row["zip"] ?>" />
		    	</div>
	    	</div>


	    	<div class="form-group">
	    		<div class="col-md-10">
		    		<label class="control-label">Country<span class="star">*</span> :</label>
		    		<?php if(@$row['country'] && @$row['country'] !== ''){  
	                	$country=explode('-',$row['country']);
	            	?>
			    		<input type='hidden'  name="country" class="form-control" value="<?php echo (isset($row["country"])) ? $row['country'] : ''; ?>"/>
			    		<input  name="selected_country" class="form-control" value="<?php echo (isset($row["country"])) ? $country[1] : ''; ?>" readonly />
	                <?php }else{?>
			    		<select class="form-control selectpicker" style='z-index: 9999' data-live-search="true" data-actions-box="true" name="country" id='country'  title="Country"  required>
	                        <?php echo getCountryDropdown(); ?>
	                    </select>
	                <?php } ?>
		    	</div>
	    	</div>

	    	<div class="form-group">
	    		<div class="col-md-10">
		    		<label class="control-label">Report title<span class="star">*</span> :</label>
		    		<input  name="title" class="form-control" required=""  value="<?=@$row["title"] ?>" />
		    	</div>
	    	</div>
			
			<div class="row">
				<div class="form-group col-md-3">
					<label class="control-label">Currency<span class="star">*</span> :</label>
					<select class="form-control" name="currency" id="currency" title="Currency"  required>
						<option value="">Select </option>
						<?php global $CURRENCY;
						foreach($CURRENCY as $key=>$value){	
							$currency_selected = "";
							if(isset($row['currency']) && !empty($row['currency'])){
								$currency_selected = $row['currency'] === $key ? 'selected' : '';
							}else{
								$currency_selected = $key==='USD' ? 'selected' : '';
							}					
							// $currency_selected = (isset($row['currency']) && !empty($row['currency']) && $row['currency'] === $key) ? 'selected' : ($key === 'USD' ? 'selected' : '');
							echo "<option value='".$key."' ".$currency_selected." >".$value."</option>";
						}?>	
					</select>
				</div>
				<div class="form-group col-md-7">
					<label class="control-label">Report Price<span class="star">*</span> :</label>
		    		<input type="number"  name="price" class="form-control" required="" value="<?=@$row["price"]?>"/>
				</div>
			</div>

	    	<div class="form-group">
	    		<div class="col-md-10">
		    		<label class="control-label">License<span class="star">*</span> :</label>
		    		<select class="form-control" name="license" id="license" title="License"  required>
                        <option value="">Select </option>
                        <option value="Single User License" <?php echo ($row["license"]=='Single User License') ? 'selected' : ''; ?> >Single User</option>
						<option value="Enterprise User License" <?php echo ($row["license"]=='Enterprise User License') ? 'selected' : ''; ?> >Enterprise User</option>
						<option value="Corporate User License" <?php echo ($row["license"]=='Corporate User License') ? 'selected' : ''; ?> >Corporate User</option>	
						<option value="Global" <?php echo ($row["license"]=='Global') ? 'selected' : ''; ?> >Global</option>	
                    </select>
		    	</div>
	    	</div>

	    	<div class="form-group">
	    		<div class="col-md-10">
		    		<label class="control-label">Date<span class="star">*</span> :</label>
		    		<input  name="date" class="form-control datepicker" required=""  value="<?=@$row["date"]?>" />
		    	</div>
	    	</div>
	    	<div class="form-group">
	    		<div class="col-md-10">
		    		<label class="control-label">Link Expiry Date :</label>
					<input type="hidden" name='old_expiry_date' value="<?=@$row["expiry_date"] == '0000-00-00 00:00:00' ? '' : @$row['expiry_date']?>" />
		    		<input  name="expiry_date" class="form-control datepicker1"  value="<?=@$row["expiry_date"] == '0000-00-00 00:00:00' ? '' : @$row['expiry_date']?>" autocomplete="off" />
		    	</div>
	    	</div>
	    	<div>
		    	<button type="submit" name="submit" class="btn btn-upload btn-update"><span class="fa fa-check-square"></span> Update</button>
		    	<button type="button" name="cancel" class="btn btn-upload" onclick="window.history.back()"><span class="fas fa-times"></span> Cancel</button>
		    </div>
	    </form>
		<?php } ?>
	</div><br/><br/>



    <!--*****************************  FOOTER  ************************************** -->

    <?php require_once 'footer.php'; ?>

    <!--*****************************  FOOTER  ************************************** -->
