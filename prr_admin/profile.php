
<!DOCTYPE html>
<html>
<head>
    <title>Profile | Prospect Research Reports</title>

    <!--*****************************  HEADER  ************************************** -->

    <?php require_once 'header.php'; ?>

    <!--*****************************  HEADER  ************************************** -->
    <?php 

//*********************** CATEGORIES ****************************//
    
    $getUserDetails=$link->query("select * from mr_login where email='".$_SESSION["email"]."'  and  user_type = '".$_SESSION["user_type"]."' ");   
	
	$user=$getUserDetails->fetch_assoc();


    ?>
    <style type="text/css">
        @media screen and (min-width: 767px){
            ul.list-group > li.list-group-item{
                margin-left: 15%;
            }
        }
    </style>

    <script type="text/javascript">
        $(document).ready(function(){

        })
    </script>

    <h1 class="stats"><span class="fa fa-user"></span> My Profile</h1><br/>

    <div class="row statistics profile-block">       
        

        <div class="col-md-8">
            <ul class="list-group">
                <li class="list-group-item"><b>Name : </b> <?php echo $user["name"]; ?></li>
                <li class="list-group-item"><b>Email : </b> <?php echo $user["email"]; ?></li>
                <li class="list-group-item"><b>Password : </b> <?php echo $user["password"]; ?></li>
            </ul><br/>
            
        </div>
        <div class="col-md-8 col-md-offset-1">            
            <div>
                <button onclick="window.location.href='profile-edit?id=<?php echo base64_encode($user["id"]); ?>'" name="submit" class="btn btn-upload btn-update"><span class="far fa-edit"></span> Edit Profile</button>
                
                <button type="button"  class="btn btn-upload" onclick="window.history.back()"><span class="fa fa-times"></span> Back</button>
            </div>
        </div>

    </div><br/><br/><br/><br/>

   
    <!--*****************************  FOOTER  ************************************** -->

    <?php require_once 'footer.php'; ?>

    <!--*****************************  FOOTER  ************************************** -->
