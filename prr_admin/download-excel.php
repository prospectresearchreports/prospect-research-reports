<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <link rel="shortcut icon" href="<?php echo BASE_URL; ?>/images/favicon.ico" type="image/x-icon" />
        <meta charset="utf-8"/> 
        <meta name="viewport" content="width=device-width, initial-scale=1"/>    
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
        <meta name="language" content="France, French"/>
        <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.1/jquery.min.js"></script>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" type="text/css" />
        <script type="text/javascript" src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    </head>
    <script>
        $(document).ready(function(){
            var progress=$(".progress-bar").attr("aria-valuenow");
            
            var i=1;
            var percentage=1;
            var width=1;
            
            var interval=setInterval(progressDisplay,200);
            $(".progress-bar").addClass("progress-bar-info progress-bar-striped active");


            function progressDisplay(){
                
                var bar=percentage+i;
                $(".progress-bar").attr("aria-valuenow",bar).css({"width":bar+"%"});
                if(bar == 100){
                    clearInterval(interval);
                    $(".progress-bar span").html("Download Complete");
                    $(".progress-bar").removeClass("progress-bar-info progress-bar-striped active").addClass(" progress-bar-success").fadeOut(1000);
                    
                    <?php
                    // Connection 
                    $link = mysqli_connect("localhost", "root", "", "qqmarket_research");
                    if (!$link) {
                        echo "Error";
                    }

                    $filename = "data.xls"; // File Name
                    // Download file
                    header("Content-Disposition: attachment; filename=\"$filename\"");
                    header("Content-Type: application/vnd.ms-excel");
                    $user_query = mysqli_query($link,'select * from mr_report where rep_archive_status="N" and rep_status="Y" order by rep_date desc');
                    // Write data to file
                    $flag = false;
                    while ($row = mysqli_fetch_assoc($user_query)) {
                        if (!$flag) {
                            // display field/column names as first row
                            echo implode("\t", array_keys($row)) . "\r\n";
                            $flag = true;
                        }
                        echo implode("\t", array_values($row)) . "\r\n";
                    }
                    ?>
                                    
                }else{
                    $(".progress-bar span").html("Downloading "+bar+" %");
                }
                
                
                 //alert(data);
                 i++;

            }
        })
    </script>


    <body><br/><br/>
        <div class="container">
            <div class="progress">
                <div class="progress-bar" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100">
                    <span>Downloading..</span>
                </div>
            </div>
        </div>
    </body>

    