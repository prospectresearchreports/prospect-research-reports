
<html>
<head>
<title>Add Report | Prospect Research Reports</title>


<script type="text/javascript">
$(document).ready(function(){
    $(".datepicker").datepicker({dateFormat:"yy-mm-dd"});
});

</script>

 <!--*****************************  HEADER  ************************************** -->

<?php require_once 'header.php'; ?>

<!--*****************************  HEADER  ************************************** -->

<?php
    if(isset($_POST["submit"])){

        $category=$_POST["category"];
        $publisher=$_POST["publisher"];
        $title=$_POST["title"];

        $url=generateReportURL($title, $link);
        $pages=$_POST["page"];
        $content=$_POST["content"];
        $description=$_POST["description"];
        $toc=$_POST["toc"];
        $tof=$_POST["tof"];
        $status=$_POST["status"];
        $archive_status=$_POST["archive_status"];

        $published_date=$_POST["date"];
        $date=date("Y-m-d H:i:s");

		// REPORT LICENSES	
		$li_key1="Single User License";
		$li_key2="Enterprise User License";
		$li_key3="Corporate User License";	
		$li_value1=$_POST["single_user_license"];
		$li_value2=$li_value3=null;
		if(isset($_POST['enterprise_user_license'])){			
			$li_value2=$_POST["enterprise_user_license"];
		}
		if(isset($_POST['corporate_user_license'])){			
			$li_value3=$_POST["corporate_user_license"];
		}
		$is_manual = true;
		$duplicate_report_details = checkDuplicateReport($title, $link);	
		if($duplicate_report_details['is_duplicate_report'] === true){  // IF EXISTS, UPDATE DATA
			$rep_id = $duplicate_report_details['rep_id'];
			
			// UPDATE REPORT TABLE DATA 
			$update_stmt = $link->prepare("update mr_report set is_manual = ?,rep_descrip = ?,rep_page = ?,rep_date = ?,rep_toc_fig = ?,rep_entry_date = ? where id = ? ");
			$update_stmt->bind_param('ssssssd', $is_manual, $description, $pages, $date, $tof, $published_date, $rep_id);
			$update_stmt->execute();
			$update_stmt->close();

			// UPDATE REPORT METADATA TABLE DATA 
			$update_meta_stmt = $link->prepare("update mr_report_metadata set rep_contents = ?, rep_table_of_contents = ? where meta_rep_id = ? ");
			$update_meta_stmt->bind_param('ssd', $content, $toc, $rep_id);
			$update_meta_stmt->execute();
			$update_meta_stmt->close();

			// UPDATE REPORT LICENSE TABLE DATA 
			if($li_value1 > 0){                            
				$li_value1 = str_ireplace(array('$','(',')',',','-'), '', @$li_value1);
				$update_license1_stmt = $link->prepare("update mr_report_license set li_value = ? where li_rep_id = ? and li_key = ? ");
				$update_license1_stmt->bind_param('dds', $li_value1, $rep_id, $li_key1);
				$update_license1_stmt->execute();
				$update_license1_stmt->close();
			}
			if($li_value2 > 0){
				@$li_value2 =  str_ireplace(array('$','(',')',',','-'), '', @$li_value2);
				$update_license2_stmt = $link->prepare("update mr_report_license set li_value = ? where li_rep_id = ? and li_key = ? ");
				$update_license2_stmt->bind_param('dds', $li_value2, $rep_id, $li_key2);
				$update_license2_stmt->execute();
				$update_license2_stmt->close();                            
			}
			if($li_value3 > 0){
				@$li_value3 =  str_ireplace(array('$','(',')',',','-'), '', @$li_value3);
				$update_license3_stmt = $link->prepare("update mr_report_license set li_value = ? where li_rep_id = ? and li_key = ? ");
				$update_license3_stmt->bind_param('dds', $li_value3, $rep_id, $li_key3);
				$update_license3_stmt->execute();
				$update_license3_stmt->close();                            
			}
			$success='<div class="alert alert-success alert-form"><span class="fa fa-check-circle"></span>&nbsp;Found Existing Report and Updated successfully !&nbsp;&nbsp; <mark>Redirecting......</mark></div> 
			<meta http-equiv="refresh" content="2,url='.BASE_URL.'report-list "/>';
		}else{
			
			// INSERT REPORT TABLE DATA 
			$insert_stmt = $link->prepare("insert into mr_report (is_manual, rep_sub_cat_1_id, publisher_id, rep_url, rep_title, rep_descrip, rep_page, rep_date, rep_toc_fig, rep_entry_date, status, archive_status) values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)  ");
			$insert_stmt->bind_param('sddsssdsssdd', $is_manual, $category, $publisher, $url, $title, $description, $pages, $date, $tof, $published_date, $status, $archive_status);
			
			if($insert_stmt->execute()){
				$insert_stmt->close();
				$rep_id =$link->insert_id;
				
				// INSERT REPORT METADATA TABLE DATA 
				$insert_meta_stmt = $link->prepare("insert into mr_report_metadata (rep_contents, rep_table_of_contents, meta_rep_id) values (?, ?, ?) ");
				$insert_meta_stmt->bind_param('ssd', $content, $toc, $rep_id);
				if($insert_meta_stmt->execute()){
					$insert_meta_stmt->close();
					

					// INSERT REPORT LICENSE TABLE DATA 
					if($li_value1 > 0){
						insert_licenses($li_key1, $li_value1, $rep_id, $link);
					}
					if($li_value2 > 0){
						insert_licenses($li_key2, $li_value2, $rep_id, $link);                          
					}
					if($li_value3 > 0){
						insert_licenses($li_key3, $li_value3, $rep_id, $link);                           
					}
					
				
					$success='<div class="alert alert-success alert-form"><span class="fa fa-check-circle"></span>&nbsp;Report Added successfully !&nbsp;&nbsp; <mark>Redirecting......</mark></div> 
					<meta http-equiv="refresh" content="2,url='.BASE_URL.'report-list "/>'; 
				}else{
					echo mysqli_error($link);
				}

			}else{
				$error='<div class="alert alert-danger alert-form"><span class="fa fa-times-circle"></span>&nbsp;Error Adding Report !'.(ENVIRONMENT == 'development' ? mysqli_error($link) : '').'</div>';
			} 
		} 
    }
    ?>

    <h1 class="stats"><span class="far fa-edit"></span> Add New Report</h1><br/><br/><br/>

    <button  class='btn btn-primary helper-item' onclick="window.history.back()">
        <a href="#"><i class="fa fa-arrow-left"></i> Back</a> 
    </button>
	<div class="form-container">
	<?php 
		if(isset($success)){
			echo "<br/>".$success;
		}else{
			if(isset($error)){
				echo "<br/>".$error;
			}
	?>    
	    <form class="form-horizontal" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method='post' name="add_report" id="add_report">
	    	<div class="form-group">
	    		<div class="col-md-10">
		    		<label class="control-label">Report Category<span class="star">*</span> :</label>
		    		<select class="form-control" name="category" id="category" title="Report Category"  required>
                        <option value="">Select Category </option>		
                        <?php
	                        $ReportCategory = "select * from mr_sub_cat_1 order by sc1_name";
	                        $ReportCategoryQuery = $link->query($ReportCategory);
	                        if ($ReportCategoryQuery->num_rows > 0){
	                            while ($row = $ReportCategoryQuery->fetch_array()){
									$category_id = $row['id'];  
									$selected =  (isset($category) && @$category == $category_id ) ? 'selected' : ''  ;                     
									echo "<option value='".$category_id."' ".@$selected."  >".$row["sc1_name"]."</option>";                                
								}
	                        }                        
                        ?>
                    </select>
		    	</div>
	    	</div>
	    	<div class="form-group">
	    		<div class="col-md-10">
		    		<label class="control-label">Publisher<span class="star">*</span> :</label>
		    		<select class="form-control" name="publisher" id="publisher" title="Report publisher"  required>
                        <option value="">Select Publisher </option>
                        <?php
	                        $ReportPublisher = "select * from mr_publisher order by publisher_name";
	                        $ReportPublisherQuery = $link->query($ReportPublisher);
	                        if ($ReportPublisherQuery->num_rows > 0){
	                            while ($row = $ReportPublisherQuery->fetch_array()){ 
									$publisher_id = $row['id'];  
									$selected =  (isset($publisher) && @$publisher == $publisher_id ) ? 'selected' : ''  ;                     
	                                echo '<option value="'. @$publisher_id.'" '.@$selected.'>'.$row["publisher_name"].'</option>';                                
	                            }
	                        }
                        ?>
                    </select>
		    	</div>
	    	</div>
	    	<div class="form-group">
	    		<div class="col-md-10">
		    		<label class="control-label">Report Title<span class="star">*</span> :</label>
		    		<input  name="title" class="form-control"  value="<?=@$title?>" required="" />
		    	</div>
	    	</div>

	    	<!-- <div class="form-group">
	    		<div class="col-md-10">
		    		<label class="control-label">Report URL<span class="star">*</span> :</label>
		    		<input  name="url" class="form-control" required="" />
		    	</div>
	    	</div> -->
	    	
	    	<div class="form-group">
	    		<div class="col-md-10">
		    		<label class="control-label">Report Description<span class="star">*</span> :</label>
		    		<?php
                        require_once 'ckeditor_old/ckeditor.php';
                        $sBasePath = "ckeditor/";
                        $CKEditor = new CKEditor();
                        $CKEditor->editor("description", @$description);
                    ?>  
		    	</div>
	    	</div>
	    	
	    	<div class="form-group">
	    		<div class="col-md-10">
		    		<label class="control-label">Report Contents<span class="star">*</span> :</label>
		    		<?php
                        require_once 'ckeditor_old/ckeditor.php';
                        $sBasePath = "ckeditor/";
                        $CKEditor = new CKEditor();
                        $CKEditor->editor("content", @$content);
                    ?>  
		    	</div>
	    	</div>
	    	
	    	<div class="form-group">
	    		<div class="col-md-10">
		    		<label class="control-label">Report Table Of Contents :</label>
		    		<?php
                        require_once 'ckeditor_old/ckeditor.php';
                        $sBasePath = "ckeditor/";
                        $CKEditor = new CKEditor();
                        $CKEditor->editor("toc", @$toc);
                    ?>  
		    	</div>
	    	</div>
	    	
	    	<div class="form-group">
	    		<div class="col-md-10">
		    		<label class="control-label">Report Table Of Figures:</label>
		    		<?php
                        require_once 'ckeditor_old/ckeditor.php';
                        $sBasePath = "ckeditor/";
                        $CKEditor = new CKEditor();
                        $CKEditor->editor("tof", @$tof);
                    ?>  
		    	</div>
	    	</div>

	    	<div class="form-group">
	    		<div class="col-md-10">
		    		<label class="control-label">Report Pages<span class="star">*</span> :</label>
		    		<input  name="page" class="form-control" value="<?=@$pages?>" required="" />
		    	</div>
	    	</div>

	    	<div class="form-group">
	    		<div class="col-md-10">
		    		<label class="control-label">Single User License<span class="star">*</span> :</label>
		    		<input  name="single_user_license" class="form-control" value="<?=@$li_value1?>" required="" />
		    	</div>
	    	</div>
	    	

	    	<div class="form-group">
	    		<div class="col-md-10">
		    		<label class="control-label">Enterprise User License :</label>
		    		<input  name="enterprise_user_license" value="<?=@$li_value2?>" class="form-control" />
		    	</div>
	    	</div>
	    	

	    	<div class="form-group">
	    		<div class="col-md-10">
		    		<label class="control-label">Corporate User License :</label>
		    		<input  name="corporate_user_license" value="<?=@$li_value3?>" class="form-control" />
		    	</div>
	    	</div>
	    	

	    	<div class="form-group">
	    		<div class="col-md-10">
		    		<label class="control-label">Published Date<span class="star">*</span> :</label>
		    		<input  name="date" class="form-control datepicker" autocomplete="off" value="<?=@$published_date?>" required="" />
		    	</div>
	    	</div>


	    	<div class="form-group">
	    		<div class="col-md-10">
		    		<label class="control-label">STATUS <span class="star">*</span> :</label>
		    		<label class="radio-inline"><input type="radio" name="status" <?=@$status == '1' ? 'checked' : ''?> value="1" required="" />Active</label>
		    		<label class="radio-inline"><input type="radio" name="status" <?=@$status == '0' ? 'checked' : ''?> value="0" required="" />Inactive</label>
		    		
		    	</div>
	    	</div>

	    	<div class="form-group">
	    		<div class="col-md-10">
		    		<label class="control-label">Archive Status <span class="star">*</span> :</label>
		    		<label class="radio-inline"><input type="radio" name="archive_status" value="1" <?=@$archive_status == '1' ? 'checked' : ''?> required="" />Archive</label>
		    		<label class="radio-inline"><input type="radio" name="archive_status" value="0" <?=@$archive_status == '0' ? 'checked' : ''?> required="" />Unarchive</label>
		    		
		    	</div>
	    	</div>

	    	<div>
		    	<button type="submit" name="submit" class="btn btn-upload"><span class="far fa-edit"></span> Add</button>
		    </div>
	    </form>
		<?php } ?>
	</div>



    <!--*****************************  FOOTER  ************************************** -->

    <?php require_once 'footer.php'; ?>

    <!--*****************************  FOOTER  ************************************** -->