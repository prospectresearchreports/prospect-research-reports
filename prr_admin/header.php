<?php 
require_once 'config.php';
require_once 'checksession.php';
?>

	<link rel="shortcut icon" href="<?php echo BASE_URL; ?>assets/images/logo-icon.png" type="image/x-icon" />
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<script type="text/javascript" src="<?php echo BASE_URL; ?>assets/js/jquery.min.js"></script>
    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
    <link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.css" type="text/css" />
	<script type="text/javascript" src="<?php echo BASE_URL; ?>assets/js/bootstrap.min.js"></script>
	<link rel="stylesheet" href="<?php echo BASE_URL; ?>assets/css/bootstrap.min.css" type="text/css"/>
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
	<link href="<?php echo BASE_URL; ?>assets/css/header.css" rel="stylesheet" type="text/css"/>
	<link href="<?php echo BASE_URL; ?>assets/css/style.css" rel="stylesheet" type="text/css"/>

</head>
<script type="text/javascript">

    $(document).ready(function(){
        $(".overlay").css({backgroundColor:  'rgb(0, 51, 102)'});
        $(".datepicker").datepicker({dateFormat:"yy-mm-dd",
            onSelect: function(datetext) {
                var d = new Date(); // for now

                var h = d.getHours();
                h = (h < 10) ? ("0" + h) : h ;

                var m = d.getMinutes();
                m = (m < 10) ? ("0" + m) : m ;

                var s = d.getSeconds();
                s = (s < 10) ? ("0" + s) : s ;

                datetext = datetext + " " + h + ":" + m + ":" + s;
             $('.datepicker').val(datetext)   
            }
        });


        <?php if(isset($_COOKIE["color"])){ ?>
            // $(".overlay,.footer").css({backgroundColor:  '<?php echo $_COOKIE["color"]; ?>'});
            // $("#color").val('<?php echo $_COOKIE["color"]; ?>');
        <?php }else{ ?>
            // $(".overlay,.footer").css({backgroundColor:  'rgba(0,0,0, 0.9)'});
            // $("#color").val('rgba(0,0,0, 0.9)');
        <?php } ?>

    });
    $(window).load(function () {
        $('.loader').fadeOut('slow');
    });
    // function changeBackground(color){
    //     //alert(color);
    //     $(".overlay,.footer").css({backgroundColor:  color});
    //     document.cookie = "color="+color+"; path=/; max-age="+365*24*60*60;
    //     location.reload(true);

    // }


    //******************  SIDENAV OPEN/ CLOSE  ********************//
    function openNav() {
        $("#myNav").css({'width': '100%'});
    }
    function closeNav() {
        $("#myNav").css({'width': '0%'});
    }
    // if($(".dropdown-container a").hasClass("active")){
    //     alert('ok');
    //     $(this).parent('.dropdown-container').css({display:'block'});
    // }else{
    //     alert('not-ok');
    //     $(this).parent('.dropdown-container').css({display:'none'});
    // }

</script>

</head>
<?php 
    
    //*********************** CHECK FOR SESSION ****************************//

    $getUserInfo=$link->query("select * from mr_login where  email='".@$_SESSION["email"]."'  and user_type='".@$_SESSION["user_type"]."'  ");    
    
    $userInfo=$getUserInfo->fetch_assoc();

?>
<body>
    <div class="loader"></div>

	<!-- <div class="container">        
        <div class="logo">
            <img src="<?php echo BASE_URL.'assets/images/Alliance Report.png' ?>"/>
        </div>
        <div class="welcome">
            <h3>Welcome: <?php echo (isset($_SESSION)) ? $_SESSION["name"] : "Undefined"; ?></h3>
        </div>
    </div> -->
    <div id="myNav" class="overlay">
        <!-- <h2 class="panel-name sm-hidden">Admin Panel</h2> -->
        <a onclick="return false;" class='dropdown-link logo-text'>                
            <span class="fa fa-copyright"></span> <br/> <p>ProspectResearch Reports</p>
        </a>
        <a href="javascript:void(0)" class="closebtn md-hidden" onclick="closeNav()">&times;</a>
        <div class="overlay-content"> 
            <a class="dropdown-btn"><i class="fa fa-user"></i>  <?=@$_SESSION["name"]." (".@$USER_TYPES[$userInfo["user_type"]].") "?>
                <i class="fa fa-caret-down"></i>
            </a>
            <div class="dropdown-container">
                <a href='<?php echo BASE_URL.'logout'; ?>' ><span class="fa fa-sign-out-alt text-danger"></span>  Logout</a>
            </div>
            <hr class="hr-line"/>

            <a href="<?php echo BASE_URL.'dashboard'; ?>" class='dropdown-link'><i class="fa fa-home"></i> Dashboard</a>

            <a href="<?php echo BASE_URL.'profile'; ?>" class='dropdown-link'><i class="fa fa-user"></i> My Profile</a>
            <a href="<?php echo BASE_URL.'csv-upload'; ?>" class='dropdown-link'><i class="fa fa-upload"></i> CSV Upload</a>


            <?php if(@$_SESSION["user_type"] == '1'){ ?>

                <a href="<?php echo BASE_URL.'login-approval';?>" class='dropdown-link'><i class="fa fa-lock"></i> User Authentication</a> 
                <a href="<?php echo BASE_URL.'category-list';?>" class='dropdown-link'><i class="fa fa-heartbeat"></i> Categories</a> 
                <a href="<?php echo BASE_URL.'press-release-list';?>" class='dropdown-link'><i class="fa fa-book"></i> Press Releases</a>  
                <a href="<?php echo BASE_URL.'publisher-list';?>" class='dropdown-link'><i class="fa fa-university"></i> Publishers</a>   
                <a href="<?php echo BASE_URL.'blog-list';?>" class='dropdown-link'><i class="fa fa-bullhorn"></i> Blogs</a>   
                <a href="<?php echo BASE_URL.'report-list';?>" class='dropdown-link'><i class="fa fa-briefcase"></i> Reports</a>  
                <a href="<?php echo BASE_URL.'bannedword-list';?>" class='dropdown-link'><i class="fa fa-ban"></i> Banned Words</a>   
                <a href="<?php echo BASE_URL.'lead-list?type=1';?>" class='dropdown-link'><i class="fa fa-envelope"></i> Contact Queries</a>  
                <a href="<?php echo BASE_URL.'lead-list?type=3';?>" class='dropdown-link'><i class="fa fa-search"></i> Search Queries</a>  
                <a href="<?php echo BASE_URL.'lead-list?type=4';?>" class='dropdown-link'><i class="fa fa-chart-bar"></i> Leads</a>  
                <a href="<?php echo BASE_URL.'lead-list?type=8';?>" class='dropdown-link'><i class="fa fa-wallet"></i> Checkouts </a>            
                <a href="<?php echo BASE_URL.'global-checkout-list';?>" class='dropdown-link'><i class="fa fa-wallet"></i>Global Checkouts </a>            

            <?php }else if(@$_SESSION["user_type"] == '2' || @$_SESSION["user_type"] == '3' || @$_SESSION["user_type"] == '4'){ ?>
               
                <a href="<?php echo BASE_URL.'category-list';?>" class='dropdown-link'><i class="fa fa-heartbeat"></i> Categories</a> 
                <a href="<?php echo BASE_URL.'press-release-list';?>" class='dropdown-link'><i class="fa fa-book"></i> Press Releases</a>  
                <a href="<?php echo BASE_URL.'publisher-list';?>" class='dropdown-link'><i class="fa fa-university"></i> Publisher List</a>   
                <a href="<?php echo BASE_URL.'blog-list';?>" class='dropdown-link'><i class="fa fa-bullhorn"></i> Blogs</a>   
                <a href="<?php echo BASE_URL.'report-list';?>" class='dropdown-link'><i class="fa fa-briefcase"></i> Reports</a>   
            
            <?php } ?>


        </div>
    </div>
    <div class="main-block">
        <!-- <span class="nav-btn" style="display: inline-block;" onclick="openNav()">&#9776;</span> -->
        <?php if(@$_SESSION["user_type"] == '1'){ ?>
            <a class="link-crm sm-hidden" href='<?=CRM_URL?>' target="_blank"><span class="fa fa-arrow-right"></span> Go To CRM Panel  </a>
        <?php } ?>
        <div class="row">
            <!-- <div class="col-md-1 sm-hidden">
                <input type="color" onchange='changeBackground(this.value)' name="color" id='color' value="<?php echo isset($color) ? $color : ''; ?>">
            </div> -->
            <div class="col-md-4 logo md-hidden">
                <img title="Go To Home" onclick="window.location.href='<?php echo BASE_URL; ?>'" src="<?php echo BASE_URL.'assets/images/prospect-research-reports-logo.jpg' ?>"/>
            </div>
            <!-- <div class="col-md-3 md-hidden">
                 <h3 class="logout dropdown" title="Logout" >
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <span class="fa fa-power-off text-danger"></span>
                    </a>
                    <ul class="dropdown-menu">
                        <li><span class="text"><strong>WELCOME</strong> </span></li>
                        <li><span class="fa fa-user fa-2x"></span>
                        <li><span class="name"> <?php echo (isset($_SESSION)) ? $_SESSION["name"] : "Undefined"; ?></span></li>
                        <li><span class="role"> <?php echo  "(".@$USER_TYPES[$userInfo["user_type"]].")" ; ?></span></li><br/>
                        <li><span title="Logout" onclick="window.location.href='<?php echo BASE_URL.'logout'; ?>'" ><span class="fa fa-sign-out-alt text-danger"></span>  Logout</span></li>
                    </ul>
                </h3>
            </div> -->
            <div class="col-md-1 md-hidden">
                <span class="nav-btn" style="display: inline-block;" onclick="openNav()">&#9776;</span>
            </div>

            <!-- <div class="col-md-4 welcome sm-hidden">
                <h3><span class="text">WELCOME</span></h3>

                <h3>                    
                    <span class="name"> <?php echo (isset($_SESSION)) ? $_SESSION["name"] : "Undefined"; ?></span>
                     &nbsp;<span class="role"> <?php echo  "(".@$USER_TYPES[$userInfo["user_type"]].")" ; ?></span>
                 </h3>
            </div>
            <div class="col-md-3 sm-hidden"> 
                <h3 class="logout <?=@$_SESSION["user_type"] != '1' ? 'user_logout' : ''?>" title="Logout" data-toggle="modal" data-target="#logMeOut" ><span class="fa fa-power-off text-danger"></span> </h3>
            </div> -->
            <div id="logMeOut" class="modal fade" role="dialog">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title">Logout Confirmation <span class="fa fa-sign-out"></span>  </h4>
                        </div>
                        <div class="modal-body">
                            <p style="font-size: 1.2em"><strong>Are you sure you want to make log out ?</strong></p>
                        </div>
                        <div class="modal-footer">
                            <button class="btn btn-primary btn-confirm" onclick="window.location.href='<?php echo BASE_URL.'logout'; ?>'">Yes, Log Out</button>
                            <button type="button" class="btn btn-warning btn-cancel" data-dismiss="modal">Cancel</button>
                        </div>
                    </div>
                </div>
            </div>
        </div><hr style="border-bottom: 1px solid #efefef">
        
