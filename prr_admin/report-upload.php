<?php

ini_set("memory_limit","1000M");
ini_set('max_execution_time', 30000);
ini_set('post_max_size', '150M');
ini_set('upload_max_filesize', '150M');
        
ini_set("date.timezone", "Asia/Kolkata");
date_default_timezone_set('Asia/Kolkata');
$success_msg = $error_msg = "";

$file_type = (isset($_FILES['csv_file'])) ? $_FILES['csv_file']['type'] : @$_POST['file_type'];
// $category_id = $_POST['category_id'];
// $publisher_id = $_POST['publisher_id'];
// echo "<pre>";print_r($file_type) ;exit;
if($file_type === 'application/vnd.ms-excel' || $file_type === 'text/csv'){
      
    $filename_tmp = (isset($_POST['file_name'])) ? $_POST['file_name'] : @$_FILES['csv_file']['tmp_name'];
    $fp = shell_exec('perl -pe \'s/\r\n|\n|\r/\n/g\' ' . escapeshellarg($filename_tmp) . ' | wc -l');

    if($_POST['flag'] == 1){
        $total_records = 0;
        $delimiter = ',';
        if(file_exists($filename_tmp)){
            if(($handle = fopen($filename_tmp, 'r')) !== false){
                $i = 0;
                while(($row = fgetcsv($handle, 5000, $delimiter)) !== false){  
                    if(!empty($row[0]) && !empty($row[1]) && !empty($row[1]) ){
                        if($i !== 0){       
                            $total_records = $i;
                        }
                        $i++;
                    }
                }
                fclose($handle);
            }//end- if
            //ENDs - read CSV
        }        
        if($total_records > 10000){
            $_SESSION['error_x'] = 'Oops sorry, Your CSV file contain <strong>'.@$total_records.'</strong> records. It must not contain more than <strong>10,000</strong> records.';
            echo "<meta http-equiv='refresh' content='0, url=".BASE_URL."csv-upload'  />";exit;
        }
        
        // $csv_data = array('upload_data' => $this->upload->data());
        
        $csv_file=$_FILES["csv_file"]["name"];
        $dir="../uploads/";
        $upload_path = $dir.$csv_file;
        move_uploaded_file($filename_tmp,$upload_path);
        $org_file_name = $csv_file;
        $fileName  = realpath($upload_path);
        // var_dump($fileName);exit;
        $_SESSION['org_file_name'] = $org_file_name;
        $_SESSION['fileName'] = $fileName;
        chmod($fileName, 0777);
        
        // $csv_data = (isset($csv_data)) ? @$csv_data : $this->session->userdata('csv_data');
        $org_file_name = (isset($org_file_name)) ? @$org_file_name : $_SESSION['org_file_name'];
        $fileName  = (isset($fileName)) ? @$fileName :  $_SESSION['fileName'];
    }

    $file_name = (isset($_POST['file_name'])) ? $_POST['file_name'] : @$fileName;
    $org_file_name = (isset($_POST['org_file_name'])) ? $_POST['org_file_name'] : @$org_file_name;

    $field_name = 'ReportTitle, PublisherID, CategoryID, Pages, Date, Content, TOC, TOF, SingleUserLicense';
    $field_array = explode(',',$field_name);
    try{
        $data['custom_header'] = $GLOBALS['CUSTOM_CSV_REPORT_HEADER'];
        
        if($_POST['csv_format'] == 1){ // Customized CSV Format
            $sheetData = array();
            //STARTs - read CSV
            $delimiter = ',';
            $header = NULL;
            $loop = 0;
            if(file_exists($file_name)){
                if(($handle = fopen($file_name, 'r')) !== false){
                    while(($row = fgetcsv($handle, 5000, $delimiter)) !== false){
                        if($loop >= 6) continue;
                        if(!$header){
                            $header = $row;
                            $org_file_header = $row;
                        }else{
                            if(count($header)==count($row)){
                                $sheetData[] = $row;
                            }					
                        }
                        $loop++;
                    }
                    fclose($handle);
                }//end- if
                //ENDs - read CSV
            }else{
                // file doesn't exist
                $_SESSION['error_x'] = 'File doesn\'t exist . Please try later';
                if(file_exists($file_name)){
                    unlink($file_name);
                }
            }
            
            $customized = 1;
            $file_header = recognize(@$org_file_header);
            // echo "<pre>";print_r($file_header);exit;
            if(empty($file_header)){
                $_SESSION['error_x'] = 'Invalid CSV Format. Your CSV file must contain column with name "'.$field_name.'". Use CSV FORMAT : (<strong>'.$field_name.'</strong>)';
                if(file_exists($file_name)){
                    unlink($file_name);
                }
                echo "<meta http-equiv='refresh' content='0, url=".BASE_URL."csv-upload'  />";exit;
            }else{
                unset($_SESSION['error_x']);
            }
        }else{
            
            if(!file_exists(@$file_name)){
                $_SESSION['error_x'] =  "Please try again. File doesn't exist";
                echo "<meta http-equiv='refresh' content='0, url=".BASE_URL."csv-upload'  />";exit;
            }else{
                unset($_SESSION['error_x']);
            }
            
            $fields = $_POST['custom_header']; 
            $csv_header = array();
            if(!empty($fields)){
                foreach($fields as $k =>$v){
                    if(trim($v) == 'skip')
                        $fields[$k] = '';
                }
                //$fields = array_diff($fields, ['skip']);
                $csv_header = array_values($fields);
                $csv_header = array_filter($csv_header);
                // $csv_header = array_unique($csv_header);
            }
            $display_error = '';
            $checkFields = checkFields($field_array,$csv_header); 
            $customized = 1;
            $org_file_header = json_decode($_POST['org_file_header'], true);
            $file_header = recognize(@$org_file_header);
            $sheetData = json_decode($_POST['sheetData'], true);
            if(empty($csv_header)){
                $display_error = '<code>You have to select '.@$field_name.' columns. You have Skipped / ignored <strong>ALL</strong> the columns!</code>';
            }elseif(is_array($fields) && !empty($fields) && strlen($checkFields) > 0 ){
                $display_error = '<code>You have to select <strong>'.$checkFields.'</strong> column. You have Skipped / ignored <strong>'.$checkFields.'</strong> columns!</code>';
            }
            // elseif(is_array($fields) && !empty($fields) && count($csv_header) != count($fields) ){
            //     $display_error = '<code>Duplicate columns are not allowed!</code>';
            // }
            // echo "<pre>";echo $display_error;die;
            if($handle = fopen($file_name, 'r')){
                $file_headers = fgetcsv($handle, 5000, ',');
                // echo "<pre>";print_r($file_headers);exit;
                $existing_count = 0;
                $valid_count = 0;
                $invalid_count = 0;
                $count = 0; 
                $report_count = 0; 
                $percent = 0; 
                $sql_chunk_metadata = '';
                $sql_chunk_license_1 = '';
                $sql_chunk_license_2 = '';
                $sql_chunk_license_3 = '';	
                $total_count = 0;	
                while(($data = fgetcsv($handle, 1000, ',')) !== false){
                    $total_count++;
                }
                // echo $total_count;die;
                $handle = fopen($file_name, 'r');
                fgetcsv($handle, 5000, ',');
            
                $qyr_part_metadata = "INSERT IGNORE INTO mr_report_metadata (`rep_contents`, `rep_table_of_contents`, `meta_rep_id`) VALUES";
                
                $qyr_license_1="INSERT IGNORE INTO mr_report_license (`li_rep_id`,`li_key`,`li_value`) VALUES";
                
                $qyr_license_2="INSERT IGNORE INTO mr_report_license (`li_rep_id`,`li_key`,`li_value`) VALUES";
                
                $qyr_license_3="INSERT IGNORE INTO mr_report_license (`li_rep_id`,`li_key`,`li_value`) VALUES";

                $header = NULL;
                while(($data = fgetcsv($handle, 5000, ',')) !== false){
                    // $report_count++;
                    $csvdata = array();
		
                    if(!$header){
                        $header = (is_array($fields) && !empty($fields)) ? $fields + $data : $data;
                    }                    
                    // echo "<pre>";print_r($header);exit;

                    if(count($header)==count($data)){
                        $csvdata = array_combine($header, $data); unset($csvdata['']);
                        // echo "<pre>";print_r($csvdata);exit;
                
                        $rep_title= @$csvdata['ReportTitle'] ?  addslashes(trim($csvdata['ReportTitle'])) : '';                        
                
                        $publisher_id = @$csvdata['PublisherID'];
                        $rep_sub_cat_1_id= @$csvdata['CategoryID'];
                        $rep_page= @$csvdata['Pages'];
                
                        $entry_date= @$csvdata['Date'];
                        $entry_date= date("Y-m-d H:i:s",strtotime($entry_date));
                
                        $rep_contents = (@$csvdata['Content'] && !empty(@$csvdata['Content'])) ? addslashes(trim(get_pure_words(@$csvdata['Content']))) : null;
                        $rep_table_of_contents = (@$csvdata['TOC'] && !empty(@$csvdata['TOC'])) ? addslashes(trim(get_pure_words(@$csvdata['TOC']))) : null;
                        $rep_toc_fig = (@$csvdata['TOF'] && !empty(@$csvdata['TOF'])) ? addslashes(trim(get_pure_words(@$csvdata['TOF']))) : null;
                        $no_of_table_fig = (@$csvdata['NumberOfTOF'] && !empty(@$csvdata['NumberOfTOF'])) ? @$csvdata['NumberOfTOF'] : 0;
                        $rep_descrip = (@$rep_contents && !empty(@$rep_contents)) ? addslashes(trim(get_shortDescription($rep_contents))) : null;
                
                        $rep_page_title = (@$csvdata['PageTitle'] && !empty(@$csvdata['PageTitle'])) ?  addslashes(trim(get_pure_words(@$csvdata['PageTitle']))) : null;
                        $rep_meta_title = (@$csvdata['MetaTitle'] && !empty(@$csvdata['MetaTitle'])) ?  addslashes(trim(get_pure_words(@$csvdata['MetaTitle']))) : null;

                        //-------------------------- REPORT LICENSES --------------------------------------
                        
                        if(isset($csvdata['SingleUserLicense'])){                            
                            $li_key1 = "Single User Price";
                            @$li_value1 = str_ireplace(array('$','(',')',',','-'), '', @$csvdata['SingleUserLicense']);
                        }
                        if(isset($csvdata['EnterpriseUserLicense'])){
                            $li_key2 = "Enterprise User License";
                            @$li_value2 =  str_ireplace(array('$','(',')',',','-'), '', @$csvdata['EnterpriseUserLicense']);
                        }
                        if(isset($csvdata['CorporateUserLicense'])){
                            $li_key3 = "Corporate User License";
                            @$li_value3 =  str_ireplace(array('$','(',')',',','-'), '', @$csvdata['CorporateUserLicense']);
                        }
                        $rep_date=date('Y-m-d H:i:s');

                        if(empty($rep_title)) { continue; }
                        $rep_url = '';
                        // CHECK IF REPORT TITLE EXISTS
                        $duplicate_report_details = checkDuplicateReport($rep_title, $link);	
                        if($duplicate_report_details['is_duplicate_report'] === true){  // IF EXISTS, UPDATE DATA
                            $rep_id = $duplicate_report_details['rep_id'];
                            
                            // UPDATE REPORT TABLE DATA 
                            $update_stmt = $link->prepare("update mr_report set rep_title = ?, rep_descrip = ?,rep_page = ?,rep_date = ?,rep_toc_fig = ?,rep_entry_date = ?, no_of_table_fig = ? where id = ? ");
                            $update_stmt->bind_param('ssssssdd', $rep_title ,$rep_descrip, $rep_page, $rep_date, $rep_toc_fig, $entry_date, $no_of_table_fig, $rep_id);
                            $update_stmt->execute();
                            $update_stmt->close();
                            // UPDATE REPORT METADATA TABLE DATA 
                            $update_meta_stmt = $link->prepare("update mr_report_metadata set rep_contents = ?, rep_table_of_contents = ? where meta_rep_id = ? ");
                            $update_meta_stmt->bind_param('ssd', $rep_contents, $rep_table_of_contents, $rep_id);
                            $update_meta_stmt->execute();
                            $update_meta_stmt->close();
                            // UPDATE REPORT LICENSE TABLE DATA 
                            if(isset($csvdata['SingleUserLicense'])){                            
                                $li_key1 = "Single User Price";
                                $li_value1 = @$csvdata['SingleUserLicense'];
                                update_licenses($li_key1, $li_value1, $rep_id, $link);
                            }
                            if(isset($csvdata['EnterpriseUserLicense'])){
                                $li_key2 = "Enterprise User License";
                                @$li_value2 =  @$csvdata['EnterpriseUserLicense'];
                                update_licenses($li_key1, $li_value1, $rep_id, $link);
                            }
                            if(isset($csvdata['CorporateUserLicense'])){
                                $li_key3 = "Corporate User License";
                                @$li_value3 =  @$csvdata['CorporateUserLicense'];
                                update_licenses($li_key1, $li_value1, $rep_id, $link);
                            }
                            
                            $existing_count++;
                            continue;
                            // $percent = (@$report_count/@$total_count)*100;
                            // $percent = ceil($percent);
                        }
                        $rep_url = generateReportURL($rep_title, $link);
                        if(empty($rep_url)) { continue; }
                        // echo $rep_url;exit;

                
                        
                        // $unique=rand(1,1000);
                        // $unique_url=$url8.'-'.$unique;
                        $check_category = checkIfCategoryExists($rep_sub_cat_1_id);
                        $check_publisher = checkIfPublisherExists($publisher_id);
                        if(empty($rep_sub_cat_1_id) || $check_category == false || empty($publisher_id) || $check_publisher == false || empty($rep_descrip) || empty($rep_page) || empty($rep_contents) || empty($li_value1)){
                            $invalid_count++;
                            continue;
                        }else{
                            
                            $sql="INSERT IGNORE INTO mr_report (rep_sub_cat_1_id,publisher_id,rep_url,rep_title,rep_descrip,rep_page,rep_date,rep_toc_fig, no_of_table_fig,rep_entry_date)  
                            VALUES ('$rep_sub_cat_1_id','$publisher_id','$rep_url','".$rep_title."','$rep_descrip','$rep_page','$rep_date','$rep_toc_fig','$no_of_table_fig','$entry_date')";
                
                
                            if($link->query($sql)){
                                $valid_count++;
                                $last_id = $link->insert_id;
                                // $last_id = SELECT LAST_INSERT_ID();
                                $sql_chunk_metadata .= sprintf("( '%s','%s','%d'),", addslashes(trim($rep_contents)), addslashes(trim($rep_table_of_contents)) , $last_id);
                                
                                if($li_value1 > 0){	
                                    $sql_chunk_license_1 .= sprintf("( '%d','%s','%d'),", addslashes(trim($last_id)), addslashes(trim($li_key1)) , $li_value1);
                                }
                                if($li_value2 > 0){	
                                    $sql_chunk_license_2 .= sprintf("( '%d','%s','%d'),", addslashes(trim($last_id)), addslashes(trim($li_key2)) , $li_value2);
                                }
                                if($li_value3 > 0){	
                                    $sql_chunk_license_3 .= sprintf("( '%d','%s','%d'),", addslashes(trim($last_id)), addslashes(trim($li_key3)) , $li_value3);
                                }
                                $count++;
                                if($count == 500){
                                    // echo "<pre>";print_r($link);die;
                                    $sql_insert_metadata = substr($sql_chunk_metadata, 0,-1);
                                    // echo $qyr_part_metadata.$sql_insert_metadata;die;
                                    $rep_query = $link->query($qyr_part_metadata.$sql_insert_metadata);
                                    $last_insert_id = $link->insert_id;
                
                                    if(!empty(@$sql_chunk_license_1)){
                                        $sql_insert_license1 = substr($sql_chunk_license_1, 0,-1);
                                        $rep_query1 = $link->query($qyr_license_1.$sql_insert_license1);
                                    }
                                    if(!empty(@$sql_chunk_license_2)){
                                        $sql_insert_license2 = substr($sql_chunk_license_2, 0,-1);
                                        $rep_query2 = $link->query($qyr_license_2.$sql_insert_license2);
                                    }
                                    if(!empty(@$sql_chunk_license_3)){
                                        $sql_insert_license3 = substr($sql_chunk_license_3, 0,-1);
                                        $rep_query3 = $link->query($qyr_license_3.$sql_insert_license3);
                                    }
                                    if(@$last_insert_id > 0){}
                                    else{ $invalid_count++; }
                                    $count = 0; $sql_chunk_metadata = '';$sql_chunk_license_1 = $sql_chunk_license_2 = $sql_chunk_license_3 ='';
                                }
                            }else{
                                echo mysqli_error($link); 
                                $invalid_count++;
                            }
                        }
                    }
                    // $percent = (@$report_count/@$total_count)*100;
                    // $percent = ceil($percent);
                }
                if(!empty($sql_chunk_metadata)){
                    $sql_insert_metadata = substr($sql_chunk_metadata, 0,-1);
                    $rep_query = $link->query($qyr_part_metadata.$sql_insert_metadata);
                    if($rep_query === false ){
                        echo mysqli_error($link);
                    }
                    $last_insert_id = $link->insert_id;
                    if(!empty(@$sql_chunk_license_1)){
                        $sql_insert_license1 = substr($sql_chunk_license_1, 0,-1);
                        // echo $qyr_license_1.$sql_insert_license1;die;
                        $rep_query1 = $link->query($qyr_license_1.$sql_insert_license1);
                    }
                    if(!empty(@$sql_chunk_license_2)){
                        $sql_insert_license2 = substr($sql_chunk_license_2, 0,-1);
                        $rep_query2 = $link->query($qyr_license_2.$sql_insert_license2);
                    }
                    if(!empty(@$sql_chunk_license_3)){
                        $sql_insert_license3 = substr($sql_chunk_license_3, 0,-1);
                        $rep_query3 = $link->query($qyr_license_3.$sql_insert_license3);
                    }
                    if(@$last_insert_id > 0){}
                    else{ $invalid_count++; }
                    $count = 0; $sql_chunk_metadata = '';$sql_chunk_license_1 = $sql_chunk_license_2 = $sql_chunk_license_3 ='';
                }
                $upload_msg = '<span class="text-success">[Valid Reports : '.@$valid_count.'</span> , ';
                $upload_msg.= '<span class="text-danger">Invalid Reports : '.@$invalid_count.'</span> , ';
                $upload_msg.= '<span class="text-info">Existing Reports : '.@$existing_count.']</span>';
                $success_msg="<h3 class='alert alert-success'>
                            <b>Reports have been imported successfully into database</b> ".@$upload_msg."<br/>
                        </h3>
                        <span class='text-danger'>*NOTE : Please Wait till page gets refreshed automatically after 10 seconds</span>
                        <meta http-equiv='refresh' content='10,url=".BASE_URL."csv-upload' />";
            }else{
                $error_msg='Unable to open file. Please try again !';
            }
            fclose($handle);
            $file_dir='../uploads/'.$org_file_name;
            if(file_exists($file_dir)){unlink($file_dir);}            
        }
    }catch (Exception $e){
        $_SESSION['error_x'] ='File not found. Please try again.';
        if(file_exists($file_name)){unlink($file_name);}
    }
}else{
    $error_msg='<span class="fa fa-times-circle"></span>&nbsp;Invalid File Format. File needs to be CSV Only';
}