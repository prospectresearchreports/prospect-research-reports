
<?php
include_once  'mpdf/mpdf.php';

$id=$_REQUEST['id'];


$link=mysqli_connect("localhost","root","","qqmarket_research");
if(!$link)
{
 echo "Error";   
}
$sql="SELECT * FROM mr_global_checkout WHERE global_checkout_id='$id'";

$result=mysqli_query($link,$sql);

if(mysqli_num_rows($result) > 0){


$row=mysqli_fetch_array($result);

$mpdf = new mpdf();
$html="<div>
            <div style='float:left;width:80%;font-family:cambria' class='header'>
                <h2 style='color:#0465ac'>QandQ Market Research</h2>
                <p>Via dell'Archeologia 29,Roma 00133,Italy.</p>
                <p>Tel : +1 833 267 4156</p>
                <p>Email : help@qandqmarketresearch.com , Website : www.qandqmarketresearch.com</p>
                <p><span style='color:maroon'>Our Tax Registration number is : </span>QRMT574125 , INVOICE NO :QQMR-".$id."-4444 </p>
                <p><span style='color:maroon'>INVOICE DATE : </span>".date("d-m-Y")."</p>
            </div>
            <div style='float:right;width:20%'>
                <img src='images/qq-logo.png' height='150px' width='auto'  />
            </div>
        </div><br/><br/>
        <h2 style='color:#0465ac;text-align:Center'>INVOICE</h2><br/>
        <table border='1' width='100%' style='border-collapse:collapse;font-size:14px;font-family:cambria' cellapdding='20px'>
            <tr><th style='padding:10px' colspan='4'>Client Details</th></tr>
            <tr><th style='padding:8px'>Customer Name</th><td>".$row['global_checkout_name']."</td><th>Company Name</th><td>".$row['global_checkout_company']."</td></tr>
            <tr><th style='padding:8px'>Contact Number</th><td>".$row['global_checkout_phone']."</td><th>Email Address</th><td>".$row['global_checkout_email']."</td></tr>
            <tr><th style='padding:8px'>Bill to Address</th><td colspan='3' style='text-align:left'>".$row['global_checkout_billtoaddress']."</td></tr>
        </table><br/><br/><br/>
        <table border='1' width='100%' style='border-collapse:collapse;font-size:14px;font-family:cambria' cellapdding='20px'>
            <tr><th style='padding:10px' colspan='4'>Report Details</th></tr>
            <tr><th style='padding:8px'>Report Title</th><td colspan='3' style='text-align:left'>".$row['global_checkout_rep_title']."</td></tr>
            <tr><th style='padding:8px'>Report Price</th><td colspan='3' style='text-align:left'> $".$row['global_checkout_rep_price']."</td></tr>
            <tr><th style='padding:8px'>License Type</th><td colspan='3' style='text-align:left'>".$row['global_checkout_lisence_type']."</td></tr>
        </table>
        <br/><br/><br/>
        <h3 style='text-align:Center'> Thank you for business with us.</h3><br/>
        <div style='width:50%;float:left;'>
            <p>QandQ Market Research</p>
        </div>
        <div style='width:50%;float:right;'>
            <p><a href='https://www.qandqmarketresearch.com' style='text-align:right;text-decoration:none'>qandqmarketresearch.com</a></p>
        </div>
";

$mpdf->WriteHTML($html);

$mpdf->Output();
//
//$pdf = new FPDF('p','mm','A4');
//$pdf->AddPage();
//
//$pdf->SetFont('Arial','B','22');
//$pdf->SetTextColor(0,0,255);
//$pdf->Cell('130','8','QandQ Market Research',0,1);
//$pdf->Image('images/qq-logo.png',150,8,-300);
//$pdf->SetFont('Arial','','8.5');
//$pdf->SetTextColor(0,0,0);
//$pdf->Cell('130','7','Prospect Lefferts Garden,Brooklyn,NY - 11225 USA : 495.',0,1);
//$pdf->Cell('130','7','Tel : USA : +1 7187 172 888',0,1);
//$pdf->Cell('130','7','Email : help@qandqmarketresearch.com , Website : www.qandqmarketresearch.com',0,1);
//$pdf->SetTextColor(255,0,0);
//$pdf->Cell('130','7','Our Tax Registration number is : QRMT574125 , INVOICE NO :QQMR-'.$id.'-4444  ',0,1);
//$pdf->Cell('130','7','INVOICE DATE : '.date("d-m-Y"),0,1);
//
//$pdf->SetFont('Arial','B','18');
//$pdf->SetTextColor(255,0,0);
//$pdf->Cell('80','8','',0,1);
//$pdf->Cell('190','8','INVOICE',0,1,'C');
//$pdf->Cell('80','7','',0,1);
//$pdf->SetFont('Arial','B','8.5');
//$pdf->SetTextColor(0,0,0);
//$pdf->Cell('30','8','Client Details',1,1);
//$pdf->SetFont('Arial','B','8.5');
//$pdf->Cell('30','8','Customer Name',1,0);
//$pdf->SetFont('Arial','','8');
//$pdf->Cell('40','8',$row['global_checkout_name'],1,0);
//$pdf->SetFont('Arial','B','8.5');
//$pdf->Cell('30','8','Company Name',1,0);
//$pdf->SetFont('Arial','','8');
//$pdf->Cell('90','8',$row['global_checkout_company'],1,1);
//$pdf->SetFont('Arial','B','8.5');
//$pdf->Cell('30','8','Contact Number',1,0);
//$pdf->SetFont('Arial','','8');
//$pdf->Cell('40','8',$row['global_checkout_phone'],1,0);
//$pdf->SetFont('Arial','B','8.5');
//$pdf->Cell('30','8','Email Address',1,0);
//$pdf->SetFont('Arial','','8');
//$pdf->Cell('90','8',$row['global_checkout_email'],1,1);
//$pdf->SetFont('Arial','B','8.5');
//$pdf->Cell('30','8','Bill To Address',1,0);
//$pdf->SetFont('Arial','','8');
//$pdf->Cell('160','8',$row['global_checkout_billtoaddress'],1,1);
//
//
//$pdf->SetFont('Arial','B','8.5');
//$pdf->Cell('80','8','',0,1);
//$pdf->Cell('30','8','Report Details',1,1);
//$pdf->Cell('50','8','Report Title',1,0);
//$pdf->SetFont('Arial','','8.5');
//$pdf->MultiCell('140','8',$row['global_checkout_rep_title'],1,'L');
//$pdf->SetFont('Arial','B','8.5');
//$pdf->Cell('50','8','Report Price',1,0);
//$pdf->SetFont('Arial','B','14');
//$pdf->Cell('140','8','$'.$row['global_checkout_rep_price'],1,1);
//$pdf->SetFont('Arial','B','8.5');
//$pdf->Cell('50','8','Lisence Type',1,0);
//$pdf->SetFont('Arial','','8.5');
//$pdf->Cell('140','8',$row['global_checkout_lisence_type'],1,1);
//
//
//$pdf->Cell('80','8','',0,1);
//$pdf->SetFont('Arial','B','8.5');
//$pdf->Cell('30','8','Bank Details',1,1);
//$pdf->SetFont('Arial','','8.5');
//$pdf->Cell('50','8','Beneficiary Name',1,0);
//$pdf->Cell('140','8','Q AND Q MARKET RESEARCH',1,1);
//$pdf->Cell('50','8','Beneficiary Account Number',1,0);
//$pdf->Cell('140','8','50200021645478',1,1);
//$pdf->Cell('50','8','Beneficiary Bank Name',1,0);
//$pdf->Cell('140','8','HDFC',1,1);
//$pdf->Cell('50','8','Account Type',1,0);
//$pdf->Cell('140','8','Current',1,1);
//$pdf->Cell('50','8','Swift Code',1,0);
//$pdf->Cell('140','8','HDFCINBB',1,1);
//$pdf->Cell('50','8','IFSC Code',1,0);
//$pdf->Cell('140','8','HDFC0000447',1,1);
//$pdf->Cell('50','8','MICR Code',1,0);
//$pdf->Cell('140','8','411240013',1,1);
//$pdf->Cell('50','16','Bank Detailed Address',1,0);
//$pdf->MultiCell('140','8','SHOP 3,4,5 and 6 Ground Floor Gurutej Bahadur co-Operative Housing Society Limited, Aundh Road, Khadki Postal Code- 41820',1,'L');  
//
//
//$pdf->Cell('80','10','',0,1);
//$pdf->SetFont('Arial','B','10.5');
//$pdf->SetTextColor(0,0,255);
//$pdf->Cell('190','5','Thank you for the business with us',0,1,'C');
//$pdf->Cell('80','8','',0,1);
//$pdf->Cell('118','5','QandQ Market Research',0,0,'L');
//$pdf->Cell('70','5','www.qandqmarketresearch.com',0,1,'R');
//
//
//
//$pdf->Output();
}else{
    echo 'ERROR :'.mysql_error();
    
}
ini_set('display_errors', 1);
?>