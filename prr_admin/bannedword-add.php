
<html>
<head>
<title>Add Banned Word | Prospect Research Reports</title>


 <!--*****************************  HEADER  ************************************** -->

<?php require_once 'header.php'; ?>

<!--*****************************  HEADER  ************************************** -->


<?php
    if(isset($_POST["submit"])){        
        $words=explode(',',$_POST["words"]);
        $status=$_POST["status"];
        $created = date('Y-m-d H:i:s');
		
		$count = 0;
		$first_ins_id = 0;
		$sql_chunk= '';
        $qyr_part ="insert ignore into mr_bannedwords(word,status,created) values ";
		foreach($words as $word){
			if(!empty(@$word)) {
				$sql_chunk .= sprintf("('%s', '%d', '%s'),", addslashes(trim(@$word)),@$status,@$created);
				$count++;
				if($count == 100){
					$sql_insert = substr($sql_chunk, 0,-1);
					//echo $qyr_part.$sql_insert; exit;
					$first_ins_id = $link->query($qyr_part.$sql_insert);
					$count = 0; $sql_chunk = '';
				}
			}
		}
		if(!empty($sql_chunk)){
				$sql_insert = substr($sql_chunk, 0,-1);
				//echo $qyr_part.$sql_insert; exit;
				$first_ins_id = $link->query($qyr_part.$sql_insert);
				$count = 0; $sql_chunk = '';
		}
        
		if(@$first_ins_id > 0 )
		{
			$success='<div class="alert alert-success alert-form"><span class="fa fa-check-circle"></span>&nbsp;Bannedword Added successfully !&nbsp;&nbsp; <mark>Redirecting......</mark></div> 
			<meta http-equiv="refresh" content="2,url='.BASE_URL.'bannedword-list "/>'; 
		}
		else
		{                
			$error='<div class="alert alert-danger alert-form"><span class="fa fa-times-circle"></span>&nbsp;Error Adding Bannedword !'.(ENVIRONMENT == 'development' ? mysqli_error($link) : '').'</div>';
			
		}
    }
    
    
    ?>

    <h1 class="stats"><span class="far fa-edit"></span> Add New Banned Word/s</h1><br/><br/><br/>

    <button  class='btn btn-primary helper-item' onclick="window.history.back()">
        <a href="#"><i class="fa fa-arrow-left"></i> Back</a> 
    </button>
	<div class="form-container">
	<?php 
		if(isset($success)){
			echo "<br/>".$success;
		}else{
			if(isset($error)){
				echo "<br/>".$error;
			}
	?>    
	    <form class="form-horizontal" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method='post' name="word_add" id="word_add" enctype='application/x-www-form-urlencoded'>
	    	<div class="form-group">
	    		<div class="col-md-10">
		    		<label class="control-label">BannedWord/s<span class="star">*</span> :</label>
		    		<textarea rows='5' name="words" class="form-control" placeholder="Add Comma separated multiple bannedwords eg: loan,debt etc" required="" ><?=@$words ? implode(',',@$words) : ''?></textarea>
		    	</div>
	    	</div>

	    	<div class="form-group">
	    		<div class="col-md-10">
		    		<label class="control-label">STATUS <span class="star">*</span> :</label>
		    		<label class="radio-inline"><input type="radio" name="status" value="1" required="" />Active</label>
		    		<label class="radio-inline"><input type="radio" name="status" value="0" required="" />Inactive</label>		    		
		    	</div>
	    	</div>
	    	
	    	<div>
		    	<button type="submit" name="submit" class="btn btn-upload"><span class="far fa-edit"></span> Add</button>
		    </div>
	    </form>
		<?php } ?>
	</div>



    <!--*****************************  FOOTER  ************************************** -->

    <?php require_once 'footer.php'; ?>

    <!--*****************************  FOOTER  ************************************** -->
