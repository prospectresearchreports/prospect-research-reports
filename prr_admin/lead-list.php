
<html>
<head>
<title>Lead List | Prospect Research Reports</title>


 <!--*****************************  HEADER  ************************************** -->
<script src="assets/js/status_toggle.js"></script>

<?php 
    require_once 'header.php';
    require_once 'auth.php';
    if(!isset($_GET['type']) && empty($_GET['type'])){
        echo "<meta http-equiv='refresh' content='0,url=".BASE_URL."' />";
    }else{
        $contact_form_type = $_GET['type'];
    }
?>

<!--*****************************  HEADER  ************************************** -->

<?php
//********************  PAGINATION  ****************************//
$form_type = $contact_form_type;
$where = '';
$input_query = '';
if($contact_form_type == '1'){
    $form_type ='1,2';
    $where='contact_form_type in ('.$form_type.')';
}elseif($contact_form_type == '4'){
    $form_type ='4,5,6,7';
    $where='contact_form_type in ('.$form_type.')';
}else{
    $where = "contact_form_type = $contact_form_type";
}

if (isset($_GET['query']) && !empty($_GET['query'])) {
    $page_query = "&query=".trim($_GET['query']);
    $input_query = trim($_GET['query']);
    $where.= " and ( contact_person like '%".$input_query."%' or contact_email   like '%".$input_query."%') ";
}  
if (isset($_GET['country']) && !empty($_GET['country'])) {
    $page_query = "&country=".trim($_GET['country']);
    $input_country = explode("-", trim($_GET['country']))[1];
    $where.= " and ( contact_country like '%".$input_country."%' or contact_real_country like '%".$input_country."%') ";
}  

if(!empty($where)){
    $where = " where ".$where;
}

$data = create_links($_GET, $link , 'mr_form_contact', $where, $contact_form_type);

if(@$data['total_rows'] > 0 && @$data['pageno'] > @$data['total_pages']){
    $url = BASE_URL.'lead-list'.@$data['last_page'];
    echo "<meta http-equiv='refresh' content='0, url=".@$url."'/>";
}
$input_placeholder = 'Search here for Lead Name, Lead Email Address';

?>

    <h1 class="stats"><span class="fas fa-list-ol"></span> <?=(@$contact_form_type == '4' ? 'Lead List' : $FORM_TYPES[@$contact_form_type].' List')?> <span class="text-danger">[</span><span class='count text-danger'><?=@$data['total_rows']?></span><span class="text-danger">]</span></h1><br/><br/>
    <?php if(@$contact_form_type == '4'){ ?>
    <button  class='btn btn-primary helper-item' onclick="window.location.href='<?php echo BASE_URL.'lead-add?type='.@$contact_form_type;?>'">
        <a href="<?php echo BASE_URL.'lead-add?type='.@$contact_form_type;?>"><i class="fa fa-plus"></i> Add New Lead</a> 
    </button>
    <button  class='btn btn-warning helper-item' onclick="window.location.href='<?php echo BASE_URL.'lead-import';?>'">
        <a href="<?php echo BASE_URL.'lead-import';?>"><i class="fa fa-upload"></i> Import Leads</a> 
    </button>
    <button  class='btn btn-warning helper-item' onclick="exportToExcel()">
        <i class="fa fa-download"></i> Export Leads
    </button>
    <?php } ?>

    <div class="pagination-form">
        <?php require_once 'pagination-form.php' ?>
    </div>

	<div class="table-responsive">
    	<table class="table table-bordered table-hover" style='text-align: center'>
    		<thead class="text-primary">
    			<tr>
                    <th><input style="transform: translateY(-0.5rem)" type="checkbox" id="selectAll" onclick="toggleSelection(this)"></th>
    				<th>Sr.No</th>
                    <th>Name</th>
                    <th>Email Address</th>
                <?php if(@$contact_form_type != '1'){ ?>
                    <th style= "min-width:250px">Report Title</th>
                <?php }else{ ?>
                    <th style= "min-width:300px">Contact Message</th>                
                    <th>Contact Budget</th>                
                <?php } ?>
                <?php if(@$form_type == '8'){ ?>
                    <th style= "min-width:120px">Payment Mode</th>
                <?php }?>
                    <th>Type</th>
                    <th>Company</th>
                    <th>Region</th>
                    <th>Country</th>
                    <th>Current Country</th>
                    <th style= "min-width:170px">Date</th>

                <?php if($_SESSION["user_type"]=='1'){ 
                    if(!in_array(@$contact_form_type,array(1,8))){ ?>
                        <th style= "min-width:130px">Create Checkout</th>
                    <?php } ?>
    				<th style='width:100px'>Options</th>
                <?php } ?>

    			</tr>
    		</thead>
    		<tbody id="deleteCount">
    			<?php
    			$getLeads=$link->query("select * from mr_form_contact ".@$where." order by contact_id desc limit ". @$data['offset']. "," . @$data['no_of_records_per_page']);
    			if($getLeads->num_rows > 0){
                    $i=1;
	    			while($row=$getLeads->fetch_assoc()){
                        $sr_no = ( @$data['no_of_records_per_page'] * (@$data['pageno'] == '1' ? 0 : @$data['pageno']-1) ) + $i;
                        $id = $row["contact_id"];
                        $msg = (strlen($row['contact_msg']) > 100) ? substr(@$row["contact_msg"],0,100)."... <br/><span class='text-primary content' id='view".@$id."' title='View More' onclick='getMoreContent(".$id.")'>View More</span>" : @$row["contact_msg"];
	    				$less = substr(@$row["contact_msg"],0,100)."... <br/><span class='text-primary content' id='view".@$id."' title='View More'  onclick='getMoreContent(".$id.")'>View More</span>";
                        echo "<tr id='leadDelete".$row['contact_id']."'><td><input type='checkbox' name='selected_ids[]' value='".@$row['contact_id']."'></td>
                        <td>".@$sr_no."</td>";
                        echo "<td>".$row["contact_person"]."</td>";
                        echo "<td>".$row["contact_email"]."</td>";
                        
                        if(@$contact_form_type != '1'){
                            if(!empty($row["contact_rep_title"])){
                                echo "<td width='200px'>".@$row["contact_rep_title"]."</td>";
                            }else{
                                echo "<td></td>";
                            }
                        }else{
                            echo "<td class='hidden' id='more".$id."'>".@$row["contact_msg"]."</td>";
                            echo "<td class='hidden' id='less".$id."'>".@$less."</td>";
                            echo "<td align='center' id='msg".$id."'>".@$msg."</td>";
                            echo "<td align='center'>".@$row["contact_budget"]."</td>";
                        }
                        
                        if(@$form_type == '8'){
                            echo "<td>".@$PAYMENT_MODES[$row['report_payment_mode']]."</td>";
                        }
                        echo "<td>".@$GLOBALS['FORM_TYPES'][@$row["contact_form_type"]]."</td>";
                        echo "<td>".$row["contact_company"]."</td>";
                        echo "<td>".$row["contact_exact_region"]."</td>";
                        echo "<td>".$row["contact_country"]."</td>";
                        echo "<td>".$row["contact_real_country"]."</td>";
                        echo "<td>".$row["contact_datetime"]."</td>";

                        if($_SESSION["user_type"]=='1'){
                            if(!in_array(@$contact_form_type, array('1','8'))) {
                                if(!empty($row["contact_rep_title"])){
                                    echo "<td><a href='".BASE_URL."global-checkout-add?id=".$id."' target='_blank' class='btn-link checkout-link'>Create Checkout</a></td>";  
                                }else{
                                    echo "<td>&ndash;</td>";  
                                }
                            }                    

                            echo "<td colspan='3'><a href='lead-edit?type=".@$contact_form_type."&id=".$id."'><i class='far fa-edit text-primary'></i></a>
                            <button data-toggle='modal' data-target='#removeMe".$id."' style='border:0;background:none'><i class='fa fa-trash text-danger'></i></button>
                                    <div id='removeMe".$id."' class='modal fade' role='dialog'>
                                        <div class='modal-dialog'>
                                            <div class='modal-content'>
                                                <div class='modal-header'>
                                                    <button type='button' class='close' data-dismiss='modal'>&times;</button>
                                                    <h4 class='modal-title'> <span class='fa fa-trash'></span> Delete Confirmation  </h4>
                                                </div>
                                                <div class='modal-body'>
                                                    <p style='font-size: 1.2em'><strong>Are you sure you want to remove this item ?</strong></p>
                                                </div>
                                                <div class='modal-footer'>
                                                    <button class='btn btn-primary btn-confirm' type='submit' onclick='deleteData(".$id.", 8)' >Confirm</button>
                                                    <button type='button' class='btn btn-warning btn-cancel' data-dismiss='modal'>Cancel</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </td>";
                        }
                        echo "</tr>";
                        $i++;
	    			}
	    		}else{
					echo "<tr style='height:60px'><td colspan='12' align='center'  style='padding:25px;font-size:1.8em'><span class='fa fa-exclamation-triangle text-danger'></span> <strong>NO LEADS FOUND !</strong></td></tr>";
	    		}
    			?>
    		</tbody>
    	</table>
    </div><br/><br/>
    <ul class="pagination">
        <?=pagination(@$data);?>
    </ul>

    <br/><br/>



    <!--*****************************  FOOTER  ************************************** -->

    <?php require_once 'footer.php'; ?>

    <!--*****************************  FOOTER  ************************************** -->
    <script type="text/javascript">
        // Function to toggle all checkboxes
        function toggleSelection(selectAllCheckbox) {
            const checkboxes = document.querySelectorAll('input[name="selected_ids[]"]');
            checkboxes.forEach(checkbox => {
                checkbox.checked = selectAllCheckbox.checked;
            });
        }
        function getMoreContent(id){
            var message = $("#more"+id).html();
            var less = "&nbsp;<span class='text-warning content' title='View Less'  onclick='getLessContent("+id+")'>View Less</span>"
            $('#msg'+id).html(message+less)
            // console.log(message+less)
        }
        function getLessContent(id){
            var message = $("#less"+id).html();
            $('#msg'+id).html(message)
        }
        var contact_country = "<?php echo @$_GET['country'] ?? ''?>"
        // console.log(contact_country)

        if(contact_country && contact_country.length > 0){
            $("select#contact_country option").each(function(){
                let option_value = $(this).val()
                if(option_value === contact_country){
                    $(this).attr('selected','selected')
                }else{
                    $(this).removeAttr('selected')
                }
            })
        }
    </script>
<script>
        function exportToExcel() {
            const selectedIds = Array.from(document.querySelectorAll('input[name="selected_ids[]"]:checked'))
                .map(checkbox => checkbox.value);

            if (selectedIds.length === 0) {
                alert('Please select at least one row to export.');
                return;
            }

            // Create XMLHttpRequest
            const xhr = new XMLHttpRequest();
            xhr.open('POST', 'lead-export.php', true);
            xhr.responseType = 'blob'; // Set response type to 'blob'

            xhr.onload = function () {
                if (xhr.status === 200) {
                    // Create a Blob from the response
                    const blob = new Blob([xhr.response], {
                        type: xhr.getResponseHeader('Content-Type'),
                    });

                    // Create a link element for download
                    const url = window.URL.createObjectURL(blob);
                    const a = document.createElement('a');
                    a.href = url;
                    const now = new Date();
                    const formattedDate = now
                        .toISOString()
                        .replace(/[-:TZ]/g, '') // Remove unwanted characters
                        .slice(0, 14);          // Keep 'YYYYMMDDHHmmSS' format

                    // Generate dynamic filename
                    const filename = `Exported Leads ${formattedDate}.xlsx`;
                    a.download = filename; // Name of the file
                    document.body.appendChild(a);
                    a.click();
                    a.remove();
                    window.URL.revokeObjectURL(url); // Clean up the URL
                } else {
                    alert('Failed to export data. Please try again.');
                }
            };

            xhr.onerror = function () {
                alert('An error occurred while trying to export data.');
            };

            // Send JSON data to the server
            const jsonData = JSON.stringify({ selected_ids: selectedIds });
            xhr.setRequestHeader('Content-Type', 'application/json');
            xhr.send(jsonData);
        }

    </script>