
<html>
<head>
<title>Add Category | Prospect Research Reports</title>


 <!--*****************************  HEADER  ************************************** -->

<?php require_once 'header.php'; ?>

<!--*****************************  HEADER  ************************************** -->


<?php
    if(isset($_POST["submit"]))
    {        
		$category=$_POST["category"];
		// $url=$_POST["url"];
		$url=generateURL($category);
		$status=$_POST["status"];
		if($_FILES['image']){
			$valid_extensions = array('image/jpg','image/jpeg','image/png');
			// echo $_FILES['image']['type'];
			if(in_array($_FILES['image']['type'],$valid_extensions)){
				$image=$_FILES["image"]["name"];
				$tmp_name=$_FILES["image"]["tmp_name"];
				$dir="../assets/images/category/";
				move_uploaded_file($tmp_name,$dir.$image);
				
				$sql=mysqli_query($link,"insert ignore into mr_sub_cat_1(sc1_name,sc1_url,status,sc1_image)  values('".$category."','".$url."','".$status."','".$image."')");
				
				if($sql===TRUE ){			
					$success='<div class="alert alert-success alert-form"><span class="fa fa-check-circle"></span>&nbsp;Category Added successfully !&nbsp;&nbsp; <mark>Redirecting......</mark></div> 
					<meta http-equiv="refresh" content="2,url='.BASE_URL.'category-list "/>'; 

				}else{
					$error='<div class="alert alert-danger alert-form"><span class="fa fa-times-circle"></span>&nbsp;Error Adding Category !'.(ENVIRONMENT == 'development' ? mysqli_error($link) : '').'</div>';
				} 			 
			}else{
				$error='<div class="alert alert-danger alert-form"><span class="fa fa-times-circle"></span>&nbsp;Invalid File Format. Allowed Only <mark><b>['.implode(' , ', @$valid_extensions).']</b></mark></div>';
			}
		}else{
			$error='<div class="alert alert-danger alert-form"><span class="fa fa-times-circle"></span>&nbsp;Invalid File</div>';
		}
    }
    
    ?>

    <h1 class="stats"><span class="far fa-edit"></span> Add New Category</h1><br/><br/><br/>

    <button  class='btn btn-primary helper-item' onclick="window.history.back()">
        <a href="#"><i class="fa fa-arrow-left"></i> Back</a> 
    </button>
	<div class="form-container">
	<?php 
		if(isset($success)){
			echo "<br/>".$success;
		}else{
			if(isset($error)){
				echo "<br/>".$error;
			}
	?>    
	    <form class="form-horizontal" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method='post' name="add_category" id="add_category" enctype='multipart/form-data'>
	    	<div class="form-group">
	    		<div class="col-md-10">
		    		<label class="control-label">Category<span class="star">*</span> :</label>
		    		<input  name="category"  value="<?=@$category?>" class="form-control" required="" />
		    	</div>
	    	</div>

	    	<!-- <div class="form-group">
	    		<div class="col-md-10">
		    		<label class="control-label">Category URL<span class="star">*</span> :</label>
		    		<input  name="url" class="form-control" value="<?=@$url?>"  required="" />
		    	</div>
	    	</div> -->

	    	<div class="form-group">
	    		<div class="col-md-10">
		    		<label class="control-label">Category Image<span class="star">*</span> :</label>
		    		<input type="file"  name="image" class="form-control" required="" />
		    	</div>
	    	</div>

	    	<div class="form-group">
	    		<div class="col-md-10">
		    		<label class="control-label">STATUS <span class="star">*</span> :</label>
		    		<label class="radio-inline"><input type="radio" name="status" value="1" <?=@$status == '1' ? 'checked' : ''?> required="" />Active</label>
		    		<label class="radio-inline"><input type="radio" name="status" value="0" <?=@$status == '0' ? 'checked' : ''?> required="" />Inactive</label>		    		
		    	</div>
	    	</div>
	    	
	    	<div>
		    	<button type="submit" name="submit" class="btn btn-upload"><span class="far fa-edit"></span> Add</button>
		    </div>
	    </form>
		<?php } ?>
	</div>



    <!--*****************************  FOOTER  ************************************** -->

    <?php require_once 'footer.php'; ?>

    <!--*****************************  FOOTER  ************************************** -->
