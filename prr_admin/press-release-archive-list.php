
<html>
<head>
<title> Press Release Archive List | Prospect Research Reports</title>


 <!--*****************************  HEADER  ************************************** -->

<?php require_once 'header.php'; ?>

<!--*****************************  HEADER  ************************************** -->
<script src="assets/js/status_toggle.js"></script>


    <h1 class="stats"><span class="fas fa-list-ol"></span> Press Release Archive List</h1><br/><br/><br/>

<button  class='btn btn-success helper-item' onclick="window.location.href='<?php echo BASE_URL.'press-release-list';?>'">
    <a href="<?php echo BASE_URL.'press-release-list';?>"><i class="fa fa-list"></i> PressRelease List</a> 
</button>

	<div class="table-responsive">
    	<table class="table table-bordered table-hover">
    		<thead class="text-primary">
    			<tr>
    				<th>Press ID</th>
    				<th>Press Release Title</th>
                    <th>Archive Status</th>
    			</tr>
    		</thead>
    		<tbody id="archiveCount">
    			<?php
    			$getArchivedPressReleases=$link->query("select * from mr_press  where archive_status='1' ");
    			if($getArchivedPressReleases->num_rows > 0){
	    			while($row=$getArchivedPressReleases->fetch_assoc()){
                        $id = $row['id'];
                        $archive_status = @$row['archive_status'];                        
                        $archive_checked = @$archive_status == '1' ? 'checked' : '';
	    				echo "<tr id='unarchiveStat".$id."'><td>".@$id."</td>";
	    				echo "<td>".$row["title"]."</td>";                        
                        echo getArchiveStatus($id, @$archive_status, $ARCHIVE_STATUS, $archive_checked, 2, 2);                        
                        echo "</tr>";
	    			}
	    		}else{
					echo "<tr style='height:60px;background:#efefef' ><td colspan='5' align='center'  style='padding:25px;font-size:1.8em'><span class='fa fa-exclamation-triangle text-danger'></span> <strong>NOTHING ARCHIVED  !</strong></td></tr>";
	    		}
    			?>
    		</tbody>
    	</table>
    </div><br/><br/>



    <!--*****************************  FOOTER  ************************************** -->

    <?php require_once 'footer.php'; ?>

    <!--*****************************  FOOTER  ************************************** -->
     <script type="text/javascript">

        function makeUnarchiveStatus(id)
            {
                 var xhr=new XMLHttpRequest();
                xhr.onreadystatechange=function()
                {
                    if(this.readyState===4 && this.status===200)
                    {
                        var response=this.responseText;
                        //alert(response.trim());
                        if(response.trim()!==""){
                            //alert("ID = "+id);
                            
                            //document.getElementById("unarchiveStat"+id).style.display="none";
                            document.getElementById("archiveCount").innerHTML=this.responseText;
                        }else{
                            $("#unarchiveStat"+id).css({display:'none'});
                            //alert(" Return ID = "+id);
                        }
                    }
                }
                xhr.open("get","ajaxoperations.php?pressUnarchiveId="+id,true);
                xhr.send();
            }
    </script>