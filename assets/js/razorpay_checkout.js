
// console.log(SITEURL + 'payment/eazypay-success')
function checkEmptyFields() {
    const form = document.getElementById('checkout-form');
    const inputs = form.querySelectorAll('input, select, textarea');
    let isEmpty = false;

    inputs.forEach(function(input) {
        if (input.type === 'radio') {
            // Check if any radio button in a group is selected
            const radioGroup = form.querySelectorAll(`input[name="${input.name}"]`);
            const isRadioChecked = Array.from(radioGroup).some(radio => radio.checked);
            if (!isRadioChecked) {
                isEmpty = true;
            }
        } else if (input.type === 'checkbox') {
            // You can add a checkbox check if needed
        } else if (input.type === 'select-one' || input.type === 'select-multiple') {
            // Check if a select option is selected (not the default empty option)
            if (input.value === "") {
                isEmpty = true;
            }
        } else if (input.tagName.toLowerCase() === 'textarea') {
            // Check if textarea is empty
            if (input.value.trim() === "") {
                isEmpty = true;
            }
        } else {
            // Check if input is empty
            if (input.value.trim() === "") {
                isEmpty = true;
            }
        }
    });

    return isEmpty;
}
$('body').on('click', '.btn-checkout', function(e){ 
    if(validate_captcha()){
        var country = $("select#country"). val();
        // country=country.replace(/\"/gi,'');
        // country=country.replace(/\\/gi,'');
        const phone_regex = new RegExp("^(^[0-9]{10}$)$");
        const emailRegex = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,}$/;
        var phone=$("#phone").val();
        var email=$("#email").val();
        var is_global=$("#is_global").val();
        var is_valid = true;
        var error_msg = ''
        if(checkEmptyFields()){
            error_msg = "All Fields are mandatory"
        }else if(is_global == "0" && !phone_regex.test(phone)){
            is_valid = false;
            error_msg = 'Invalid Phone Number'
        }else if(!emailRegex.test(email)){
            is_valid = false;
            error_msg = "Invalid Email Address"
        }
        if(error_msg.length > 0){
            alert(error_msg);
            e.preventDefault();
        }else{
            console.log('Valid form')
            // e.preventDefault()
            if(!$("button.btn-checkout").hasClass("disabled")){
                var radio = $("input[name='radio-inline']:checked"). val();
                if(radio == 'EazyPay'){
                    // alert(radio);
                    var name=$("#name").val();
                    var company=$("#company").val();
                    var country=$("#country").val();
                    // alert(country)
                    var zip=$("#zip").val();
                    var address=$("#address").val();

                    var title=$("#rep_title").val();
                    var price=parseInt($("#rep_price").val());
                    var license=$("#rep_license").val();
                    var currency=$("#currency").val();
                    
                    let form_data = {
                        "name":name,
                        "email":email,
                        "phone":phone,
                        "company":company,
                        "country":country,
                        "zip":zip,
                        "address":address,
                        "title":title,
                        "price":price,
                        "currency":currency,
                        "license":license,
                        "is_global":is_global,
                    }

                    // alert('working'+price);
                    var options = {
                        "key": "rzp_live_xLDXJ4EQ7XkGSP",
                        "currency": currency,
                        "amount": (price*100), // 2000 paise = INR 20
                        "name": "ProspectResearch Reports",
                        "description": title,
                        "image": SITEURL + "assets/images/prospect-research-reports-logo.png",
                        "handler": function (response){
                            form_data['response'] = response
                            $.ajax({
                                url: SITEURL + 'payment/eazypay-success',
                                type: 'post',
                                data: form_data, 
                                success: function () {
                                    window.location.href = SITEURL + 'payment/eazypay-thanks';
                                }
                            });                            
                        },                        
                        "theme": {
                            "color": "#528FF0"
                        }
                    };
                    var rzp1 = new Razorpay(options); 
                                        
                    rzp1.on('payment.failed', function (response){
                            form_data['response'] = response
                            $.ajax({
                            url: SITEURL + 'payment/eazypay-cancel',
                                type: 'post',
                                data: form_data, 
                                success: function () {
                                // alert("Cancelled");
                                window.location.href = SITEURL + 'eazypay/payment-failed';
                                }
                            });
                    });
                    rzp1.open();
                    e.preventDefault();
                }
            }
        }           
    }
}); 