<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/userguide3/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'Index_controller';
$route['404_override'] = 'error_404/404';
$route['translate_uri_dashes'] = FALSE;
$route["welcome"]="Welcome/index";


$route["404"]="Cms_controller/error_404";

// $route["blog/home"]= "Index_controller/home";
// $route["blog/home-1"]= "Index_controller/home_1";


//******************************    REPORTS         ****************************************//

$route["latest-reports"]              ="Report_controller/latest_reports";
$route["latest-reports/(:any)"]       ="Report_controller/report_details";
$route["reports/page/(:num)"]         ="Report_controller/latest_reports";
$route["reports"]                     ="Report_controller/reports_filter";
$route["reports/(:num)/(:any)"]              ="Report_controller/report_details";
$route["reports/amp/(:num)"]              ="Report_controller/report_details_amp";
$route["country-list"]                ="Report_controller/country_list";
$route["report-list"]                 ="Report_controller/report_list";
$route["reports/region/(:any)"]       ="Report_controller/report_list";
$route["reports/region/(:any)/page/(:num)"]  ="Report_controller/report_list";
$route["reports-filter/page/(:num)"]         ="Report_controller/reports_filter";



//******************************    CMS          ****************************************//

$route["contact-us"]          ="Cms_controller/contact_us";
$route["about-us"]            ="Cms_controller/about_us";
$route["terms-and-conditions"]="Cms_controller/terms_conditions";
$route["privacy-policy"]      ="Cms_controller/privacy_policy";
$route["return-policy"]       ="Cms_controller/return_policy";
$route["payment-mode"]        ="Cms_controller/payment_mode";
$route["careers"]             ="Cms_controller/careers";
$route["careers/apply"]       ="Cms_controller/careers_apply";
$route["custom-research"]       ="Cms_controller/custom_research";
$route["syndicated-research"]       ="Cms_controller/syndicated_research";
$route["procurement-solutions"]       ="Cms_controller/procurement_solutions";
$route["consulting-services"]       ="Cms_controller/consulting_services";
$route["research-methodology"]       ="Cms_controller/research_methodology";



//******************************    COOKIE         ****************************************//

$route["cookie-update"]          ="Cms_controller/cookie_update";


//******************************    CART          ****************************************//

// $route["my-cart"]          ="Cart_controller/cart";
// $route["cart-item/(:num)/(:any)"]="Cart_controller/cart_item";
// $route["cart-item/(:num)/(:any)/yes-add"]="Cart_controller/cart_item";
// $route["cart-item-remove/(:num)"]="Cart_controller/cart_item_remove";
// $route["cart-checkout/(:num)"]="Cart_controller/cart_to_checkout";
// $route["update-cart"]="Cart_controller/cart_update";




//******************************    CATEGORIES         ****************************************//

$route["industry-sectors"]                          ="Category_controller/categories";
$route["category/(:any)"]                     ="Category_controller/category_details";
$route["category/(:any)/page/(:any)"]         ="Category_controller/category_details";
$route["category?(:any)"]                     ="Category_controller/category_filter";
$route["category/(:num)/region/(:any)"]              ="Category_controller/category_report_list";
$route["category/(:num)/region/(:any)/page/(:num)"]  ="Category_controller/category_report_list";
$route["category/page/(:num)"]                ="Category_controller/category_filter";
$route["category-filter?(:any)"]                ="Category_controller/category_filter";



//******************************    PUBLISHERS        ****************************************//

// $route["publisher/(:any)"]                          ="Index_controller/publisher_reports";
// $route["publisher/(:any)/page/(:num)"]              ="Index_controller/publisher_reports";
// $route["publisher/(:any)/reports"]                     ="Index_controller/publisher_reports_filter";
// $route["publisher/(:any)/reports/page/(:num)"]         ="Index_controller/publisher_reports_filter";
// $route["publisher/(:any)/region/(:any)"]               ="Index_controller/publisher_reports_list";
// $route["publisher/(:any)/region/(:any)/page/(:num)"]   ="Index_controller/publisher_reports_list";


//******************************   FORMS         ****************************************//

    //*************************    REGISTER FOR MAILING LIST   **********************//  

    $route['register-mail']          = "Form_controller/register_mail";
    $route['report/(:any)']          = "Report_controller/process_form_request";
    $route['report-form-process']    = "Form_controller/report_form_process";    
   
       
   //*************************    CONTACT US         **********************//  

    $route['contact-form-process']    = "Form_controller/contact_form_process";
    $route["contact-form/thanks"]     ="Form_controller/thanks";
    $route["report-request/thanks"]     ="Form_controller/thanks";

    //*************************    CUSTOM RESEARCH         **********************//  

    $route['custom-research-process']    = "Form_controller/custom_research_process";
    $route["custom-research/thanks"]     ="Form_controller/thanks";
                  
    //************************ REQUEST CUSTOMIZATION  **********************// 

    // $route['request-customize/(:any)']  = "Report_controller/request_customize";
    // $route['request-customize-process'] = "Form_controller/request_customize_process";
    $route["request-customization/thanks"]  ="Form_controller/thanks";


    //************************ ENQUIRE BEFORE PURCHASE  **********************// 

    // $route['enquire-purchase/(:any)']  = "Report_controller/enquire_purchase";
    // $route['enquire-purchase-process'] = "Form_controller/enquire_purchase_process";
    $route["enquire-before-purchase/thanks"]  ="Form_controller/thanks";


    //************************ REQUEST DISCOUNT  **********************// 

    // $route['request-discount']  = "Form_controller/request_discount_email";

    // $route['request-discount/(:any)']  = "Report_controller/request_discount";
    // $route['request-discount-process'] = "Form_controller/request_discount_process";
    $route["request-discount/thanks"]  ="Form_controller/thanks";
    
    //*************************    CAREEERS       **************************//  
                                                                                      
    $route['career-form-process']     = "Form_controller/do_upload";
    $route['career-apply-process']    = "Form_controller/file_upload";

                                                                                      
    //*************************    REPORT SAMPLE          **********************//

    // $route["report-sample/(:any)"]  ="Report_controller/report_sample";
    // $route["report-sample-process"] ="Form_controller/report_sample_process";
    $route["request-sample/thanks"]  ="Form_controller/thanks";
                                                                                      

//******************************************************************************************//
    
    
    

//******************************    SEARCH         ****************************************//

$route["search-result"]                           ="Search_controller/search_result";
$route["search-form"]                             ="Search_controller/search_form";
$route["search"]                                  ="Search_controller/search_result";
$route["search-result/page/(:any)"]               ="Search_controller/search_result";

$route["search-form-process"]                      ="Form_controller/search_form_process";
$route["search-form/thanks"]                       ="Form_controller/thanks";

$route["ajax-search"]                              ="Search_controller/search_suggestions";
// $route["search-region/region/(:any)"]                     ="Search_controller/search_report_region";
// $route["search-region/region/(:any)/page/(:num)"]         ="Search_controller/search_report_region";
// $route["search-filter/page/(:num)"]                ="Search_controller/search_filter";
// $route["search-filter"]                     ="Search_controller/search_filter";
// $route["search-category/page/(:num)"]              ="Search_controller/search_category";
// $route["search-category?(:any)"]                   ="Search_controller/search_category";




//******************************    CHECKOUT        ****************************************//

$route["checkout/(:any)"]        ="Checkout_controller/checkout";
$route["checkout-process/(:num)"]="Checkout_controller/checkout_process";

$route['paypal/thanks']     = "Form_controller/thanks";
$route['bank-transfer/thanks']     = "Form_controller/thanks";
$route['payment-thanks']     = "Checkout_controller/paypal_thanks";
$route['cancel']     = "Form_controller/cancel";

$route["payment/eazypay-success"]= "Checkout_controller/razorpay_success";
$route["payment/eazypay-thanks"]= "Form_controller/thanks";
$route["payment/eazypay-cancel"]= "Checkout_controller/razorpay_cancel";
$route["eazypay/payment-failed"]= "Checkout_controller/payment_failed";
$route["link-error"]            = "Checkout_controller/link_error";
// $route["payment/bank-transfer"]= "Checkout_controller/bank_transfer";


// $route["validate-coupon"]="Checkout_controller/validate_coupon";

//******************************    GLOBAL CHECKOUT        ****************************************//

$route['global-checkout/(:any)'] = "Checkout_controller/global_checkout";
$route['global-checkout-process/(:any)'] = "Checkout_controller/checkout_process";



//******************************   PRESS RELEASE       ****************************************//

$route["press-release"] ="Press_controller/press_release";
$route["press/(:any)"]  ="Press_controller/press_details";

//******************************   BLOGS       ****************************************//

$route["blogs"] ="Press_controller/blogs";
$route["blog/(:any)"]  ="Press_controller/blog_details";

//******************************   SITEMAP       ****************************************//


$route["rss-feeds"]              ="Cms_controller/rss";

$route["sitemap.html"]           ="Cms_controller/sitemap_html";
$route["sitemap-web.xml"]        ="Cms_controller/sitemap_web";
$route["sitemap.xml"]            ="Cms_controller/sitemap";
$route["sitemap-(:any).xml"]     ="Cms_controller/sitemap_monthwise";



//******************************   CSV       ****************************************//

$route['sitemap-(:any)']="Cms_controller/sitemap_monthwise";




