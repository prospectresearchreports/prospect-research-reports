<?php
defined('BASEPATH') OR exit('No direct script access allowed');


$conf = array(
    "international_address" => "8 PILKINGTON CLOSE, STOKE GIFFORD BRISTOL, UNITED KINGDOM, BS34 8JU",
    "local_address" => "Building 1, Lane 1, Rajeev Gandhi Int. IT Park, PUNE, MH, India- 411-057",
    "contact_number" => "+44-77-6742-7537",
    "email_address" => "help@prospectresearchreports.com"
);

$config['conf'] =  $conf;
$config['company_name'] = "Prospect Research Reports";
$config['email_from'] = 'query@prospectresearchreports.com';
$config['email_to'] = array('opportunity@prospectresearchreports.com');
$config['email_cc'] = array('prospectresearchreports.a@gmail.com');


$config['contact_email_to'] = array('prospectresearchreports.a@gmail.com');

$config['sample_email_from'] = 'opportunity@prospectresearchreports.com';
$config['sample_email_to'] = array('opportunity@prospectresearchreports.com');
$config['sample_email_cc'] = array('prospectresearchreports.s@gmail.com');


$config['checkout_email_from'] = 'opportunity@prospectresearchreports.com';
$config['checkout_email_to'] = array('opportunity@prospectresearchreports.com');
$config['checkout_email_cc'] = array('prospectresearchreports.s@gmail.com');

$config['career_email_from'] = 'query@prospectresearchreports.com';
$config['career_email_to'] = array('hr@prospectresearchreports.com');

$config['visible_categories'] = array(2, 3, 5, 7, 12, 13);
$config['optional_fields'] = array('message');

?>