<?php

class Form_controller extends CI_Controller {

    function __construct()
    {
        parent::__construct();
        $this->load->model('Reports_model');
        $this->load->library("email");
        $this->load->helper(array('html','menu'));
        $this->load->model(array("Cms_model", "Reports_model"));
        ini_set("date_default_timezone_set", "Asia/Kolkata");
    }
     public function thanks() {
        $this->load->view('thanks');
    } 
    public function cancel() {        
        $this->load->view('cancel');
    }
    public function request_discount_email(){
        redirect("../contact-us");
        // $email=$this->input->post("email");
        // $report_title=$this->input->post("title");

        // $this->email->initialize($this->config->item('mail_config'));
        // $this->email->from('query@prospectresearchreports.com', 'Prospect Research Reports');
        // $this->email->to(array('siri@prospectresearchreports.com'));
        // $this->email->cc(array('prospectresearchreports.a@gmail.com'));
        // $this->email->subject('New Request to Avail 10% Discount');
        
        // $this->email->message('
        // Dear Admin,<br><br>       
        // You have a New Request to Avail 10% Discount..<br><br>
        // <font size=2><b>Email :- </b></font> ' . $email. '<br><br>
        // <font size=2><b>Report Title :- </b></font> ' . $report_title. '<br><br>
        // // <font size=2><b>IP Address:- </b></font>' . $ip . '<br /><br />
        // Thanks.<br><br>
        // ');

        // if($this->email->send()){
        //     echo "You have successfully applied to avail discount . One of our executives will get back to you soon.";
        // }
        
    }



    // public function register_mail(){
    //     $email=$this->input->post("email");

    //     $this->email->initialize($this->config->item('mail_config'));
    //     $this->email->from('query@prospectresearchreports.com', 'Prospect Research Reports');
    //     $this->email->to('sales@prospectresearchreports.com');

    //     $this->email->subject('New Mailing List Registration Request');
        
    //     $this->email->message('
    //     Dear Admin,<br><br>       
    //     You have a New Mailing List Registration Request..<br><br>
    //     <font size=2><b>Email :- </b></font> ' . $email. '<br><br>
    //     Thanks.<br><br>
    //     ');

    //     if($this->email->send()){
    //         echo "You have successfully added into our mailing List <br/><meta http-equiv='refresh' content='2,url=".base_url()."' />";
    //     }
        
    // }
    
    
    public function search_form_process()
    {
        if(!empty($_POST)){ 
            // echo "<pre>";print_r($_POST);exit;
            $data['name']=$contact_person=$this->input->post("name");
            $data['email']=$contact_email=$this->input->post("email");
            $data['phone']=$contact_phone=$this->input->post("phone");
            $data['company']=$contact_company=$this->input->post("company");
            $data['search'] = $rep_title=$this->input->post("rep_title");
            $data['contact_msg']=$contact_msg=$this->input->post("message");
            $data['job_role']=$contact_job_role=$this->input->post("job_role");
            $data['country']=$contact_country=$this->input->post("country"); 
             
            $invalid_message = validate_form_input($_POST);
            // echo $invalid_message;exit;
            if(!empty(@$invalid_message)){            
                $data['message']=@$invalid_message;
                $this->load->view('search-form',$data);
            }else{ 
                $data=getDetailsFromIP();

                $ip = $data['ipAddress'];
                $countryCode = $data['countryCode'];
                $countryName = $data['countryName'];
                $regionName = $data['regionName'];
                $cityName = $data['cityName'];

                $this->load->library("email");
                 
                $country= explode('-', str_ireplace('\"',"",$contact_country));
                
                $contact_form_type = array_search('Search Form',getFormTypes()) ? array_search('Search Form',getFormTypes()) :  0;
                            
                $this->email->initialize($this->config->item('mail_config'));
                $this->email->from($this->config->item('email_from'), $this->config->item('company_name'));
                $this->email->to($this->config->item('email_to'));
                $this->email->cc($this->config->item('email_cc'));
                $this->email->subject('New Search Query');
                $this->email->message('
                    Dear Admin,<br><br>       
                    You have a new Search Query..<br><br>
                    <font size=2><b>Report Title :- </b></font>' . $rep_title . '<br><br>
                    <font size=2><b>Contact Person :- </b></font>' . $contact_person . '<br><br>
                    <font size=2><b>Email :- </b></font> ' . $contact_email. '<br><br>
                    <font size=2><b>Country Name :- </b></font> ' . $country[1] . '<br><br>
                    <font size=2><b>Contact Number :- </b></font> ' .$contact_phone . '<br><br>
                    <font size=2><b>Contact Company :- </b></font> ' . $contact_company . '<br><br>
                    <font size=2><b>Contact Job Role :- </b></font> ' . $contact_job_role . '<br><br>
                    <font size=2><b>Message :- </b></font>' . $contact_msg . '<br><br>
                    <hr>
                    <font size=2><b>Country:- </b></font> ' . $countryName . '<br /><br />
                    <font size=2><b>State:- </b></font> ' . $regionName . '<br /><br />
                    <font size=2><b>City:- </b></font>' . $cityName . '<br /><br />
                    Thanks.<br><br>
                    ');

                if($this->email->send())
                {
                    $data= array("contact_rep_title"=>$rep_title,
                            "contact_person"=>$contact_person,
                            "contact_email"=>$contact_email,
                            // "contact_phone"=>$country[0].'-'.$contact_phone,
                            "contact_phone"=>$contact_phone,
                            "contact_msg"=>$contact_msg,
                            "contact_job_role"=>$contact_job_role,
                            'contact_country' => $country[1],
                            "contact_company" => $contact_company,
                            'contact_exact_region' => $regionName,
                            'contact_real_country' => $countryName,
                            'contact_city' => $cityName,
                            'contact_ip' => $ip,
                            "contact_datetime" => date("Y-m-d H:i:s"),
                            'contact_form_type' => @$contact_form_type,
                            'query_source' => 'web');
                    $this->Cms_model->insert($data);
                    $query=$this->db->get("mr_form_contact");
                    redirect("search-form/thanks");
                }
            }       
        }else{
            redirect("latest-reports");            
        }                        
    }

    public function contact_form_process()
    {
        if(!empty($_POST)){
            // echo "<pre>";print_r($_POST);exit;
            $data['name']=$contact_person=$this->input->post("name");
            $data['email']=$contact_email=$this->input->post("email");
            $data['company']=$contact_company=$this->input->post("company");
            $data['phone']=$contact_phone=$this->input->post("phone");
            $data['contact_msg']=$contact_msg=$this->input->post("message");
            $data['job_role']=$contact_job_role=$this->input->post("job_role");
            $data['country']=$contact_country=$this->input->post("country");
            $data['rep_title']=$contact_rep_title=$this->input->post("rep_title") ? $this->input->post("rep_title") : "";
            $data['rd_type']=$rd_type=$this->input->post("rd_type") ? $this->input->post("rd_type") : "";
                        
            $invalid_message=validate_form_input($_POST);
            // echo $invalid_message;exit;
            if(!empty(@$invalid_message)){
                $data['message']=@$invalid_message;
                $view = !empty($contact_rep_title) ? 'report-details' : 'contact-us';
                if($view === 'report-details'){
                    $data['rep_id'] = $rep_id =  $this->input->post("rep_id");
                    $data['report_data'] = $report_data = $this->Reports_model->get_report_data($rep_id);
                    $data = $this->Reports_model->get_report_details($rep_id, $data);
                }
                $this->load->view($view, $data);
            }else{
                
                $data=getDetailsFromIP();

                $ip = $data['ipAddress'];
                $countryCode = $data['countryCode'];
                $countryName = $data['countryName'];
                $regionName = $data['regionName'];
                $cityName = $data['cityName'];

                $country= explode('-', str_ireplace('\"',"",$contact_country));

                $mail_subject = '';
                $contact_form_type = 0;
                if($this->input->post("rep_title")){
                    $mail_subject = 'New Report Details Contact Query';
                    $contact_form_type = array_search('Report Details',getFormTypes());
                }else{
                    $mail_subject = 'New Contact Query';
                    $contact_form_type = array_search('Contact Form',getFormTypes());
                }
                $mail_message='';
                $mail_message.="Dear Admin,<br><br>       
                    You have a ".$mail_subject."..<br><br>";
                $mail_message.=($this->input->post("rep_title")) ? '<font size=2><b>Report Title :- </b></font>' . $contact_rep_title . '<br><br>' : '';
                    
                $mail_message.='<font size=2><b>Contact Person :- </b></font>' . $contact_person . '<br><br>
                    <font size=2><b>Email :- </b></font> ' . $contact_email. '<br><br>
                    <font size=2><b>Country Name :- </b></font> ' . $country[1] . '<br><br>
                    <font size=2><b>Contact Number :- </b></font> ' . $contact_phone . '<br><br>
                    <font size=2><b>Contact Company :- </b></font> ' . $contact_company . '<br><br>
                    <font size=2><b>Contact Job Role :- </b></font> ' . $contact_job_role . '<br><br>
                    <font size=2><b>Message :- </b></font>' . $contact_msg . '<br><br>
                    <hr>
                    <font size=2><b>Country:- </b></font> ' . $countryName . '<br /><br />
                    <font size=2><b>State:- </b></font> ' . $regionName . '<br /><br />
                    <font size=2><b>City:- </b></font>' . $cityName . '<br /><br />
                    Thanks.<br><br>';
                            
                $this->email->initialize($this->config->item('mail_config'));
                $this->email->from($this->config->item('email_from'), $this->config->item('company_name'));
                if($this->input->post("rep_title")){
                    $this->email->to($this->config->item('sample_email_to'));
                    $this->email->cc($this->config->item('sample_email_cc')); 
                }else{
                    $this->email->to($this->config->item('contact_email_to'));
                }  
                $this->email->subject($mail_subject);
                $this->email->message($mail_message);

                if($this->email->send())
                {
                    $db_data= array("contact_rep_title"=>$contact_rep_title,
                        "contact_person"=>$contact_person,
                        "contact_email"=>$contact_email,
                        // "contact_phone"=>$country[0].'-'.$contact_phone,
                        "contact_phone"=>$contact_phone,
                        "contact_msg"=>$contact_msg,
                        "contact_job_role"=>$contact_job_role,
                        'contact_country' => $country[1],
                        "contact_company" => $contact_company,
                        'contact_exact_region' => $regionName,
                        'contact_real_country' => $countryName,
                        'contact_city' => $cityName,
                        'contact_ip' => $ip,
                        "contact_datetime" => date("Y-m-d H:i:s"),
                        'contact_form_type' => @$contact_form_type,
                        'query_source' => 'web');
                    $this->Cms_model->insert($db_data);

                    if($this->input->post("rep_title")){
                        redirect("report-request/thanks");
                    }else{
                        redirect("contact-form/thanks");
                    }
                }
            }
        }else{
            redirect("contact-us");            
        }
                        
    }
    public function custom_research_process()
    { 
        if(!empty($_POST)){     
            $data['name']=$contact_person=$this->input->post("name");
            $data['email']=$contact_email=$this->input->post("email");
            $data['company']=$contact_company=$this->input->post("company");
            $data['phone']=$contact_phone=$this->input->post("phone");
            $data['contact_msg']=$contact_msg=$this->input->post("message");
            $data['job_role']=$contact_job_role=$this->input->post("job_role");
            $data['country']=$contact_country=$this->input->post("country");
            $data['budget']=$contact_budget=$this->input->post("budget");
            
            $invalid_message=validate_form_input($_POST);
            if(!empty($invalid_message)){            
                $data['message']=@$invalid_message;
                $this->load->view('custom-research',$data);
            }else{
                $data=getDetailsFromIP();

                $ip = $data['ipAddress'];
                $countryCode = $data['countryCode'];
                $countryName = $data['countryName'];
                $regionName = $data['regionName'];
                $cityName = $data['cityName'];
                        
                $country= explode('-', str_ireplace('\"',"",$contact_country));
                $contact_form_type = array_search('Custom Research',getFormTypes()) ??  0;
                            
                $this->email->initialize($this->config->item('mail_config'));
                $this->email->from($this->config->item('email_from'), $this->config->item('company_name'));
                $this->email->to($this->config->item('email_to'));
                $this->email->cc($this->config->item('email_cc'));       

                $this->email->subject('New Custom Research Request');
                
                $this->email->message('
                Dear Admin,<br><br>       
                You have a New Custom Research Request..<br><br>
                <font size=2><b>Contact Person :- </b></font>' . $contact_person . '<br><br>
                <font size=2><b>Email :- </b></font> ' . $contact_email. '<br><br>
                <font size=2><b>Country Name :- </b></font> ' . $country[1] . '<br><br>
                <font size=2><b>Contact Number :- </b></font> ' . $contact_phone . '<br><br>
                <font size=2><b>Contact Company :- </b></font> ' . $contact_company . '<br><br>
                <font size=2><b>Contact Job Role :- </b></font> ' . $contact_job_role . '<br><br>
                <font size=2><b>Message :- </b></font>' . $contact_msg . '<br><br>
                <font size=2><b>Budget :- </b></font>' . "$".$contact_budget . '<br><br>
                <hr>
                <font size=2><b>Country:- </b></font> ' . $countryName . '<br /><br />
                <font size=2><b>State:- </b></font> ' . $regionName . '<br /><br />
                <font size=2><b>City:- </b></font>' . $cityName . '<br /><br />
                Thanks.<br><br>
                ');

                if($this->email->send())
                {
                    $db_data= array("contact_person"=>$contact_person,
                        "contact_email"=>$contact_email,
                        // "contact_phone"=>$country[0].'-'.$contact_phone,
                        "contact_phone"=>$contact_phone,
                        "contact_msg"=>$contact_msg,
                        "contact_job_role"=>$contact_job_role,
                        'contact_country' => $country[1],
                        "contact_company" => $contact_company,
                        'contact_exact_region' => $regionName,
                        'contact_real_country' => $countryName,
                        'contact_city' => $cityName,
                        'contact_ip' => $ip,
                        "contact_datetime" => date("Y-m-d H:i:s"),
                        'contact_form_type' => @$contact_form_type,
                        'query_source' => 'web');
                    $this->Cms_model->insert($db_data);
                    redirect("custom-research/thanks");
                }
            }
        }else{
            redirect("custom-research");            
        }                        
    }
    public function report_form_process(){
        if(!empty($_POST)){  
                 
            $data['name']=$contact_person=$this->input->post("name");
            $data['email']=$contact_email=$this->input->post("email");
            $data['company']=$contact_company=$this->input->post("company");
            $data['phone']=$contact_phone=$this->input->post("phone");
            $data['contact_msg']=$contact_msg=$this->input->post("message");
            $data['job_role']=$contact_job_role=$this->input->post("job_role");
            $data['country']=$contact_country=$this->input->post("country");
            $data['rep_title']=$contact_report_title=$this->input->post("rep_title");
            $data['report_id']=$report_id=$this->input->post("report_id");
            $data['form_type']=$form_type=$this->input->post("form_type");
            $data['rd_url']=$rd_url=$this->input->post("rd_url");
            
            $invalid_message=validate_form_input($_POST);
            if(!empty($invalid_message)){            
                $data['message']=@$invalid_message;
                $data['report_data'] = $this->Reports_model->get_report_form_data($report_id);
                $this->load->view('report-sample',$data);
            }else{

                $data=getDetailsFromIP();

                $ip = $data['ipAddress'];
                $countryCode = $data['countryCode'];
                $countryName = $data['countryName'];
                $regionName = $data['regionName'];
                $cityName = $data['cityName'];

                $contact_form_type = array_search($form_type,getFormTypes()) ??  0;
                
                if($contact_form_type === 0){
                    $data['message']= "Invalid sample request. Please submit the request from the URL : <a href='".@$rd_url."' target='_blank'>".@$contact_report_title."</a>";
                    $data['report_data'] = $this->Reports_model->get_report_form_data($report_id);
                    $this->load->view('report-sample',$data);
                }else{            
                    $country= explode('-', str_ireplace('\"',"",$contact_country));        
                                
                    $this->email->initialize($this->config->item('mail_config'));
                    $this->email->from($this->config->item('sample_email_from'), $this->config->item('company_name'));
                    $this->email->to($this->config->item('sample_email_to'));
                    // $this->email->cc($this->config->item('email_cc'));
                    
                    $this->email->subject('New '.@$form_type.'  Query');
                    $this->email->message('
                    Dear Admin,<br><br>       
                    You have a new '.@$form_type.' Query..<br><br>
                    <font size=2><b>Report Title:- </b></font>' . $contact_report_title . '<br><br>
                    <font size=2><b>Contact Person :- </b></font>' . $contact_person . '<br><br>
                    <font size=2><b>Email :- </b></font> ' . $contact_email. '<br><br>
                    <font size=2><b>Country Name :- </b></font> ' . $country[1] . '<br><br>
                    <font size=2><b>Contact Number :- </b></font> ' . $contact_phone . '<br><br>
                    <font size=2><b>Contact Company :- </b></font> ' . $contact_company . '<br><br>
                    <font size=2><b>Contact Job Role :- </b></font> ' . $contact_job_role . '<br><br>
                    <font size=2><b>Message :- </b></font>' . $contact_msg . '<br><br>
                    <hr>
                    <font size=2><b>Country:- </b></font> ' . $countryName . '<br /><br />
                    <font size=2><b>State:- </b></font> ' . $regionName . '<br /><br />
                    <font size=2><b>City:- </b></font>' . $cityName . '<br /><br />
                    Thanks.<br><br>
                    ');
        
                    if($this->email->send())
                    {
                        $db_data= array("contact_rep_title"=>$contact_report_title,
                            "contact_person"=>$contact_person,
                            "contact_email"=>$contact_email,
                            // "contact_phone"=>$country[0].'-'.$contact_phone,
                            "contact_phone"=>$contact_phone,
                            "contact_msg"=>$contact_msg,
                            "contact_job_role"=>$contact_job_role,
                            'contact_country' => $country[1],
                            "contact_company" => $contact_company,
                            'contact_exact_region' => $regionName,
                            'contact_real_country' => $countryName,
                            'contact_city' => $cityName,
                            'contact_ip' => $ip,
                            "contact_datetime" => date("Y-m-d H:i:s"),
                            'contact_form_type' => @$contact_form_type,
                            'query_source' => 'web');
                        $this->Cms_model->insert($db_data);
                        // $this->db->get("mr_form_contact");
                        $redirect=str_ireplace(' ','-',strtolower(@$form_type));
                        redirect(@$redirect.'/thanks');
                    }
                }
            }
        }else{
            redirect("latest-reports");            
        }                        
    }
    
    public function do_upload()
    {
        if(!empty($_POST)){
            $config['upload_path']          = './Career-CV/';
            $config['allowed_types']        = 'doc|docx|xls|pdf';
            $config['max_size']             = 5000;

            $this->load->library('upload', $config);

            if (!$this->upload->do_upload('resume')){
                $error = array('error' => $this->upload->display_errors());
                $this->load->view('careers', $error);
            }else{
                $file=$this->upload->data();
                 $values= array(
                    "candidate_name"=>$this->input->post("name"),
                    "candidate_email"=>$this->input->post("email"),
                    "candidate_skill_info"=>$this->input->post("message"),
                    "candidate_resume"=>$file["file_name"],
                    "position_applied"=>"NULL",
                    "criteria_meet"=>"N",
                );
                 
                $this->email->initialize($this->config->item('mail_config'));
                $this->email->from($this->config->item('career_email_from'), $this->config->item('company_name'));
                $this->email->to($this->config->item('career_email_to'));
                
                $this->email->subject('New Job Application');
                $this->email->attach(base_url().'/Career-CV/'.$file["file_name"]);
                $this->email->message('
                Dear HR Team,<br><br>       
                You have a new Job Application.<br><br>
                <font size=2><b>Applicant Name :- </b></font>' . $this->input->post("name") . '<br><br>
                <font size=2><b>Email :- </b></font> ' . $this->input->post("email"). '<br><br>
                <font size=2><b>Skills :- </b></font> ' . $this->input->post("message") . '<br><br>
                Thanks.<br><br>
                ');
               if($this->email->send()){
                    if($this->Cms_model->insert_values($values)){
                        $data["successText"] ="<b style='color:green;font-family:calibri;font-size:1.2em'>Congrats ! We have received your application. We will get back to you soon !</b>";
                        $this->load->view('careers', $data);
                    }
                }
            }
        }else{
            redirect("../careers");            
        }
        
    }
    public function file_upload()
    {
        if(!empty($_POST)){
            $config['upload_path']          = './Career-CV/';
            $config['allowed_types']        = 'doc|docx|xls|pdf';
            $config['max_size']             = 5000;

            $data["role"]=$this->input->post("role");

            $this->load->library('upload', $config);

            if (!$this->upload->do_upload('resume')){
                $error = array('error' => $this->upload->display_errors());
                $this->load->view('careers-apply', $error);
            }else{
                $file=$this->upload->data();
                 $values= array(
                    "candidate_name"=>$this->input->post("name"),
                    "candidate_email"=>$this->input->post("email"),
                    "candidate_skill_info"=>$this->input->post("message"),
                    "candidate_resume"=>$file["file_name"],
                    "position_applied"=>$this->input->post("role"),
                    "criteria_meet"=>"Y",
                );

                $this->email->initialize($this->config->item('mail_config'));
                $this->email->from($this->config->item('career_email_from'), $this->config->item('company_name'));
                $this->email->to($this->config->item('career_email_to'));
                
                $this->email->subject('New Job Application');
                $this->email->attach(base_url().'/Career-CV/'.$file["file_name"]);
                $this->email->message('
                Dear HR Team,<br><br>       
                You have a new Job Application.<br><br>
                <font size=2><b>Position Applied For :- </b></font>' . $this->input->post("role") . '<br><br>
                <font size=2><b>Applicant Name :- </b></font>' . $this->input->post("name") . '<br><br>
                <font size=2><b>Email :- </b></font> ' . $this->input->post("email"). '<br><br>
                <font size=2><b>Skills :- </b></font> ' . $this->input->post("message") . '<br><br>
                Thanks.<br><br>
                ');
                
                if($this->email->send()){
                    if($this->Cms_model->insert_values($values)){
                        $data["successText"] ="<b style='color:green;font-family:calibri;font-size:1.2em'>Congrats ! We have received your application. We will get back to you soon !</b>";
                        // $data["role"]=$this->input->post("role");
                        $this->load->view('careers', $data);
                    }
                }
            }
        }else{
            redirect("../careers");            
        }
    }
}
?>