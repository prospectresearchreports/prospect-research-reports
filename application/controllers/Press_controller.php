<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Press_controller extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model("Reports_model");
        $this->load->database("");
        $this->load->helper(array('text'));
    }

    public function press_release() {

        $data = array();

        $where = array();
        $where_between = array();
        $orderarray=null;

        $data['year']= $year = $this->input->get('year') ? $this->input->get('year') : '';
        $data['date_order']= $date = $this->input->get('date') ? $this->input->get('date') : '';
        $data['category']= $category = $this->input->get('category') ? $this->input->get('category') : '';
        
        if(!empty($category)){
            $where['cat_id'] = $category;
        }
        if(!empty($year)){
            $where_between[] ="published_on BETWEEN '".$year."-01-01' AND '".$year."-12-31'";
        }
        if(!empty($date)){
            $orderarray ="published_on ".$date;
        }else{
            $orderarray="id DESC";
        }
        $config['base_url'] = base_url('press-release/page/');  
        
        $base_url = base_url('press-release/page/');
        $countrep = $this->Reports_model->get_press_releases($where, $where_between, $orderarray,  NULL, NULL);
        $rows= $countrep ? $countrep->num_rows() : 0; 
        $uri_segment=3; 
        $first_url = '';
        if(!empty($_GET)){
            $first_url .='1?year='.@$year.'&date='.@$date;
        }
        $config=array();
        $config=initializePagination($rows,$base_url,$uri_segment); // Initialize Pagination
        $config['per_page'] = 10;
        $this->pagination->initialize($config);

        $data['total_pages'] = ceil($config["total_rows"] / $config["per_page"]); //$config["num_links"];

        $data['page'] = ($this->uri->segment(3)) ? $this->uri->segment(3) : 1;
        $data['uri_suffix'] = $config['suffix'];
        $data['press_list'] = $this->Reports_model->get_press_releases($where, $where_between, $orderarray, $config["per_page"], $data['page'] - 1);
        // $data['count'] = 0;
        if(!empty($_GET) || $config['total_rows'] > 0){
            $data['count'] = $config['total_rows'];
        }
        $data['current_url']=str_ireplace('/index.php','',current_url());

        $data['pagination'] = $this->pagination->create_links();
        
        $this->load->view('press-release', $data);
    }

    public function press_details() {
        
        $data = array();

        $specials = array('\r', '\n', '%5Cr', '&Atilde;', '%5Cn');
        $data["press_url"] = str_replace($specials, '', $this->uri->segment(2));
        $data["records"] = $this->Reports_model->press_details($data["press_url"]);
        if ($data['records'] != false):
            $this->load->view('press-details', $data);
        else:
            $this->load->view('404', $data);
        endif;
    }

    public function blogs() {
        $this->load->library("pagination");
        $data = array();

        $where = NULL;
        $orderarray = 'blog_date DESC';
        $config['base_url'] = base_url('blog/page/');

        $countrep = $this->Reports_model->get_blogs($where, NULL, NULL, NULL);
        $config['total_rows'] = $countrep->num_rows();
        $config['per_page'] = 15;
        $config["uri_segment"] = 3;
        $config['use_page_numbers'] = TRUE;
        $config['page_query_string'] = FALSE;
        $config["num_links"] = 2; //ceil($config["total_rows"] / $config["per_page"]);
        $config['full_tag_open'] = '<ul class="pagination">';
        $config['full_tag_close'] = '</ul>';
        $config['first_url'] = '1';
        $config['first_link'] = 'First';
        $config['last_link'] = 'Last';
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['prev_link'] = '&laquo;';
        $config['prev_tag_open'] = '<li rel=prev >';
        $config['prev_tag_close'] = '</li>';
        $config['next_link'] = '&raquo;';
        $config['next_tag_open'] = '<li rel="next">';
        $config['next_tag_close'] = '</li>';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="active"><a href="javascript:void()">';
        $config['cur_tag_close'] = '</a></li>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $config['suffix'] = ($_GET) ? '?' . http_build_query($_GET, '', "&") : NULL;
        $config['attributes']['rel'] = true;
        $this->pagination->initialize($config);

        $data['total_pages'] = ceil($config["total_rows"] / $config["per_page"]); //$config["num_links"];

        $data['page'] = ($this->uri->segment(3)) ? $this->uri->segment(3) : 1;
        $data['uri_suffix'] = $config['suffix'];
        $data['press_list'] = $this->Reports_model->get_blogs($where, $orderarray, $config["per_page"], $data['page'] - 1);
        $data['total_reports'] = $config['total_rows'];

        $data['pagination'] = $this->pagination->create_links();

        
        $this->load->view('blogs', $data);
    }

    public function blog_details() {
        
        $data = array();

        $specials = array('\r', '\n', '%5Cr', '&Atilde;', '%5Cn');
        $data["Pressurl"] = str_replace($specials, '', $this->uri->segment(2));
        $data["records"] = $this->Reports_model->blog_details($data["Pressurl"]);
        if ($data['records'] != false):
            $this->load->view('blog-details', $data);
        else:
            $this->load->view('404', $data);
        endif;
    }
}
