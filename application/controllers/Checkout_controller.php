<?php

class checkout_controller extends CI_Controller
{
    public function __construct() {
        parent::__construct();
        $this->load->library(array("email"));
        $this->load->model(array("Reports_model",'Cms_model'));
        $this->load->helper(array("url","cookie"));
    }


    public function global_checkout(){
        
        $get_id=$this->uri->segment(2);

        $decrypted_key=encrypt_decrypt('decrypt',$get_id);
        if(!empty($decrypted_key)){
            $split_key=explode('-', $decrypted_key);
            // $name=$split_key[0];
            $id=@$split_key[1];
            $data['checkout_id'] = $id;
            $data['report_data']=$this->Cms_model->get_checkout_details($id);
            if($data['report_data']){
                $this->load->view('global-checkout',$data);
            }else{
                $this->load->view('link-error');
            }
        }else{
            $this->load->view('404');
        }
    }
    public function validate_coupon(){
        $code=$this->input->post("code");
        $validate=$this->Reports_model->get_coupon($code);
        if($validate){
            echo $validate->discount;
        }else{
            echo 'Error';
        }
    }

    
    public function paypal_thanks(){
        $this->load->view("paypal-thanks");
    }

    public function payment_failed(){
        $this->load->view("payment-failed");
    }

    public function link_error(){
        $this->load->view("link-error");
    }
    public function bank_transfer(){
        $invalid = false;
        if(isset($_COOKIE['sid']) && isset($_COOKIE['order_id'])){
            if(!empty($_COOKIE['sid']) && !empty($_COOKIE['order_id'])){
                $sid=$_COOKIE["sid"];
                $order_id=$_COOKIE["order_id"];
        
                $data['order_data']=$this->Reports_model->get_order_data($sid,$order_id);
        
                $data['publishers']=$this->Reports_model->get_publishers();
                $this->load->view("bank-transfer",$data);
            }else{
                $invalid = true;
            }
        }else{
            $invalid = true;
        }
        if($invalid){
            redirect('../404');
        }
    }
    public function checkout() {
        
        // if ($this->input->post("discount_code") != ""){
        //     $rep_title = $this->input->post("rep_title");
        //     $rep_id = $this->input->post("rep_id");
        //     $this->session->set_userdata('rep_id', $rep_id);
        //     $price = $this->input->post("price");
        //     $lice = $this->input->post("lice");
        //     $currency = $this->input->post("currency");

        //     //$discount_code = $this->input->post("discount_code");

        //     $query_1 = $data['discount']->num_rows();
        //     $data['rep_id'] = $this->uri->segment(2);
        //     $data['rep_data'] = $this->Reports_model->get_report_checkout($data['rep_id']);

        //     $data['report_title'] = $rep_title;
        //     $data['rep_id'] = $rep_id;

        //     $data['lice'] = $lice;
            
        // }else{

            $data['rep_id'] = $this->uri->segment(2);
            $data['report_data'] = $this->Reports_model->get_report_checkout($data['rep_id']);

            @$optionsRadios = $this->input->post("report_price");
            @$ch = explode("-", $optionsRadios);
            $data['report_license'] = @$ch[0];
            $data['report_price'] = @$ch[1];
        // }
        if(@$data["report_data"] && $data['report_price']){
            $this->load->view('checkout', $data);
        }else{
            $this->load->view("404");
        }
    }

    public function checkout_process() {

        if(!empty($_POST)){
             
            // echo "<pre>";print_r($_POST);exit;
            $data['name']=$name=$this->input->post("name");
            $data['email']=$email=$this->input->post("email");
            $data['phone']=$phone=$this->input->post("phone");
            $data['company']=$company_name=$this->input->post("company");
            $data['job_role']=$contact_job_role=$this->input->post("job_role");
            $data['country']=$contact_country=$this->input->post("country"); 
            $data['zip']=$zipcode = $this->input->post("zip");
            $data['address']=$address = $this->input->post("address");
            $data['report_price']=$price = $this->input->post("rep_price");
            $data['title']=$rep_title = $this->input->post("rep_title");
            $data['report_license']=$license = $this->input->post("rep_license");
            $data['currency']=$currency = $this->input->post("currency");
            $data['mode'] =$mode = $this->input->post("radio-inline");
            // echo "<pre>";print_r($_POST);exit;
                          
            $is_global = ($this->input->post("is_global") && $_POST['is_global'] == '1') ? true : false;
            $validate_phone = $is_global ? false : true;
            $invalid_message = validate_form_input($_POST, $validate_phone);
            // echo $invalid_message;exit;
            if(!empty(@$invalid_message)){            
                $data['message']=@$invalid_message;  
                $data['rep_id'] =$rep_id = $this->input->post("rep_id");  
                if($is_global){
                    $data['report_data']=$this->Cms_model->get_checkout_details($rep_id);
                }else{
                    $data['report_data'] = $this->Reports_model->get_report_checkout($rep_id);
                }
                // echo "<pre>";print_r($data['report_data']);exit;
                $this->load->view('checkout',$data);
            }else{

                $data=getDetailsFromIP();

                $ip = $data['ipAddress'];
                $countryCode = $data['countryCode'];
                $countryName = $data['countryName'];
                $regionName = $data['regionName'];
                $cityName = $data['cityName'];

                //$rep_id = $this->uri->segment(2);
                $payment_mode = ($mode == "Paypal") ? 1 : 3;
                $payment_text = $payment_mode == '1' ? 'PayPal' : 'Bank Transfer';
                $country_name = '';
                if(!$is_global){         
                    $country= explode('-', str_ireplace('\"',"",$contact_country));            
                    // $country_code =str_ireplace(array('(',')'),'',$country[0]);
                    // $phone = $country_code."-".$phone;
                    $country_name = $country[1];
                }else{
                    $country_name = $contact_country;
                }                
                
                $contact_form_type = array_search('Checkout Form',getFormTypes()) ? array_search('Checkout Form',getFormTypes()) :  0;
                
                $db_data = array(
                    'contact_person' => $name,
                    'contact_email' => $email,
                    'contact_company' => $company_name,
                    'contact_phone' => $phone,
                    'contact_country' => $country_name,
                    "contact_job_role"=>$contact_job_role,
                    'contact_rep_title' => $rep_title,
                    'contact_datetime' => date("Y-m-d H:i:s"),
                    'contact_real_country' => $countryName,
                    'contact_exact_region' => $regionName,
                    'contact_city' => $cityName,
                    'contact_ip' => $ip,
                    'report_price' => $price,
                    'contact_address' => $address,
                    'contact_zip' => $zipcode,
                    'report_payment_mode' => $payment_mode,
                    'contact_form_type' => @$contact_form_type,
                    'query_source' => 'web'
                );

                $this->db->insert('mr_form_contact', $db_data);


                $this->email->initialize($this->config->item('mail_config'));

                $this->email->from($this->config->item('checkout_email_from'), $this->config->item('company_name'));
                $this->email->to($this->config->item('checkout_email_to'));
                $this->email->cc($this->config->item('checkout_email_cc'));
                $this->email->subject('New Checkout Attempt Query - '.$payment_text);
                $this->email->message('
                    Dear Admin,<br><br> 
                    You have a new checkout Query..<br><br>
                    <font size=2><b>Report Title :- </b></font>' . $rep_title . '<br><br>
                    <font size=2><b>Contact Person :- </b></font>' . $name . '<br><br>
                    <font size=2><b>Email :- </b></font> ' . $email . '<br><br>
                    <font size=2><b>Company Name :- </b></font> ' . $company_name . '<br><br>
                    <font size=2><b>Job Role :- </b></font> ' . $contact_job_role . '<br><br>
                    <font size=2><b>Phone :- </b></font> ' . @$phone . '<br><br>
                    <font size=2><b>Country Name :- </b></font> ' . $country_name. '<br><br>
                    <font size=2><b>Zip Code :- </b></font> ' . $zipcode . '<br><br>
                    <font size=2><b>Address :- </b></font> ' . $address . '<br><br>
                    <font size=2><b>Price :- </b></font>' . $currency . ' ' . @$price . '<br><br>
                    <font size=2><b>License Type :- </b></font>' . $license . '<br><br>
                    <font size=2><b>Payment Mode :- </b></font>' . $payment_text . '<br><br>
                    <hr>
                    <font size=2><b>Country:- </b></font> ' . $countryName . '<br /><br />
                    <font size=2><b>State:- </b></font> ' . $regionName . '<br /><br />
                    <font size=2><b>City:- </b></font>' . $cityName . '<br /><br />
                    Thanks.<br><br>'
                );
                // $this->email->send();
                // $this->email->clear();

                //echo $currency;
                if ($mode == "Paypal") {
                    redirect("bank-transfer/thanks");

                    // $entry = date('Y-m-d');
                    $code = "PRR-INV-" . rand(5, 5000);
                    //$this->Checkout_model->add_order_data($code,$rep_id,$entry,$name,$companyname,$email,$phone,$lice,$price);O
                    //$paypal_url = 'https://www.sandbox.paypal.com/cgi-bin/webscr'; 
                    $paypal_url = 'https://www.paypal.com/cgi-bin/webscr';
                    //Test PayPal API URL//Test PayPal API URL
                    $business_id = 'Prospectresearchreports@gmail.com';
                    ?>
                    <!-- <form action="<?=$paypal_url ?>" method="post" name="checkout_form">

                        <input type="hidden" name="business" value="<?=$business_id;?>">

                        <input type="hidden" name="cmd" value="_xclick">

                        <input type="hidden" name="item_name" value="<?php echo $rep_title; ?>">
                        <input type="hidden" name="item_number" value="<?php echo $code; ?>">
                        <input type="hidden" name="amount" id="amount" value="<?php echo $price; ?>">
                        <input type="hidden" name="currency_code" value="USD">
                        <input type="hidden" name="charset" value="utf-8">

                        <input type='hidden' name='cancel_return' value='<?=base_url();?>cancel'>
                        <input type='hidden' name='return' value='<?=base_url();?>paypal/thanks'>
                        <center>
                            <input type="image" name="submit" border="0" src="<?=base_url();?>assets/images/loader.gif" alt="PayPal - The safer, easier way to pay online">
                            <img alt="PayPal Objects Pixel" border="0" width="1" height="1" src="https://www.paypalobjects.com/en_US/i/scr/pixel.gif" >
                        </center>
                    </form> -->
                    <script>
                        // setTimeout('document.checkout_form.submit()', 1000);
                    </script> 
                    <?php
                }else {
                    redirect("bank-transfer/thanks");
                }
            }
        }else{
            redirect("latest-reports");
        }
    }

    public function razorpay_success() {
        if(!empty($_POST)){
            $data=getDetailsFromIP();

            $ip = $data['ipAddress'];
            $countryCode = $data['countryCode'];
            $countryName = $data['countryName'];
            $regionName = $data['regionName'];
            $cityName = $data['cityName'];

            $rep_title = $this->input->post("title");
            $zipcode = $this->input->post("zip");
            $address = $this->input->post("address");
            $name = $this->input->post("name");
            $email = $this->input->post("email");
            $mode = 'Razorpay';
            $company_name = $this->input->post("company");
            $is_global = ($this->input->post("is_global") && $_POST['is_global'] == '1') ? true : false;
            $contact_country=$this->input->post("country");
            @$phone = $this->input->post("phone");  
            $country_name = '';
            if(!$is_global){         
                $country= explode('-', str_ireplace('\"',"",$contact_country));            
                // $country_code =str_ireplace(array('(',')'),'',$country[0]);
                // $phone = $country_code."-".$phone;
                $country_name = $country[1];
            }else{
                $country_name = $contact_country;
            }
            @$price = $this->input->post("price");
            $license = $this->input->post("license");
            $response = $this->input->post("response") ? $this->input->post("response") : array();
            $currency = $this->input->post("currency");
            $razorpay_payment_id = '';
            $razorpay_order_id  = '';
            $razorpay_signature = '';

            // echo json_encode($response);
            if(!empty($response)){
                $razorpay_payment_id = array_key_exists('razorpay_payment_id', @$response) ? $response['razorpay_payment_id'] : '';
                $razorpay_order_id = array_key_exists('razorpay_order_id', @$response) ? $response['razorpay_order_id'] : '';
                $razorpay_signature = array_key_exists('razorpay_signature', @$response) ? $response['razorpay_signature'] : '';
            }  
            $contact_form_type = array_search('Checkout Form',getFormTypes()) ? array_search('Checkout Form',getFormTypes()) :  0;
            
            $data = array(
                'contact_person' => $name,
                'contact_email' => $email,
                'contact_company' => $company_name,
                'contact_phone' => $phone,
                'contact_country' => $country_name,
                'contact_rep_title' => $rep_title,
                'contact_datetime' => date("Y-m-d H:i:s"),
                'contact_real_country' => $countryName,
                'contact_exact_region' => $regionName,
                'contact_city' => $cityName,
                'contact_ip' => $ip,
                'report_price' => $price,
                'contact_address' => $address,
                'contact_zip' => $zipcode,
                'report_payment_mode' => 2,
                'payment_response' => json_encode($response),
                'contact_form_type' => @$contact_form_type,
                'query_source' => 'web'
            );

            $this->db->insert('mr_form_contact', $data);


            //// Script for adding Category wise emails in cc
                $this->email->initialize($this->config->item('mail_config'));

                $this->email->from($this->config->item('checkout_email_from'), $this->config->item('company_name'));
                $this->email->to($this->config->item('checkout_email_to'));
                $this->email->cc($this->config->item('checkout_email_cc'));
                $this->email->subject('New Checkout Attempt Success Query - EazyPay');
                $this->email->message('
                    Dear Admin,<br><br> 
                    You have a new checkout Query..<br><br>
                    <font size=2><b>Report Title :- </b></font>' . $rep_title . '<br><br>
                    <font size=2><b>Contact Person :- </b></font>' . $name . '<br><br>
                    <font size=2><b>Email :- </b></font> ' . $email . '<br><br>
                    <font size=2><b>Company Name :- </b></font> ' . $company_name . '<br><br>
                    <font size=2><b>Phone :- </b></font> ' . @$phone . '<br><br>
                    <font size=2><b>Country Name :- </b></font> ' . $country_name . '<br><br>
                    <font size=2><b>Zip Code :- </b></font> ' . $zipcode . '<br><br>
                    <font size=2><b>Address :- </b></font> ' . $address . '<br><br>
                    <font size=2><b>Price :- </b></font>' . $currency . ' ' . $price . '<br><br>
                    <font size=2><b>License Type :- </b></font>' . $license . '<br><br>
                    <font size=2><b>Payment Mode :- </b></font>' . $mode . '<br><br>
                    <hr>
                    <font size=2><b>Country:- </b></font> ' . $countryName . '<br /><br />
                    <font size=2><b>State:- </b></font> ' . $regionName . '<br /><br />
                    <font size=2><b>City:- </b></font>' . $cityName . '<br /><br />
                    <hr>
                    <font size=2><b>Payment Success Details :  </b></font> <br /><br />
                    <font size=2><b>EazyPay Payment ID:- </b></font>' . $razorpay_payment_id. '<br /><br />
                    <font size=2><b>EazyPay Order ID:- </b></font>' . $razorpay_order_id. '<br /><br />
                    <font size=2><b>EazyPay Signature:- </b></font>' . $razorpay_signature. '<br /><br />
                    Thanks.<br><br>'
                );
            $this->email->send();
            echo "Status OK"; 
        }else{
            redirect("latest-reports");
        }
    }

    public function razorpay_cancel() {
        if(!empty($_POST)){
            $data=getDetailsFromIP();

            $ip = $data['ipAddress'];
            $countryCode = $data['countryCode'];
            $countryName = $data['countryName'];
            $regionName = $data['regionName'];
            $cityName = $data['cityName'];

            $rep_title = $this->input->post("title");
            $zipcode = $this->input->post("zip");
            $address = $this->input->post("address");
            $name = $this->input->post("name");
            $email = $this->input->post("email");
            $mode = 'Razorpay';
            $company_name = $this->input->post("company");
            $is_global = ($this->input->post("is_global") && $_POST['is_global'] == '1') ? true : false;
            $contact_country=$this->input->post("country");
            @$phone = $this->input->post("phone");  
            $country_name = '';
            // echo "<pre>";print_r($is_global);exit;
            if(!$is_global){         
                $country= explode('-', str_ireplace('\"',"",$contact_country));            
                // $country_code =str_ireplace(array('(',')'),'',$country[0]);
                // $phone = $country_code."-".$phone;
                $country_name = $country[1];
            }else{
                $country_name = $contact_country;
            }
            @$price = $this->input->post("price");
            $license = $this->input->post("license");
            $response = $this->input->post("response") ? $this->input->post("response") : array();
            $currency = $this->input->post("currency");
            $payment_status = '';
            $reason = '';
            $payment_order_id = '';
            $source = '';
            $step = '';            
            

            // echo json_encode($response);
            if(!empty($response) && isset($response['error']) && !empty($response['error'])){
                $payment_status = array_key_exists('description',@$response['error']) ? @$response['error']['description'] : 'Failed';
                $reason = array_key_exists('reason',@$response['error']) ? $response['error']['reason'] : 'internal_error';
                $source = array_key_exists('source',@$response['error']) ? $response['error']['source'] : 'unknown';
                $step = array_key_exists('step',@$response['error']) ? $response['error']['step'] : 'unknown';
                if(array_key_exists('metadata', $response['error'])){
                    $payment_order_id = array_key_exists('payment_id',@$response['error']['metadata']) ? $response['error']['metadata']['payment_id'] : '';
                }
            }
            $contact_form_type = array_search('Checkout Form',getFormTypes()) ? array_search('Checkout Form',getFormTypes()) :  0;
            
            //// Script for adding Category wise emails in cc
                $this->email->initialize($this->config->item('mail_config'));

                $this->email->from($this->config->item('checkout_email_from'), $this->config->item('company_name'));
                $this->email->to($this->config->item('checkout_email_to'));
                $this->email->cc($this->config->item('checkout_email_cc'));
                $this->email->subject('New Checkout Attempt Cancelled Query - EazyPay');
                $this->email->message('
                    Dear Admin,<br><br> 
                    You have a new checkout Query..<br><br>
                    <font size=2><b>Report Title :- </b></font>' . $rep_title . '<br><br>
                    <font size=2><b>Contact Person :- </b></font>' . $name . '<br><br>
                    <font size=2><b>Email :- </b></font> ' . $email . '<br><br>
                    <font size=2><b>Company Name :- </b></font> ' . $company_name . '<br><br>
                    <font size=2><b>Phone :- </b></font> ' . @$phone . '<br><br>
                    <font size=2><b>Country Name :- </b></font> ' . $country_name . '<br><br>
                    <font size=2><b>Zip Code :- </b></font> ' . $zipcode . '<br><br>
                    <font size=2><b>Address :- </b></font> ' . $address . '<br><br>
                    <font size=2><b>Price :- </b></font>' . $currency . ' ' . $price . '<br><br>
                    <font size=2><b>License Type :- </b></font>' . $license . '<br><br>
                    <font size=2><b>Payment Mode :- </b></font>' . $mode . '<br><br>
                    <hr>
                    <font size=2><b>Country:- </b></font> ' . $countryName . '<br /><br />
                    <font size=2><b>State:- </b></font> ' . $regionName . '<br /><br />
                    <font size=2><b>City:- </b></font>' . $cityName . '<br /><br />
                    <hr>
                    <font size=2><b>Payment Failed Details :  </b></font> <br /><br />
                    <font size=2><b>Status:- </b></font> ' . $payment_status . '<br /><br />
                    <font size=2><b>Reason:- </b></font> ' . $reason . '<br /><br />
                    <font size=2><b>Source:- </b></font> ' . $source . '<br /><br />
                    <font size=2><b>Step:- </b></font> ' . $step . '<br /><br />
                    <font size=2><b>Payment Order ID:- </b></font>' . $payment_order_id. '<br /><br />
                    Thanks.<br><br>'
                );
            if($this->email->send()){
                
                $db_data = array(
                    'contact_person' => $name,
                    'contact_email' => $email,
                    'contact_company' => $company_name,
                    'contact_phone' => $phone,
                    'contact_country' => $country_name,
                    'contact_rep_title' => $rep_title,
                    'contact_datetime' => date("Y-m-d H:i:s"),
                    'contact_real_country' => $countryName,
                    'contact_exact_region' => $regionName,
                    'contact_city' => $cityName,
                    'contact_ip' => $ip,
                    'report_price' => $price,
                    'contact_address' => $address,
                    'contact_zip' => $zipcode,
                    'report_payment_mode' => 2,
                    'payment_response' => json_encode($response),
                    'contact_form_type' => @$contact_form_type,
                    'query_source' => 'web'
                );

                $this->db->insert('mr_form_contact', $db_data);
            }            
        }else{
            redirect("latest-reports");
        }
    }
}

?>
