<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Index_controller extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('Reports_model');
        $this->load->model('Category_model');
    }

    public function index() {
        $data['report'] = $this->Reports_model->get_top_latest_reports(12); 
        $data['press_list'] = $this->Reports_model->get_press_list(5);
        $this->load->view('index',$data);
    }

    // public function publisher_reports(){
    //     $publisher_url=$this->uri->segment("2");

    //     $data['publisher_data']=$this->Reports_model->get_publisher_data($publisher_url);
    //     $data["publisher_id"]=$data["publisher_data"]->publisher_id;
    //     $data["publisher_name"]=$data["publisher_data"]->publisher_name;
    //     $data["publisher_url"]=$data["publisher_data"]->publisher_url;

    //     $where = NULL;
    //     $base_url = base_url('publisher/'.$publisher_url.'/page/');

    //     $countrep = $this->Reports_model->get_publisher_reports($data["publisher_id"],  NULL, NULL);
    //     $rows=$countrep->num_rows();
    //     $uri_segment=4;
    //     $config=array();
    //     $config=initializePagination($rows,$base_url,$uri_segment); // Initialize Pagination
    //     $this->pagination->initialize($config);

    //     $data['total_pages'] = ceil($config["total_rows"] / $config["per_page"]); 
    //     $data['page'] = ($this->uri->segment($uri_segment)) ? $this->uri->segment($uri_segment) : 1;
    //     $data['uri_suffix'] = $config['suffix'];

    //     $data['report_list'] = $this->Reports_model->get_publisher_reports($data["publisher_id"], $config["per_page"], $data['page'] - 1);
    //     $data['total_reports'] = $config['total_rows'];

    //     $data['pagination'] = $this->pagination->create_links();
    //     $data["region"]=$this->Reports_model->get_region();
    //     $data['publishers']=$this->Reports_model->get_publishers();
    //     $this->load->view("publisher-reports",$data);
    // }

    // public function publisher_reports_list(){ 
    //     $region=$this->uri->segment("4");
    //     $data["publisher_url"]=$publisher_url=$this->uri->segment("2");
    //     $data["current_region"]=$region=trim(str_replace(array( '%20' ), ' ',$region));
        
    //     $data['publisher_data']=$this->Reports_model->get_publisher_data($publisher_url);
    //     $data["publisher_id"]=$data["publisher_data"]->publisher_id;
    //     $data["publisher_name"]=$data["publisher_data"]->publisher_name;        
        
    //     $data["region"]=$this->Reports_model->get_country($region);
         
    //     $data["reports"]=null;
    //     $data["total_rows"]=array();
    //     $data["country"]="";
    //     if($region !== "Global"){
    //         foreach($data["region"]->result() as $countryName){
    //              $country=$countryName->country;
    //              $data["country"]=$country.",".$data["country"];
    //         }     
    //     } 
        
    //     $base_url = base_url('publisher/'.$publisher_url.'/region/'.$region.'/page/');
        
    //     $limit="";
    //     $start="";

    //     $countrep = $this->Reports_model->get_publisher_region_reports($data["publisher_id"],$limit,$start,$data["country"],$region);
    //     if($countrep->num_rows() > 0):  
    //         $rows=$countrep->num_rows(); 
    //         $uri_segment=6; 
    //         $config=array();
    //         $config=initializePagination($rows,$base_url,$uri_segment); // Initialize Pagination
    //         // echo "<pre>";print_r($config);die();
    //         $this->pagination->initialize($config);

    //         $data['total_pages'] = ceil($config["total_rows"] / $config["per_page"]); 
    //         $data['page'] = ($this->uri->segment($uri_segment)) ? $this->uri->segment($uri_segment) : 1;
    //         $data['uri_suffix'] = $config['suffix'];

    //         $data['report_list'] = $this->Reports_model->get_publisher_region_reports($data["publisher_id"],$config["per_page"], $data['page'] - 1,$data["country"],$region);
    //         $data['reports'] = $config['total_rows'];

    //     $data['pagination'] = $this->pagination->create_links();
    //     endif;
        
    //     $data['total_reports']=$data['reports'];
    //     $data["region"]=$this->Reports_model->get_region();
    //     $data['publishers']=$this->Reports_model->get_publishers();

    //     $data["helpText"] = "<p style='font-family:asul;font-size:1.2em;'>Showing <mark style='font-style:italic'>".$data["total_reports"]."</mark> results for <mark style='font-style:italic'>" . $region ."</mark></p>";
    //     $data["errorText"] = "<p style='text-align:center;font-family:asul;font-size:1.6em;color:maroon;transform:translate(0,70px)'>Sorry ! No Reports Found For <mark style='font-style:italic'>" . $region ."</mark> Region</p>";
    //     $this->load->view('publisher-reports', $data);        
    // }
    
    // public function publisher_reports_filter() {
        
    //     $data["publisher_url"]=$publisher_url=$this->uri->segment("2");

    //     $data['publisher_data']=$this->Reports_model->get_publisher_data($publisher_url);
    //     $data["publisher_id"]=$data["publisher_data"]->publisher_id;
    //     $data["publisher_name"]=$data["publisher_data"]->publisher_name;

    //     $base_url = base_url('publisher/'.$data["publisher_url"].'/reports/page/');

    //     $year = $this->input->get("year");

    //     $data["year"] = $year;
    //     $year1 = $year . "-01-01";
    //     $year2 = $year . "-12-31";        

    //     $countrep = $this->Reports_model->get_publisher_year_report($data["publisher_id"],NULL, NULL, $year1, $year2);

    //     if($countrep){
    //     $first_url='';
    //     $first_url= '1?year=' . $year;       

    //     $rows=$countrep->num_rows(); 
    //     $uri_segment=5; 
    //     $config=array();
    //     $config=initializePagination($rows,$base_url,$uri_segment,$first_url); // Initialize Pagination
    //     // echo "<pre>";print_r($config);die();
    //     $this->pagination->initialize($config);

    //     $data['total_pages'] = ceil($config["total_rows"] / $config["per_page"]); 
    //     $data['page'] = ($this->uri->segment($uri_segment)) ? $this->uri->segment($uri_segment) : 1;
    //     $data['uri_suffix'] = $config['suffix'];
    //     $data["report_year"] = $year;

    //     $data['pagination'] = $this->pagination->create_links();

    //     $data["report_list"] = $this->Reports_model->get_publisher_year_report($data["publisher_id"],$config["per_page"], $data['page'] - 1, $year1, $year2);
    //     $data['total_reports'] = $config['total_rows'];
    //     if ($this->input->get("year")) {
    //         $data["helpText"] = "<b class='help-text'>Showing Top <mark>".$data["total_reports"]."</mark> results for year <mark>" . $year . "</mark></b>";
    //     }
        
    //     $data["resultText"] = "<b>Sorry ! No Results Found !</b>";
        
    //     $data["region"]=$this->Reports_model->get_region();
    //     $data['publishers']=$this->Reports_model->get_publishers();


    //     $this->load->view("publisher-reports", $data);
    //      }else{
    //         $this->load->view('404');
    //     }
    // }

}
