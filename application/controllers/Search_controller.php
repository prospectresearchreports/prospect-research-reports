<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Search_controller extends CI_Controller {

    public function __construct() {

        parent:: __construct();
        $this->load->model("Cms_model");
        $this->load->model("Category_model");
        $this->load->model("Reports_model");
        $this->load->helper(array('form'));
    }
    

    // for the search other than home / index page



    public function search_suggestions() {

        $data = array();
        $search=$this->input->post('search');
        $data['report_search'] = $this->Cms_model->get_report_search_ajax(trim($search));
        //echo $data['report_search']->num_rows();
        if($data['report_search']->num_rows() > 0){
            foreach ($data['report_search']->result() as $row) {
                echo '<a href="'.base_url().'reports/'.$row->rep_id.'" style="color:#000;font-family:asul,text-decoration:none;font-size:0.8em">'.substr($row->rep_title,0,50).'..'.'</a><br/>';
            }
        }else{
            echo 'Sorry ! No results found !';
        } 
    }
    public function search_form() {
        $data['search']= $search = $this->input->get('search') ? $this->input->get('search') : '';
        $data['year']= $this->input->get('year') ? $this->input->get('year') : '';
        $data['current_region']= $this->input->get('region') ? $this->input->get('region') : '';
        $data['date_order']= $this->input->get('date') ? $this->input->get('date') : '';
        $data['price']= $this->input->get('price') ? $this->input->get('price') : '';
        

        if(!empty(@$search)){
            $data["helpText"] = "Report Search Request For <strong>" . $search ."</strong>";
        }

        $this->load->view("search-form",$data);
    }
    
     public function search_result() {

        $data = array();
	    $data['search']=$search=$this->input->get('search');

        $data['category'] = $this->Category_model->get_cat_list_id();

        $data['year']= $year = $this->input->get('year') ? $this->input->get('year') : '';
        $data['current_region']= $region = $this->input->get('region') ? $this->input->get('region') : '';
        $data['date_order']= $date = $this->input->get('date') ? $this->input->get('date') : '';
        $data['price']= $price = $this->input->get('price') ? $this->input->get('price') : '';
        $data['category']= $category = $this->input->get('category') ? $this->input->get('category') : '';

        
        $where = array();
        $where_like = null;
        $where_between = array();
        $region_data=array();
        $orderarray=null;

        if(!empty($search)){
            $where_like['rep_title'] = explode(' ',$search);
        }
        if(!empty($category)){
            $where['rep_sub_cat_1_id'] = $category;
        }
        if(!empty($year)){
            $where_between[] ="rep_entry_date BETWEEN '".$year."-01-01' AND '".$year."-12-31'";
        }
        if(!empty($region)){
            $data["region"]=$this->Reports_model->get_country($region);
            $country="";
            foreach($data["region"]->result() as $countryName){
                 $country=$countryName->country.",".$country;
            }
            $country=rtrim(',',$country);
            $region_data=array('country' => $country, 'region' => $region);
        }
        if(!empty($price)){
            if(stristr($price,'-')){
                $report_price=explode('-',$price);
                $where_between[] ="li_value BETWEEN '".@$report_price[0]."' and '".@$report_price[1]."' ";
            }elseif(stristr($price,'<')){
                $where['li_value <']=str_ireplace('<','',$price);
            }elseif(stristr($price,'>')){
                $where['li_value > ']=str_ireplace('>','',$price);
            }
        }
        if(!empty($date)){
            $orderarray ="rep_entry_date ".$date;
        }else{
            $orderarray="rep_id DESC";
        }

        
        $base_url = base_url('search-result/page/');
        $rows = $this->Cms_model->get_report_search_result($where, $where_between, $where_like,  $orderarray, $region_data, NULL, NULL, true);
        // $rows=$countrep->num_rows();
        // $query = $data['report_search'] = $this->Cms_model->get_report_search_result($where, $where_between, $where_like, $orderarray, $region_data,null,null);

        $first_url = @$search ? '1?search='.@$search : '';
        $data['current_url']=str_ireplace('/index.php','',current_url()).'?search='.@$search;

        if(!empty($_GET)){
            $temp_get=$_GET;
            if(!empty(array_intersect_key($temp_get,['search' => @$search]))){
                unset($temp_get['search']);
                if(!empty($temp_get)){
                    $first_url ='1?search='.@$search.'&year='.@$year.'&region='.@$region.'&date='.@$date.'&price='.@$price;
                    $data['current_url']=str_ireplace('/index.php','',current_url()).'?search='.@$search.'&year='.@$year.'&region='.@$region.'&date='.@$date.'&price='.@$price;
                }
            }
        }
        // $rows=$query->num_rows(); 
        $uri_segment=3; 
        $config=array();
        $config=initializePagination($rows,$base_url,$uri_segment,$first_url); // Initialize Pagination
        // echo "<pre>";print_r($config);die();
        $this->pagination->initialize($config);

        $data['total_pages'] = ceil($config["total_rows"] / $config["per_page"]); 
        $data['page'] = ($this->uri->segment($uri_segment)) ? $this->uri->segment($uri_segment) : 1;
        $data['uri_suffix'] = $config['suffix'];

        $data['report_search'] = $this->Cms_model->get_report_search_result($where, $where_between , $where_like , $orderarray, $region_data ,$config["per_page"], $data['page'] - 1);
 
        //echo $this->db->last_query();

        //$data['total_reports'] = $config['total_rows'];
        // $data['total_reports']= $count = $query->num_rows();
        $data['total_reports']= $count = $rows;
        $data['pagination'] = $this->pagination->create_links();

        $data['cat_list'] = $data['cat_1_menu'] = $data['cat_1_list_id'] = $this->Category_model->get_cat_list_id();
              
        $data['cat_1_menu'] = $data['cat_1_list_id'] = $this->Category_model->get_cat_list_id();

        $data['SHOW'] = false;

        if(@$count > 0 && !empty(@$search)){
            $data['SHOW'] = true;
            $data["helpText"] = "Showing <strong>".$data["total_reports"]."</strong> result/s for <strong>" . $search ."</strong>";
        }
        if(!empty($_GET)){
            $temp_get=$_GET;
            if(!empty(array_intersect_key($temp_get,['search' => @$search]))){
                unset($temp_get['search']);
                if(!empty($temp_get)){
                    $data['SHOW'] = true;
                }
            }
        }
        $data['action_url']=str_ireplace('/index.php','',current_url()).'?search='.@$search;

        // $data['publishers']=$this->Reports_model->get_publishers();
        $data["region_data"]=$this->Reports_model->get_region();
        if(@$count == 0){
            $data['helpText']="Sorry ! No Results Found For Your Search Query";
            $this->load->view('search-form',$data);
        }else{
            $this->load->view('search-result', $data);
        }
    }

    public function search_report_region(){ 
        // $data['publishers']=$this->Reports_model->get_publishers();
        // $data['category'] = $this->Category_model->get_cat_list_id();
        
        $region=$this->uri->segment("3");
        $data["search"]=$search=$this->input->get('q');
        $data["current_region"]=$region=trim(str_replace(array( '%20' ), ' ',$region));
        //echo "<script>alert('".$region."')</script>";

        if($region==="North America"){
            //echo "<script>alert('hi')</script>";
            $region="America";
        }  
        
        $data["region"]=$this->Reports_model->get_country($region);
         
        $data["reports"]=null;
        $data["total_rows"]=array();
        $data["country"]="";
        foreach($data["region"]->result() as $countryName){
             $country=$countryName->country;
             $data["country"]=$country.",".$data["country"];
        }
                     //$data["country"]=implode(" ",$country);

        //print_r($data["country"]);         
     
        
        $base_url = base_url('search-region/region/'.$region.'/page/');
        
        $limit="";
        $start="";

        $countrep = $this->Cms_model->get_search_reports($limit,$start,$data["country"],$region,$data["search"]);
        
        if($countrep){
            if($countrep->num_rows() > 0):

            $rows=$countrep->num_rows(); 
            $uri_segment=5; 
            $config=array();
            $config=initializePagination($rows,$base_url,$uri_segment); // Initialize Pagination
            // echo "<pre>";print_r($config);die();
            $this->pagination->initialize($config);

            $data['total_pages'] = ceil($config["total_rows"] / $config["per_page"]); 
            $data['page'] = ($this->uri->segment($uri_segment)) ? $this->uri->segment($uri_segment) : 1;
            $data['uri_suffix'] = $config['suffix'];

            $data["report_search"] = $this->Cms_model->get_search_reports($config["per_page"], $data['page'] - 1,$data["country"],$region,$data["search"]);

            $data['reports'] = $config['total_rows'];

            $data['pagination'] = $this->pagination->create_links();
            endif;

            
            $data['total_reports']=$data['reports'];
            $data["region"]=$this->Reports_model->get_region();

            $data["helpText"] = "<b class='help-text'>Showing <mark style='font-style:italic'>".$data["total_reports"]."</mark> results for <mark style='font-style:italic'>" . $region ."</mark></b>";
            $data["resultText"] = "<b>Sorry ! No Reports Found For <mark style='font-style:italic'>" . $region ."</mark> Region</b>";
            $this->load->view('search-result', $data);

        }else{

            $this->load->view('404');

        }
        
    }

    public function search_filter() {
        // $data['publishers']=$this->Reports_model->get_publishers();
        // $data['category'] = $this->Category_model->get_cat_list_id();

        $year = $this->input->get("year");
        $data["search"]=$search= $this->input->get("q");
        //echo "<script>alert('".$year."')</script>";

        $base_url = base_url('search-filter/page/');
        $year1 = $year . "-01-01";
        $year2 = $year . "-12-31";
        $price1 = NULL;
        $price2 = NULL;
        if ($this->input->get("price")) {
            $price = $this->input->get("price");
            $priceRange = explode("-", $price);
            $price1 = $priceRange[0];
            $price2 = $priceRange[1];
        }


        $countrep = $this->Cms_model->get_search_report(NULL, NULL, $year1, $year2, $price1, $price2, $search);
        
        $first_url='';
        if ($this->input->get("year")) {
            $first_url= '1?year=' . $year . '&q=' . $search;
        }
        if ($this->input->get("price")) {
            $first_url = '1?price=' . $price . '&q=' . $search;
        }

        $rows=$countrep->num_rows(); 
        $uri_segment=3; 
        $config=array();
        $config=initializePagination($rows,$base_url,$uri_segment,$first_url); // Initialize Pagination
        // echo "<pre>";print_r($config);die();
        $this->pagination->initialize($config);

        $data['total_pages'] = ceil($config["total_rows"] / $config["per_page"]); 
        $data['page'] = ($this->uri->segment($uri_segment)) ? $this->uri->segment($uri_segment) : 1;
        $data['uri_suffix'] = $config['suffix'];

        $data['pagination'] = $this->pagination->create_links();
        $data["report_year"] = $year;

        if ($this->input->get("price")) {
            $price = $this->input->get("price");
            $data["report_price"] = $price;
        }

        $data["report_search"] = $this->Cms_model->get_search_report($config["per_page"], $data['page'] - 1, $year1, $year2, $price1, $price2, $search);
        $data['total_reports'] = $config['total_rows'];
        if ($this->input->get("year")) {
            $data["helpText"] = "<br/><b class='help-text'>Showing ".$data['total_reports']." results for year " . $year . "</b>";
        }
        if ($this->input->get("price")) {
            $data["helpText"] = "<br/><b class='help-text'>Showing ".$data['total_reports']."  results for " . $price1 . " - " . $price2 . " USD </b>";
        }
        $data["resultText"] = "<b>Sorry ! No Results Found !</b>";
        
        $data["region"]=$this->Reports_model->get_region();

        $this->load->view("search-result", $data);

        
    }

    public function search_category() {
        // $data['publishers']=$this->Reports_model->get_publishers();
        // $data['category'] = $this->Category_model->get_cat_list_id();

        $cat_id = $this->input->get("id");

        $data['category_details'] = $this->Category_model->get_report_cat_id($cat_id);

        $category=$data["category_details"]->sc1_name;

        $data["search"]=$search= $this->input->get("q");




        $config['base_url'] = base_url('search-category/page/');


        $countrep = $this->Cms_model->get_search_category_report(NULL, NULL, $cat_id, $search);
        $config['total_rows'] = $countrep->num_rows();
        $config['per_page'] = 15;
        $config["uri_segment"] = 3;
        $config['use_page_numbers'] = TRUE;
        $config['page_query_string'] = FALSE;
        $config["num_links"] = 2; //ceil($config["total_rows"] / $config["per_page"]);
        $config['full_tag_open'] = '<ul class="pagination">';
        $config['full_tag_close'] = '</ul>';

        $config['first_url'] = '1?id=' . $cat_id . '&q=' . $search;
        
        $config['first_link'] = 'First';
        $config['last_link'] = 'Last';
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['prev_link'] = '&laquo;';
        $config['prev_tag_open'] = '<li rel=prev >';
        $config['prev_tag_close'] = '</li>';
        $config['next_link'] = '&raquo;';
        $config['next_tag_open'] = '<li rel="next">';
        $config['next_tag_close'] = '</li>';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="active"><a href="javascript:void()">';
        $config['cur_tag_close'] = '</a></li>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $config['suffix'] = ($_GET) ? '?' . http_build_query($_GET, '', "&") : NULL;
        $config['attributes']['rel'] = true;
        $this->pagination->initialize($config);

        $data['total_pages'] = ceil($config["total_rows"] / $config["per_page"]); //$config["num_links"];
        $data['page'] = ($this->uri->segment(3)) ? $this->uri->segment(3) : 1;

        $data['pagination'] = $this->pagination->create_links();

        $data["report_search"] = $this->Cms_model->get_search_category_report($config["per_page"], $data['page'] - 1, $cat_id, $search);
        $data['total_reports'] = $config['total_rows'];

        $data["helpText"] = "<b class='help-text'>Showing ".$data['total_reports']."  results for  <mark>" . $category . "</mark></b>";
        
        $data["resultText"] = "<b>Sorry ! No Results Found !</b>";
        
        $data["region"]=$this->Reports_model->get_region();

        $this->load->view("search-result", $data);

        
    }
    function __destruct() 
    {
        $this->db->close();
    }

}
