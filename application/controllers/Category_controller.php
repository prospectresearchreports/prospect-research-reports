<?php

class Category_controller extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model(array('Reports_model','Category_model','Cms_model'));
        // $this->load->library('session');
    }

    public function categories() {
        $url=$this->uri->segment(1);
        $data['meta']=$this->Cms_model->get_meta_info($url);
        // $data['publishers']=$this->Reports_model->get_publishers();
        $this->load->view("categories",$data);
        
    }

    public function category_details() {
        
        $data = array();
        $where = NULL;

        $data = array();

        // $page = trim($this->uri->segment(1));
        $data["category_url"]=$caturl = trim($this->uri->segment(2));
        $data['category'] = $this->Category_model->get_cat_byurl($caturl);

        $data['year']= $year = $this->input->get('year') ? $this->input->get('year') : '';
        $data['current_region']= $region = $this->input->get('region') ? $this->input->get('region') : '';
        $data['date_order']= $date = $this->input->get('date') ? $this->input->get('date') : '';
        $data['price']= $price = $this->input->get('price') ? $this->input->get('price') : '';

        if ($data["category"]){
            $where = array();
            $where_between = array();
            $region_data=array();
            $orderarray=null;
            
            $where['rep_sub_cat_1_id'] = $data['category']->sc1_id;
            if(!empty($year)){
                $where_between[] ="rep_entry_date BETWEEN '".$year."-01-01' AND '".$year."-12-31'";
            }
            if(!empty($region)){
                // $where_between[] ="rep_entry_date BETWEEN '".$year."-01-01' AND '".$year."-12-31'";
                $data["region"]=$this->Reports_model->get_country($region);
                $country="";
                foreach($data["region"]->result() as $countryName){
                     $country=$countryName->country.",".$country;
                }
                $country=rtrim(',',$country);
                $region_data=array('country' => $country, 'region' => $region);
            }
            if(!empty($price)){
                if(stristr($price,'-')){
                    $report_price=explode('-',$price);
                    $where_between[] ="li_value BETWEEN '".@$report_price[0]."' and '".@$report_price[1]."' ";
                }elseif(stristr($price,'<')){
                    $where['li_value <']=str_ireplace('<','',$price);
                }elseif(stristr($price,'>')){
                    $where['li_value > ']=str_ireplace('>','',$price);
                }
            }
            if(!empty($date)){
                $orderarray ="rep_entry_date ".$date;
            }else{
                $orderarray="rep_id DESC";
            }

            $base_url = base_url('category/' . $caturl . '/page/');
            $data["category_name"] = $data["category"]->sc1_name;
            $data["category_id"] = $data["category"]->sc1_id;
            $data["category_image"] = $data["category"]->sc1_image;

            $rows = $this->Reports_model->get_category_reports($where, $where_between, $orderarray, $region_data, NULL, NULL, true);
            // $rows=$countrep->num_rows();
            $uri_segment=4;
            $first_url = '';
            if(!empty($_GET)){
                $first_url .='1?year='.@$year.'&region='.@$region.'&date='.@$date.'&price='.@$price;
            }
            $config=array();
            $config=initializePagination($rows,$base_url,$uri_segment,$first_url); // Initialize Pagination
            // echo "<pre>";print_r($config);die();
            $this->pagination->initialize($config);

            $data['total_pages'] = ceil($config["total_rows"] / $config["per_page"]); 
            $data['page'] = ($this->uri->segment($uri_segment)) ? $this->uri->segment($uri_segment) : 1;
            $data['uri_suffix'] = $config['suffix'];

            $data['category_report_list'] = $this->Reports_model->get_category_reports($where, $where_between , $orderarray, $region_data , $config["per_page"], $data['page'] - 1);

            if ($data["category_report_list"] != FALSE){

                if(!empty($_GET) || $config['total_rows'] > 0){
                    $data['total_reports'] = $config['total_rows'];
                }
                $data['report_text'] = "<br/><br/><b>Sorry ! Currently there are no reports for Category <span style='color:#182b3e'>".$data["category_name"]."</span>  !</b><br/><br/>";

                $data['pagination'] = $this->pagination->create_links();
                $data["region_data"]=$this->Reports_model->get_region();
                $data['current_url']=str_ireplace('/index.php','',current_url());

                // $data['publishers']=$this->Reports_model->get_publishers();
           
                $this->load->view("category-details", $data);
            }else{
                $this->load->view('404');
            }

            
        }else{
            $this->load->view('404');
        }
            

    }

    // public function category_filter() {
    //     $data['publishers']=$this->Reports_model->get_publishers();

    //     $year = $this->input->get("year");
    //     $cat_id = $this->input->get("category_id");


    //     $data['category'] = $this->Category_model->get_report_cat_id($cat_id);
    //     $data["current_region"] = '';

    //     if($data["category"]){

    //         $data["category_name"] = $category_name = $data["category"]->sc1_name;
    //         $data["category_url"] = $data["category"]->sc1_url;
    //         $data["category_id"] = $data["category"]->sc1_id;
    //         $data["category_image"] = $data["category"]->sc1_image;


    //         $year1 = $year . "-01-01";
    //         $year2 = $year . "-12-31";
    //         $price1 = NULL;
    //         $price2 = NULL;
    //         if ($this->input->get("price")) {
    //             $price = $this->input->get("price");
    //             $priceRange = explode("-", $price);
    //             $price1 = $priceRange[0];
    //             $price2 = $priceRange[1];
    //         }           
    //         $base_url= base_url('category/page/');

    //         $countrep = $this->Reports_model->get_category_report(NULL, NULL, $year1, $year2, $price1, $price2, $cat_id);
    //         $first_url='';
    //         if ($this->input->get("year")) {
    //             $first_url= '1?year=' . $year . '&category_id=' . $cat_id;
    //         }
    //         if ($this->input->get("price")) {
    //             $first_url = '1?price=' . $price . '&category_id=' . $cat_id;
    //         }

    //         $rows=$countrep->num_rows(); 
    //         $uri_segment=3; 
    //         $config=array();
    //         $config=initializePagination($rows,$base_url,$uri_segment,$first_url); // Initialize Pagination
    //         // echo "<pre>";print_r($config);die();
    //         $this->pagination->initialize($config);

    //         $data['total_pages'] = ceil($config["total_rows"] / $config["per_page"]); 
    //         $data['page'] = ($this->uri->segment($uri_segment)) ? $this->uri->segment($uri_segment) : 1;
    //         $data['uri_suffix'] = $config['suffix'];

    //         $data['pagination'] = $this->pagination->create_links();
    //         $data["report_year"] = $year;

    //         if ($this->input->get("price")) {
    //             $price = $this->input->get("price");
    //             $data["report_price"] = $price;
    //         }

    //         $data["category_report_list"] = $this->Reports_model->get_category_report($config["per_page"], $data['page'] - 1, $year1, $year2, $price1, $price2, $cat_id);
    //         $data['total_reports']= $count = $config['total_rows'];
    //         @$text= ($_GET['year']) ? " for year  <mark style='font-style:italic'>" . $year ."</mark>" : (($_GET['price']) ? " for " . $price1 . " - " . $price2 . " USD " : '' );
    //         if (!empty(@$text)) {
    //             if(@$count > 0){
    //                 $data["helpText"] = "<b class='help-text'>Showing ".$count.' reports '. @$text . "</b>";
    //             }else{
    //                 $data["errorText"] = "<b class='help-text'>Sorry ! No Reports Found For <mark style='font-style:italic'>". @$text ."</mark> </b>";  
    //             }
    //         }
    //     $data["region"]=$this->Reports_model->get_region();

    //         $this->load->view("category-details", $data);

    //     }else{

    //         $this->load->view('404');

    //     }
    // }
    
    // public function category_report_list(){ 
    //     $data['publishers']=$this->Reports_model->get_publishers();
        
    //     $region=$this->uri->segment("4");
    //     $cat_id=$this->uri->segment("2");
    //     $data["current_region"]=$region=trim(str_replace(array( '%20' ), ' ',$region));
    //     //echo "<script>alert('".$region."')</script>";

    //     if($region==="North America"){
    //         //echo "<script>alert('hi')</script>";
    //         $region="America";
    //     }
        
               
    //     $data['category'] = $this->Category_model->get_report_cat_id($cat_id);
    //     $data["category_name"] = $category_name = $data["category"]->sc1_name;
    //     $data["category_url"] = $data["category"]->sc1_url;
    //     $data["category_id"] = $data["category"]->sc1_id;
    //     $data["category_image"] = $data["category"]->sc1_image;
    //     $data["region"]=$this->Reports_model->get_country($region);
         
    //     $data["reports"]=null;
    //     $data["total_rows"]=array();
    //     $data["country"]="";
    //     foreach($data["region"]->result() as $countryName){
    //          $country=$countryName->country;
    //          $data["country"]=$country.",".$data["country"];
    //     }
    //                  //$data["country"]=implode(" ",$country);

    //     //print_r($data["country"]);         
     
        
    //     $base_url = base_url('category/'.$cat_id.'/region/'.$region.'/page/');
        
    //     $limit="";
    //     $start="";

    //     $countrep = $this->Reports_model->get_category_reports($limit,$start,$data["country"],$region,$data["category_id"]);
        
    //     if($countrep){
    //         if($countrep->num_rows() > 0):

    //         $rows=$countrep->num_rows(); 
    //         $uri_segment=6; 
    //         $config=array();
    //         $config=initializePagination($rows,$base_url,$uri_segment); // Initialize Pagination
    //         // echo "<pre>";print_r($config);die();
    //         $this->pagination->initialize($config);

    //         $data['total_pages'] = ceil($config["total_rows"] / $config["per_page"]); 
    //         $data['page'] = ($this->uri->segment($uri_segment)) ? $this->uri->segment($uri_segment) : 1;
    //         $data['uri_suffix'] = $config['suffix'];

    //         $data["category_report_list"] = $this->Reports_model->get_category_reports($config["per_page"], $data['page'] - 1,$data["country"],$region,$data["category_id"]);
    //         $data['reports'] = $config['total_rows'];
    //         $data['pagination'] = $this->pagination->create_links();
    //         endif;

            
    //         $data['total_reports'] = $count = $data['reports'];
    //         $data["region"]=$this->Reports_model->get_region();
    //         if(@$count > 0){
    //             $data["helpText"] = "<b class='help-text'>Showing <mark style='font-style:italic'>".$count."</mark> results for <mark style='font-style:italic'>". @$region . "</mark> region </b>";
    //         }else{
    //             $data["errorText"] = "<b class='help-text'>Sorry ! No Reports Found For <mark style='font-style:italic'>". @$region ."</mark> region</b>";  
    //         }
            
            
    //         $this->load->view('category-details', $data);

    //     }else{

    //         $this->load->view('404');

    //     }
        
    // }

}

?>
