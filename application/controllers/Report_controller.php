<?php

defined('BASEPATH') OR exit('No direct script access allowed');


class Report_controller extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('Reports_model');
        $this->load->database("");
    }
    
    
    // public function report_list(){   
    //     // $data['publishers']=$this->Reports_model->get_publishers();
        
    //     $region=$this->uri->segment("3");
    //     $data["current_region"]=$region=trim(str_replace(array( '%20' ), ' ',$region));
    //     //echo "<script>alert('".$region."')</script>";

    //     if($region==="North America"){
    //         //echo "<script>alert('hi')</script>";
    //         $region="America";
    //     }
        
        
    //     $data["region"]=$this->Reports_model->get_country($region);
         
    //     $data["reports"]=null;
    //     $data["total_rows"]=array();
    //     $data["country"]="";
    //     if($region !== "Global"){
    //         foreach($data["region"]->result() as $countryName){
    //              $country=$countryName->country;
    //              $data["country"]=$country.",".$data["country"];
    //         }     
    //     }   
        
    //     $base_url = base_url('reports/region/'.$region.'/page/');
        
    //     $limit="";
    //     $start="";

    //     $countrep = $this->Reports_model->get_reports($limit,$start,$data["country"],$region);
    //     if($countrep->num_rows() > 0):           

    //         $rows=$countrep->num_rows(); 
    //         $uri_segment=5; 
    //         $config=array();
    //         $config=initializePagination($rows,$base_url,$uri_segment); // Initialize Pagination
    //         $this->pagination->initialize($config);

    //         $data['total_pages'] = ceil($config["total_rows"] / $config["per_page"]); 
    //         $data['page'] = ($this->uri->segment($uri_segment)) ? $this->uri->segment($uri_segment) : 1;
    //         $data['uri_suffix'] = $config['suffix'];

    //         $data['report_list'] = $this->Reports_model->get_reports($config["per_page"], $data['page'] - 1,$data["country"],$region);
    //         $data['reports'] = $config['total_rows']+$data["reports"];

    //         $data['pagination'] = $this->pagination->create_links();
    //     endif;

        
    //     $data['total_reports']=$data['reports'];
    //     $data["region"]=$this->Reports_model->get_region();
    //     $data["helpText"] = "<b class='help-text'>Showing <mark style='font-style:italic'>".$data["total_reports"]."</mark> results for <mark style='font-style:italic'>" . $region ."</mark></b>";
    //     $data["errorText"] = "<b class='help-text'>Sorry ! No Reports Found For <mark style='font-style:italic'>" . $region ."</mark> Region</b>";
    //     $this->load->view('latest-reports', $data);
        
    // }
    public function country_list(){
        $data['publishers']=$this->Reports_model->get_publishers();

        $region=$this->input->post("region");
        $data["country"]=$this->Reports_model->get_country($region);
        echo "<ul class='country'>";
        foreach($data["country"]->result() as $row){
            $country=$row->country;
            $id=$row->id;
            echo "<li><input class='country$id' onclick='getReports(".$id.")' type='checkbox' name='country[]' value='$country'><span>".$country."</span></li>";
        }
        echo "</ul>";
        
    }
    
    
    public function latest_reports() {
        $where = array();
        $where_between = array();
        $region_data=array();
        $orderarray=null;

        $data['year']= $year = $this->input->get('year') ? $this->input->get('year') : '';
        $data['current_region']= $region = $this->input->get('region') ? $this->input->get('region') : '';
        $data['date_order']= $date = $this->input->get('date') ? $this->input->get('date') : '';
        $data['price']= $price = $this->input->get('price') ? $this->input->get('price') : '';
        $data['current_url']=str_ireplace('/index.php','',current_url());
        
        if(!empty($year)){
            $where_between[] ="rep_entry_date BETWEEN '".$year."-01-01' AND '".$year."-12-31'";
        }
        if(!empty($region)){
            // $where_between[] ="rep_entry_date BETWEEN '".$year."-01-01' AND '".$year."-12-31'";
            $data["region"]=$this->Reports_model->get_country($region);
            $country="";
            foreach($data["region"]->result() as $countryName){
                 $country=$countryName->country.",".$country;
            }
            $country=rtrim(',',$country);
            $region_data=array('country' => $country, 'region' => $region);
        }
        if(!empty($price)){
            if(stristr($price,'-')){
                $report_price=explode('-',$price);
                $where_between[] ="li_value BETWEEN '".@$report_price[0]."' and '".@$report_price[1]."' ";
            }elseif(stristr($price,'<')){
                $where['li_value <']=str_ireplace('<','',$price);
            }elseif(stristr($price,'>')){
                $where['li_value > ']=str_ireplace('>','',$price);
            }
        }
        if(!empty($date)){
            $orderarray ="rep_entry_date ".$date;
        }else{
            $orderarray="rep_id DESC";
        }
        $base_url = base_url('reports/page/');

        $rows = $this->Reports_model->get_latest_reports($where, $where_between, $orderarray, $region_data, NULL, NULL, true);
        // $rows=$countrep->num_rows();
        $uri_segment=3;
        $first_url = '';
        if(!empty($_GET)){
            $first_url .='1?year='.@$year.'&region='.@$region.'&date='.@$date.'&price='.@$price;
        }
        $config=array();
        $config=initializePagination($rows,$base_url,$uri_segment,$first_url); // Initialize Pagination
        // echo "<pre>";print_r($config);die();
        $this->pagination->initialize($config);

        $data['total_pages'] = ceil($config["total_rows"] / $config["per_page"]); 
        $data['page'] = ($this->uri->segment($uri_segment)) ? $this->uri->segment($uri_segment) : 1;
        $data['uri_suffix'] = $config['suffix'];

        $data['report_list'] = $this->Reports_model->get_latest_reports($where, $where_between , $orderarray, $region_data , $config["per_page"], $data['page'] - 1);
        
        if(!empty($_GET) || $config['total_rows'] > 0){
            $data['total_reports'] = $config['total_rows'];
        }

        $data['pagination'] = $this->pagination->create_links();
        $data["region_data"]=$this->Reports_model->get_region();
        // $data["country"]=$this->Reports_model->get_country();

        $this->load->view('latest-reports', $data);
    }

   
    public function report_details() {
        $rep_id=$this->uri->segment(2);
        // $report_url=$this->uri->segment(3);        
        // $split_url=explode('-market', $report_url);
        // $rep_url=$split_url[0];


       //*****************************        REPORT VISIT COUNT      ************************************** 

       // $data["views"]=$this->Reports_model->report_views($rep_id,$uid);
       // $count=$data["views"]->num_rows();
       // if($count >0 || $count==1){
       //  $views=$data["views"]->views;
       //  $data["report_count"]=$views+1;
       //  $data=array("views"=>$data["report_count"]);
       //     $this->Reports_model->update_report_views($uid,$rep_id,$data);
       // }else {
       //  $data["report_count"]=1;
       //      $data=array("rep_id"=>$rep_id,"uid"=>$uid,"views"=>$data["report_count"]);
       //     $this->Reports_model->insert_report_views($data);
       // }
       //  $data["report_views"]=$this->Reports_model->report_views($rep_id,$uid);


       //*****************************             ************************************** 


        $data['report_data'] = $report_data = $this->Reports_model->get_report_data($rep_id);
        if($data['report_data']){
            $data = $this->Reports_model->get_report_details($rep_id, $data);  
            // echo "<pre>";print_r(array_keys($data));exit;      
            $this->load->view('report-details',$data);
        }else{
            $this->load->view('404');
        }
    }


    public function report_details_amp() {
        $data['publishers']=$this->Reports_model->get_publishers();

        //global $ip,$id;

        if ($this->session->has_userdata("badge_count")):
            $data["badge"] = $this->session->userdata("badge_count");
        endif;
       $rep_id=$this->uri->segment(3);
        $data['report_data'] = $this->Reports_model->get_report_data($rep_id);
        $data['report_license'] = $this->Reports_model->get_report_license($rep_id);
        $related_product_category=$data["report_data"]->rep_sub_cat_1_id;
        $data["related_reports"]= $this->Reports_model->get_related_reports($related_product_category,10);

        $this->load->view('report-details-amp',$data);
    }
    public function process_form_request() {
        $rep_id=$this->uri->segment(2);
        $type=$this->input->get('type');
        $data['form_type']=ucwords(str_ireplace('_',' ',$type));
        $data['report_data'] = $this->Reports_model->get_report_form_data($rep_id);
        $this->load->view('report-sample',$data);
    }
    // public function report_sample() {
    //     $rep_id=$this->uri->segment(2);
    //     $data['report_data'] = $this->Reports_model->get_report_form_data($rep_id);
    //     $this->load->view('report-sample',$data);
    // }
    
    // public function request_customize() {
    //     $rep_id=$this->uri->segment(2);
    //     $data['report_data'] = $this->Reports_model->get_report_form_data($rep_id);
    //     $this->load->view('request-customize',$data);
    // }
    // public function enquire_purchase() {
    //     $rep_id=$this->uri->segment(2);
    //     $data['report_data'] = $this->Reports_model->get_report_form_data($rep_id);
    //     $this->load->view('enquire-purchase',$data);
    // }
    // public function request_discount() {
    //     $rep_id=$this->uri->segment(2);
    //     $data['report_data'] = $this->Reports_model->get_report_form_data($rep_id);
    //     $this->load->view('request-discount',$data);
    // }    
    // public function reports_filter() {

    //     $base_url = base_url('reports-filter/page/');
    //     $this->load->library("pagination");

    //     $year = $this->input->get("year");

    //     $data["year"] = $year;
    //     $year1 = $year . "-01-01";
    //     $year2 = $year . "-12-31";
        

    //     $countrep = $this->Reports_model->get_filtered_report(NULL, NULL, $year1, $year2);        
    //     $first_url='';
    //     $first_url= '1?year=' . $year;       

    //     $rows=$countrep->num_rows(); 
    //     $uri_segment=3; 
    //     $config=array();
    //     $config=initializePagination($rows,$base_url,$uri_segment,$first_url); // Initialize Pagination
    //     // echo "<pre>";print_r($config);die();
    //     $this->pagination->initialize($config);

    //     $data['total_pages'] = ceil($config["total_rows"] / $config["per_page"]); 
    //     $data['page'] = ($this->uri->segment($uri_segment)) ? $this->uri->segment($uri_segment) : 1;
    //     $data['uri_suffix'] = $config['suffix'];

    //     $data["report_year"] = $year;

    //     $data['pagination'] = $this->pagination->create_links();


    //     $data['report_list'] = $this->Reports_model->get_filtered_report($config["per_page"], $data['page'] - 1, $year1, $year2);
    //     $data['total_reports'] = $config['total_rows'];
    //     if ($this->input->get("year")) {
    //         $data["helpText"] = "<b class='help-text'>Showing results for year " . $year . "</b>";
    //     }
    //     // if ($this->input->get("price")) {
    //     //     $data["helpText"] = "<b class='help-text'>Showing results for " . $price1 . " - " . $price2 . " USD </b>";
    //     // }
    //     $data["resultText"] = "<b>Sorry ! No Results Found !</b>";
        
    //     $data["region"]=$this->Reports_model->get_region();

    //     $this->load->view("latest-reports", $data);
    // }

}
