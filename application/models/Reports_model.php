<?php

class Reports_model extends CI_Model 
{
        /* function get_usage_in_kb(){
          echo memory_get_usage()/1024.0 . " kb \0";

        } */

    public function __construct() 
    {
        parent::__construct();
        ini_set("memory_limit", "500M");
        $this->load->database("");
    }
    /* ------------------------------------------------------------------------------------ */

    
    public function get_top_latest_reports($limit) {
        $this->db->select('rep_sub_cat_1_id,mr_report.id as rep_id,rep_url,rep_title,rep_entry_date, category_image');
        $this->db->from('mr_report');
        $this->db->join("mr_report_images","mr_report.rep_sub_cat_1_id=mr_report_images.category_id","left");
        $this->db->where(array("status" => 1,"archive_status" => 0));
        $this->db->order_by("rep_entry_date","DESC");
        $this->db->limit($limit);
        return $this->db->get();
    } 
    /* ------------------------------------------------------------------------------------ */
   
    public function get_publishers() {
        $this->db->select('*');
        $this->db->from('mr_publisher');
        $this->db->where(array("status" => 1,"archive_status" => 0));
        $this->db->order_by("publisher_name","ASC");
        return $this->db->get();
    }

    /************************************* FILTER REPORTS ********************************/
    
    public function get_country($region){
        $this->db->select("*");
        $this->db->from("mr_country");
        $this->db->where(array("continent"=>$region));
        //$this->db->order_by("country","desc");
        return $this->db->get();
    }


    public function get_all_country(){
        $this->db->select("*");
        $this->db->from("mr_country");
        $this->db->order_by("mr_country.country","asc");
        return $this->db->get();
    }
    
    public function get_region(){
        $this->db->select("*");
        $this->db->from("mr_regions");
        $this->db->order_by("region","asc");
        return $this->db->get();
    }

    /* ----------------------------------- CATEGORY REPORTS & FILTER ------------------------------- */

    public function get_category_reports($where = NULL, $where_between = null , $orderarray = null,$region_data = array() , $limit = NULL, $start = NULL, $is_count_only = false) {
        $this->db->select('mr_report.id as rep_id,rep_price,rep_entry_date,rep_url,rep_descrip,rep_sub_cat_1_id,rep_title,rep_date,sc1_url,mr_sub_cat_1.id as sc1_id,sc1_name,publisher_name,li_value,category_image');
        $this->db->from('mr_report');
        $this->db->join('mr_sub_cat_1', 'mr_sub_cat_1.id = mr_report.rep_sub_cat_1_id', 'left');
        $this->db->join('mr_publisher', 'mr_publisher.id = mr_report.publisher_id','left');
        $this->db->join('mr_report_license', 'mr_report_license.li_rep_id = mr_report.id','left');
        $this->db->join("mr_report_images","mr_report_images.category_id=mr_report.rep_sub_cat_1_id","left");
        $this->db->where("mr_report_license.li_key = 'Single User Price' ");
        if(!empty($region_data)){
            $this->db->where('match(rep_title) against("'.$region_data['country'].','.$region_data['region'].'")',null,false);
        }

        if ($where):
            $this->db->where($where);
        endif;
		
		if($where_between){
			foreach($where_between as $k=>$value){
				$this->db->where($value, NULL, FALSE );
			}
		}
        $this->db->where(array('mr_report.status' => 1, 'mr_report.archive_status' => 0));
        $this->db->order_by($orderarray);
        if ($limit)
        {
            $this->db->limit($limit, $start * $limit);
        }
        return $is_count_only ? $this->db->count_all_results() : $this->db->get();
    }

    /* ---------------------------------- LATEST REPORTS & FILTER  -------------------------------- */
    
    public function get_latest_reports($where = NULL, $where_between = null , $orderarray = null,$region_data = array() , $limit = NULL, $start = NULL, $is_count_only = false) 
    {
        $this->db->select('mr_report.id as rep_id,rep_entry_date,rep_url,rep_descrip,rep_title,rep_date,li_value,category_image');
        $this->db->from('mr_report');
        $this->db->join('mr_report_license', 'mr_report_license.li_rep_id = mr_report.id','left');
        $this->db->join("mr_report_images","mr_report_images.category_id=mr_report.rep_sub_cat_1_id","left");
        $this->db->where("mr_report_license.li_key = 'Single User Price' ");
        if(!empty(@$region_data)){
            $this->db->where('match(rep_title) against("'.$region_data['country'].','.$region_data['region'].'")',null,false);
        }       
        $this->db->where(array('status' => 1, 'archive_status' => 0));

        if(@$where):
            $this->db->where($where);
        endif;
		
		if(@$where_between){
			foreach($where_between as $k=>$value){
				$this->db->where($value, NULL, FALSE );
			}
		}

        $this->db->where(array('mr_report.status' => 1, 'mr_report.archive_status' => 0));
        $this->db->order_by(@$orderarray);
        if (@$limit)
        {
            $this->db->limit(@$limit, @$start * @$limit);
        }
        return $is_count_only ? $this->db->count_all_results() : $this->db->get();
    }

    //*********************      PRESS RELEASE          ***********************************//

    public function get_press_list($limit)
    {
        $this->db->select('title,content,published_on,image,url,cat_id');
        $this->db->from('mr_press');
        $this->db->where(array('status' => 1,"archive_status"=>0));

        if ($limit) {
            $this->db->limit($limit);
        }
        return $this->db->get();
    }

    public function get_press_releases($where = NULL, $where_between = null , $orderarray = null, $limit = NULL, $start = NULL)
    {
        $this->db->select('title,content,published_on,image,url,cat_id');
        $this->db->from('mr_press');        

        if ($where):
            $this->db->where($where);
        endif;
		
		if($where_between){
			foreach($where_between as $k=>$value){
				$this->db->where($value, NULL, FALSE );
			}
		}
        $this->db->order_by($orderarray);
        $this->db->where(array('status' => 1,"archive_status"=>0));

        if ($limit) {
            $this->db->limit($limit, $start * $limit);
        }
        return $this->db->get();
    }

    public function press_details($press_url){
        $this->db->select('title,content,published_on,image,url,cat_id');
        $this->db->from('mr_press');
        $this->db->where(array('status' => 1,"archive_status"=> 0, 'url' => $press_url));
        return $this->db->get()->row();
    }
    

    //*********************      BLOGS          ***********************************//
    
    public function get_blogs($where = NULL, $orderarray = 'blog_date DESC', $limit = NULL, $start = NULL)
    {
        $this->db->select('blog_title,blog_content,blog_date,blog_img,blog_url,blog_cat_id');
        $this->db->from('mr_blogs');
        if ($where):
            $this->db->where($where);
        endif;
        if ($orderarray):
            $this->db->order_by($orderarray);
        endif;
        $this->db->where(array("status" => 1,"archive_status" => 0));

        if ($limit) {
            $this->db->limit($limit, $start * $limit);
        }
        return $this->db->get();
    }

    public function blog_details($url) 
    {
        $this->db->select('blog_title,blog_content,blog_date,blog_img,blog_url,blog_cat_id');
        $this->db->from('mr_blogs');
        $this->db->where(array("status" => 1,"archive_status" => 0, 'blog_url' => $url));
        return $this->db->get()->row();
    }
    
    /* --------------------------------- REPORT DETAILS ------------------------------------- */


    public function get_report_data($rep_id){
        $this->db->select('mr_report.id as rep_id,mr_report.is_manual,rep_url,rep_entry_date,rep_sub_cat_1_id,publisher_id,rep_title,rep_page,rep_toc_fig,rep_contents,rep_table_of_contents,category_image');
        $this->db->from('mr_report');
        $this->db->join('mr_report_metadata', 'mr_report_metadata.meta_rep_id = mr_report.id','left');
        // $this->db->join('mr_sub_cat_1', 'mr_sub_cat_1.id =mr_report.rep_sub_cat_1_id','left');
        $this->db->join("mr_report_images","mr_report.rep_sub_cat_1_id=mr_report_images.category_id","left");
        $this->db->where(array('mr_report.id' => $rep_id ,'status' => 1, 'archive_status' => 0));
        return $this->db->get()->row();
    }   

    public function get_report_license($id){
        $this->db->select('*');
        $this->db->from('mr_report_license');
        $this->db->where(array('li_rep_id' => $id));
        return $this->db->get();
    }
    
    public function get_related_reports($category_id='',$rep_id=0,$limit=0){
        $this->db->select('mr_report.id as rep_id,rep_url,rep_sub_cat_1_id,rep_title,rep_entry_date,li_value,category_image');
        $this->db->from('mr_report');
        $this->db->join('mr_report_license', 'mr_report_license.li_rep_id = mr_report.id','left');
        $this->db->where("mr_report_license.li_key = 'Single User Price' and mr_report.rep_sub_cat_1_id='$category_id'");
        $this->db->join("mr_report_images","mr_report.rep_sub_cat_1_id=mr_report_images.category_id","left");
        $this->db->where("mr_report.id != ".$rep_id);        
        $this->db->where(array('status' => 1, 'archive_status' => 0));
        $this->db->order_by("rep_entry_date","desc");
        $this->db->limit($limit);
        
        return $this->db->get();
    }

    /* --------------------------------- REQUEST SAMPLE FORM -------------------------- */

    public function get_report_form_data($rep_id){
        $this->db->select('mr_report.id as rep_id,rep_url,rep_date,rep_sub_cat_1_id,mr_report.publisher_id,rep_title,mr_report.rep_page,mr_report.rep_toc_fig,category_image');
        $this->db->from('mr_report');
        // $this->db->join('mr_sub_cat_1', 'mr_sub_cat_1.id =mr_report.rep_sub_cat_1_id','left');
        $this->db->join("mr_report_images","mr_report.rep_sub_cat_1_id=mr_report_images.category_id","left");
        $this->db->where(array('mr_report.id' => $rep_id, 'status' => 1, 'archive_status' => 0));
        return $this->db->get()->row();
    }

    /* ------------------------------------------------------------------------------------ */

      public function get_report_checkout($id){
        $this->db->select('mr_report.id as rep_id,rep_url,rep_sub_cat_1_id,rep_title,publisher_id,rep_page,category_image');
        $this->db->from('mr_report');
        // $this->db->join('mr_sub_cat_1', 'mr_report.rep_sub_cat_1_id=mr_sub_cat_1.id','left');
        $this->db->join("mr_report_images","mr_report.rep_sub_cat_1_id=mr_report_images.category_id","left");
        $this->db->where(array('mr_report.id' => $id));
        $this->db->where(array('status' => 1, 'archive_status' => 0));
        return $this->db->get()->row();
    }

    /* --------------------------- RSS & SITEMAP --------------------------------- */  
    
    public function get_top_list_rss($limit){
        $this->db->select('mr_report.id as rep_id,rep_url,rep_descrip,rep_title,rep_date,sc1_name,li_value');
        $this->db->from('mr_report');
        $this->db->join('mr_sub_cat_1', 'mr_sub_cat_1.id = mr_report.rep_sub_cat_1_id', 'left');
        $this->db->join('mr_report_license', 'mr_report_license.li_rep_id = mr_report.id','left');
        $this->db->where("mr_report_license.li_key = 'Single User Price'");
        $this->db->where(array("mr_report.status" => 1,"mr_report.archive_status" => 0));
        $this->db->order_by("rep_date","DESC");
        $this->db->limit($limit);
        return $this->db->get();
    }
    
	public function count_reports(){
        $this->db->select("id");
        $this->db->from("mr_report");        
        $this->db->where(array('status' => 1, 'archive_status' => 0));
        return $this->db->get();
    }
	public function get_sitemap_reports($offset,$limit) {
        $this->db->select('id as rep_id,rep_url,rep_date');
        $this->db->from('mr_report');
        $this->db->where(array("status" => 1,"archive_status" => 0));				
        $this->db->order_by("id","DESC");
        if($limit !== NULL):
        $this->db->limit($limit,$offset);
        endif;
        return $this->db->get();
    }
    
    public function get_report_details($rep_id, $data){  

        $regionName=array("South America","Oceania","North America","Global","Europe","Asia","Antarctica","Africa","Zimbabwe","Zealand","Zambia","Yemen","Walli sutuna","Vietnam","Venezuela","Vatican","Vanuatu","Uzbekistan","US","America","United States","Uruguay","Ukraine","Uganda","Tuvalu","Turks Caicos","Turkmenistan","Turkey","Tunisia","Trinidad","Tobago","Tonga","Tokelau","Togo","Timor-Leste","Thailand","Tanzania","Tajikistan","Taiwan","Syrian","Syria","Switzerland","Sweden","Swaziland","Svalbard","Mayen","Suriname","Sudan","Sri Lanka","Spain","Somalia","Solomon","Slovenia","Slovakia","Singapore","Sierra Leone","Seychelles","Serbia and Montenegro","Senegal","Saudi Arabia","Samoa","Samoa","Salvador"," Saint Vincent","Grenadines","Saint Kitts Nevis","Saint Helena","Sahara","Rwanda","Romania","Reunion","Qatar","Puerto Rico","Portugal","Poland","Pitcairn","Pierre Miquelon","Philippines","Peru","Paraguay","Papua","Panama","Palestinian","Palau","Pakistan","Oman","Norway","Norfolk","Niue","Nigeria","Niger","Nicaragua","Netherlands Antilles","Netherlands","Nepal","Nauru","Namibia","Myanmar","Mozambique","Morocco","Montserrat","Mongolia","Monaco","Moldova","Micronesia","Mexico","Mayotte","Mauritius","Mauritania","Martinique","Marshall","Marino","Mariana","Malta","Mali","Maldives","Malaysia","Malawi","Madagascar","Macedonia","Macao","Luxembourg","Lucia","Lithuania","125Liechtenstein","Libyan Jamahiriya","Liberia","Lesotho","Lebanon","Latvia","Latin","Lao","Kyrgyzstan","Kuwait","Korea","Korea","Kiribati","Kingdom","London","Kenya","Kazakhstan","Jordan","Japan","Jamaica","Italy","Israel","Ireland","Iraq","Iran","Indonesia","India","Iceland","Hungary","Hong Kong","Honduras","Haiti","Guyana","Guinea-Bissau","Guinea","Guiana","Guatemala","Guam","Guadeloupe","Grenada","Greenland","Greece","Gibraltar","Ghana","Germany","Georgia","Georgia","Gambia","Gabon","France","Finland","Fiji","Faroe","Falkland","Malvinas","Ethiopia","Estonia","Eritrea","Egypt","Ecuador","Dominican","Dominica","Djibouti","Denmark","Czech","Cyprus","Cuba","Croatia","CoteDIvoire","Costa Rica","ongo","Congo","Comoros","Colombia","Cocos","China","Chile","Chad","Cayman","CapeVerde","Canada","Cameroon","Cambodia","Caledonia","Burundi","Burkina Faso","Bulgaria","Brunei Darussalam","Brazil","Bouvet","Botswana","Bosnia","Herzegovina","Bolivia","Bhutan","Bermuda","Benin","Belize","Belgium","Belarus","Barbados","Bangladesh","Bahrain","Bahamas","Azerbaijan","Austria","Australia","Aruba","Armenia","Argentina","ArabEmirates","Antigua","Barbuda","Antarctica","Anguilla","Angola","Andorra","Algeria","Albania","Africa","Afghanistan","Polynesia","United States","European Union","and",")",",","(");
        $data['report_license'] = $this->Reports_model->get_report_license($rep_id);
        $related_product_category=$data['report_data']->rep_sub_cat_1_id;
        $data["related_reports"]= $this->Reports_model->get_related_reports($related_product_category,$rep_id,10);


        $report_title=$data['report_data']->rep_title;

        //$report_year=explode('20', $report_title);
        $report_year=str_ireplace('|','',preg_replace('/[^0-9]/','', $report_title));
        $sub_string=strlen($report_year)+1;
        if(strlen($report_year) > 4){
            $data['report_year'] =substr(chunk_split($report_year,4,'-'),0,$sub_string);
        }else{
            $data['report_year'] =$report_year;
        }
        $report_title=str_ireplace('global','',$report_title);
        $start_year='2015';
        $end_year=date('Y')+10;
        $year_array=array();
        for($i=$start_year;$i<=$end_year;$i++){
            array_push($year_array,$i);
        }
        // $year_string=['2017','2018','2019','2020','2021','2022','2023','2024','2025','2026'];
        $report_title=str_ireplace($year_array,'',$report_title);
        //$report_title=str_ireplace($regionName,'',$report_title);
        if(stristr($report_title,'market')){
            $meta_title=explode('Market',$report_title);
        }
        if(stristr($report_title,'industry')){
            $meta_title=explode('Industry',$report_title);
        }

        $meta_title[0]=str_ireplace($regionName,'',$meta_title[0]);

        $data['meta_report_title']=$meta_title[0];
        return $data;
    }

    
    // public function get_coupon($code) {
    //     $this->db->select('*');
    //     $this->db->from('mr_coupon');
    //     $this->db->where(array("coupon"=>$code));
    //     return $this->db->get()->row();
    // }
    /* ------------------------------------------------------------------------------------ */
    
    // public function get_order_data($sid,$order_id) {
    //     $this->db->select('*');
    //     $this->db->from('mr_bank_checkout');
    //     $this->db->where(array("sid"=>$sid,'order_id'=>$order_id));
    //     return $this->db->get()->row();
    // }
    /* ------------------------------------------------------------------------------------ */
    
    // public function report_views($id,$uid) {
    //     $this->db->select('views,mr_report.id');
    //     $this->db->from('mr_report_views');
    //     $this->db->where(array("mr_report.id"=>$id,"uid"=>$uid));
    //     return $this->db->get();
    // }


/* ------------------------------------------------------------------------------------ */
    
    // public function update_report_views($uid,$id,$data) {
    //     $this->db->set($data);
    //     $this->db->where(array("mr_report.id"=>$id,"uid"=>$uid));
    //     $this->db->update('mr_report_views',$data);
    // }

    /* ------------------------------------------------------------------------------------ */
    
    // public function insert_report_views($data) {
    //     if($this->db->insert('mr_report_views',$data)){
    //         return true;
    //     }
    // }
   
    
    
    /* ------------------------------------------------------------------------------------ */
    
    // public function get_top_list_next($limit,$offset) {
    //     $this->db->select('mr_report.id,rep_url,rep_title,rep_date,status,archive_status');
    //     $this->db->from('mr_report');
    //     $this->db->where(array("status" => 1,"archive_status" => 0));
    //     $this->db->order_by("rep_date","DESC");
    //     $this->db->limit($limit,$offset);
    //     return $this->db->get();
    // }
    /* ------------------------------------------------------------------------------------ */
    
    // public function get_top_list($limit) {
    //     $this->db->select('mr_report.id,rep_url,rep_title,rep_date,status,archive_status');
    //     $this->db->from('mr_report');
    //     $this->db->where(array("status" => 1,"archive_status" => 0));
    //     $this->db->order_by("rep_date","DESC");
    //     $this->db->limit($limit);
    //     return $this->db->get();
    // }



    /* ------------------------------------------------------------------------------------ */
    
    // public function get_publisher_data($url) {
    //     $this->db->select('*');
    //     $this->db->from('mr_publisher');
    //     $this->db->where(array("status" => 1,"archive_status" => 0,"publisher_url"=>$url));
    //     return $this->db->get()->row();
    // }

    /* ------------------------------------------------------------------------------------ */
    
    // public function get_publisher_reports($id,$limit,$start) {
    //     $this->db->select('mr_report.id,category_image,rep_entry_date,rep_url,rep_descrip,rep_title,rep_date,status,archive_status,li_value,rep_sub_cat_1_id');
    //     $this->db->from('mr_report');        
    //     $this->db->join('mr_report_license', 'mr_report_license.li_rep_id = mr_report.id','left');
    //     $this->db->join("mr_report_images","mr_report_images.category_id=mr_report.rep_sub_cat_1_id","left");
    //     $this->db->where("mr_report_license.li_key = 'Single User Price' ");
    //     $this->db->where(array("publisher_id"=>$id));
    //     $this->db->order_by("rep_date","desc");
    //     if ($limit)
    //     {
    //         $this->db->limit($limit, $start * $limit);
    //     }
    //     return $this->db->get();
    // }


    // public function get_publisher_region_reports($id,$limit,$start,$country,$region) {
    //     $this->db->select('mr_report.id,rep_url,rep_descrip,rep_title,rep_entry_date,rep_date,li_value,rep_sub_cat_1_id,publisher_id,category_image');
    //     $this->db->from('mr_report');
    //     $this->db->join('mr_report_license', 'mr_report_license.li_rep_id = mr_report.id','left');
    //     $this->db->join("mr_report_images","mr_report_images.category_id=mr_report.rep_sub_cat_1_id","left");

    //     if($region== 'North America'){
    //         $this->db->not_like('rep_title','latin');
    //         $region='America';
    //     }
    //     if($region== 'South America'){
    //         $this->db->not_like('rep_title','Southeast');
    //     }
    //     $this->db->where("match(rep_title) against('".$country.", ".$region." '  IN BOOLEAN MODE)",null,true);
        
    //     $this->db->where('mr_report_license.li_key="Single User Price"');
    //     $this->db->where(array("publisher_id"=>$id));
    //     $this->db->order_by("mr_report.id","DESC");
        
    //     if ($limit)
    //     {
    //         $this->db->limit($limit, $start * $limit);
    //     } 
    //     return $this->db->get();
        
    // }
    
    // public function get_publisher_year_report($id,$limit,$start,$year1,$year2) {
    //      $this->db->select('mr_report.id,rep_url,rep_entry_date,rep_date,rep_descrip,rep_title,rep_sub_cat_1_id,li_value,publisher_id,category_image');
    //     $this->db->from('mr_report');
    //     $this->db->join('mr_report_license', 'mr_report_license.li_rep_id = mr_report.id','left');
    //     $this->db->join("mr_report_images","mr_report_images.category_id=mr_report.rep_sub_cat_1_id","left");
    //     $this->db->where('rep_entry_date between "' .$year1.'" and "' .$year2.'" ');
    //     $this->db->where('mr_report_license.li_key="Single User Price"');
    //     $this->db->where(array("publisher_id"=>$id));
    //     $this->db->order_by("mr_report.id","DESC");
        
    //     if ($limit) {
    //         $this->db->limit($limit, $start * $limit);
    //     }

    //     return $this->db->get();
        
    // }
    
    // public function get_report_data_id($rep_id){
    //     $this->db->select('mr_report.id,rep_url,rep_date,rep_sub_cat_1_id,mr_report.publisher_id,rep_title,mr_report.rep_page,mr_report.rep_toc_fig,mr_report_metadata.rep_contents,mr_report_metadata.rep_table_of_contents,sc1_name,category_image,publisher_code,publisher_name');
    //     $this->db->from('mr_report');
    //     $this->db->join('mr_publisher', 'mr_publisher.id =mr_report.publisher_id','left');
    //     $this->db->join('mr_report_metadata', 'mr_report_metadata.meta_rep_id =mr_report.id','left');
    //     $this->db->join('mr_sub_cat_1', 'mr_sub_cat_1.id =mr_report.rep_sub_cat_1_id','left');
    //     $this->db->join("mr_report_images","mr_report.rep_sub_cat_1_id=mr_report_images.category_id","left");

    //     $this->db->like('mr_report.id', $rep_id);
    //     return $this->db->get()->row();
    // }



    /* ------------------------------------------------------------------------------------ */
    

    /* ------------------------------------------------------------------------------------ */

    // public function get_report_list_replica()
    // {
    //     $this->db->select('reid,reprice,reentry_date,reurl,redescrip,resub_cat_1_id,retitle,redate,sc1_url,sc1_id,sc1_name,status,archive_status,publisher_name,li_value');
    //     $this->db->from('report');
    //     $this->db->join('mr_sub_cat_1', 'mr_sub_cat_1.id = report.resub_cat_1_id', 'left');
    //     $this->db->join('mr_publisher', 'mr_publisher.id = mr_report.publisher_id','left');
    //      $this->db->join('mr_report_license', 'mr_report_license.li_reid = mr_report.reid','left');
    //     $this->db->where("mr_report_license.li_key = 'Single User Price'");
    //     $this->db->where(array('status' => 1, 'archive_status' => 0));     
    //     $this->db->order_by("redate", "DESC");
    //     $this->db->limit(50);
    //     return $this->db->get();
    // }
    // public function get_popular_report()
    // {
    //    $this->db->select('mr_report.id,rep_price,rep_url,rep_descrip,rep_sub_cat_1_id,rep_title,rep_date,sc1_url,sc1_id,sc1_name,status,archive_status,publisher_name,li_value');
    //     $this->db->from('report');
    //     $this->db->join('mr_sub_cat_1', 'mr_sub_cat_1.id = report.rep_sub_cat_1_id', 'left');
    //     $this->db->join('mr_publisher', 'mr_publisher.id = mr_report.publisher_id','left');
    //     $this->db->join('mr_report_currency', 'mr_report_currency.cur_id = mr_report.id','left');
    //     $this->db->join('mr_report_license', 'mr_report_license.li_reid = mr_report.id','left');
    //     $this->db->where("mr_report_license.li_key = 'Single User Price' and mr_report.id='3' ");
    //     $this->db->where(array("status" => 1,"archive_status" => 0));
    //     return $this->db->get()->row();  
    // }


    /* ------------------------------------------------------------------------------------ */

    // public function get_report_list_tab($cat = null) 
    // {
    //     $this->db->select('resub_cat_1_id,reurl,retitle,redescrip,redate');
    //     $this->db->from('report');
    //     $this->db->where_in('resub_cat_1_id', $cat);
    //     $this->db->where(array('status' => 1));
    //     $this->db->order_by("redate", "DESC");
    //     $this->db->limit(500);

    //     return $this->db->get();
    // }

    /* ------------------------------------------------------------------------------------ */

    // public function get_report_data_id($url) 
    // {
    //     $this->db->select("*");
    //     $this->db->from('report');
    //     $this->db->where(array('status' => 1, 'archive_status' => 0));
    //     $this->db->like('rep_url', $url);
    //     $query = $this->db->get()->row();
    //     return $query;
    // }
    

    /* ------------------------------------------------------------------------------------ */

    
    /* ------------------------------------------------------------------------------------ */

    // public function get_report_views($url)
    // {
    //     $this->db->select('user_id,visitor_id');
    //     $this->db->from('mr_report_visit_count');
    //     $this->db->where("report_url='$url'");
    //     return $this->db->get();
    // }
    //  public function get_report_user_id($visitor_id,$url)
    // {
    //     $this->db->select('user_id,visitor_id');
    //     $this->db->from('mr_report_visit_count');
    //     $this->db->where("visitor_id='$visitor_id' and report_url='$url'");
    //     return $this->db->get();
    // }
    // public function  insert_visit_count($data){
    //     if($this->db->insert('mr_report_visit_count',$data)){
    //         return true;
    //     }
    // }
    
    
    // public function get_press_release_list($limit)
    // {
    //     $this->db->select('title,content,published_on,image,url,status,cat_id');
    //     $this->db->from('mr_press');
    //     $this->db->where(array("status" => 1,"archive_status"=>0));
    //     $this->db->limit($limit);
    //     return $this->db->get();
    // }

    // public function get_press_release_list_replica() 
    // {
    //     $this->db->select('title,content,published_on,image,url,status,cat_id');
    //     $this->db->from('mr_press');
    //     $this->db->order_by("published_on", "DESC");
    //     return $this->db->get();
    // }

    // public function get_press_release_details()
    // {
    //     $this->db->select('title,content,published_on,image,url,status,cat_id');
    //     $this->db->from('mr_press');
    //     return $this->db->get();
    // }

    // public function get_blog_list($limit)
    // {
    //     $this->db->select('blog_title,blog_content,blog_date,blog_img,blog_url,status,blog_cat_id');
    //     $this->db->from('mr_blogs');
    //     $this->db->where(array("status" => 1,"archive_status" => 0));

    //     if ($limit) {
    //         $this->db->limit($limit);
    //     }
    //     return $this->db->get();
    // }
    
    
    // public function get_blogs_list($limit)
    // {
    //     $this->db->select('blog_title,blog_content,blog_date,blog_img,blog_url,status,blog_cat_id');
    //     $this->db->from('mr_blogs');
    //     $this->db->where(array("status" => 1,"archive_status"=>0));
    //     $this->db->limit($limit);
    //     return $this->db->get();
    // }

    // public function get_blog_details()
    // {
    //     $this->db->select('blog_title,blog_content,blog_date,blog_img,blog_url,status,blog_cat_id');
    //     $this->db->from('mr_blogs');
    //     return $this->db->get();
    // }

    
    
    
    //**************************************************************************************************//
    
     
    
    // public function get_cat_report_data($id, $params = array(), $cat = null) 
    // {
    //     $this->db->select('rep_sub_cat_2_id,mr_report.id,resub_cat_1_id,rep_url,status,rep_title,rep_descrip,rep_date');
    //     $this->db->from('report');
    //     $this->db->where(array('status' => 1, 'rep_sub_cat_1_id' => $id));
    //     $this->db->where_in('rep_sub_cat_2_id', $cat);
    //     $this->db->order_by('rep_date', 'DESC');

    //         if (array_key_exists("start", $params) && array_key_exists("limit", $params)) 
    //         {
    //             $this->db->limit($params['limit'], $params['start']);
    //         } 
    //         elseif (!array_key_exists("start", $params) && array_key_exists("limit", $params))
    //         {
    //             $this->db->limit($params['limit']);
    //         }
    //     $query = $this->db->get();
    //     return ($query->num_rows() > 0) ? $query->result_array() : FALSE;
    // }

   
    /* ------------------------------------------------------------------------------------ */

    // public function report_visit_count($url, $count)
    // {
    //     $data = array('revisit_count' => $count + 1);
    //     $this->db->like(array('reurl' => $url));
    //     $this->db->update('report', $data);
    // }
    
    // public function get_reports($limit=NULL, $start=NULL,$country,$region) {
    //     $this->db->select('mr_report.id,rep_url,rep_descrip,rep_title,rep_entry_date,status,archive_status,li_value,category_image');
    //     $this->db->from('mr_report');
    //     $this->db->join('mr_report_license', 'mr_report_license.li_rep_id = mr_report.id','left');
    //     $this->db->join("mr_report_images","mr_report_images.category_id=mr_report.rep_sub_cat_1_id","left");
    //     $this->db->where('match(rep_title) against("'.$country.','.$region.'")',null,false);
    //     $this->db->where('mr_report_license.li_key="Single User Price"');
    //     $this->db->order_by("mr_report.id","DESC");
        
    //     if ($limit)
    //     {
    //         $this->db->limit($limit, $start * $limit);
    //     } 
    //     return $this->db->get();
        
    // }

    // public function get_category_reports($limit=NULL, $start=NULL,$country,$region,$category_id) {
    //     $this->db->select('mr_report.id,rep_sub_cat_1_id,rep_url,rep_descrip,rep_title,rep_entry_date,status,archive_status,li_value,category_image');
    //     $this->db->from('mr_report');
    //     $this->db->join('mr_report_license', 'mr_report_license.li_rep_id = mr_report.id','left');
    //     $this->db->join("mr_report_images","mr_report_images.category_id=mr_report.rep_sub_cat_1_id","left");
    //     $this->db->where('match(rep_title) against("'.$country.','.$region.'")',null,false);
    //     $this->db->where(' mr_report.rep_sub_cat_1_id="'.$category_id.'" and mr_report_license.li_key="Single User Price"');
    //     $this->db->order_by("mr_report.id","DESC");
    //     if ($limit)
    //     {
    //         $this->db->limit($limit, $start * $limit);
    //     } 
    //     return $this->db->get();
        
    // }

   

    // public function get_filtered_report($limit,$start,$year1,$year2) {
    //      $this->db->select('mr_report.id,rep_price,rep_entry_date,rep_url,rep_descrip,rep_title,rep_date,status,archive_status,li_value,category_image');
    //     $this->db->from('mr_report');
    //     $this->db->join('mr_report_license', 'mr_report_license.li_rep_id = mr_report.id','left');
    //     $this->db->join("mr_report_images","mr_report_images.category_id=mr_report.rep_sub_cat_1_id","left");
    //     $this->db->where('rep_entry_date between "' .$year1.'" and "' .$year2.'" and status=1 and archive_status=0 and mr_report_license.li_key = "Single User Price"   ');
    //     $this->db->order_by("mr_report.id","DESC");
        
    //     if ($limit) {
    //         $this->db->limit($limit, $start * $limit);
    //     }

    //     return $this->db->get();
        
    // }
    // public function get_category_report($limit,$start,$year1,$year2,$price1,$price2,$cat_id) {
    //      $this->db->select('mr_report.id,rep_entry_date,rep_url,rep_descrip,rep_sub_cat_1_id,rep_title,rep_date,sc1_url,sc1_id,sc1_name,status,archive_status,li_value,category_image');
    //     $this->db->from('mr_report');
    //     $this->db->join('mr_sub_cat_1', 'mr_sub_cat_1.id = mr_report.rep_sub_cat_1_id', 'left');
    //     $this->db->join('mr_report_license', 'mr_report_license.li_rep_id = mr_report.id','left');
    //     $this->db->join("mr_report_images","mr_report_images.category_id=mr_report.rep_sub_cat_1_id","left");
    //     $this->db->where('rep_entry_date between "' .$year1.'" and "' .$year2.'" and  rep_sub_cat_1_id ='.$cat_id.' and mr_report_license.li_key = "Single User Price" or li_value between "' .$price1.'" and "' .$price2.'" and  rep_sub_cat_1_id ='.$cat_id.' and mr_report_license.li_key = "Single User Price" ');
    //     $this->db->order_by("mr_report.id","DESC");
    //     if ($limit)
    //     {
    //         $this->db->limit($limit, $start * $limit);
    //     }
    //     $this->db->where(array('status' => 1, 'archive_status' => 0));

        

    //     return $this->db->get();
    // }
    
    
    //*******************************************************************************************************//
    
    // public function get_report_data_url($url) 
    // {
    //     $this->db->select("rep_title,li_value");
    //     $this->db->from('report');
    //     $this->db->join('mr_report_license', 'mr_report_license.li_rep_id = mr_report.id','left');
    //     $this->db->where('mr_report_license.li_key = "Single User Price" ');
    //     $this->db->where(array('status' => 1, 'archive_status' => 0));
    //     $this->db->like('rep_url', $url);
    //     $query = $this->db->get()->row();
    //     return $query;
    // }
    

    

}

?>