<!DOCTYPE html>
<html>
<head>
<!-- Basic -->
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>Thanks</title>
<meta name="keywords" content="thanks" />
<meta name="description" content="thanks">

<?php
include_once 'header.php';
?>
<br/>
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <article>
                <div class="jumbotron text-xs-center">
                    <h1 class="display-3">Thank You!</h1>
                    <p class="lead"><strong>Please check your email</strong> for further instructions on how to complete your account setup.</p>
                    <hr/>
                    <p>
                        Having trouble? <a href="<?php echo base_url(); ?>contact-us">Contact us</a>
                    </p>
                    <p class="lead">
                        <a class="btn btn-primary btn-md" href="<?php echo base_url(); ?>" role="button">Continue to homepage</a>
                    </p>
                </div>
            </article>
        </div>
    </div>
</div>
    
<?php
include_once 'footer.php';
?>