<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title><?php echo $meta_report_title; ?>Market Share Industry Size Growth Forecast Report <?php echo $report_year; ?></title>
    <!-- Basic -->
    <meta charset="utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <meta name="robots" content="index,follow"/>
    <meta name="keywords" content="<?php echo $meta_report_title; ?>Market - Global <?php echo $meta_report_title; ?>Market Industry Size, Share, Price, Growth Trends, Application, Research Report, Technology, Forecast, Competitive Analysis, PDF Report" />
    <meta name="description" content="ProspectResearch Reports Brings Premium Market Research Report of <?php echo $meta_report_title; ?>Market with Industry Share, Size, Future Trends and Competitive Analysis"/>
    <meta name="author" content="ProspectResearch Reports"/>
    
    <!--**********************    HEADER OPEN      ***************************-->
    <?php require_once 'layouts/header.php'; ?>

    <!--**********************    HEADER CLOSE     ***************************--> 
    <link rel='stylesheet' href="<?php echo base_url(); ?>assets/css/RD-structure.css" />
    <link rel="stylesheet" href="<?=base_url()?>assets/vendor/slick-carousel/slick.css">
    <link rel="stylesheet" href="<?=base_url()?>assets/vendor/slick-carousel/slick-theme.css">
    <script src="<?php echo base_url();?>assets/js/validator.min.js"></script>
    <!-- <script src="<?php echo base_url();?>assets/js/captcha.js"></script> -->

    <!-- *****************   BOOTSTRAP SELECT          ******************  -->


    <script type="text/javascript">  
        //  Captcha Generation
        function getRandomChar()
        {
            var text = "";
            var possible = "0123456789";
            text = possible.charAt(Math.floor(Math.random() * possible.length));
            return text;
        }
        function DrawCaptcha(type)
        {
            var a = getRandomChar()+ '';
            var b = getRandomChar()+ '';
            var c = getRandomChar()+ '';
            var d = getRandomChar()+ '';
            var e = getRandomChar()+ '';
            var f = getRandomChar()+ '';
            var code = a + ' ' + b + ' ' + c + ' ' + d + ' ' + e + ' '+ f  ;
            $("#txtCaptcha_"+type).val(code);
        }
        $(document).ready(function(){
            DrawCaptcha('desc');
            DrawCaptcha('toc');
            DrawCaptcha('tof');
        })

        $(".form").validator();

    // Form validation 
       
    
    function validate_captcha(type) {
        var text=$("#txtCaptcha_"+type).val();
        str = text.replace(/ +/g, "");
        var code=$("#security_code_"+type).val();

        if(str !== code){
            $("#CaptchaError_"+type).html("Invalid Captcha Code");
            return false;
        }else{
            $("#CaptchaError_"+type).html("");
            return true;
        }
    }
</script> 

<ul class="breadcrumb m-b-0">
    <li class="breadcrumb-item"><a href="<?=base_url();?>"><img src="<?=base_url().'assets/images/home.png'?>" title='Go To Home' height="30px" width="30px" alt="Home Breadcrumb Icon" /></a></li>
    <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>latest-reports" title="Go Back">Reports</a></li>
    <li class="breadcrumb-item">
    <?php  
        $report_title = explode('Market',$report_data->rep_title);
        if(isset($report_title[0]) && strlen($report_title[0]) == 0){
            $report_title_array = explode(" ", $report_data->rep_title);
            $first_word_length = strlen($report_title_array[0]);
            $report_title = explode('Market',substr($report_data->rep_title, $first_word_length));
        }
        // echo "<pre>";print_r($report_title);exit;
        $report_title = str_ireplace(array(' -','- ','-'),' - ',$report_title[0]).' Market';
        echo strlen($report_title) > 130 ? substr($report_title , 0 ,130)."..." : $report_title; 
    ?>
    </li>
</ul> 
   
<div class="row"> 
    <div class="rd-block rd-title-block">
        <div class="col-md-9">
            <!-- <div class="col-md-3">
                <div class="txt-center">                    
                    <img class="img img-responsive report-img rd-image" style='width:auto;vertical-align:middle' src="<?php echo base_url(); ?>assets/images/category/<?php echo $report_data->category_image; ?>" alt="Report Details"/>
                </div>
            </div> -->
            <h4 class="report-sample-title"><?php echo $report_data->rep_title; ?></h4>
            
            <div class="col-md-3 request_sample">
                <a target="_blank"  href='<?php echo base_url() ?>report/<?=$report_data->rep_id?>?type=request_sample' class="btn btn-default btn-md btn-tab btn-sample" >
                    <i class="fa fa-download" aria-hidden="true"></i>&nbsp;&nbsp;Request Sample       
                </a>
            </div>            
            <div class="col-md-3">
                <a target="_blank" href='<?php echo base_url() ?>report/<?=$report_data->rep_id?>?type=request_customization' class="btn btn-default btn-md btn-tab btn-customization" >
                    <i class="fa fa-pencil" aria-hidden="true"></i>&nbsp;&nbsp;Request Customization       
                </a>
            </div>
            <div class="col-md-3">
                <a target="_blank"  href='<?php echo base_url() ?>report/<?=$report_data->rep_id?>?type=request_discount' class="btn btn-default btn-md btn-tab btn-discount" >
                    <i class="fa fa-money" aria-hidden="true"></i>&nbsp;&nbsp;Request Discount       
                </a>
            </div>
            <div class="col-md-3">
                <a target="_blank"  href='<?php echo base_url() ?>report/<?=$report_data->rep_id?>?type=enquire_before_purchase' class="btn btn-default btn-md btn-tab btn-enquire" >
                    <i class="fa fa-envelope" aria-hidden="true"></i>&nbsp;&nbsp;Enquire Before Purchase       
                </a>
            </div>
        </div>
        <div class="col-md-3 rd-col-4">             
            <section class="prr-about-work-with-us bg10 p-t-10 p-b-0 rd-details">                                      
                <div class="row">
                    <div class="feature-block">
                        <div class="rd-item"><span>ID</span> : PRR-<?=$report_data->rep_id?></div>
                        <div class="rd-item"><span>Pages</span> : <?=$report_data->rep_page?></div>
                        <div class="rd-item" title="Report is available in Excel, PDF, Powerpoint and Word Formats"><span>Format</span> : 
                            <i class="fa fa-file-excel-o cl3"></i>&nbsp;
                            <i class="fa fa-file-pdf-o cl3"></i>&nbsp;
                            <i class="fa fa-file-powerpoint-o cl3"></i>&nbsp;
                            <i class="fa fa-file-word-o cl3"></i>&nbsp;
                        </div>
                        <div class="rd-item"><span>Industry</span> : <?=categories()[@$report_data->rep_sub_cat_1_id]?></div>
                    </div>
                </div> 
            </section>
        </div>
    </div> 
</div> 

    
<div class="row">
    <div class="col-md-8"> 
        <div class="tabs">
            <ul class="nav nav-tabs nav-justified">
                <li class="nav-item"><a href="#desc" data-toggle="tab" class="text-center nav-link <?= isset($rd_type) && $rd_type === "desc" ? " active" : (!isset($rd_type) ? " active" : " ")?>">Description</a> </li>
                <li class="nav-item"> <a href="#toc" data-toggle="tab" class="text-center nav-link <?= isset($rd_type) && $rd_type === "toc" ? " active" : " "?>"> Table Of Content</a> </li>
                <li class="nav-item"> <a href="#tof" data-toggle="tab" class="text-center nav-link <?= isset($rd_type) && $rd_type === "tof" ? " active " : " "?>"> Table Of Figures</a> </li>
                <!--<li> <a href="#summary" data-toggle="tab" class="text-center">  Summary</a> </li>-->
            </ul>
            <div class="tab-content">
                <div id="desc" class="tab-pane fade in  <?= isset($rd_type) && $rd_type === "desc" ? " active show" : (!isset($rd_type) ? " active show" : " ")?>">
                    <?php if(isset($report_data->rep_contents) &&  strlen($report_data->rep_contents) > 0){ 
                        // echo "<pre>";print_r(trim($report_data->rep_contents));exit;
                        echo $report_data->is_manual ? trim($report_data->rep_contents) : nl2br(trim($report_data->rep_contents));
                    }else{ 
                        $type = 'desc';
                        require 'report-details-form.php';
                     } ?>
                </div>
                <div id="toc" class="tab-pane fade in <?= isset($rd_type) && $rd_type === "toc" ? " active show" : " "?>">
                    <?php if(isset($report_data->rep_table_of_contents) && strlen($report_data->rep_table_of_contents) > 0){ 
                        echo $report_data->is_manual ? trim($report_data->rep_table_of_contents) : nl2br(trim($report_data->rep_table_of_contents));
                    }else{
                        $type = 'toc';
                        require 'report-details-form.php';
                     } ?>
                </div>
                <div id="tof" class="tab-pane fade in <?= isset($rd_type) && $rd_type === "tof" ? " active show" : " "?>">
                    <?php if(isset($report_data->rep_toc_fig) && strlen($report_data->rep_toc_fig) > 0){ 
                        echo $report_data->is_manual ? trim($report_data->rep_toc_fig) : nl2br(trim($report_data->rep_toc_fig));
                    }else{ 
                        $type = 'tof';
                        require 'report-details-form.php';
                     } ?>                        
                </div>
                <!--<div id="summary" class="tab-pane fade in"><?php echo nl2br(trim($report_data->rep_contents)); ?></div>-->
            </div>
        </div><br/>        
    </div>
    <div class="col-md-4" style="background: #eee;height : 100%">
        <div class="">
            <div class="panel-purchase">
                <h3>Choose License Type</h3><br/>
                <form action="<?php echo base_url(); ?>checkout/<?php echo $report_data->rep_id; ?>" name="" method="post">
                    <input type="hidden" name="rep_title" value="<?php echo $report_data->rep_title; ?>" />
                    <input type="hidden" name="rep_id" value="<?php echo $report_data->rep_id; ?>"/>
                    <input type="hidden" name="publisher_id" value="<?php echo $report_data->publisher_id; ?>"/>
                    <div class="row">

                        <!-- <div class="col-md-4 col-display md-hidden" align="center">
                            <br/>
                            <span class="display"></span>
                            <br/><br/>
                        </div> -->
                        <div class="col-md-12 rd-license-12">
                            <ul class="list-group">
                                
                                <?php $i=0; foreach($report_license->result() as $row): ?>
                                <li class="list-group-item">
                                    <?php if($i==0){?>
                                    <label class="container-radio"><?php echo $row->li_key;?> 
                                        <input type="radio" name="report_price" value="<?php echo $row->li_key;?>-<?php echo $row->li_value;?>" checked /><span class="checkmark"></span>
                                    </label>                                        
                                    <span><?=@$row->li_value?></span>

                                    <?php  }else{ ?>
                                    <label class="container-radio"><?php echo $row->li_key;?> 
                                        <input type="radio" name="report_price"  value="<?php echo $row->li_key;?>-<?php echo $row->li_value;?>" /><span class="checkmark"></span>
                                    </label>
                                    <span><?=@$row->li_value?></span>

                                    <?php } ?>
                                </li>
                                <?php $i++; endforeach;?>
                                
                                <!-- <li style="background: none;padding:12px 12px 0px 12px;text-align: center" class="list-unstyled">
                                    <button type="submit" name="submit_todo"  class="btn-buynow btn-md">ADD TO CART&nbsp;&nbsp;<i class="fa fa-shopping-cart" aria-hidden="true"></i></button>
                                </li>-->
                            </ul>
                        </div>
                        <!-- <div class="col-md-4 col-display sm-hidden" align="center">
                            <br/><br/>
                            <span class="display"></span>
                            <br/>
                        </div>   -->
                    </div> 
                    <div align='center'>
                        <button type="submit" name="submit_todo"  class="btn btn-md btn-tab btn-buynow"><i class="fa fa-shopping-cart" aria-hidden="true"></i>&nbsp;&nbsp;Add To Cart</button> 
                    </div>
                </form>
            </div> 
            
            <hr class="hr-line"/>

            <div class="question-block" align='center'>
                <h5 class="p-t-10 p-b-10"><strong>Do you have any questions?</strong></h5>
                
                <a target="_blank" href='<?php echo base_url() ?>contact-us' class="btn btn-md btn-tab btn-email" >
                    <strong><i class="fa fa-envelope" aria-hidden="true"></i>&nbsp;&nbsp;Email Us</strong>       
                </a>
            </div> 
            
            
            <hr class="hr-line"/>
            
            <div class="custom-research" align='center'><br/>
                <h5 class="p-t-10 p-b-10"><strong>Custom Research</strong></h5>
                <p class="p-t-10 p-b-10 custom-text">Still haven't found what you're looking for ?<br/> Speak to our Custom Research Team</p>
                
                <a target="_blank" href='<?php echo base_url() ?>custom-research' class="btn btn-md btn-tab btn-email btn-enquire" >
                    <strong><i class="fa fa-search" aria-hidden="true"></i>&nbsp;&nbsp;Learn More</strong>       
                </a>
            </div> 
            
            <hr class="hr-line"/>
            
            <div class="why-us-block">
                <h3 class="cl5">Why ProspectResearch Reports</h3>
                <ul>
                    <li>500,000 market research reports and growing.</li>
                    <li>Top Fortune 500 Organizations trust us for research data.</li>
                    <li>24/7 support on call as well as emails.</li>
                    <li>Your Details are safe with us.</li>
                    <li>Free support for your research requirements.</li>
                    <li>Report Delivery: Email</li>
                    <li>Delivery Time:
                        <ul>
                            <li>Upto 24 hrs - working days</li>
                            <li>Upto 48 hrs max - weekends and public holidays</li>
                        </ul>
                    </li>
                </ul>
            </div> 
            
            <hr class="hr-line"/>

            <div class="why-us-block related-reports">
                <h3 class="cl5">Related Reports</h3>
                <ul>
                <?php
                if(@$related_reports->result()){
                    foreach($related_reports->result() as $row): 
                        $report_title = explode('Market',$row->rep_title);
                        if(isset($report_title[0]) && strlen($report_title[0]) == 0){
                            $report_title_array = explode(" ", $row->rep_title);
                            $first_word_length = strlen($report_title_array[0]);
                            $report_title = explode('Market',substr($row->rep_title, $first_word_length));
                        }
                        // echo "<pre>";print_r($report_title);exit;
                        $related_report_title = str_ireplace(array(' -','- ','-'),' - ',$report_title[0]).' Market';
                        
                    ?>
                    <li>
                        <a href='<?php echo base_url(); ?>reports/<?php echo $row->rep_id; ?>/<?php echo $row->rep_url; ?>'>
                            <?=strlen($related_report_title) > 100 ? substr($related_report_title , 0 ,100)."..." : $related_report_title;?>
                        </a>
                    </li>
                <?php endforeach;
                }else{ 
                ?>                
                    <li style="list-style: none;font-size:1.2rem" class="text-center">
                        <span class="text-danger"><strong><span class="fa fa-exclamation-triangle"></span> No Results Found !</strong></span>
                    </li>
                <?php  }  ?>
                </ul>
            </div><br/>
        </div>        
    </div>
</div>

  <!-- Slider : Testimonials -->
<section class="sec-product bg0 p-t-30 p-b-30 testimonial-block">
    <div class="container">
        <div class="p-b-32">
            <h3 class="ltext-105 cl5 txt-center respon1">
                Client Testimonials
            </h3>
        </div>

        <!-- Tab01 -->
        <div class="tab01">
            <!-- Tab panes -->
            <div class="tab-content p-t-50">
                <!-- - -->
                <div class="tab-pane fade show active" id="best-seller" role="tabpanel">
                    <div class="wrap-slick2">
                        <div class="slick2">                
                            <div class="item-slick2 p-l-15 p-r-15 p-t-15 p-b-15">
                                <div class="block2">
                                    <div class="block2-pic hov-img0 testi-img">
                                    <img src="<?=base_url()?>assets/images/male-avatar.png" alt="Testimonial Avatar">
                                    </div>
                                    <div class="block2-txt flex-w flex-t p-t-14">
                                        <div class="block2-txt-child1 flex-col-l ">
                                            <span class="stext-102 cl3">
                                            I am completely satisfied with the information given in the sample pages of the report and I am waiting for the affirmative response from my peers and colleagues to purchase it.hey answered to all my queries promptly. I am highly satisfied!!
                                            </span>

                                            <span class="mtext-107 cl5 hov-cl1 trans-04 js-name-b2 p-b-6 p-t-10">
                                            <b>John Keats</b>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>

                                
                            <div class="item-slick2 p-l-15 p-r-15 p-t-15 p-b-15">
                                <div class="block2">
                                    <div class="block2-pic hov-img0 testi-img">
                                    <img src="<?=base_url()?>assets/images/male-avatar.png" alt="Testimonial Avatar">
                                    </div>

                                    <div class="block2-txt flex-w flex-t p-t-14">
                                    <div class="block2-txt-child1 flex-col-l ">
                                        <span class="stext-102 cl3">
                                        The market research reports provided by PRR team are top-notch. They deliver accurate, in-depth analysis that helps us stay ahead of the competition. The professionalism and expertise shown in their reports have made them an indispensable resource for our business. Excellent work!
                                        </span>

                                        <span class="mtext-107 cl5 hov-cl1 trans-04 js-name-b2 p-b-6 p-t-10">
                                        <b>Olivia Garcia</b>
                                        </span>
                                    </div>
                                    </div>
                                </div>
                            </div>

                                
                            <div class="item-slick2 p-l-15 p-r-15 p-t-15 p-b-15">
                                <div class="block2">
                                    <div class="block2-pic hov-img0 testi-img">
                                    <img src="<?=base_url()?>assets/images/male-avatar.png" alt="Testimonial Avatar">
                                    </div>

                                    <div class="block2-txt flex-w flex-t p-t-14">
                                        <div class="block2-txt-child1 flex-col-l ">
                                            <span class="stext-102 cl3">
                                            I’m extremely satisfied with the market research reports we receive from PRR. The reports are not only rich in data but also presented in a clear and understandable format. They’ve become a crucial tool for our strategic planning. I can’t recommend them enough.
                                            </span>

                                            <span class="mtext-107 cl5 hov-cl1 trans-04 js-name-b2 p-b-6 p-t-10">
                                            <b>David Thompson</b>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="item-slick2 p-l-15 p-r-15 p-t-15 p-b-15">
                                <div class="block2">
                                    <div class="block2-pic hov-img0 testi-img">
                                    <img src="<?=base_url()?>assets/images/male-avatar.png" alt="Testimonial Avatar">
                                    </div>

                                    <div class="block2-txt flex-w flex-t p-t-14">
                                        <div class="block2-txt-child1 flex-col-l ">
                                            <span class="stext-102 cl3">
                                            I’ve been consistently impressed with the quality of market research reports provided by PRR. The insights are always detailed and actionable, making it easy for us to make informed decisions. Their reports have been instrumental in shaping our strategy and driving growth. Highly recommended!
                                            </span>

                                            <span class="mtext-107 cl5 hov-cl1 trans-04 js-name-b2 p-b-6 p-t-10">
                                            <b>Emily Johnson</b>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="item-slick2 p-l-15 p-r-15 p-t-15 p-b-15">
                                <div class="block2">
                                    <div class="block2-pic hov-img0 testi-img">
                                    <img src="<?=base_url()?>assets/images/male-avatar.png" alt="Testimonial Avatar">
                                    </div>

                                    <div class="block2-txt flex-w flex-t p-t-14">
                                        <div class="block2-txt-child1 flex-col-l ">
                                            <span class="stext-102 cl3">
                                            The market research reports from PRR have exceeded our expectations time and again. The data is comprehensive and well-organized, which has helped us identify key trends and opportunities in our industry. Excellent service with a high level of accuracy!
                                            </span>

                                            <span class="mtext-107 cl5 hov-cl1 trans-04 js-name-b2 p-b-6 p-t-10">
                                            <b>Michael Lee</b>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="item-slick2 p-l-15 p-r-15 p-t-15 p-b-15">
                                <div class="block2">
                                    <div class="block2-pic hov-img0 testi-img">
                                    <img src="<?=base_url()?>assets/images/male-avatar.png" alt="Testimonial Avatar">
                                    </div>

                                    <div class="block2-txt flex-w flex-t p-t-14">
                                        <div class="block2-txt-child1 flex-col-l ">
                                            <span class="stext-102 cl3">
                                            Working with PRR has been a game-changer for our business. Their reports are thorough, well-researched, and tailored to our specific needs. We’ve seen significant improvements in our market positioning thanks to their valuable insights. Fantastic job!
                                            </span>

                                            <span class="mtext-107 cl5 hov-cl1 trans-04 js-name-b2 p-b-6 p-t-10">
                                            <b>Sophia Martinez</b>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<script type="text/javascript">
$(".btn-avail").click(function(){
    var email=$("#DiscountEmail").val();
    var title="<?=trim($report_data->rep_title);?>";
    if(email !==""){
        var pattern=/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
        if(email.match(pattern)){
            // alert(email);
            $("#discountEmailError").html("");
            $.ajax({
                
                type:'post',
                url:'<?php echo base_url(); ?>request-discount',
                data:{email:email,title:title},
                
                success:function(data){
                    $("#discountEmailSuccess").html(data).append("<meta http-equiv='refresh' content='2,url=<?php echo base_url(); ?>reports/<?php echo $report_data->rep_id; ?>/<?php echo $report_data->rep_url; ?>' />");                   }
                
            });
        }else{
            $("#discountEmailError").html("Invalid Email Address");
        }
    }else{
        $("#discountEmailError").html("Please enter email address");
    }

})


</script>

<script src="<?=base_url()?>assets/vendor/slick-carousel/slick.min.js"></script>
    
<!--**********************    FOOTER OPEN      ***************************-->

<?php require_once 'layouts/footer.php'; ?>

<!--**********************    FOOTER CLOSE     ***************************-->

<script type="text/javascript">

$(document).ready(function(){
    $("input[name='report_price']:checked").parents(".list-group-item").addClass(" active")

    $("input[name='report_price']").change(function(){
        $(".list-group-item").removeClass("active")
        $(this).parents(".list-group-item").addClass(" active")
    })
       
    var w=$(window).width();
    if(w>767){
        $(window).scroll(function () {
            let doc_body_height = document.body.scrollHeight
            doc_body_height-=1000
            if ($(this).scrollTop() > 100 && $(this).scrollTop() < doc_body_height) {
                $('.request_sample a').removeClass("btn-tab").addClass(" animate_sample_btn");
            } else {
                $('.request_sample a').removeClass("animate_sample_btn").addClass(" btn-tab")
            }
        });
    }
})
</script>
