<!DOCTYPE html>
<html lang="en">
<head>
<title>404 Page Not Found</title>

<!--**********************    HEADER OPEN      ***************************-->

<?php require_once 'layouts/header.php'; ?>

<!--**********************    HEADER CLOSE     ***************************--> 

<!---******************************  ERROR MESSAGE  **************************** -->

<main class="error-page">
    <div class="container">
        <h1 class="error-code">404</h1>
        <p class="error-description">Oops! The page you are looking for does not exist. It might have been removed or deleted.Go back to home page now, or stay, it is quiet out here.</p>
        <p class="prr-homepage-link lead">
            <a class="btn btn-home" href="<?=base_url(); ?>" role="button">Back To Homepage</a>
        </p>
    </div>
</main>
<!---******************************  ERROR MESSAGE  **************************** -->



<!--**********************    FOOTER OPEN      ***************************-->

<?php require_once 'layouts/footer.php'; ?>

<!--**********************    FOOTER CLOSE     ***************************-->
