<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title><?php echo isset($meta->title) ? $meta->title : 'Terms and conditions | ProspectResearch Reports';?></title>
<!-- Basic -->
<meta charset="utf-8"/>
<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
<meta name="robots" content="index,follow"/>
<meta name="keywords" content="<?php echo isset($meta->keywords) ? $meta->keywords : 'Terms and conditions ProspectResearch Reports';?>" />
<meta name="description" content="<?php echo isset($meta->description) ? $meta->description : 'Terms and conditions ProspectResearch Reports';?>"/>
<meta name="author" content="ProspectResearch Reports"/>

<!--**********************    HEADER OPEN      ***************************-->

<?php require_once 'layouts/header.php'; ?>

<!--**********************    HEADER CLOSE     ***************************-->
  
<style>
    p, h3{
        line-height: 1.8 !important;
    }
</style>

<ul class="breadcrumb m-b-0">
    <li class="breadcrumb-item"><a href="<?=base_url();?>"><img src="<?=base_url().'assets/images/home.png'?>" title='Go To Home' height="30px" width="30px" alt="Home Breadcrumb Icon" /></a></li>
    <li class="breadcrumb-item">Terms &amp; Conditions</li>
</ul>


<!-- Title page -->
<section class="bg10 txt-center p-lr-15 p-tb-50 ">
    <h2 class="ltext-103 cl5 txt-center">
        Terms &amp; Conditions
    </h2>
</section>	


<!-- Content page -->
<section class="bg0 p-t-50 p-b-30">
    <div class="container">
        <div class="row p-b-30">
            <div class="col-md-12 col-lg-12 p-b-30">
                <div class="p-t-7 p-r-85 p-r-15-lg p-r-0-md res-challenge-div">
                    <h3 class="mtext-113 cl14 p-b-30">
                    Terms &amp; Conditions
                    </h3>
                    <h3 class="mtext-106 cl5 p-b-16">
                    Terms Of Use:
                    </h3>
                    <p class="stext-110 cl8 p-b-26">
                    Subject to those Terms of Use (this "Agreement"), our website and/or its subsidiaries, as applicable, (collectively and individually, "we", "our" or "us") make available certain on-line information and services on various websites ("Our Service") to registered and/or authorized users ("you" or "your"). Our Service presents information, data, content, news, reports, programs, video, audio and other materials and services, communications, transmissions and other items, tangible or intangible, which are brought up as "Material." Your use of Our Service constitutes your acknowledgment of and assent to be bound by this Agreement.<br/>
                    Unless there's another official document between you and us that covers your use of part or all of Our Service, this is often the whole agreement between you and us. If there's another legal document between you and us that covers your use of a part of Our Service, this Agreement covers all other use of Our Service by you. Whenever new products or services become available, your use of them are going to be under this Agreement unless we notify you otherwise or another official document covers your use of these new products or services. <br/>
                    In certain cases, additional "passthrough" terms and conditions may apply to the employment of third-party content, software or other services (collectively, "Third-Party Content"). Any such additional terms and conditions shall be delivered with applicable Third-Party Content. If upon reading such terms and conditions, you discover you're ineligible or unable to adjust to them through circumstances outside your control, then you're hereby required to contact us within twenty-four (24) hours of purchase (in the case of Instant Online Delivery content) or receipt (in the case of other content) to elucidate your ineligibility. An appropriate solution will figured out at that point. If a call isn't received within the allotted time, you're deemed to own approved and to be in compliance with the extra terms and conditions. <br/>
                    In addition, particular sites or features of Our Service may have different or additional terms ("Special Terms"), which is able to be disclosed to you once you access those sites or features. Such Special Terms are incorporated into this Agreement with reference to such sites or features. If there's a conflict between the terms of this Agreement and therefore the Special Terms, the Special Terms will govern with regard to such sites or features. <br/>
                    We may change this Agreement at any time and you'll be able to read a current copy of this Agreement at any time on our website. If any change isn't acceptable, you need to discontinue your use immediately; using Our Service after the date that this Agreement changes means you accept the changes. No change to the present Agreement that's not posted on the Service is valid unless it's in writing and signed by both you and us. 
                    </p>

                    <h3 class="mtext-106 cl5 p-b-16">
                    Permitted Use, Limitations on Use:
                    </h3>
                    <p class="stext-110 cl8 p-b-26">
                    You may access and download the fabric only pro re nata to look at the fabric on your browser for your individual use, keeping all copyright and other notices on the fabric. you will print one copy of fabric for your use. you'll not republish or distribute any Material or do the rest with the fabric, which isn't specifically permitted during this Agreement. You comply with suits all notices and requirements accompanying Third-Party Content (see paragraph 3 under Terms of Use).
                    </p>

                    <h3 class="mtext-106 cl5 p-b-16">
                    Anti-hacking Provision:
                    </h3>
                    <p class="stext-110 cl8 p-b-26">
                    You may not, nor may you permit others to, directly or indirectly: (a) try to or actually disrupt, impair or interfere with, alter or modify Our Service or any Material; or (b) collect or try and collect any information of others, including passwords, account or other information.
                    </p>

                    <h3 class="mtext-106 cl5 p-b-16">
                    No Advice:
                    </h3>
                    <p class="stext-110 cl8 p-b-26">
                    The Material available on Our Service is for informational purposes only.
                    </p>

                    <h3 class="mtext-106 cl5 p-b-16">
                    Links to 3rd Party Sites:
                    </h3>
                    <p class="stext-110 cl8 p-b-26">
                    Various links on Our Service will take you out of Our Service. These linked sites aren't necessarily under our control. We don't seem to be accountable for the contents of any linked page or the other page not under our control. we offer these links only as a convenience; the inclusion of a link doesn't imply endorsement of that linked site.
                    </p>

                    <h3 class="mtext-106 cl5 p-b-16">
                    Limitation of Liability:
                    </h3>
                    <p class="stext-110 cl8 p-b-26">
                    You are entirely responsible for activities conducted by you or anyone else in reference to your browsing and use of Our Service. If you're dissatisfied with the fabric or Our Service or with these Terms of Use, your sole and exclusive remedy is to prevent using the fabric and Our Service. we'll not pay you any damages. <br/>               
                    We don't warrant the accuracy, completeness, currentness or other characteristics of any Material available on or through Our Service. we are going to not be to blame for any loss or injury resulting directly or indirectly from Our Service, whether or not caused in whole or partly by our negligence or by contingencies within or beyond our control. Neither we, nor suppliers of Third-Party Content, are responsible or liable, directly or indirectly, for any loss or damage caused by use of or reliance on or inability to use or access Our Service or the fabric. <br/>
                    <q>your access to and use of our service are at your sole risk. our service is provided "as is" and "as available." our service is for your personal use only and that we make no representation or warranty of any kind, express or implied. we expressly disclaim any warranties of merchant ability or fitness for a specific purpose or use.<br/>
                    we don't seem to be and can not be a celebration to any transaction between you and any third party, whether or not that third party's website is linked from our service.</q>
                    </p>

                    <h3 class="mtext-106 cl5 p-b-16">
                    Governing Law:
                    </h3>
                    <p class="stext-110 cl8 p-b-26">
                    The laws of India govern this Agreement and your use of Our Service. You comply with fits all laws, regulations, obligations and restrictions, which apply to you. You agree that the courts located in India have exclusive jurisdiction for any claim, action or dispute under this Agreement. you furthermore mght agree and expressly consent to the exercise of non-public jurisdiction in India. No failure or delay in enforcing any right shall be a waiver of that or the other right. If any term of this Agreement is held invalid, illegal or unenforceable, the remaining portions shall not be affected.
                    </p>

                    <h3 class="mtext-106 cl5 p-b-16">
                    Copyright:
                    </h3>
                    <p class="stext-110 cl8 p-b-26">
                    Unless specifically stated in conjunction with particular Material, all Material is copyrighted by us. you've got no rights in or to the fabric and you will not use any Material aside from as permitted under this Agreement.
                    </p>

                    <h3 class="mtext-106 cl5 p-b-16">
                    Trademark:
                    </h3>
                    <p class="stext-110 cl8 p-b-26">
                    All trade names, trademarks, service marks and other product and repair names and logos on Our Service or within the Material are the proprietary trademarks of their respective owners and are protected by applicable trademark and copyright laws.
                    </p>

                    <h3 class="mtext-106 cl5 p-b-16">
                    Return Policy:
                    </h3>
                    <p class="stext-110 cl8 p-b-26">
                    Due to the nature of the knowledge being sold, we unfortunately cannot accept returns of products once they need been delivered.<br/>
                    Please make sure to read all available information a few report before you place your order.<br/>
                    If you've got any questions about a report's coverage or relevance, simply contact us for expert assistance from a look Specialist.
                    </p>

                    <h3 class="mtext-106 cl5 p-b-16">
                    Contact  Information:
                    </h3>
                    <p class="stext-110 cl8 p-b-26">
                    Need help regarding the Terms of Service ?  <a href="mailto:help@prospectresearchreports.com" class="bg7 cl0 mtext-103 p-t-10 p-r-10 p-b-10 p-l-10 prr-static-btn" >Get Help</a>
                    </p>


                </div>
            </div>
        </div>
    </div>
</section>


<!--**********************    FOOTER OPEN      ***************************-->

<?php require_once 'layouts/footer.php'; ?>

<!--**********************    FOOTER CLOSE     ***************************-->
