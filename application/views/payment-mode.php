<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title><?php echo isset($meta->title) ? $meta->title : 'Mode of Payments | ProspectResearch Reports';?></title>
<!-- Basic -->
<meta charset="utf-8"/>
<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
<meta name="robots" content="index,follow"/>
<meta name="keywords" content="<?php echo isset($meta->keywords) ? $meta->keywords : 'Mode of Payments ProspectResearch Reports,Payment Modes ,Transaction modes ,Transaction Modes,Payment Methods';?>" />
<meta name="description" content="<?php echo isset($meta->description) ? $meta->description : 'ProspectResearch Reports offers various payment methods like Bank Wire Transfer,Online Payment Systems like PayPal, Paytm, Google Pay and Credit Cards.';?>"/>
<meta name="author" content="ProspectResearch Reports"/>

<!--**********************    HEADER OPEN      ***************************-->

<?php require_once 'layouts/header.php'; ?>

<!--**********************    HEADER CLOSE     ***************************-->

<style>
    p, h3{
        line-height: 1.8 !important;
    }
</style>

<ul class="breadcrumb m-b-0">
    <li class="breadcrumb-item"><a href="<?=base_url();?>"><img src="<?=base_url().'assets/images/home.png'?>" title='Go To Home' height="30px" width="30px" alt="Home Breadcrumb Icon" /></a></li>
    <li class="breadcrumb-item">Payment Modes</li>
</ul>


<!-- Title page -->
<section class="bg10 txt-center p-lr-15 p-tb-50 ">
    <h2 class="ltext-103 cl5 txt-center">
        Payment Modes
    </h2>
</section>	


<!-- Content page -->
<section class="bg0 p-t-50 p-b-30">
    <div class="container">
        <div class="row p-b-30">
            <div class="col-md-12 col-lg-12 p-b-30">
                <div class="p-t-7 p-r-85 p-r-15-lg p-r-0-md res-challenge-div">
                    <h3 class="mtext-113 cl14 p-b-30">
                    Payment Modes
                    </h3>
                    <p class="mtext-106 cl8 p-b-5">
                    After selection of report, the client can make payment through following mediums:
                    </p>
                    <ul class="list-item">
                        <li>Bank Transfer</li>
                        <li>Net banking</li>
                        <li>Payment Wallet (Paytm/UPI/BHIM)</li>
                        <li>Online Payment via Credit Card/Debit Card (Master Card, Visa Card, American Express Cards).</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>

<!--**********************    FOOTER OPEN      ***************************-->

<?php require_once 'layouts/footer.php'; ?>

<!--**********************    FOOTER CLOSE     ***************************-->

