<div class="row">
    <div class="col-md-12">
        <div class="content-block-shadow">
            <div class="panel-body">
                <div class='form-side-block contact-info'>
                    <p class="form-title txt-center">*Please fill the following form to know more details about this Report!</p>
                </div>
                <?php
                    $invalid_message=(@$message) ? @$message : '';
                    if(!empty(@$invalid_message)){
                        echo "<div class='alert alert-danger alert-message mt-2'>".@$invalid_message."</div>";
                    }
                ?>
                <form class="form-horizontal rd-form form p-t-20 p-b-20" id="form<?=@$type?>" role="form" action="<?php echo base_url(); ?>contact-form-process" method="post" data-toggle="validator" onsubmit="return validate_captcha('<?=@$type?>')" >
                    <input type='hidden' name='rep_title' value='<?php echo $report_data->rep_title; ?>' />
                    <input type='hidden' name='rd_type' value='<?=@$type?>' />
                    <input type='hidden' name='rep_id' value='<?=@$report_data->rep_id;?>' />
                    <div class="row">
                        <div class="form-group col-sm-6">
                            <label class="control-label">Name <span class="star">*</span> :</label>
                            <input class="form-control" name="name" id="name"  placeholder="Your Name" data-bv-field="full_name" type="text" value="<?=@$name?>" data-error="Please enter your name" required/>
                            <div class="help-block with-errors"></div> 
                        </div>
                        <div class="form-group col-sm-6">
                            <label class="control-label">Email <span class="star">*</span> :</label>
                            <input class="form-control" name="email" id="email"  placeholder="Business Email" data-bv-field="email" type="email" value="<?=@$email?>" data-error="Please enter valid email address" required/>
                            <div class="help-block with-errors"></div> 
                        </div>

                        <div class="form-group col-sm-6">
                            <label class="control-label">Company <span class="star">*</span> :</label>
                            <input class="form-control" name="company" id="company" placeholder="Company Name" data-bv-field="company" type="text" value="<?=@$company?>" data-error="Please enter company / organization name"   required/>
                            <div class="help-block with-errors"></div> 
                        </div>
                        <div class="form-group col-sm-6">
                            <label class="control-label">Job Role <span class="star">*</span> :</label>
                            <input class="form-control" name="job_role" id="job_role" value="<?=@$job_role?>" placeholder="Job Role" data-bv-field="job_role" type="text" data-error="Please mention job role"  required/>
                            <div class="help-block with-errors"></div> 
                        </div>
                        <div class="form-group col-sm-6">
                            <label class="control-label">Country <span class="star">*</span> :</label>
                            <select name="country" class="form-control selectpicker" data-bv-field="country" data-live-search="true" data-actions-box="true" data-error="Please select your country"  required>
                                <?=getCountryDropdown()?>
                            </select>
                            <div class="help-block with-errors"></div> 
                        </div>
                        <div class="form-group col-sm-6">
                            <label class="control-label">Contact Number <span class="star">*</span> :</label>
                            <input class="form-control"  pattern="^[0-9]{10}$" name="phone" id="phone" value="<?=@$phone?>" maxlength="10" placeholder="Contact Number without country code" data-bv-field="phone" type="text" data-error="Please enter contact number" required/>
                            <div class="help-block with-errors"></div> 
                        </div>
                        <div class="form-group col-sm-12">
                            <label class="control-label">Message  :</label>
                            <textarea  maxlength='200'  placeholder="Your Message" rows="5"  class="form-control" name="message" id="message_<?=@$type?>"><?=@$contact_msg?></textarea>
                        </div>
                        <div class="form-group col-sm-6">
                            <label class="control-label" for="security_code">Security Code: <span class="star">*</span>: </label>
                            <div class="input-group">
                                <input data-error="Please enter security code" onkeyup="validate_captcha('<?=@$type?>')" type="text" name="captcha_code" maxlength="6" placeholder="Security Code" class="form-control" id="security_code_<?=@$type?>"  required/>
                                <input type="text" id="txtCaptcha_<?=@$type?>" readonly class="form-control txtCaptcha" />
                                <span class="input-group-append">
                                    <button class="btn btn-captcha" type="button" onClick="DrawCaptcha('<?=@$type?>');"> <span class="fa fa-refresh"></span> </button>
                                </span> 
                            </div>
                            <p class="feedback" id="CaptchaError_<?=@$type?>"></p>
                            <div class="help-block with-errors"></div>
                        </div>
                        <div class="form-group col-sm-12 m-t-10">
                            <button type="submit" class="btn btn-default btn-submit">
                                <span class="fa fa-send-o"></span> Submit
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>