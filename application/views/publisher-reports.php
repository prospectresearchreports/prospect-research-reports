<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <title><?php echo $publisher_name; ?> Reports | ProspectResearch Reports</title>
        <!-- Basic -->
        <meta charset="utf-8"/>
        <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
        <meta name="robots" content="index,follow"/>
        <meta name="keywords" content="ProspectResearch Reports reports,  reports" />
        <meta name="description" content="Company Reports,  company reports"/>
        <meta name="author" content="ProspectResearch Reports"/>
        
        <!--**********************    HEADER OPEN      ***************************-->
        
        <?php require_once 'layouts/header.php'; ?>
        
        <!--**********************    HEADER CLOSE     ***************************-->

        <style type="text/css">
             @media screen and  (min-width:440px) and (max-width:767px){
                .page-number{
                    margin-top:4%;
                }
            }
            @media screen and  (max-width:440px){
                .page-number{
                    margin-top:8%;
                }
            }
            @media screen and (max-width:370px){
                .company-report-button button.btn-primary{
                    margin-bottom: 10px;
                }
            }
            @media screen and (min-width:767px){
                .page-number{
                    margin-top:2%;
                }
                .button-group{
                    float:right;
                }
            }   
            @media screen and (max-width:767px){
                .text-category-banner-left > h1{
                    font-size: 1.3em;
                    text-align: center;
                }
            }
            .page-number,.page-no{
                font-size: 1.2em;
            }
            div.panel-body{
                color: #000;
                border: 1px solid rgb(0,0,0,0.2);
            }
            span.region{
                color: #000;
            }
            .plus{
                color: #000;
                font-size: 1.1em;
                cursor: pointer;
                padding: 5px;
            }
            ul.country li{
                list-style-type: none;
                margin-left: -5%;
                padding: 5px;
            }
            ul.country > li > span{
                margin-left: 3%;
            }
            span.fa-plus{
                color:#000;
                font-size:0.8em;
                margin-right:5px;
            }
            
        </style>
        <script>
            $(document).ready(function(){
                $(".list").removeClass("country-list");
                $(".report-data-3").addClass("sticky");
                $(".fa-plus").removeClass("fa-minus");
                
            })
               
           function getReports(id){
               
               var region = $(".region"+id).html();
                
               //alert(region);
               if(region != ""){
                   
                  window.location.href="<?php echo base_url(); ?>publisher/<?php echo $publisher_url; ?>/region/"+region;
                }
           }
            
            
            function getCountry(id){
                
                if($(".plus"+id).hasClass("fa-plus")){                  
                    
                 
                var region=$(".region"+id).html();

                //alert(region);

                $.ajax({
                   type:'post',
                   url:'<?php echo base_url(); ?>country-list',
                   data:{region:region},
                   success:function(data){
                        $("#list"+id).html(data);
                        $(".list"+id).addClass("country-list").css({'display':'block'});
                        $(".report-data-3").removeClass("sticky");
                        $(".plus"+id).removeClass("fa-plus").addClass("fa-minus");
                   }
                });
                
                }else{
                    if($(".plus"+id).hasClass("fa-minus")){
                        $(".list"+id).removeClass("country-list").css({'display':'none'});
                        $(".plus"+id).removeClass("fa-minus").addClass("fa-plus");
                    }
                }  
            }
        
            
        </script>


        <div class="breadcrumb-div">
            <div class="container">
                <ul class="breadcrumb" style="margin-bottom:0">
                    <li><a href="<?php echo base_url(); ?>" style="font-size:20px"><img src="<?php echo base_url();?>assets/images/home.jpg"  height="30px" width="40px"/></a></li>
                    <li><?php echo $publisher_name; ?> Reports</li>
                </ul>
            </div>
        </div>
        <div style="margin-bottom:20px;text-align: center;color: #000;">
            <h1><?php echo $publisher_name; ?> Reports</h1>
            <hr style="border-bottom: 2px dotted chocolate;width:330px"/>            
        </div> 
        <br/>
        
        <div class="container">
            <div class="clearfix">
                <div style="display:inline-block"><?php if (isset($pagination)) echo $pagination; ?> </div>
                <div class="pull-right page-number"  style="display:inline-block">
                    <?php if (isset($total_reports)) {if ($total_reports > 15) { ?> Page  <?php if (isset($page)) echo $page; ?> of <?php if (isset($total_pages)) echo $total_pages; } } ?>
                </div>
            </div>
            <div align="center">
            <?php
            if (isset($total_reports)) {
                if ($total_reports > 0) {
                    if (isset($helpText)) {
                        echo $helpText."<br/><br/>";
                    }
                } 
            }
            else {
                    if (isset($errorText)) {
                        echo $errorText;
                    }
                }
            ?>
            </div>
            <div class="row">
                <div class="col-md-3 report-data-3">
                    <div class="panel-info">
                        <center><button type="button"  class="btn-filter btn-md">FILTER BY YEAR&nbsp;&nbsp;</button></center>
                        <div class="panel-body filter-content">
                            <form name="yearForm">
                                <div class="radio" id="yRadio1" >
                                    <label><input type="radio" name="year" value="2021" onclick="filterYearReport(this.value)" <?php if(isset($report_year)){ if($report_year==='2021'){ echo "checked"; } } ?>/><b>2021</b></label>
                                </div>
                                <div class="radio" id="yRadio1" >
                                    <label><input type="radio" name="year" value="2020" onclick="filterYearReport(this.value)" <?php if(isset($report_year)){ if($report_year==='2020'){ echo "checked"; } } ?>/><b>2020</b></label>
                                </div>                 
                            </form>
                        </div>
                    </div><br/>
                    <div class="panel-info">
                        <center><button type="button"  class="btn-filter btn-md">FILTER BY REGION&nbsp;&nbsp;</button></center>
                        <div class="panel-body filter-content">
                            <form name="form" id="form" method="get">
                                <div>
                                    <?php foreach($region->result() as $row){
                                            echo '<div><p class="plus" onclick="getReports('.$row->id.')">';
                                            if(isset($current_region)){
                                                if($current_region === $row->region){
                                                    echo'<span class="fa fa-check" style="color:green"></span>';
                                                }
                                            }
                                            echo'&nbsp;<span class="region'.$row->id.'">'.$row->region.'</span></p>
                                            </div>';
                                            //echo '<div id="list'.$row->id.'" class="list list'.$row->id.' country-list"></div>';

                                    } ?>
                                </div>                     
                            </form>
                        </div>
                    </div><br/><br/><br/>
                </div>
                <?php if ($total_reports > 0) {?>
                <div class="col-md-9 report-data-9">
                    <?php if (isset($report_list)) { ?>
                    <div id="report-details">
                        <?php foreach ($report_list->result() as $row) {

                            $report_url=str_ireplace('%2f','-', $row->rep_url);
                         ?>
                        <div class="report-details-block"><br/>
                            <div>
                               <div class="row ">
                                    <div class="col-md-2" align="center">
                                        <img class="img-responsive"  onclick="window.location.href='<?php echo base_url();?>reports/<?php echo $row->rep_id; ?>/<?php echo $row->rep_url; ?>'" style="height:130px;width:110.22px;" src="<?php echo base_url(); ?>assets/images/category/<?php echo $row->category_image; ?>" alt="<?php echo $row->rep_title; ?>"  />
                                    </div>
                                    <div class="col-md-10">
                                        <p style="font-size:1.25em;font-weight: bold">
                                            <a href="<?php echo base_url(); ?>reports/<?php echo $row->rep_id; ?>/<?php echo $report_url; ?>" style="text-decoration:none;color:#000" rel="follow"><?php echo substr($row->rep_title,0,70)."..."; ?>
                                            </a>
                                        </p>

                                        <p style="padding:3px;font-size: 1.15em;"><?=substr($row->rep_descrip, 0, 100).".." ?></p>

                                        <div style="padding:3px;color:#000;;display: inline-block" class="report-data-content">
                                            <h5 style="font-size:1.15em">
                                                <span> Published :
                                                    <?php $date = date_create($row->rep_date);
                                                    echo date_format($date, 'M Y'); ?>
                                                </span> <strong style="color:black">|</strong>&nbsp;
                                                <span>From: <?php if ($row->li_value > 0) {
                                                        echo "$" . $row->li_value;
                                                    } ?>
                                                </span>
                                            </h5>
                                        </div>
                                        <div style="display: inline-block;" class="button-group">
                                            <button class="btn btn-primary btn-details"  onclick="window.location.href='<?php echo base_url();?>reports/<?php echo $row->rep_id; ?>/<?php echo $report_url; ?>'">View Report</button>
                                        </div>
                                    </div>
                                </div>
                            </div><hr style="border:1px solid #eee"/>
                        </div>
                        <?php } ?>
                    </div>
                    <?php } ?>
                    
                </div>
                <?php }else{
                    
                    echo  " <div class='col-md-9'>
                                <div class='jumbotron text-jumbotron'>
                                    <div class='report-text'><span class='fa fa-warning'></span> Sorry ! No Results Found</div>
                                </div>
                            </div>";
                    
                } ?>
            </div>
            <div class="clearfix page-no">
                <div style="display:inline-block"><?php if (isset($pagination)){ echo $pagination; }?> </div>
            </div>
        </div>

        <script type="text/javascript">
            function filterYearReport(year){
                window.location.href = "<?php echo base_url(); ?>publisher/<?php echo $publisher_url; ?>/reports?year=" + year;
          }
            function filterRegionReport(region){
                window.location.href = "<?php echo base_url(); ?>publisher/<?php echo $publisher_url; ?>/reports?region=" + region;
            }
        </script>
        
        
<!--**********************    FOOTER OPEN      ***************************-->

<?php require_once 'layouts/footer.php'; ?>

<!--**********************    FOOTER CLOSE     ***************************-->

