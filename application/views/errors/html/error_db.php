<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$CI =& get_instance();
if($CI && $CI->load->database()){
    $CI->load->database();
}
$error_code = 0;
if($CI && @$CI->db->error()){
    $error_code = @$CI->db->error()['code'];
}
// echo "DB - ". $error_code;die;
if(ENVIRONMENT === 'development') {
?>

<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<title>Database Error</title>
<style type="text/css">

::selection { background-color: #E13300; color: white; }
::-moz-selection { background-color: #E13300; color: white; }

body {
	background-color: #fff;
	margin: 40px;
	font: 13px/20px normal Helvetica, Arial, sans-serif;
	color: #4F5155;
}

a {
	color: #003399;
	background-color: transparent;
	font-weight: normal;
}

h1 {
	color: #444;
	background-color: transparent;
	border-bottom: 1px solid #D0D0D0;
	font-size: 19px;
	font-weight: normal;
	margin: 0 0 14px 0;
	padding: 14px 15px 10px 15px;
}

code {
	font-family: Consolas, Monaco, Courier New, Courier, monospace;
	font-size: 12px;
	background-color: #f9f9f9;
	border: 1px solid #D0D0D0;
	color: #002166;
	display: block;
	margin: 14px 0 14px 0;
	padding: 12px 10px 12px 10px;
}

#container {
	margin: 10px;
	border: 1px solid #D0D0D0;
	box-shadow: 0 0 8px #D0D0D0;
}

p {
	margin: 12px 15px 12px 15px;
}
</style>
</head>
<body>
	<div id=>
		<h1><?php echo $heading; ?></h1>
		<?php echo $message; ?>
	</div>
</body>
</html>

<?php }elseif(ENVIRONMENT === 'production') { ?>
<!DOCTYPE html>
<html lang="en">
<head>
<title><?=@$error_code == '2002' ? '500 | Internal Server Error' : '404 | Page Not Found'?></title>
</head>

<style type="text/css">
    .wrap{
        text-align: center;
        background-color: rgb(0, 0, 0, 0.6);
    }
    .wrap > h3{
        color: #f51904;
        background-color: #efefef;
        line-height: 1.8;
        padding: 20px;
        font-size: 1.6em;
    }
    .wrap > h2{
        color: #f51904;
        line-height: 1.8;
        padding: 20px;
        font-size: 2em;
    }
    .wrap h1{
        font-size: 60px;
        color: #f51904;
        text-transform: capitalize;
        font-weight: 900;
        line-height: 2;
    }
    @media screen and (min-width:767px){
        body{
            min-height: 600px;
            width: 100%;
        }
        .wrap{
            margin-top: 12%;
        }
    }
    @media screen and (max-width:767px){
        body{
            min-height: 400px;
        }
    }
    body{
        overflow: hidden;
        background: url('<?=BASE_URL?>assets/images/track-bg.jpg') center center no-repeat #090d2b;
        background-attachment: fixed;
        width: 100%;
        font-family: 'calibri';
    }
    
</style>


<!---******************************  ERROR MESSAGE  **************************** -->

<body>
<div class="jumbotron">
    <?php if(@$error_code == '2002') { ?>
    <div class="container wrap">
        <h1>Internal Server Error</h1>
        <h2>Oops ! Something went wrong ! This is not YOU . This is Us</h2>
        <h3>We are working on it to server you better !</h3>
    </div>
    <?php exit; }else{  header("location:".BASE_URL."404"); } ?>
</div>
</body>

<!---******************************  ERROR MESSAGE  **************************** -->

<?php } ?>