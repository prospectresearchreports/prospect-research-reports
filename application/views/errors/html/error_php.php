<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$CI =& get_instance();
if($CI && $CI->load->database()){
    $CI->load->database();
}
$error_code = 0;
if($CI && @$CI->db->error()){
    $error_code = @$CI->db->error()['code'];
}
// echo 'PHP - '.$error_code;die;
if(ENVIRONMENT === 'development') {
?>

<div style="border:1px solid #990000;padding-left:20px;margin:0 0 10px 0;">

<h4>A PHP Error was encountered</h4>

<p>Severity: <?php echo $severity; ?></p>
<p>Message:  <?php echo $message; ?></p>
<p>Filename: <?php echo $filepath; ?></p>
<p>Line Number: <?php echo $line; ?></p>

<?php if (defined('SHOW_DEBUG_BACKTRACE') && SHOW_DEBUG_BACKTRACE === TRUE): ?>

	<p>Backtrace:</p>
	<?php foreach (debug_backtrace() as $error): ?>

		<?php if (isset($error['file']) && strpos($error['file'], realpath(BASEPATH)) !== 0): ?>

			<p style="margin-left:10px">
			File: <?php echo $error['file'] ?><br />
			Line: <?php echo $error['line'] ?><br />
			Function: <?php echo $error['function'] ?>
			</p>

		<?php endif ?>

	<?php endforeach ?>

<?php endif;
exit; ?>

</div>
<?php }elseif(ENVIRONMENT === 'production') { ?>
<!DOCTYPE html>
<html lang="en">
<head>
<title><?=@$error_code == '2002' ? '500 | Internal Server Error' : '404 | Page Not Found'?></title>
</head>

<style type="text/css">
    .wrap{
        text-align: center;
        background-color: rgb(0, 0, 0, 0.6);
    }
    .wrap > h3{
        color: #f51904;
        background-color: #efefef;
        line-height: 1.8;
        padding: 20px;
        font-size: 1.6em;
    }
    .wrap > h2{
        color: #f51904;
        line-height: 1.8;
        padding: 20px;
        font-size: 2em;
    }
    .wrap h1{
        font-size: 60px;
        color: #f51904;
        text-transform: capitalize;
        font-weight: 900;
        line-height: 2;
    }
    @media screen and (min-width:767px){
        body{
            min-height: 600px;
            width: 100%;
        }
        .wrap{
            margin-top: 12%;
        }
    }
    @media screen and (max-width:767px){
        body{
            min-height: 400px;
        }
    }
    body{
        overflow: hidden;
        background: url('<?=BASE_URL?>assets/images/track-bg.jpg') center center no-repeat #090d2b;
        background-attachment: fixed;
        width: 100%;
        font-family: 'calibri';
    }
    
</style>


<!---******************************  ERROR MESSAGE  **************************** -->

<body>
<div class="jumbotron">
    <?php if(@$error_code == '2002') { ?>
    <div class="container wrap">
        <h1>Internal Server Error</h1>
        <h2>Oops ! Something went wrong ! This is not YOU . This is Us</h2>
        <h3>We are working on it to server you better !</h3>
    </div>
    <?php exit; }else{  header("location:".BASE_URL."404"); } ?>
</div>
</body>

<!---******************************  ERROR MESSAGE  **************************** -->

<?php } ?>