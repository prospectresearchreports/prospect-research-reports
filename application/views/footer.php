<footer>
    <div style="background: #191970"><br/>
        <div class="container"><br/>
            
            <div class="row footer-menu"> 
                <div class="col-md-3 footer-menu-list">
                    <h4>COMPANY</h4>
                    <ul class="list">
                        <li><a href="<?php echo base_url(); ?>about-us">ABOUT US</a></li>
                        <li><a href="<?php echo base_url(); ?>terms-and-conditions">TERMS &amp; CONDITIONS</a></li>
                        <li><a href="<?php echo base_url(); ?>privacy-policy">PRIVACY POLICY</a></li>
                        <li><a href="<?php echo base_url(); ?>careers">CAREERS</a></li>
                    </ul>
                </div>               
                <div class="col-md-3 footer-menu-list">
                    <h4>OUR PRODUCTS</h4>
                    <ul class="list">
                        <li><a href="<?php echo base_url(); ?>categories">INDUSTRIES</a></li>
                        <li><a href="<?php echo base_url(); ?>latest-reports">INDUSTRY REPORTS</a></li>
                        <li><a href="<?php echo base_url(); ?>press-release">PRESS RELEASES</a></li>
                    </ul>
                </div>
                <div class="col-md-3 footer-menu-list">
                    <h4>CUSTOMER SUPPORT</h4>
                    <ul class="list">
                        <li><a href="<?php echo base_url(); ?>contact-us">CONTACT US</a></li>
                        <li><a href="<?php echo base_url(); ?>custom-research">CUSTOM RESEARCH</a></li>
                        <li><a href="<?php echo base_url(); ?>return-policy">RETURN POLICY</a></li>
                        <li><a href="<?php echo base_url(); ?>payment-mode">PAYMENT MODES</a></li>
                    </ul>
                </div>
                <div class="col-md-3 footer-menu-list">
                    <h4>SOCIAL</h4>
                    <ul class="list">
                        <li><a href="<?php echo base_url(); ?>sitemap.html">SITEMAP</a></li>
                        <ul class="social-link">
                            <li class="twitter"  title="Twitter "><a href="https://twitter.com/ProspectResear3" target="_blank" ><i class="fab fa-twitter"></i></a></li>
                            <li class="blogger"    title="Blogger"><a href="https://www.blogger.com/u/6/blog/posts/6260696524988884741" target="_blank" ><i class="fab fa-blogger"></i></a></li>
                            <li class="linkedin" title="LinkedIn"><a href="#" target="_blank"><i class="fab fa-linkedin"></i></a></li>
                            <li class="blogger" title="Medium"><a href="https://medium.com/@prospectresearchreports" target="_blank"><i class="fa fa-bullhorn"></i></a></li>
                        </ul>
                    </ul>
                </div>

            </div>
            <hr style="border-top:1px solid #eee"/>

            <div class="row">
                <div class="col-md-12" align="center">
                    <p class="copyright">Copyright <span class="fa fa-copyright"></span>  <?php echo date("Y"); ?> &nbsp;| &nbsp; ProspectResearch Reports&nbsp; |&nbsp; All Rights Reserved &nbsp; </p>
                </div>
            </div><br/>

            <!--<h5 class="disclaimer"> <b>Legal Disclaimer : -  </b><span>We  have the right to reveal your personally identifiable information as necessary by law and when we trust that disclosure is needed to safeguard our rights and/or to comply with judicial proceedings, court order, or legal process served on our website.</span></h5>-->

        </div><br/>
        <?php if(!isset($_COOKIE['cookie_consent'])){ ?>
        <br/>
        <?php } ?>
        

        <div class="footer-fixed-block">
            <div class="container-fluid">
                <h3>We use cookies to ensure that we give you the best experience on our website. If you continue to use this site we will assume that you are happy with it. Please read our <a class="blue" href="<?php echo base_url(); ?>privacy-policy" target="_blank">privacy policy</a><br class="md-hidden" />
                <button class="btn-dismiss"><span class="fa fa-close"></span></button>
                <button class="btn-accept">ACCEPT</button>

                </h3>
            </div>            
        </div>
        <p id="back-top">
            <a href="#top"><span class=" fas fa-chevron-circle-up" style="font-size:35px"></span></a>
        </p>
    </div>
    
</footer>
<style type="text/css">
    @media screen and (min-width: 767px){
        .footer-form .form-control{
            width:40%;
            border-radius: 0px;
            height: 40px;        
        }
        .footer-form .form-control, .footer-form button{
            float:left;
            margin: 0px 15px;
        }
        .footer-form span {
            float:left;
            margin: 7px 25px 0px 5px;
        }
        .footer-form button{
            height: 40px;
        }  
    }

.footer-fixed-block  h3{    
    bottom: 0;
    font-size: 1.15em;
}
</style>

<script type="text/javascript">
    var path = "<?=str_ireplace("/index.php","",current_url())?>";
    var href=$(' a[href="'+path+'"]').addClass('active');


    // fade in #back-top
    $("#back-top").hide();

    $(function () {
        $(window).scroll(function () {
            if ($(this).scrollTop() > 100) {
                    $('#back-top').fadeIn();
            } else {
                    $('#back-top').fadeOut();
            }
        });
            // scroll body to 0px on click
        $('#back-top a').click(function () {
            $('body,html').animate({
                    scrollTop: 0
            }, 800);
            return false;
        });
    });
    <?php if(isset($_COOKIE["cookie_consent"])){
        echo "$('.footer-fixed-block').css({'display':'none'})";
    } ?>

    $(".btn-dismiss").click(function(){
        document.cookie = "cookie_consent=decline; path=/; max-age="+24*60*60;
        location.reload(true);           
    })
    $(".btn-accept").click(function(){
        document.cookie = "cookie_consent=accept; path=/; max-age="+365*24*60*60;
        location.reload(true);           
    })
</script>
</body>
</html>