<!DOCTYPE html>
<html>
<head>
<!-- Basic -->
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title><?php echo isset($meta->title) ? $meta->title : 'Payment Successful - ProspectResearch Reports';?></title>
<meta name="keywords" content="<?php echo isset($meta->keywords) ? $meta->keywords : 'Paypal Payment ProspectResearch Reports';?>" />
<meta name="description" content="<?php echo isset($meta->description) ? $meta->description : 'Paypal Payment Received | ProspectResearch Reports. Thank You for making payment';?>"/>
<meta name="author" content="ProspectResearch Reports"/>


<!--**********************    HEADER OPEN      ***************************-->

<?php require_once 'layouts/header.php'; ?>

<!--**********************    HEADER CLOSE     ***************************-->



<style>
    
    article.paypal p{
        font-size: 1.2em;
    }
    .paypal h1{
        font-size: 2em;
        color: #0465ac;
    }
    a.btn-home{
        border-radius: 0px;
        font-size:1em;
        ;
        background: #000;
        text-transform: uppercase;
    }
        
   
</style>
<div class= align="center">
    <div class="row">
        <div class="col-md-12">
            <article class="paypal">
                <br/><h1>We received your payment with thanks  </h1><br/>
                <p>Thank you for getting in touch! and filling out your information!<br/><br/>
                    One of our business development manager will get back to you shortly. <br/><br/>Have a great day ahead! </p>
                <p class="lead">
                    <a class="btn btn-primary btn-home" href="<?php echo base_url(); ?>" role="button">Continue To Homepage</a>
                </p>
            </article>
        </div>
    </div>
</div><br/><br/>

<!--**********************    FOOTER OPEN      ***************************-->

<?php require_once 'layouts/footer.php'; ?>

<!--**********************    FOOTER CLOSE     ***************************-->


