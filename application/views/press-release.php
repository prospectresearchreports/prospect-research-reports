<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
<title>Press Relase |  ProspectResearch Reports</title>
<!-- Basic -->
<meta charset="utf-8"/>
<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
<meta name="robots" content="index,follow"/>
<meta name="keywords" content="ProspectResearch Reports Press Release, Press Release " />
<meta name="description" content="ProspectResearch Reports&#039;s press release section features all the industry statistics cited by the global news publication sites."/>
<meta name="author" content="ProspectResearch Reports"/>


<!--**********************    HEADER OPEN      ***************************-->

<?php require_once 'layouts/header.php'; ?>

<!--**********************    HEADER CLOSE     ***************************-->

<style>
    p, h3{
        line-height: 1.8 !important;
    }
</style>

<ul class="breadcrumb m-b-0">
    <li class="breadcrumb-item"><a href="<?=base_url();?>"><img src="<?=base_url().'assets/images/home.png'?>" title='Go To Home' height="30px" width="30px" alt="Home Breadcrumb Icon" /></a></li>
    <li class="breadcrumb-item">Press Releases</li>
</ul>
<section class="bg10 txt-center p-lr-5 p-tb-20">
    <div class="scroll-div"></div>
    <h2 class="ltext-103 cl5 txt-center category-title">
    Press Releases
    </h2>
</section>	

<main class="portfolio-list">
    <div class="container">
        <?php if(isset($count)) { ?>
                
        <div class="filter-block sticky m-t-20 m-b-20">
            <h5 align='center' class="btn-filter">Search By Filters </h5>
            <div class="panel-info panel-accordion">
                <form method="get" id='filterForm' action="<?=@$current_url?>" enctype="application/x-www-form-urlencoded">
                    <div class="row">
                        <div class="col-sm-6 col-md-3 col-lg-3 panel-body">
                            <select class="form-control"  name="category" id="category" placeholder='Filter By Category'>
                                <option value='' <?=(@$category == '') ? 'selected' : ''?> >Filter By Categories</option>
                                <?php foreach(categories() as $key => $value){
                                    $selected=(@$key == @$category) ? 'selected' : '';
                                    echo "<option value='".@$key."' ".@$selected." >".ucfirst(@$value)."</option>";
                                } ?>
                            </select>
                        </div>
                        <div class="col-sm-6 col-md-3 col-lg-3 panel-body">
                            <select class="form-control" data-live-search="true" data-actions-box="true" name="year" id="year" placeholder='Filter By Year'>
                                <option value='' <?=(@$year == '') ? 'selected' : ''?> >Filter By Year</option>
                                <?php 
                                    $current_year = date('Y');
                                    $start_year = $current_year-2;
                                    for($index=$start_year;$index<=$current_year;$index++){ 
                                    $selected=(@$index == @$year) ? 'selected' : '';
                                    echo "<option value='".$index."' ".$selected.">".$index."</option>";
                                } ?>
                            </select>
                        </div>
                        <div class="col-sm-6 col-md-3 col-lg-2 panel-body">
                            <select class="form-control" data-live-search="true" data-actions-box="true" name="date" id="date" placeholder='Filter By Date Order'> 
                                <option value='' <?= (@$date_order == '') ? 'selected' : ''?>>Filter By Date</option>
                                <option value='DESC'  <?= (@$date_order == 'DESC') ? 'selected' : ''?> >Recent</option>
                                <option value='ASC' <?= (@$date_order == 'ASC') ? 'selected' : ''?> >Old</option>                                
                            </select>
                        </div>
                        <div class="col-sm-6 col-md-6 col-lg-4 panel-body">
                            <button type="submit" class="btn-search-filter btn-apply" title="Apply Filter">Apply &nbsp;<span class="fa fa-check-circle"></span></button>
                            <button type="reset" class="btn-search-filter btn-clear" onclick="window.location.href='<?=@$current_url?>'" title="Reset Filter" >Reset &nbsp;<span class="fa fa-times-circle"></span></button> 
                        </div>
                    </div>

                </form>
            </div>
        </div>
        <?php if (isset($count)) {
                if ($count == 0){
                    echo "<br/><div class='no-reports'>Sorry ! No Press Releases found for your search criteria. Try again with different criteria.</div>";
                }
            }elseif (isset($errorText)) {
                echo "<div class='no-reports'>".@$errorText."</div>";
            }
        ?>
        <br/>
        <?php if(@$press_list){ ?>            
        <div class="m-t-20">
            <?php $i=1;
            foreach ($press_list->result() as $row) { 
                $press_url= urlencode($row->url);
                $date = date_create($row->published_on);
            ?>
                <div class="row">
                    <div class="col-lg-6 col-md-6 col-sm-12">
                        <!-- item blog -->
                        <div class="p-b-63">
                            <a href="<?=base_url();?>press/<?=@$row->url; ?>" class="hov-img0 how-pos5-parent">
                                <img style="max-height:280px;width:100%;" src="<?=base_url(); ?>assets/images/press/<?=$row->image; ?>" alt="<?=$row->title; ?>">

                                <div class="flex-col-c-m size-123 bg9 how-pos5">
                                    <span class="ltext-107 cl14 txt-center">
                                        <?=date_format($date, 'd')?>
                                    </span>
                                    <span class="stext-109 cl3 txt-center">
                                        <?=date_format($date, 'M, Y')?>
                                    </span>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-12">
                        <div class="p-b-30 p-t-0">
                            <h4 class="p-b-15" title="<?=$row->title?>">
                                <a href="<?=base_url();?>press/<?=@$row->url; ?>" class="ltext-108 cl14 hov-cl1 trans-04">
                                <?=substr($row->title,0,70)."..."; ?> 
                                </a>
                            </h4>

                            <p class="stext-117 cl6">
                            <?=substr(strip_tags($row->content), 0, 300).".." ?>
                            </p>

                            <div class="flex-w flex-sb-m p-t-18">
                                <span class="flex-w flex-m stext-111 cl14 p-r-30 m-tb-10">
                                    <!-- <span>
                                        <span class="cl4">By</span> Admin  
                                        <span class="cl12 m-l-4 m-r-6">|</span>
                                    </span> -->

                                    <span>
                                        <?=categories()[@$row->cat_id]?>  
                                        <!-- <span class="cl12 m-l-4 m-r-6">|</span> -->
                                    </span>
                                </span>

                                <a href="<?=base_url();?>press/<?=@$row->url; ?>" class="stext-101 cl14 hov-cl1 trans-04 m-tb-10">
                                    Continue Reading
                                    <i class="fa fa-long-arrow-right m-l-9"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            <?php $i++; }  ?>
        </div>
        <?php } ?>                    
        <div class="clearfix m-t-10 m-b-20">
            <div style="display:inline-block"><?php if (isset($pagination)) echo $pagination; ?> </div>
        </div>
    </div>
    <?php }else{ ?>   
        <div class='col-md-12' align='center'>
            <h3 class="mtext-113 bg8 cl14 txt-center respon1 wow pulse infinite m-b-30 m-t-30 coming-soon" style="padding: 20px;" data-wow-duration="2s" data-wow-delay="0.5s">
                <span class="fa fa-clock-o"></span> Coming Soon.......
            </h3>
        </div>  
    <?php }  ?>
</main>
<script type="text/javascript">

    var data=<?=(isset($_GET) && !empty($_GET)) ? '1' : '0' ?>;
    var w=$(window).width();

    $(".btn-filter").removeClass('accordion')
    if(data == '1' && w > 767){
        $(document).ready(function(){
            $("body,html").animate({scrollTop:150},'slow');
        })
    }               
    if(w <= 767){ 
        $(".btn-filter").addClass('accordion').attr("title","Click here to collapse and filter reports").css({cursor:'pointer'})
        if(data == '1'){
            $('.filter-block').css({'height' : '100%'});                
        }else{
            $('.filter-block').css({'height' : '52px'});
        }
        var panel = $('.accordion').siblings('.panel-accordion')[0];
        if(data == '1'){
            $('.accordion').addClass('active')
            let maxHeight = panel.scrollHeight + "px";
            $(panel).css({'max-height' : maxHeight})
            
        }else{
            // console.log(w)
            $('.accordion').removeClass('active')
            $(panel).css({'max-height' :0,'display' : 'none'})
            
            $('.accordion').click(function(){
                $(this).toggleClass("active");
                var panel = $('.accordion').siblings('.panel-accordion')[0];
                if (panel.style.maxHeight === '0px') {
                    let maxHeight = panel.scrollHeight + "px";
                    // console.log(maxHeight)
                    $(panel).css({'max-height' : maxHeight,'display' : 'block'})
                } else {
                    $(panel).css({'max-height' : 0,'display' : 'none'})
                } 
                if(w<767){
                    let maxHeight = panel.scrollHeight + "px";
                    $(panel).css({'max-height' : maxHeight});
                    // console.log($(this).hasClass('active'))
                    if($(this).hasClass('active')){
                        $('.filter-block').css({'height' : '100%'});
                    }else{
                        $('.filter-block').css({'height' : '52px'});
                    }
                }
            })
        }
    }
    if(w>767){
        $(window).scroll(function () {
            if ($(this).scrollTop() > 100) {
                $('.btn-filter').hide();
            } else {
                $('.btn-filter').show();
            }
        });
    }
    </script>


<!--**********************    FOOTER OPEN      ***************************-->

<?php require_once 'layouts/footer.php'; ?>

<!--**********************    FOOTER CLOSE     ***************************-->