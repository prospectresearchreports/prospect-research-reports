<!DOCTYPE html>
<html>
<head>
<!-- Basic -->
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title><?php echo isset($meta->title) ? $meta->title : 'Thanks';?></title>
<meta name="keywords" content="<?php echo isset($meta->keywords) ? $meta->keywords : '' ;?> " />
<meta name="description" content="<?php echo isset($meta->description) ? $meta->description : 'Thank you for contacting ProspectResearch Reports . We will get Back to you soon.';?>">
<meta name="author" content="ProspectResearch Reports"/>



<!--**********************    HEADER OPEN      ***************************-->

<?php require_once 'layouts/header.php'; ?>

<!--**********************    HEADER CLOSE     ***************************-->

<main class="thanks-page">
    <div align='center'>
        <article class="prr-thanks-block">
            <h1 class="prr-thanks-title">THANK YOU!</h1>
            <p class="prr-thanks-text mtext-111">We are pleased to receive your Request. Our Business Development Team will soon get in touch with you.</p>
            <h2 class="fa fa-check check-thanks m-b-30"></h2>
            <hr/>
            <p class="prr-contact-para mtext-101">
                Having trouble? <a class="contact-link" href="<?=base_url(); ?>contact-us"><span class='fa fa-send'></span> Contact us</a>
            </p><br/>
            <p class="prr-homepage-link lead">
                <a class="btn btn-home" href="<?=base_url(); ?>" role="button">Continue To Homepage</a>
            </p>
        </article>
    </div>
</div>
    

<!--**********************    FOOTER OPEN      ***************************-->

<?php require_once 'layouts/footer.php'; ?>

<!--**********************    FOOTER CLOSE     ***************************-->