<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title><?php echo isset($meta->title) ? $meta->title : 'Privacy Policy | ProspectResearch Reports';?></title>
<!-- Basic -->
<meta charset="utf-8"/>
<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
<meta name="robots" content="index,follow"/>
<meta name="keywords" content="<?php echo isset($meta->keywords) ? $meta->keywords : 'Privacy Policy, ProspectResearch Reports privacy policy,Privacy Policy ProspectResearch Reports';?>" />
<meta name="description" content="<?php echo isset($meta->description) ? $meta->description : 'ProspectResearch Reports Refund policy states that The reports provided by ProspectResearch Reports are strictly intended for in-house use by the client and should not be used for general publication or distribution to third parties. ProspectResearch Reports is not responsible for the incorrect report received from publisher.';?>"/>
<meta name="author" content="ProspectResearch Reports"/>

<!--**********************    HEADER OPEN      ***************************-->

<?php require_once 'layouts/header.php'; ?>

<!--**********************    HEADER CLOSE     ***************************-->

<style>
    p, h3{
        line-height: 1.8 !important;
    }
</style>


<ul class="breadcrumb m-b-0">
    <li class="breadcrumb-item"><a href="<?=base_url();?>"><img src="<?=base_url().'assets/images/home.png'?>" title='Go To Home' height="30px" width="30px" alt="Home Breadcrumb Icon" /></a></li>
    <li class="breadcrumb-item">Privacy Policy</li>
</ul>


<!-- Title page -->
<section class="bg10 txt-center p-lr-15 p-tb-50 ">
    <h2 class="ltext-103 cl5 txt-center">
        Privacy Policy
    </h2>
</section>	


<!-- Content page -->
<section class="bg0 p-t-50 p-b-30">
    <div class="container">
        <div class="row p-b-30">
            <div class="col-md-12 col-lg-12 p-b-30">
                <div class="p-t-7 p-r-85 p-r-15-lg p-r-0-md res-challenge-div">
                    <h3 class="mtext-113 cl14 p-b-30">
                        Privacy Policy
                    </h3>
                    <h3 class="mtext-106 cl5 p-b-16">
                        The Personal Information We Collect:
                    </h3>
                    <p class="stext-110 cl8 p-b-26">
                        We use your IP address to assist diagnose problems with our server and to administer our website by creating statistical reports and tracking errors. 
                    </p>

                    <h3 class="mtext-106 cl5 p-b-16">
                        We collect Device Information using the following technology:
                    </h3>
                    <p class="stext-110 cl8 p-b-26">
                        We invite personal information (1) after you buy our Alert or newsletter services, (2) after you want to look at detailed information about our products, (3) once you want to submit an issue about our products/services to our customer staff or (4) once you buy products. the categories of private information which will be collected on these pages are: name, name, title, address, e-mail address, signaling, fax number and mastercard information.<br/>
                        If you choose our feature to tell others about our site or about particular products on our site, we ask you for your name and email address and your colleague’s name and email address. Our site will automatically send the colleague a one-time email inviting them to go to the positioning. Our site does stores this information and is employed for the only real purpose of sending emails and tracking the success of our referral program.<br/>
                        This site also uses "cookies", certain bits of data that are deposited on your computer after you visit this site and sent back to the current site after you visit again. Cookies give us information about what number people visit certain pages on the positioning. We also use cookies so you don’t need to reenter your login address or password whenever you come to our site. In certain instances, we also use cookies to trace the world of the positioning that you just are on after you register as a user. If you are doing not have cookies enabled, you'll still use our site, but you may should log in when you come to the location.
                    </p>

                    
                    <h3 class="mtext-106 cl5 p-b-16">
                        The Way We Use Information :
                    </h3>
                    <p class="stext-110 cl8 p-b-26">
                        We use your personal information to supply requested information and services to you and to assist make sure that the data we offer to you has relevancy to your specific needs.<br/>
                        When you buy something, your information are going to be stored on our system to finish the buying process. It must be shared with an outdoor party so as to verify your mastercard information. We also record information about your inquiries to us and your purchases in order that we and you'll keep track of what you've asked about or ordered directly on our site. Additionally, we may have to contact you about your questions and/or your orders.<br/>
                        We use email addresses to send you the Alerts or newsletters you've requested, to answer the e-mail we receive and to speak with you about publications which you have got told us you'd have an interest in hearing about.
                    </p>
                    
                    <h3 class="mtext-106 cl5 p-b-16">
                        Other Parties' Collection and Use of Your Information :
                    </h3>
                    <p class="stext-110 cl8 p-b-10">
                        We don't share, sell, rent, or trade personal identifiable information with third parties for his or her promotional purposes.<br/> We do use agents, service providers and other third parties to produce some services on our site. These companies don't retain, share, store or use personally identifiable information for the other purposes then those described below:
                    </p>
                    <p class="mtext-101 cl8 p-b-26">
                        <b>Agents</b>
                    </p>
                    <p class="mtext-101 cl8 p-b-10">
                        <b>Publishers</b>
                    </p>
                    <p class="stext-110 cl8 p-b-26">
                        We may share your contact information with the publisher(s) whose reports you get. The publisher(s) may send you marketing materials. If you are doing not wish to receive these materials you'll have to contact the publisher directly so as to opt-out (providing the publisher offers an opt-out).
                    </p>
                    <p class="mtext-101 cl8 p-b-26">
                        <b>Service Providers</b>
                    </p>
                    
                    <h3 class="mtext-101 cl8 p-b-10">
                        <b>Shipping :</b>
                    </h3>
                    <p class="stext-110 cl8 p-b-26">
                        Our publishers use an out of doors shipping service provider to fulfil orders. These orders are going to be shipped to you directly from the publisher. In these cases, the publisher receives your information.
                    </p>

                    <h3 class="mtext-101 cl8 p-b-10">
                        <b>Credit Card Processing :</b>
                    </h3>
                    <p class="stext-110 cl8 p-b-26">
                        When you buy something with a mastercard, we use a mastercard processing service provider to verify your mastercard and bill you. additionally, the name of your company (but not your name or contact information) are shared with the publisher of the knowledge you obtain.
                    </p>

                    
                    <h3 class="mtext-101 cl8 p-b-26">
                        <b>Other Third Parties</b>
                    </h3>

                    
                    <h3 class="mtext-101 cl8 p-b-10">
                        <b>Tracking Service :</b>
                    </h3>
                    <p class="stext-110 cl8 p-b-26">
                        A third party tracking service provider may set cookies on our website to assist us track to what extent sales are being generated from various marketing efforts. This information is merely utilized in the mixture. We don't link the data we store in cookies to any personally identifiable information you submit while on our site.<br/> 
                        Clear gifs are tiny graphics with a novel identifier, similar in function to cookies, and are wont to track the web movements of website users. the most difference between the 2 is that clear gifs are invisible on the page and are much smaller, about the scale of the amount at the tip of this sentence. Clear gifs are tied to users' personally identifiable information but are used only by us to see the effectiveness of varied marketing programs.
                    </p>
                    
                    
                    <h3 class="mtext-106 cl5 p-b-16">
                        Changes during this Privacy Statement :
                    </h3>
                    <p class="stext-110 cl8 p-b-26">
                        If we arrange to change our privacy policy, we are going to post those changes to the present privacy statement on the house page and other places we deem appropriate in order that you're alert to what information we collect, how we use it, and under what circumstances, if any, we disclose it. We reserve the proper to change this privacy statement at any time, so please review it frequently. If we make material changes to the current policy, we'll notify you here, by email, or by means of a notice on our home page.
                    </p>
                    
                    <h3 class="mtext-106 cl5 p-b-16">
                        Legal Disclaimer :
                    </h3>
                    <p class="stext-110 cl8 ">
                        We reserve the right to disclose your information by law.
                    </p>


                </div>
            </div>
        </div>
    </div>
</section>


<!--**********************    FOOTER OPEN      ***************************-->

<?php require_once 'layouts/footer.php'; ?>

<!--**********************    FOOTER CLOSE     ***************************-->
