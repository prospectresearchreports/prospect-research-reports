<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <title><?php  if($category_name!=="Company Reports" ) { echo $category_name." Reports"; }else{ echo $category_name; } ?>  | ProspectResearch Reports</title>
        <!-- Basic -->
        <meta charset="utf-8"/>
        <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
        <meta name="robots" content="index,follow"/>
        <meta name="keywords" content="<?php  echo $category_name;?> Reports, <?php  echo $category_name;?>  Reports,Industry Reports, Business Research Process, Market News,  Research Report, Business Research, Industrial Analysis, Market Research Analysis,  USA Marketing Research Process, China Research Report, Japan Business Market Research Report, Europe Industrial Analysis,Industry Analysis Report" />
        <meta name="description" content="ProspectResearch Reports offers <?php  echo $category_name;?> Reports across worldwide and regions and keeps you updated daily."/>
        <meta name="author" content="ProspectResearch Reports"/>

        <!--**********************    HEADER OPEN      ***************************-->
        
        <?php require_once 'layouts/header.php'; ?>
        
        <!--**********************    HEADER CLOSE     ***************************-->

        <style>
            p, h3{
                line-height: 1.8 !important;
            }
        </style>

        <ul class="breadcrumb m-b-0">
            <li class="breadcrumb-item"><a href="<?=base_url();?>"><img src="<?=base_url().'assets/images/home.png'?>" title='Go To Home' height="30px" width="30px" alt="Home Breadcrumb Icon" /></a></li>
            <li class="breadcrumb-item"><a href="<?=base_url().'industry-sectors';?>">Industry Sectors</a></li>
            <li class="breadcrumb-item" title="<?=@$category_name?>"><?= (@$category_id == 14) ? 'BFSI' : $category_name; ?></li>
        </ul>

        <!-- Title page -->
        <section class="bg10 txt-center p-lr-5 p-tb-20">
            <div class="scroll-div"></div>
            <div class="sm-hidden">                
                <h2 class="ltext-103 cl5 txt-center category-title">
                    <img class='report-img' src="<?=base_url()?>assets/images/category/<?=@$category_image?>" alt="<?=@$category_name?>" />
                    <?=@$category_name?>
                </h2>
            </div>
            <div class="md-hidden">                
                <img class='report-img' src="<?=base_url()?>assets/images/category/<?=@$category_image?>" alt="<?=@$category_name?>" />
                <h2 class="mtext-109 cl5 txt-center category-title m-t-15 m-b-15"><?=@$category_name?></h2>
            </div>
        </section>	
        <main class="portfolio-list">
            <div class="container">  
                <?php if (isset($total_reports)) {?>
                <div class="filter-block sticky m-t-20 m-b-20">
                    <h5 align='center' class="btn-filter">Search By Filters </h5>
                    <div class="panel-info panel-accordion">
                        <form method="get" id='filterForm' action="<?=@$current_url?>" enctype="application/x-www-form-urlencoded">
                            <div class="row">
                                <div class="col-sm-6 col-md-3 col-lg-2 panel-body">
                                    <select class="form-control" data-live-search="true" data-actions-box="true" name="year" id="year" placeholder='Filter By Year'>
                                        <option value='' <?=(@$year == '') ? 'selected' : ''?> >Filter By Year</option>
                                        <?php 
                                            $current_year = date('Y');
                                            $start_year = $current_year-2;
                                            for($index=$start_year;$index<=$current_year;$index++){ 
                                            $selected=(@$index == @$year) ? 'selected' : '';
                                            echo "<option value='".$index."' ".$selected.">".$index."</option>";
                                        } ?>
                                    </select>
                                </div>
                                <div class="col-sm-6 col-md-3 col-lg-2 panel-body">
                                    <select class="form-control" data-live-search="true" data-actions-box="true" name="region" id="region" placeholder='Filter By Region'>
                                        <option value='' <?=(@$current_region == '') ? 'selected' : ''?> >Filter By Region</option>
                                        <?php foreach(@$region_data->result() as $row){
                                            $selected=(@$row->region == @$current_region) ? 'selected' : '';
                                            echo "<option value='".@$row->region."' ".@$selected." >".ucfirst(@$row->region)."</option>";
                                        } ?>
                                    </select>
                                </div>
                                <div class="col-sm-6 col-md-3 col-lg-2 panel-body">
                                    <select class="form-control" data-live-search="true" data-actions-box="true" name="date" id="date" placeholder='Filter By Date Order'> 
                                        <option value='' <?= (@$date_order == '') ? 'selected' : ''?>>Filter By Date</option>
                                        <option value='DESC'  <?= (@$date_order == 'DESC') ? 'selected' : ''?> >Recent</option>
                                        <option value='ASC' <?= (@$date_order == 'ASC') ? 'selected' : ''?> >Old</option>                                
                                    </select>
                                </div>
                                <div class="col-sm-6 col-md-3 col-lg-2 panel-body">
                                    <select class="form-control" name="price" id="price" placeholder='Filter By Price'>
                                        <option value=''<?= (@$price == '') ? 'selected' : ''?>>Filter By Price</option>
                                        <option value='<1000' <?= (@$price == '<1000') ? 'selected' : ''?> >&lt;1000 USD</option>
                                        <option value='1000-2000' <?= (@$price == '1000-2000') ? 'selected' : ''?>>1000-2000 USD</option>  
                                        <option value='2000-3000' <?= (@$price == '2000-3000') ? 'selected' : ''?>>2000-3000 USD</option>  
                                        <option value='3000-4000' <?= (@$price == '3000-4000') ? 'selected' : ''?>>3000-4000 USD</option>   
                                        <option value='>4000' <?= (@$price == '>4000') ? 'selected' : ''?>>&gt;4000 USD</option>                                
                                    </select>
                                </div>

                                <div class="col-sm-6 col-md-6 col-lg-4 panel-body">
                                    <button type="submit" class="btn-search-filter btn-apply" title="Apply Filter">Apply &nbsp;<span class="fa fa-check-circle"></span></button>
                                    <button type="reset" class="btn-search-filter btn-clear" onclick="window.location.href='<?=@$current_url?>'" title="Reset Filter" >Reset &nbsp;<span class="fa fa-times-circle"></span></button> 
                                </div>
                            </div>

                        </form>
                    </div>
                </div>
                                    
                <div align="center">
                <?php if (isset($total_reports)) {
                        if ($total_reports == 0){
                            echo "<br/><div class='no-reports'>Sorry ! No reports found for your search criteria. Try again with different criteria.</div>";
                        }
                    }elseif (isset($errorText)) {
                        echo "<div class='no-reports'>".@$errorText."</div>";
                    }
                    ?>
                </div><br/>
                <?php if(@$total_reports && @$total_reports > 0){
                if (@$category_report_list){ ?>
                <article class="report-list">
                    <div class="row">
                        <?php foreach ($category_report_list->result() as $row) { 
                            $report_url=urlencode($row->rep_url);
                        ?>
                        <div class="col-md-12 col-lg-6 m-t-10 report-list-item">
                            <h2 class="report-list-title" title="<?=$row->rep_title?>">
                                <a class="report-list-title-link" href="<?php echo base_url(); ?>reports/<?php echo $row->rep_id; ?>/<?php echo $report_url; ?>"  rel="follow">
                                    <?php  
                                        $report_title = explode('Market',$row->rep_title);
                                        if(isset($report_title[0]) && strlen($report_title[0]) == 0){
                                            $report_title_array = explode(" ", $row->rep_title);
                                            $first_word_length = strlen($report_title_array[0]);
                                            $report_title = explode('Market',substr($row->rep_title, $first_word_length));
                                        }
                                        // echo "<pre>";print_r($report_title);exit;
                                        $report_title = str_ireplace(array(' -','- ','-'),' - ',$report_title[0]).' Market';
                                        echo strlen($report_title) > 130 ? substr($report_title , 0 ,130)."..." : $report_title; 
                                    ?>
                                </a>
                            </h2>
                            <div class='report-desc mb-3'>
                                <?=(strlen($row->rep_descrip) > 200) ? substr($row->rep_descrip, 0, 200).".." : $row->rep_descrip ?>
                            </div>
                            <h5 class="report-date mb-3">
                                <span> <strong>Published :</strong>
                                    <?php $date = date_create($row->rep_entry_date);
                                    echo date_format($date, 'M Y'); ?>
                                </span> &nbsp;<strong>|</strong>&nbsp;
                                <span><strong>From: </strong><?php if ($row->li_value > 0) {
                                        echo "$" . $row->li_value;
                                    } ?>
                                </span>
                            </h5>
                            <a class="report-link" href="<?=base_url().'reports/'.$row->rep_id.'/'. $report_url; ?>" title="View Report Details">view report &nbsp;<span class="fa fa-line-chart"></span></a>
                        </div>
                        <?php }?>
                    </div>
                </article>
                <?php  } ?>
                <div class="clearfix">                        
                    <div class="row">
                        <div class="col-md-8"><?php if (isset($pagination)) echo $pagination; ?> </div>
                        <div class="col-md-4 sm-hidden">
                            <h4 class="pull-right page-no">
                                <strong>Page</strong> <?= @$page?> <strong>of</strong> <?=@$total_pages?>
                            </h4>
                        </div>
                    </div>
                </div><br/>
                <?php } 
                }else{ ?>
                <div class='col-md-12' align='center'>
                    <h3 class="mtext-113 bg8 cl14 txt-center respon1 wow pulse infinite m-b-30 m-t-30 coming-soon" style="padding: 20px;" data-wow-duration="2s" data-wow-delay="0.5s">
                        <span class="fa fa-clock-o"></span> Coming Soon.......
                    </h3>
                </div>
                <?php } ?>
            </div>
        </main>

        
        <script type="text/javascript">

        var data=<?php echo (isset($_GET) && !empty($_GET)) ? '1' : '0' ?>;
        var w=$(window).width();

        $(".btn-filter").removeClass('accordion')
        if(data == '1' && w > 767){
            $(document).ready(function(){
                $("body,html").animate({scrollTop:235},'slow');
            })
        }               
        if(w <= 767){ 
            $(".btn-filter").addClass('accordion').attr("title","Click here to collapse and filter reports").css({cursor:'pointer'})
            if(data == '1'){
                $('.filter-block').css({'height' : '100%'});                
            }else{
                $('.filter-block').css({'height' : '52px'});
            }
            var panel = $('.accordion').siblings('.panel-accordion')[0];
            if(data == '1'){
                $('.accordion').addClass('active')
                let maxHeight = panel.scrollHeight + "px";
                $(panel).css({'max-height' : maxHeight})
                
            }else{
                // console.log(w)
                $('.accordion').removeClass('active')
                $(panel).css({'max-height' :0,'display' : 'none'})
                
                $('.accordion').click(function(){
                    $(this).toggleClass("active");
                    var panel = $('.accordion').siblings('.panel-accordion')[0];
                    if (panel.style.maxHeight === '0px') {
                        let maxHeight = panel.scrollHeight + "px";
                        // console.log(maxHeight)
                        $(panel).css({'max-height' : maxHeight,'display' : 'block'})
                    } else {
                        $(panel).css({'max-height' : 0,'display' : 'none'})
                    } 
                    if(w<767){
                        let maxHeight = panel.scrollHeight + "px";
                        $(panel).css({'max-height' : maxHeight});
                        // console.log($(this).hasClass('active'))
                        if($(this).hasClass('active')){
                            $('.filter-block').css({'height' : '100%'});
                        }else{
                            $('.filter-block').css({'height' : '52px'});
                        }
                    }
                })
            }
        }
        if(w>767){
            $(window).scroll(function () {
                if ($(this).scrollTop() > 100) {
                    $('.btn-filter').hide();
                } else {
                    $('.btn-filter').show();
                }
            });
        }
        </script>
                
<!--**********************    FOOTER OPEN      ***************************-->

<?php require_once 'layouts/footer.php'; ?>

<!--**********************    FOOTER CLOSE     ***************************-->


