<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">

<head>
<title>Home | ProspectResearch Reports</title>
<!-- Basic -->
<meta charset="utf-8"/>
<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
<meta name="robots" content="index,follow"/>
<meta name="keywords" content="ProspectResearch Reports,Industry Reports, Business Research Process, Market News,  Research Report, Business Research, Industrial Analysis, Market Research Analysis,  USA Marketing Research Process, China Research Report, Japan Business Market Research Report, Europe Industrial Analysis,Industry Analysis Report" />
<meta name="description" content="ProspectResearch Reports is a Global Leader In Strategic Research Reports and is the leading provider of market research reports which offers premium progressive statistical surveying, market research reports, analysis &amp; forecast data for industries around the globe. "/>
<meta name="author" content="ProspectResearch Reports"/>

<!--**********************    HEADER OPEN      ***************************-->
<?php require_once 'layouts/header.php'; ?>

<!--**********************    HEADER CLOSE     ***************************-->
  

  <!-- Slider -->
  <section class="section-slide">
    <div class="wrap-slick1 rs1-slick1">
      <div class="slick1">
        <div class="item-slick1" style="background: url(<?=base_url()?>assets/images/home-banner.png) no-repeat fixed center #eee">
          <div class="container h-full">
            <div class="flex-col-l-m h-full p-t-20 p-b-20">							
                
              <div class="layer-slick1 animated visible-false" data-appear="fadeInUp" data-delay="800"  align='center'>
                <img src="<?=base_url()?>assets/images/wide-variety.png" alt="Wide Variety At PRR" style="height:95px" />
                <h2 class="ltext-104 cl14 p-t-19 p-b-43 respon1 respon1-h2 respon1-custom-h2 " >
                  Wide Variety
                </h2>
              </div>

              <div class="layer-slick1 animated visible-false p-b-30" data-appear="fadeInDown" data-delay="0">
                <p class="mtext-101 cl5 respon2 slider-para">
                  PRR offers the wide collection of the Market types,industrial data, company profiles, syndicated reports. so to have abundant choices as a client.
                </p>
              </div>
                
              <div class="layer-slick1 animated visible-false" data-appear="zoomIn" data-delay="1600" align='center'>
                <a href="<?=base_url()?>industry-sectors" class="layer-slick1-link flex-c-m stext-101 cl0 size-101 bg1 bor1 hov-btn1 p-lr-15 trans-04">
                  View Categories &nbsp;<span class="fa fa-industry"></span>
                </a>
              </div>
            </div>
          </div>
        </div>

        <div class="item-slick1" style="background: url(<?=base_url()?>assets/images/home-banner.png) no-repeat fixed center #eee">
          <div class="container h-full">
            <div class="flex-col-l-m h-full p-t-100 p-b-20">
              
              <div class="layer-slick1 animated visible-false" data-appear="lightSpeedIn" data-delay="800" align='center'>
                <img src="<?=base_url()?>assets/images/business-consulting.png" alt="Business Consulting At PRR" style="height:95px" />
                <h2 class="ltext-104 cl14 p-t-19 p-b-43 respon1 respon1-h2" >
                  Business Consulting
                </h2>
              </div>
              
              <div class="layer-slick1 animated visible-false p-b-30" data-appear="rollIn" data-delay="0">
                <p class="mtext-101 cl5 respon2 slider-para">
                  We provide full consulting support to deliver suggestions, comments and advice for result-oriented strategic business decisions.
                </p>
              </div>
                
                
              <div class="layer-slick1 animated visible-false" data-appear="slideInUp" data-delay="1600" align='center'>
                <a href="<?=base_url()?>latest-reports" class="layer-slick1-link flex-c-m stext-101 cl0 size-101 bg1 bor1 hov-btn1 p-lr-15 trans-04">
                  View Reports &nbsp;<span class="fa fa-bar-chart"></span>
                </a>
              </div>
            </div>
          </div>
        </div>

        <div class="item-slick1" style="background: url(<?=base_url()?>assets/images/home-banner.png) no-repeat fixed center #eee">
          <div class="container h-full">
            <div class="flex-col-l-m h-full p-t-100 p-b-20">
              
              <div class="layer-slick1 animated visible-false" data-appear="rotateInUpRight" data-delay="800" align='center'>
                <img src="<?=base_url()?>assets/images/marketing.png" alt="Marketing At PRR" style='height:100px;width:auto;max-width:100%' />
                <h2 class="ltext-104 cl14 p-t-19 p-b-43 respon1 respon1-h2 respon1-custom-h2" >
                  Marketing
                </h2>
              </div>

              <div class="layer-slick1 animated visible-false p-b-30" data-appear="rotateInDownLeft" data-delay="0">
                <p class="mtext-101 cl5 respon2 slider-para">
                  Get notified with recent updation on your specific intrested topics, categories or verticles. Also, avail bulk reports purchase discount.
                </p>
              </div>
                
                
              <div class="layer-slick1 animated visible-false" data-appear="rotateIn" data-delay="1600" align='center'>
                <a href="<?=base_url()?>latest-reports" class="layer-slick1-link flex-c-m stext-101 cl0 size-101 bg1 bor1 hov-btn1 p-lr-15 trans-04">
                  View Reports &nbsp;<span class="fa fa-bar-chart"></span>
                </a>
              </div>
            </div>
          </div>
        </div>
        
        <div class="item-slick1" style="background: url(<?=base_url()?>assets/images/home-banner.png) no-repeat fixed center #eee;">
          <div class="container h-full">
            <div class="flex-col-l-m h-full p-t-100 p-b-20">
                
              <div class="layer-slick1 animated visible-false" data-appear="rotateInUpRight" data-delay="800" align='center'>
                <img src="<?=base_url()?>assets/images/customized-research.png" alt="Customized Research At PRR" style='height:100px;width:auto;max-width:100%' />
                <h2 class="ltext-104 cl14 p-t-19 p-b-43 respon1 respon1-h2" >
                  Customized Research
                </h2>
              </div>

              <div class="layer-slick1 animated visible-false p-b-30" data-appear="rotateInDownLeft" data-delay="0">
                <p class="mtext-101 cl5 respon2 slider-para">
                  Give your report data a personalized touch with us, as we provide the professionally customized reports according to your niche requirements.
                </p>
              </div>
              
                
              <div class="layer-slick1 animated visible-false" data-appear="rotateIn" data-delay="1600" align='center'>
                <a href="<?=base_url()?>latest-reports" class="layer-slick1-link flex-c-m stext-101 cl0 size-101 bg1 bor1 hov-btn1 p-lr-15 trans-04">
                  View Reports &nbsp;<span class="fa fa-bar-chart"></span>
                </a>
              </div>
            </div>
          </div>
        </div>

      </div>
    </div>
  </section>
  
  <!-- Banner -->
  <div class="sec-banner bg0 p-t-30 p-b-30">
    <div class="container">
      <div class="p-b-32">
        <h3 class="ltext-105 cl5 txt-center respon1">
          Industries
        </h3>
      </div>

      <div class="row">
        <?php 
            $categories = get_categories();
            // echo "<pre>"; print_r($categories->result());exit;
            // $i = 0;
            foreach($categories->result() as $row):
                // $i++;
                $homepage_categories = $this->config->item("visible_categories");
                if(!in_array($row->sc1_id, $homepage_categories)){ continue; }
        ?>
        <div class="col-sm-6 col-lg-4 m-lr-auto m-t-5 m-b-5 respon4">
        <!-- Block1 -->
          <div class="block1 wrap-pic-w">
            <div align='center'>
              <img src="<?=base_url()?>assets/images/category/<?=getCategoryIcon()[@$row->sc1_id]?>" alt="<?=$row->sc1_name?>" style="opacity:0.8;">
            </div>

            <a href="<?=base_url(); ?>category/<?=$row->sc1_url;?>" class="block1-txt ab-t-l s-full flex-col-l-sb p-lr-38 p-tb-34 trans-03 respon3">
              <div class="block1-txt-child1 flex-col-l">
                <span class="block1-name mtext-101 trans-04 p-b-8">
                  <?=str_ireplace('and','&amp;',$row->sc1_name)?>
                </span>
              </div>

              <div class="block1-txt-child2 p-b-4 trans-05">
                <div class="block1-link stext-101 cl0 trans-09">
                  View Reports
                </div>
              </div>
            </a>
          </div>
        </div>
        <?php endforeach;  ?>
      </div>
    </div>
  </div>

  
	<!-- Product -->
	<section class="sec-product bg0 p-t-30 p-b-30">
		<div class="container">
			<div class="p-b-32">
				<h3 class="ltext-105 cl5 txt-center respon1">
					Latest Reports
				</h3>
			</div>

			<!-- Tab01 -->
			<div class="tab01">
				<!-- Tab panes -->
				<div class="tab-content p-t-50">
					<!-- - -->
					<div class="tab-pane fade show active" id="best-seller" role="tabpanel">
						

              <?php if($report->num_rows() > 0){ ?>
                <!-- Slide2 -->
              <div class="wrap-slick2">
                <div class="slick2">
                
                  <?php foreach($report->result() as $row): ?>
                    <div class="item-slick2 p-l-15 p-r-15 p-t-15 p-b-15">
                      <!-- Block2 -->
                      <div class="block2">
                        <div class="block2-pic hov-img0">
                          <img src="<?=base_url()?>assets/images/category/<?=@$row->category_image?>" alt="<?=@$row->rep_title?>">
                        </div>

                        <div class="block2-txt flex-w flex-t p-t-14">
                          <div class="block2-txt-child1 flex-col-l ">
                            <a href="<?=base_url(); ?>reports/<?=$row->rep_id; ?>/<?=$row->rep_url; ?>" class="stext-104 cl4 hov-cl1 trans-04 js-name-b2 p-b-6">
                              <?=categories()[@$row->rep_sub_cat_1_id]?>
                            </a>
                            <a href="<?=base_url(); ?>reports/<?=$row->rep_id; ?>/<?=$row->rep_url; ?>" class="stext-104 cl4 hov-cl1 trans-04 js-name-b2 p-b-6">
                              <span class="stext-105 cl3">
                              <?php 
                                $report_title = explode('Market',$row->rep_title);
                                if(isset($report_title[0]) && strlen($report_title[0]) == 0){
                                    $report_title_array = explode(" ", $row->rep_title);
                                    $first_word_length = strlen($report_title_array[0]);
                                    $report_title = explode('Market',substr($row->rep_title, $first_word_length));
                                } 
                                $report_title = ucwords(str_ireplace(array(' -','- ','-'),' - ',$report_title[0]).' market');
                                echo strlen($report_title) > 130 ? substr($report_title , 0 ,130)."..." : $report_title; 
                              ?>
                              </span>
                            </a>
                          </div>
                        </div>
                      </div>
                    </div>
                    <?php endforeach;  ?>
                  </div>
              </div>
              <?php  }else{ ?><br/>
                <div>
                  <h3 class="mtext-113 bg8 cl5 txt-center respon1 wow pulse infinite coming-soon" style="padding: 20px;" data-wow-duration="2s" data-wow-delay="0.5s">
                    <span class="fa fa-clock-o"></span> Coming Soon.......
                  </h3>
                </div> 
              <?php }  ?>							
					</div>
				</div>
			</div>
		</div>
	</section>


  <!-- Blog -->
	<section class="sec-blog bg0 p-t-30 p-b-30">
		<div class="container">
			<div class="p-b-66">
				<h3 class="ltext-105 cl5 txt-center respon1">
					Press Releases
				</h3>
			</div>
			
      <?php if(@$press_list && @$press_list->num_rows() > 0){ ?>
        <div class="row">
        <?php foreach($press_list->result() as $row):
          $date = date_create($row->published_on);
        ?>
				<div class="col-sm-6 col-md-4 p-b-40">
					<div class="blog-item">
						<div class="hov-img0 txt-center">
							<a href="<?=base_url(); ?>press/<?=$row->url; ?>">
								<img style="height: 200px;width:auto;max-width:100%" src="<?=base_url()?>assets/images/press/<?=@$row->image?>" alt="<?=@$row->image?>">
							</a>
						</div>

						<div class="p-t-15">
							<div class="stext-107 flex-w p-b-14">								

								<span>
									<span class="cl4">
										Published On
									</span>

									<span class="cl5">
										<?=$date->format("F d, Y")?> 
									</span>
								</span>
							</div>

							<h4 class="p-b-12">
								<a href="<?=base_url(); ?>press/<?=$row->url; ?>" class="mtext-101 cl14 hov-cl14 trans-04">
                  <?=substr($row->title,0,90)?>
								</a>
							</h4>

							<p class="stext-108 cl6">
                <?=substr(strip_tags($row->content), 0, 120).".." ?>
							</p>
						</div>
					</div>
				</div>
        <?php endforeach; ?>
			</div>
      <?php }else{ ?><br/>            
          <div>
            <h3 class="mtext-113 bg8 cl5 txt-center respon1 wow pulse infinite coming-soon" style="padding: 20px;" data-wow-duration="2s" data-wow-delay="0.5s">
              <span class="fa fa-clock-o"></span> Coming Soon.......
            </h3>
          </div>
      <?php }  ?>
	</section>

  <!-- Slider : Testimonials -->
  <section class="sec-product bg0 p-t-30 p-b-30 testimonial-block">
		<div class="container">
			<div class="p-b-32">
				<h3 class="ltext-105 cl5 txt-center respon1">
          Client Testimonials
				</h3>
			</div>

			<!-- Tab01 -->
			<div class="tab01">
				<!-- Tab panes -->
				<div class="tab-content p-t-50">
					<!-- - -->
					<div class="tab-pane fade show active" id="best-seller" role="tabpanel">

            <div class="wrap-slick2">
              <div class="slick2 slick2-testi">
                
              <div class="item-slick2 p-l-15 p-r-15 p-t-15 p-b-15">
                  <div class="block2">
                    <div class="block2-pic hov-img0 testi-img">
                      <img src="<?=base_url()?>assets/images/male-avatar.png" alt="Testimonial Avatar">
                    </div>

                    <div class="block2-txt flex-w flex-t p-t-14">
                      <div class="block2-txt-child1 flex-col-l ">
                        <span class="stext-102 cl3">
                          I am completely satisfied with the information given in the sample pages of the report and I am waiting for the affirmative response from my peers and colleagues to purchase it.hey answered to all my queries promptly. I am highly satisfied!!
                        </span>

                        <span class="mtext-107 cl5 hov-cl1 trans-04 js-name-b2 p-b-6 p-t-10">
                          <b>John Keats</b>
                        </span>
                      </div>
                    </div>
                  </div>
                </div>

                
                <div class="item-slick2 p-l-15 p-r-15 p-t-15 p-b-15">
                  <div class="block2">
                    <div class="block2-pic hov-img0 testi-img">
                      <img src="<?=base_url()?>assets/images/male-avatar.png" alt="Testimonial Avatar">
                    </div>

                    <div class="block2-txt flex-w flex-t p-t-14">
                      <div class="block2-txt-child1 flex-col-l ">
                        <span class="stext-102 cl3">
                        The market research reports provided by PRR team are top-notch. They deliver accurate, in-depth analysis that helps us stay ahead of the competition. The professionalism and expertise shown in their reports have made them an indispensable resource for our business. Excellent work!
                        </span>

                        <span class="mtext-107 cl5 hov-cl1 trans-04 js-name-b2 p-b-6 p-t-10">
                          <b>Olivia Garcia</b>
                        </span>
                      </div>
                    </div>
                  </div>
                </div>

                
                <div class="item-slick2 p-l-15 p-r-15 p-t-15 p-b-15">
                  <div class="block2">
                    <div class="block2-pic hov-img0 testi-img">
                      <img src="<?=base_url()?>assets/images/male-avatar.png" alt="Testimonial Avatar">
                    </div>

                    <div class="block2-txt flex-w flex-t p-t-14">
                      <div class="block2-txt-child1 flex-col-l ">
                        <span class="stext-102 cl3">
                        I’m extremely satisfied with the market research reports we receive from PRR. The reports are not only rich in data but also presented in a clear and understandable format. They’ve become a crucial tool for our strategic planning. I can’t recommend them enough.
                        </span>

                        <span class="mtext-107 cl5 hov-cl1 trans-04 js-name-b2 p-b-6 p-t-10">
                          <b>David Thompson</b>
                        </span>
                      </div>
                    </div>
                  </div>
                </div>

                <div class="item-slick2 p-l-15 p-r-15 p-t-15 p-b-15">
                  <div class="block2">
                    <div class="block2-pic hov-img0 testi-img">
                      <img src="<?=base_url()?>assets/images/male-avatar.png" alt="Testimonial Avatar">
                    </div>

                    <div class="block2-txt flex-w flex-t p-t-14">
                      <div class="block2-txt-child1 flex-col-l ">
                        <span class="stext-102 cl3">
                        I’ve been consistently impressed with the quality of market research reports provided by PRR. The insights are always detailed and actionable, making it easy for us to make informed decisions. Their reports have been instrumental in shaping our strategy and driving growth. Highly recommended!
                        </span>

                        <span class="mtext-107 cl5 hov-cl1 trans-04 js-name-b2 p-b-6 p-t-10">
                          <b>Emily Johnson</b>
                        </span>
                      </div>
                    </div>
                  </div>
                </div>

                <div class="item-slick2 p-l-15 p-r-15 p-t-15 p-b-15">
                  <div class="block2">
                    <div class="block2-pic hov-img0 testi-img">
                      <img src="<?=base_url()?>assets/images/male-avatar.png" alt="Testimonial Avatar">
                    </div>

                    <div class="block2-txt flex-w flex-t p-t-14">
                      <div class="block2-txt-child1 flex-col-l ">
                        <span class="stext-102 cl3">
                        The market research reports from PRR have exceeded our expectations time and again. The data is comprehensive and well-organized, which has helped us identify key trends and opportunities in our industry. Excellent service with a high level of accuracy!
                        </span>

                        <span class="mtext-107 cl5 hov-cl1 trans-04 js-name-b2 p-b-6 p-t-10">
                          <b>Michael Lee</b>
                        </span>
                      </div>
                    </div>
                  </div>
                </div>

                <div class="item-slick2 p-l-15 p-r-15 p-t-15 p-b-15">
                  <div class="block2">
                    <div class="block2-pic hov-img0 testi-img">
                      <img src="<?=base_url()?>assets/images/male-avatar.png" alt="Testimonial Avatar">
                    </div>

                    <div class="block2-txt flex-w flex-t p-t-14">
                      <div class="block2-txt-child1 flex-col-l ">
                        <span class="stext-102 cl3">
                        Working with PRR has been a game-changer for our business. Their reports are thorough, well-researched, and tailored to our specific needs. We’ve seen significant improvements in our market positioning thanks to their valuable insights. Fantastic job!
                        </span>

                        <span class="mtext-107 cl5 hov-cl1 trans-04 js-name-b2 p-b-6 p-t-10">
                          <b>Sophia Martinez</b>
                        </span>
                      </div>
                    </div>
                  </div>
                </div>


              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
	</section>

  









<!--**********************    FOOTER OPEN      ***************************-->

<?php require_once 'layouts/footer.php'; ?>

<!--**********************    FOOTER CLOSE     ***************************-->
