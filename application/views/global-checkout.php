<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>GLOBAL CHECKOUT- <?php if(isset($report_data)) { echo $report_data->title;} ?></title>

<!--**********************    HEADER OPEN      ***************************-->

<?php require_once 'layouts/header.php'; ?>

<!--**********************    HEADER CLOSE     ***************************-->

<script src="<?php echo base_url();?>assets/js/validator.min.js"></script>
<script src="<?php echo base_url();?>assets/js/captcha.js"></script>

<!-- *****************   BOOTSTRAP SELECT          ******************  -->
<link href="<?php echo base_url();?>assets/css/checkout.css" rel='stylesheet' />

<script type="text/javascript">
    $(document).ready(function(){
        var w=$(window).width();
        if(w>767){
            $('html,body').animate({
                scrollTop: $('.form-title').offset().top        
            }, 'slow');
        }
    });
    
    function validate_captcha()
    {
        var text = txtCaptcha.value;
        str = text.replace(/ +/g, "");

        var code = security_code.value;

        if (str !== code)
        {
            CaptchaError.innerHTML = "Invalid Captcha Code";
            return false;
        } else
        {
            CaptchaError.innerHTML = "";
            return true;
        }
    }
    var SITEURL = <?php echo json_encode(base_url()) ?>;
    SITEURL = SITEURL.replace("http://","https://");
</script>
<style>    
    .feature-block .feature-block-div:nth-child(1), .feature-block .feature-block-div:nth-child(2){
        display: inline-block;
    }
    .feature-block .feature-block-div:nth-child(2){
        float: right;
    }
    .feature-block{
        padding: 0;
        margin-top: 0;
    }
</style>


<script src="https://checkout.razorpay.com/v1/checkout.js"></script>
<script src="<?=base_url()?>assets/js/razorpay_checkout.js"></script>
<?php 
    $license=$report_data->license;
    $report_license=str_replace("_", " ", $license)." License";
?>
<ul class="breadcrumb m-b-0">
    <li class="breadcrumb-item"><a href="<?php echo base_url();?>"><img src="<?=base_url().'assets/images/home.png'?>" title='Go To Home' height="30px" width="30px" alt="Home Breadcrumb Icon" /></a></li>
    <li class="breadcrumb-item"><a href="<?php echo base_url().'latest-reports';?>">Industry Reports</a></li>
    <li class="breadcrumb-item" title="<?php echo $report_data->title; ?>">Global Checkout</li>
</ul>


<!-- Title page -->
<section class="bg10 txt-center p-lr-15 p-t-30 p-b-30 form-title">
    <h2 class="ltext-103 cl5 txt-center">
        Global Checkout
    </h2>
</section>	

<div class="row"> 
    <div class="rd-block">
        <div class="col-md-12 txt-center">
            <h4 class="report-sample-title"><?php echo $report_data->title; ?></h4>
        </div>
    </div> 
</div>

<main class="contact-page p-b-10">
    <div class="container-fluid">
        <div class="contact">
            <div class="row">
                <div class="col-md-4 md-hidden">
                    <div class="panel-body checkout-body cart-body">                    
                        <h3 class='cart-title'>Cart Total</h3>
                        
                        <section class="prr-about-work-with-us bg10 p-t-20 p-b-10"> 
                            <div class="row">
                                <div class='col-md-12'>
                                    <div class="feature-block">
                                        <div class="feature-block-div"><span>ID</span> : PRR-<?=$report_data->id?></div>
                                        <div class="feature-block-div" title="Report is available in Excel, PDF, Powerpoint and Word Formats"><span>Format</span> : 
                                            <i class="fa fa-file-excel-o cl3"></i>&nbsp;
                                            <i class="fa fa-file-pdf-o cl3"></i>&nbsp;
                                            <i class="fa fa-file-powerpoint-o cl3"></i>&nbsp;
                                            <i class="fa fa-file-word-o cl3"></i>&nbsp;
                                        </div>
                                    </div>
                                </div>
                            </div> 
                        </section>

                        <div class="row">
                            <div class="col-md-3 m-t-10">
                                <span class="col-title"><b>Report Title:</b></span>
                            </div>
                            <div class="col-md-9 m-t-10 m-b-10">
                                <a class="cart-report-title" onclick="return false;"><?=$report_data->title?> </a>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-2 m-t-10 m-b-10 col-block-1">
                                <span class="col-title"><b>License:</b></span>
                            </div>
                            <div class="col-md-10 m-b-10 col-block-2">
                                <span class="pull-right"><?php  echo $report_license;  ?></span>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <span class="col-title"><b>Sub Total:</b></span>
                            </div>
                            <div class="col-md-8">
                                <span class="pull-right price-quote">
                                    <?= (isset($report_data->currency) && !empty($report_data->currency) ? $report_data->currency : 'USD').' '.@$report_data->price?>
                                </span>
                            </div>
                        </div>
                        <div class="row grand-total m-t-10">
                            <div class="col-md-8">
                                <b>GRAND TOTAL :</b>
                            </div>
                            <div class="col-md-4">
                                <span class="total-right">
                                    <span id='grand_total'>
                                        <?=(isset($report_data->currency) && !empty($report_data->currency) ? $report_data->currency : 'USD'); ?>
                                        <?php  if (isset($report_data->price)) {  echo $report_data->price; }  ?>
                                    </span>
                                </span>
                            </div>
                        </div>    
                    </div>
                </div>
                <div class="col-md-8">
                    <?php
                        $invalid_message=(@$message) ? @$message : '';
                        if(!empty(@$invalid_message)){
                            echo "<div class='alert alert-danger alert-message'>".@$invalid_message."</div>";
                        }
                    ?>
                    <div class="contact-form">
                        
                        <div class=" form-side-block contact-info" align="center">
                            <h4 class="form-title">Fill the form below to proceed further for Checkout !</h4>
                        </div>
                        <form  class="form-horizontal" id="checkout-form" role="form" action="<?php echo base_url(); ?>checkout-process/<?=$report_data->id?>" method="post" data-toggle="validator" onsubmit="return validate_captcha()">
                        
                        <!--*************************    HIDDEN FIELDS    *****************************--> 

                            <input type="hidden" name="rep_title" id="rep_title" value="<?php if(isset($report_data)) {  echo $report_data->title;}  ?>"/>
                            <input type="hidden" id='rep_license' name="rep_license" value="<?php   echo $report_license;  ?>"/>
                            <input type="hidden" id='rep_price' name="rep_price" value="<?php echo $report_data->price;?>"/>
                            <input type="hidden" id='currency' name="currency" value="<?=@$report_data->currency?>"/>
                            <input type="hidden" id='is_global' name="is_global" value="1"/>
                            <input type="hidden" name="rep_id" id="rep_id" value="<?=$checkout_id ?>"/>  

                        <!--**************************    HIDDEN FIELDS     ***************************-->
                            <div class="row">
                                <div class="form-group col-sm-5 col-sm-offset-1">
                                    <label class="control-label">Name <span class="star">*</span> :</label>
                                    <input class="form-control" name="name" id="name"  value="<?=@$name ?? @$report_data->name?>" placeholder="Your Name" data-bv-field="full_name" type="text" data-error="Please enter your name" required/>
                                    <div class="help-block with-errors"></div> 
                                </div>
                                <div class="form-group  col-sm-5 col-sm-offset-1">
                                    <label class="control-label">Email <span class="star">*</span> :</label>
                                    <input class="form-control" name="email" id="email" value="<?=@$email ?? @$report_data->email?>"  placeholder="Business Email" data-bv-field="email" type="email" data-error="Please enter valid email address" required/>
                                    <div class="help-block with-errors"></div> 
                                </div>
                                <div class="form-group  col-sm-5 col-sm-offset-1">
                                    <label class="control-label">Phone <span class="star">*</span> :</label>
                                    <input class="form-control" name="phone" id="phone"  placeholder="Contact Number without country code" data-bv-field="phone" type="text" value="<?=@$phone ?? @$report_data->phone?>" data-error="Please enter phone number" required/>
                                    <div class="help-block with-errors"></div> 
                                </div>
                                <div class="form-group  col-sm-5 col-sm-offset-1">
                                    <label class="control-label">Company <span class="star">*</span> :</label>
                                    <input class="form-control" name="company" id="company" placeholder="Company Name" value="<?=@$company ?? @$report_data->company?>" data-bv-field="company" type="text" data-error="Please enter company / organization name"   required/>
                                    <div class="help-block with-errors"></div> 
                                </div>
                                <div class="form-group  col-sm-5 col-sm-offset-1">
                                    <label class="control-label">Country <span class="star">*</span> :</label>
                                    <?php if(!empty(@$report_data->country)) {
                                        $gc_country = explode('-',@$report_data->country);
                                        if(isset($country)){
                                            $country= explode('-', str_ireplace('\"',"",$country));
                                            $country_name = $country[1];
                                        }
                                    ?> 
                                        <input class="form-control" name="country" id="country" placeholder="country" data-bv-field="country" type="text" value="<?=isset($country) ? $country_name : @$gc_country[1]?>" data-error="Please enter country"   required/>
                                    <?php }else{ ?>
                                    <select name="country" id="country" class="form-control required selectpicker" data-bv-field="country" data-live-search="true" data-actions-box="true" data-error="This field can not be blank"  required>
                                        <?=getCountryDropdown()?>
                                    </select>
                                    <?php } ?>
                                    <div class="help-block with-errors"></div> 
                                </div>
                                <div class="form-group  col-sm-5 col-sm-offset-1">
                                    <label class="control-label">Zip Code <span class="star">*</span> :</label>
                                    <input type="text" name="zip" placeholder="Enter your Zip code" data-error="Please enter your zip code" id="zip" value="<?=@$zip ?? @$report_data->zip?>" class="form-control" required/>
                                    <div class="help-block with-errors"></div>
                                </div>
                                <div class="form-group col-sm-11 col-sm-offset-1">
                                    <label class="control-label">Adderss <span class="star">*</span> :</label>
                                    <textarea data-error="Please provide your address" rows="3"  class="form-control" name="address" id="address" data-bv-field="address"  required><?=@$address ?? @$report_data->address?></textarea>
                                    <div class="help-block with-errors"></div> 
                                </div>
                                
                                <div class="form-group col-sm-10 col-sm-offset-1">
                                    <strong>Payment Options: <span class="star">*</span>:</strong><br/><br/>
                                    <label class="container-radio">
                                        <img src="<?php echo base_url(); ?>assets/images/paypal.png" class="img img-responsive img-paypal"/> 
                                        <input type="radio" class="mode radio-inline pull-left" name="radio-inline" required="" value="Paypal" <?=@$mode === 'Paypal' ? 'checked' : '';?>  /><span class="checkmark"></span>
                                    </label>
                                    <label class="container-radio">
                                        <img src="<?php echo base_url(); ?>assets/images/bank-transfer.jpg" class="img img-responsive img-bank" alt="bank transfer" title="Bank Transfer"/> 
                                        <input type="radio" class="mode radio-inline pull-left" name="radio-inline" required=""  value="Bank Transfer" <?=@$mode === 'Bank Transfer' ? 'checked' : '';?>/><span class="checkmark"></span>
                                    </label>
                                    <label class="container-radio">
                                        <img src="<?php echo base_url(); ?>assets/images/eazypay.jpg" class="img img-responsive img-eazypay" alt="EazyPay" title="EazyPay"/> 
                                        <input type="radio" class="mode radio-inline pull-left" name="radio-inline"  required="" value="EazyPay"  <?=@$mode === 'EazyPay' ? 'checked' : '';?>/><span class="checkmark"></span>
                                    </label> 
                                    <i class="form-control-feedback" style="display: none;"></i>
                                    <div class="help-block with-errors"></div> 

                                </div>         
                                <div class="form-group col-sm-5 col-sm-offset-1">
                                    <label class="control-label" for="security_code">Security Code: <span class="star">*</span>: </label>
                                    <div class="input-group">
                                        <input data-error="Please enter security code" onkeyup="validate_captcha()" type="text" name="captcha_code" maxlength="6" placeholder="Security Code" class="form-control" id="security_code"  required/>
                                        <input type="text" id="txtCaptcha" readonly class="form-control" />
                                        <span class="input-group-append">
                                            <button title="Refresh Security Code" class="btn btn-captcha" type="button" onClick="DrawCaptcha();"> <span class="fa fa-refresh"></span> </button>
                                        </span> 
                                    </div>
                                    <p class="feedback" id="CaptchaError"></p>
                                    <div class="help-block with-errors"></div>
                                </div>
                                <div class="form-group col-sm-10 col-sm-offset-1 m-t-10"> 
                                    <button type="button" onclick="window.location.href='<?php echo base_url(); ?>reports/<?= @$report_data->rep_id;  ?>/<?= @$report_data->rep_url;  ?>'" class="btn btn-default btn-submit btn-shopping" >
                                        <strong><i class="fa fa-chevron-left" aria-hidden="true"></i>&nbsp;&nbsp;Continue Shopping</strong>       
                                    </button>
                                    <button type="submit"  class="btn btn-default btn-submit btn-checkout" >
                                        <strong>Proceed To Checkout&nbsp;&nbsp;<i class="fa fa-chevron-right" aria-hidden="true"></i></strong>       
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="col-md-4 sm-hidden">
                    <div class='sticky'>
                        <div class="panel-body checkout-body cart-body">                    
                            <h3 class='cart-title'>Cart Total</h3>

                            <section class="prr-about-work-with-us bg10 p-t-20 p-b-10 m-b-10"> 
                                <div class="row">
                                    <div class='col-md-12'>
                                        <div class="feature-block">
                                            <div class="feature-block-div"><span>ID</span> : PRR-<?=$report_data->id?></div>
                                            <div class="feature-block-div" title="Report is available in Excel, PDF, Powerpoint and Word Formats"><span>Format</span> : 
                                                <i class="fa fa-file-excel-o cl3"></i>&nbsp;
                                                <i class="fa fa-file-pdf-o cl3"></i>&nbsp;
                                                <i class="fa fa-file-powerpoint-o cl3"></i>&nbsp;
                                                <i class="fa fa-file-word-o cl3"></i>&nbsp;
                                            </div>
                                        </div>
                                    </div>
                                </div> 
                            </section>

                            <div class="row m-b-10">
                                <div class="col-md-3">
                                    <span class="col-title"><b>Report Title:</b></span>
                                </div>
                                <div class="col-md-9">
                                    <a class="cart-report-title" onclick="return false;"><?=$report_data->title?></a>
                                </div>
                            </div>
                            <div class="row m-b-10">
                                <div class="col-md-2">
                                    <span class="col-title"><b>License:</b></span>
                                </div>
                                <div class="col-md-10">
                                    <span class="pull-right"><?=@$report_license;  ?></span>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4">
                                    <span class="col-title"><b>Sub Total:</b></span>
                                </div>
                                <div class="col-md-8">
                                    <span class="pull-right price-quote">
                                        <?= (isset($report_data->currency) && !empty($report_data->currency) ? $report_data->currency : 'USD').' '.@$report_data->price?>
                                    </span>
                                </div>
                            </div>
                            <div class="row grand-total m-t-10">
                                <div class="col-md-8">
                                    <b>GRAND TOTAL :</b>
                                </div>
                                <div class="col-md-4">
                                    <span class="total-right">
                                        <span id='grand_total'>
                                            <?=(isset($report_data->currency) && !empty($report_data->currency) ? $report_data->currency : 'USD'); ?>
                                            <?php  if (isset($report_data->price)) {  echo $report_data->price; }  ?>
                                        </span>
                                    </span>
                                </div>
                            </div>    
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <hr class="contact-line"/>
    <section class="prr-about-work-with-us bg10 p-t-20 p-b-20">
        <div class="row landing-about-content">
            <div class="col-md-12" align='center'>
                <h3 class="mtext-113 cl14 p-b-16">
                    Contact PRR
                </h3>
            </div>
        </div><br/><br/>
        <div class="row p-b-20">
            <div class="col-md-4 landing-about-feature" align='center'>
                <span class="about-feature-icon fa fa-map-marker fa-3x"></span>
                <h5 class="about-feature-title">Location</h5>
                <?php
                    $conf = $this->config->item('conf');
                    if (array_key_exists('international_address', $conf)){ ?>
                    <p class="stext-116 text-capitalize"><?=strtolower(@$conf["international_address"])?></p>
                <?php } 
                    //if (array_key_exists('local_address', $conf)){
                ?>
                    <!-- <p class="stext-116 text-capitalize"><b>India : </b><?=strtolower(@$conf["local_address"])?></p> -->
                <?php //} ?>
            </div>
            <div class="col-md-4 landing-about-feature" align='center'>
                <span class="about-feature-icon fa fa-phone fa-3x"></span>
                <h5 class="about-feature-title">International</h5>
                <p class="stext-116"><a href='tel:<?=@$this->config->item('conf')["contact_number"]?>'><?=@$this->config->item('conf')["contact_number"]?></a></p>
            </div>
            <div class="col-md-4 landing-about-feature" align='center'>
                <span class="about-feature-icon fa fa-envelope fa-3x"></span>
                <h5 class="about-feature-title">Email Us</h5>
                <p class="stext-116" style="overflow-x: auto;"><a href='mailto:<?=@$this->config->item('conf')["email_address"]?>'><?=@$this->config->item('conf')["email_address"]?></a></p>
            </div>
        </div> 
    </section>
</main>

<!--**********************    FOOTER OPEN      ***************************-->

<?php require_once 'layouts/footer.php'; ?>

<!--**********************    FOOTER CLOSE     ***************************-->
