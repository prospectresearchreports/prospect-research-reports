<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Job Application | ProspectResearch Reports</title>
<!-- Basic -->
<meta charset="utf-8"/>
<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
<meta name="robots" content="index,follow"/>
<meta name="keywords" content="<?php echo $_GET["role"]; ?> Opening,Careers Application Form ,Career Opportunities at ProspectResearch Reports" />
<meta name="description" content="Go through our Careers page and here you will find various Career Opportunities at ProspectResearch Reports"/>
<meta name="author" content="ProspectResearch Reports"/>
<!--**********************    HEADER OPEN      ***************************-->

<?php require_once 'layouts/header.php'; ?>

<!--**********************    HEADER CLOSE     ***************************-->

<script src="<?php echo base_url();?>assets/js/validator.min.js"></script>
<script src="<?php echo base_url();?>assets/js/captcha.js"></script>
<script type="text/javascript">
    
// Form validation 

    function validate_captcha()
    {
        var text=txtCaptcha.value;
        str = text.replace(/ +/g, "");
        var code=security_code.value;

        if(str !== code)
        {
           CaptchaError.innerHTML="Invalid Captcha Code";
           return false;
        }
        else
        {
            CaptchaError.innerHTML="";
            return true;
        }
        

    }
</script>
<style>
    input.form-control {
        border-radius: 0px;
    }

    .star {
        color: red;
    }

    @media screen and (max-width:992px) {
        .form-horizontal .form-group {
            margin: 0px;
        }
        .col-md-offset-2 {
            text-align: center;
        }
    }
    @media screen and (max-width:767px){
        #txtCaptcha{
            width: 45%;
        }
    }
    @media screen and (min-width:767px){
        #txtCaptcha{
           width: 35%;
        }
    }

    .jumbotron ul li {
        padding: 8px;
        list-style-type: circle;
        font-size: 1.2em;
    }

    button.btn-submit {
        width: 150px;
        border-radius: 0px;
        font-size: 1.2em;
        font-weight: bold;
        margin-bottom: 10px;
        color: #fff;
    }

    .heading {
        padding: 15px;
        font-size: 1.32em;
        font-weight: 700;
        color: #090d2b;
        text-transform: uppercase;
    }

    h1.title {
        color: #090d2b;
        text-align: center;
        text-transform: uppercase;
        font-weight: bold;
        font-size: 2em;
    }
    button.go-back{
        background:#090d2b;
        border:none;
        border-radius: 2px;
        color:#FFFFFF;
        font-weight: bold;
        font-size: 1.2em;
        min-height:45px;
        max-height: 65px;
        padding: 10px;
        margin-left: 10px;
        cursor: pointer;
    }
</style>

 <?php  $role=($_GET["role"]) ? $_GET["role"] : (($role) ? $role : ""); ?>
<div class="row">
  <div class="col-md-12" style="padding: 0px;"> 
      <img class="mega-banner-image" src="<?php echo base_url(); ?>assets/images/careers-apply.jpg" alt="" />
  </div>    
</div>
 <br/>
 <button class="go-back" title="Go Back To Openings" onclick="window.history.back(-1)"><span class="fa fa-arrow-left"></span> Go Back To Openings</button><br/>

<div class="container-fluid">
    <div>
        <h1 class="title"><?php if(isset($role)){ echo $role;} ?></h1>
        <br/>
    </div>
    
<center>
    <?php
    if (isset($successText)) {
        echo $successText;
        echo '<meta http-equiv="refresh" content="2,url=' . base_url() . '"/>';
    }
    ?>
</center><br/>
<h4 class="heading">Job Description &amp; Responsibilties :</h4>
<div class="jumbotron">
    <?php if(isset($role)){if($role === "Accountant"){ ?>
    <ul>
        <li>Tally, data entry including sales, purchase, receipts, payments, bank reconciliation statement.</li>
        <li> Receiving and processing all invoices, expense forms and requests for payments Handling petty cash, preparing bills and receipts accounting records.</li>
    </ul>
    <?php } } ?>
    
    <?php if(isset($role)){if($role === "HR Manager"){ ?>
    <ul>
        <li>Hiring ,Coordinate various HR and admin operations, Be involved in process improvements.</li>
        <li> Resource allocations, Setting targets for team, Achieve organizational targets, Involve in training and Development.</li>
    </ul>
    <?php } } ?>
    
    <?php if(isset($role)){if($role === "Digital Marketing Intern"){ ?>
    <ul>
        <li>Drive traffic to the website utilizing various online channels.</li>
        <li>Improve search-engine rankings.</li>
        <li>Brainstorm, plan and execute SEO and digital marketing campaigns.</li>
        <li>Manage on page and off page optimization.</li>
        <li>Research popularity of sites linking to page and Finding new Sites.</li>
        <li>Monitors brand mentions and engagement opportunities.</li>
        <li>Should manage projects on organic optimization, paid search marketing, blog     implementation and link building.</li>
        <li>Article Posting</li>
        <li>Press Release Posting</li>
        <li>Blog Posting</li>
        <li>Doc and PPT submission</li>
        <li>Presence on Social Media Channels, Such as LinkedIn, Twitter, FB and More.</li>
    </ul>
    <?php } } ?>
    
    <?php if(isset($role)){if($role === "Web Designer/Graphics Designer"){ ?>
    <ul>
        <li>Designing new websites</li>
        <li>Rendering design mockups</li>
        <li>Work in co-ordination with developers</li>
        <li>Must be excellent in HTML and CSS Knowledge of Responsive HTML designing is preferred and understand the possibilities within HTML Layout will be added advantage.</li>
        <li>Must have good knowledge of CSS3 and HTML, Jquery, JavaSript, Dreamweaver, Adobe Photoshop & command on graphics</li>
    </ul>
    <?php } } ?>
    
     <?php if(isset($role)){if($role === "Sales/BD"){ ?>
    <ul>
        <li>Represent the company at National and International Industry Elements</li>
        <li>Maintain client account</li>
        <li>Map client's requirements and work accordingly</li>
        <li>Consult clients on their requirements</li>
        <li>Generating new business relationships and negotiating new revenue for agreed and new targets</li>
        <li>Maintaining effective client relationships and resolving their queries</li>
        <li>Keeping sales contract up to date</li>
        <li>Continually refine the sales process and ensure the sales work effectively</li>
        <li>Meet with business target set for the accounting year and ensure a steady booking of orders throughout the year</li>
        <li>Understand the client business challenges and position</li>
        <li>Track leads, proposals, status and appointments</li>
    </ul>
    <?php } } ?>
    
     <?php if(isset($role)){if($role === "Content Writer"){ ?>
    <ul>
        <li>Keeps up-to-date with best practices in writing for the web, social media.</li>
        <li>Create exciting creative & engaging content. </li>
        <li>Strong writing and editing skills with an understanding of the special requirements of writing for the Web. </li>
        <li>Writing engaging Blog & Editorial Content, Press releases, Promotional emails & Website content </li>
        <li>Writing Social Media Content & Engagement with followers</li>
    </ul>
    <?php } } ?>
</div>   

<h4 class="heading">Requirements :</h4>
<div class="jumbotron">
    <?php if(isset($role)){if($role === "Accountant"){ ?>
        <ul style="list-style-type: circle">
            <li> <strong>Sub Role</strong>  :  TALLY, MS-Excel, ERP, Others, TDS-provisioning, Service-tax </li>
            <li> <strong>Education</strong> : Any Graduate with domain knowledge </li>
            <li> <strong>Location</strong>  : Pune</li>
            <li> <strong>Vacancy</strong>   : 1</li>
        </ul>
    <?php } } ?>
    
    <?php if(isset($role)){if($role === "HR Manager"){ ?>
        <ul style="list-style-type: circle">
            <li> <strong>Skills</strong>    : Excellent Communication Skills required.Good Presentational & Organizational Skills. </li>
            <li> <strong>Education</strong> : MBA in HR(Freshers can apply) </li>
            <li> <strong>Location</strong>  : Pune</li>
            <li> <strong>Vacancy</strong>   : 1</li>
        </ul>
    <?php } } ?>
    
    <?php if(isset($role)){if($role === "Digital Marketing Intern"){ ?>
        <ul style="list-style-type: circle">
            <li> <strong>Education</strong> : B.Tech / B.E. - Any Specialization , Diploma - Any Specialization , B.Sc - Any Specialization , BCA - Computers , BCOM BBA </li>
            <li> <strong>Location</strong>  : Pune</li>
            <li> <strong>Vacancy</strong>   : 25</li>
        </ul>
    <?php } } ?>
    
    <?php if(isset($role)){if($role === "Web Designer/Graphics Designer"){ ?>
        <ul style="list-style-type: circle">
            <li> <strong>Experience</strong>  :  1 - 2 Years</li>
            <li> <strong>Education</strong>   :  Any graduate / Designing skills Preferred</li>
            <li> <strong>Location</strong>    :  Pune</li>
        </ul>
    <?php } } ?>
    
     <?php if(isset($role)){if($role === "Sales/BD"){ ?>
        <ul style="list-style-type: circle">
            <li> <strong>Experience</strong>  :  1 - 2 Years[Market Research]</li>
            <li> <strong>Education</strong>   :  Any Graduate</li>
            <li> <strong>Location</strong>    :  Pune</li>
            <li> <strong>Vacancy</strong>     : 10</li>
        </ul>
    <?php } } ?>
    
     <?php if(isset($role)){if($role === "Content Writer"){ ?>
        <ul style="list-style-type: circle">
            <li> <strong>Experience</strong>  :  1 - 2 Years</li>
            <li> <strong>Education</strong>   :  BBA / MBA / B.E / B.TEch / MCA / BCA / Gradutae</li>
            <li> <strong>Location</strong>    :  Pune</li>
            <li> <strong>Vacancy</strong>     : 10</li>
        </ul>
    <?php } } ?>
</div> 

<h4 class="heading">Apply for this position :</h4>
<div class="content-block-shadow"><br/>
    <form class="form-horizontal" id="form" role="form" action="<?php echo base_url(); ?>career-apply-process" method="post" data-toggle="validator" onsubmit="return validate_captcha()" style="text-align:left" enctype="multipart/form-data">
        <div class="form-group">
            <label class="col-md-2 control-label" style="float:left">Position Applied For : </label>
            <div class="col-md-7">
                <input class="form-control" readonly="readonly" name="role" id="role"  value="<?php if(isset($role)) { echo $role; } ?>"  type="text" />
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-2 control-label" style="float:left">NAME <span class="star">*</span> :</label>
            <div class="col-md-7">
                <input class="form-control" autocomplete="name" name="name" id="name"  placeholder="Please Enter Your Name" data-bv-field="name" type="text" data-error="Please enter your name" required/>
                <div class="help-block with-errors"></div> 
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-2 control-label">EMAIL <span class="star">*</span> :</label>
            <div class="col-md-7">
                <input class="form-control" autocomplete="email" name="email" id="email"  placeholder="Please Enter Your Email" data-bv-field="email" type="email" data-error="Please enter valid email id"  required/>
                <div class="help-block with-errors"></div> 
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-2 control-label">RESUME <span class="star">*</span> :</label>
            <div class="col-md-7">
                <input name="resume" id="resume" class="form-control" data-bv-field="resume" type="file" data-error="Please attach resume"  onchange="return ValidateExtension();"    required/>
                <div class="help-block with-errors"></div> 
                <div style="color:#a94442"><span id="feedback"></span><?php if (isset($error)) { echo $error ; } ?></div>
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-2 control-label">ABOUT YOURSELF <span class="star">*</span> :</label>
            <div class="col-md-7">
                <textarea data-error="Please mention your skills in brief..." rows="5"  class="form-control" name="message" id="message" data-bv-field="message"  required></textarea>
                <i class="form-control-feedback" data-bv-icon-for="message" style="display: none;"></i> 
                <div class="help-block with-errors"></div> 
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-md-2" for="security_code">SECURITY CODE: <span class="star">*</span>: </label>
            <div class="col-md-7">
                <div class="input-group">
                    <div class="input-group" style="padding-right:0;">
                        <input data-error="Please enter security code" type="text" name="captcha_code" maxlength="6"   placeholder="Security Code" class="form-control" id="security_code" style="height:34px;width:55%" required data-bv-field="security_message"/>
                        <input type="text" id="txtCaptcha" readonly class="form-control"  style="font-weight: bold;font-size: 1.2em" />
                        <span class="input-group-btn">
                            <button class="btn btn-secondary" type="button" style="border:none;height:34px;float: left" onclick="DrawCaptcha();"> <span class="fa fa-refresh"></span> </button>
                        </span> 
                    </div>
                </div><br/>
                <p style="color:#a94442" id="CaptchaError"></p>
                <i class="form-control-feedback" data-bv-icon-for="security_message" style="display: none;"></i> 
                <div class="help-block with-errors"></div> 
            </div>
        </div>
        <div class="col-md-offset-2">
            <button type="submit" class="btn btn-success btn-lg btn-submit"  >APPLY</button>
        </div>
    </form><br/>
</div>

<div style="font-size: 20px;text-align: center">
    <h5 style="color:green;font-weight: bold">--- OR ---</h5><br/>
    <h4>Mail us your resume :  <a href="mailto:hr@prospectresearchreports.com" target="_top" style="text-decoration:none;color: #dd4b39">hr@prospectresearchreports.com</a></h4>
</div>
</div><br/><br/>

<!--**********************    FOOTER OPEN      ***************************-->

<?php require_once 'layouts/footer.php'; ?>

<!--**********************    FOOTER CLOSE     ***************************-->

<script>
function ValidateExtension() {
        var allowedFiles = [".doc", ".docx", ".pdf", ".xls"];
        var fileUpload = document.getElementById("resume");
        var feedback = document.getElementById("feedback");
        var regex = new RegExp("([a-zA-Z0-9\s_\\.\-:])+(" + allowedFiles.join('|') + ")$");
        if (!regex.test(fileUpload.value.toLowerCase())) {
            feedback.innerHTML = "Please upload files having extensions: <b>" + allowedFiles.join(', ') + "</b> only.";
            return false;
        }
        feedback.innerHTML = "";
        return true;
    }
</script>