<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title><?php echo isset($meta->title) ? $meta->title : 'Careers | ProspectResearch Reports'; ?></title>
<!-- Basic -->
<meta charset="utf-8"/>
<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
<meta name="robots" content="index,follow"/>
<meta name="keywords" content="<?php echo isset($meta->keywords) ? $meta->keywords : 'Careers Application Form ,Career Opportunities at ProspectResearch Reports' ; ?>" />
<meta name="description" content="<?php echo isset($meta->description) ? $meta->description : 'Go through our Careers page and here you will find various Career Opportunities at ProspectResearch Reports'; ?>  "/>
<meta name="author" content="ProspectResearch Reports"/>

<!--**********************    HEADER OPEN      ***************************-->

<?php require_once 'layouts/header.php'; ?>

<!--**********************    HEADER CLOSE     ***************************-->

<script src="<?php echo base_url();?>assets/js/validator.min.js"></script>
<script src="<?php echo base_url();?>assets/js/captcha.js"></script>
<script type="text/javascript">
    
    // Form validation 

    function validate_captcha()
    {
        var text=txtCaptcha.value;
        str = text.replace(/ +/g, "");
        var code=security_code.value;

        if(str !== code)
        {
           CaptchaError.innerHTML="Invalid Captcha Code";
           return false;
        }
        else
        {
            CaptchaError.innerHTML="";
            return true;
        }
    }
    function ValidateExtension() {
        var allowedFiles = [".doc", ".docx", ".pdf", ".xls",".xlx"];
        var resume = document.getElementById("resume");
        var feedback = document.getElementById("feedback");
        var regex = new RegExp("([a-zA-Z0-9\s_\\.\-:])+(" + allowedFiles.join('|') + ")$");
        console.log(resume.value)
        if (!regex.test(resume.value.toLowerCase())) {
            feedback.innerHTML = "Please upload files having extensions: <b>" + allowedFiles.join(', ') + "</b> only.";
            return false;
        }
        feedback.innerHTML = "";
        return true;
    }
</script>

<style>
    p, h3{
        line-height: 1.8 !important;
    }
</style>

<ul class="breadcrumb m-b-0">
    <li class="breadcrumb-item"><a href="<?=base_url();?>"><img src="<?=base_url().'assets/images/home.png'?>" title='Go To Home' height="30px" width="30px" alt="Home Breadcrumb Icon" /></a></li>
    <li class="breadcrumb-item">Careers</li>
</ul>

<!-- Title page -->
<section class="bg10 txt-center p-lr-15 p-tb-50">
    <h2 class="ltext-103 cl5 txt-center">
        Careers
    </h2>
</section>	

<!-- <div class="page-banner" align="center">
    <h1 class="career-page-title">CAREERS</h1>
    <img class="img img-responsive" style="margin-top: 0;" src="<?php echo base_url(); ?>/assets/images/careers-background.jpg" alt="Careers at ProspectResearch Reports"/>
</div>  -->
<section class="bg0 p-t-40 p-b-60">
    <div class="container">
        <center>
            <?php
            if (isset($successText)) {
                echo "<p class='m-t-10 m-b-10 p-b-10 p-t-10'>".$successText."</p>";
                echo '<meta http-equiv="refresh" content="2,url=' . base_url() . 'careers"/>';
            }
            ?>
        </center>
        <div class="row">
            <div class="col-11 col-md-6 col-lg-5 m-lr-auto md-hidden m-b-10 p-b-20">
                <div class="how-bor1">
                    <div class="hov-img0 txt-center">
                        <img src="<?=base_url()?>assets/images/career-side.jpg" style='height:250px;width:auto;max-width:100%' alt="What's Next">
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-lg-7">
                <blockquote class="blockquote">
                    <p class="blockquote-para">Your work is going to fill a large part of your life, and the only way to be truly satisfied is to do what you believe is great work. And the only way to do great work is to love what you do. </p>
                    <footer class="blockquote-footer">Steve Jobs</footer>
                </blockquote>
                <br/>
                <div class="contact-info">                    
                        <!-- <button class="btn no-jobs wow wobble infinite"  data-wow-duration="6s" data-wow-delay="0.5s"><span class="fa fa-hourglass-end"></span>&nbsp;&nbsp; No Job Openings</button> -->
                    <button class="btn no-jobs"><span class="fa fa-hourglass-end"></span>&nbsp;&nbsp; No Job Openings</button>
                    <div class="career-help-block">
                        <h4 class="block-1">Sorry ! We do not have job openings right now !</h4>
                    </div>
                </div>
            </div>
            
            <div class="col-11 col-md-6 col-lg-5 m-lr-auto sm-hidden">
                <div class="how-bor1">
                    <div class="hov-img0 txt-center">
                        <img src="<?=base_url()?>assets/images/career-side.jpg" style='height:250px;width:auto;max-width:100%' alt="What's Next">
                    </div>
                </div>
            </div>
        </div>

        <div class="contact">
            <div class="row">
                <div class="col-md-10 col-contact-9">
                    
                    <div class=" form-side-block contact-info sticky" align="center">
                        <p class="fa fa-envelope fa-3x"></p>
                        <h4 class="form-title">Fill the form below to apply !</h4>
                    </div>
                </div>
                <div class="col-md-10 col-contact">
                    <div class="contact-form">
                        <h5 class="form-help-block m-b-20">Don't worry ! Drop your resume here we will get back to you !</h5>
                        
                        <form id="form" role="form" action="<?php echo base_url(); ?>career-form-process" method="post" data-toggle="validator" onsubmit="return validate_captcha()"  enctype="multipart/form-data">
                            <div class="row">
                                <div class="form-group col-sm-6">
                                    <label class="control-label">Name <span class="star">*</span> :</label>
                                    <input class="form-control" name="name" id="name"  placeholder="Your Name" data-bv-field="full_name" type="text" value="<?=@$name?>" data-error="Please enter your name" required/>
                                    <div class="help-block with-errors"></div> 
                                </div>
                                <div class="form-group col-sm-6">
                                    <label class="control-label">Email <span class="star">*</span> :</label>
                                    <input class="form-control" name="email" id="email"  placeholder="Business Email" data-bv-field="email" type="email" value="<?=@$email?>" data-error="Please enter valid email address" required/>
                                    <div class="help-block with-errors"></div> 
                                </div>
                                

                                <div class="form-group col-sm-12">
                                    <label class="control-label">About Yourself <span class="star">*</span> :</label>
                                    <textarea data-error="Please mention your skills in brief..." rows="5"  class="form-control" name="message" id="message" data-bv-field="message"  required placeholder="Mention your skills and work experience in brief..."></textarea>
                                    <i class="form-control-feedback" data-bv-icon-for="message" style="display: none;"></i> 
                                    <div class="help-block with-errors"></div> 
                                </div>

                                <div class="form-group col-sm-6">
                                    <label class="control-label">Resume <span class="star">*</span> :</label>
                                    <input name="resume" id="resume" class="form-control" data-bv-field="resume" type="file" data-error="Please attach resume"  onchange="return ValidateExtension();" required/>
                                    <div class="help-block with-errors"></div> 
                                    <div class="feedback"><span id="feedback"></span><?php if (isset($error)) { echo $error ; } ?></div>
                                </div>
                                <div class="form-group col-sm-6">
                                    <label class="control-label" for="security_code">Security Code: <span class="star">*</span>: </label>
                                    <div class="input-group">
                                        <input data-error="Please enter security code" onkeyup="validate_captcha()" type="text" name="captcha_code" maxlength="6" placeholder="Security Code" class="form-control" id="security_code"  required/>
                                        <input type="text" id="txtCaptcha" readonly class="form-control" />
                                        <span class="input-group-append">
                                            <button title="Refresh Security Code" class="btn btn-captcha" type="button" onClick="DrawCaptcha();"> <span class="fa fa-refresh"></span> </button>
                                        </span> 
                                    </div>
                                    <p class="feedback" id="CaptchaError"></p>
                                    <div class="help-block with-errors"></div> 
                                </div>
                            </div>

                            <div>        
                                <button type="submit" class="btn btn-default btn-submit">
                                    <span class="fa fa-send-o"></span> Submit Application
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!--**********************    FOOTER OPEN      ***************************-->

<?php require_once 'layouts/footer.php'; ?>

<!--**********************    FOOTER CLOSE     ***************************-->
