<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title><?=@$form_type?> - <?php echo $report_data->rep_title; ?></title>
<!-- Basic -->
<meta charset="utf-8"/>
<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
<meta name="robots" content="index,follow"/>
<meta name="keywords" content="<?=@$form_type?> ProspectResearch Reports,<?=@$form_type?> " />
<meta name="description" content="<?=@$form_type?> Request for Report - <?php echo $report_data->rep_title; ?>"/>
<meta name="author" content="ProspectResearch Reports"/>

<!--**********************    HEADER OPEN      ***************************-->

<?php require_once 'layouts/header.php'; ?>

<!--**********************    HEADER CLOSE     ***************************-->

<script src="<?php echo base_url(); ?>assets/js/validator.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/captcha.js"></script>

<!-- *****************   BOOTSTRAP SELECT          ******************  -->


<script type="text/javascript">
    $(document).ready(function(){    
        var w=$(window).width();
        if(w>767){
            $('html,body').animate({
                scrollTop: $('.breadcrumb').offset().top        
            }, 'slow');
        }
    });
    
    function validate_captcha(){
        var text = txtCaptcha.value;
        str = text.replace(/ +/g, "");

        var code = security_code.value;

        if (str !== code)
        {
            CaptchaError.innerHTML = "Invalid Captcha Code";
            return false;
        } else
        {
            CaptchaError.innerHTML = "";
            return true;
        }
    }
</script>
<?php $rd_url = base_url()."reports/".$report_data->rep_id."/".$report_data->rep_url ?>
<ul class="breadcrumb m-b-0">
    <li class="breadcrumb-item"><a href="<?=base_url();?>"><img src="<?=base_url().'assets/images/home.png'?>" title='Go To Home' height="30px" width="30px" alt="Home Breadcrumb Icon" /></a></li>
    <li class="breadcrumb-item"><a href="<?=@$rd_url?>" title="Go Back">Report Details</a></li>
    <li class="breadcrumb-item">
    <?php  
        $report_title = explode('Market',$report_data->rep_title);
        if(isset($report_title[0]) && strlen($report_title[0]) == 0){
            $report_title_array = explode(" ", $report_data->rep_title);
            $first_word_length = strlen($report_title_array[0]);
            $report_title = explode('Market',substr($report_data->rep_title, $first_word_length));
        }
        // echo "<pre>";print_r($report_title);exit;
        $report_title = str_ireplace(array(' -','- ','-'),' - ',$report_title[0]).' Market';
        echo strlen($report_title) > 130 ? substr($report_title , 0 ,130)."..." : $report_title; 
    ?>
    </li>
</ul>

<!-- Title page -->
<section class="bg10 txt-center p-lr-15 p-t-30 p-b-30 form-title">
    <h2 class="ltext-103 cl5 txt-center">
        <?=@$form_type?>
    </h2>
</section>	


<div class="row"> 
    <div class="rd-block">
        <div class="col-md-12 txt-center">
            <!-- <h2 class="request-sample text-center"><strong><?=@$form_type?></strong></h2> -->
            <h4 class="report-sample-title"><?php echo $report_data->rep_title; ?></h4>
        </div>
    </div> 
</div>
<section class="bg0 p-b-30">
    <div class="container">
        <!-- <h1 class="prr-page-title"><?=ucwords(@$form_type)?></h1> -->
        <div class="contact">
            <div class="row">  
                <div class="col-md-3 md-hidden lg-hidden m-b-10 p-l-0 p-r-0"> 
                    <section class="prr-about-work-with-us bg10 p-t-20 p-b-0 sticky">                                      
                        <div class="txt-center">                    
                            <img class="img img-responsive report-img" style='width:auto;vertical-align:middle' src="<?php echo base_url(); ?>assets/images/category/<?php echo $report_data->category_image; ?>" alt="Report Details"/>
                        </div>
                        <div class="row">
                            <div class="feature-block">
                                <div class="feature-block-div"><span>ID</span> : PRR-<?=$report_data->rep_id?></div>
                                <div class="feature-block-div"><span>Pages</span> : <?=$report_data->rep_page?></div>
                                <div class="feature-block-div" title="Report is available in Excel, PDF, Powerpoint and Word Formats"><span>Format</span> : 
                                    <i class="fa fa-file-excel-o cl3"></i>&nbsp;
                                    <i class="fa fa-file-pdf-o cl3"></i>&nbsp;
                                    <i class="fa fa-file-powerpoint-o cl3"></i>&nbsp;
                                    <i class="fa fa-file-word-o cl3"></i>&nbsp;
                                </div>
                                <div class="feature-block-div"><span>Industry</span> : <?=categories()[@$report_data->rep_sub_cat_1_id]?></div>
                            </div>
                        </div> 
                    </section>
                </div>
                <div class="col-md-9 p-sm-lr-0">              
                    <div class="col-contact-9 p-t-10">
                        <div class=" form-side-block contact-info sticky" align="center">
                            <!-- <p class="fa fa-envelope fa-3x"></p> -->                        
                            <p class="form-help-text m-b-20"><strong>Unlock valuable insights</strong> — request your sample today to experience our market research firsthand!</p>
                            <h4 class="form-title">Fill the form below to <?=strtolower(@$form_type)?> !</h4>
                        </div>
                        
                        <?php
                            $invalid_message=(@$message) ? @$message : '';
                            if(!empty(@$invalid_message)){
                                echo "<div class='alert alert-danger alert-message'>".@$invalid_message."</div>";
                            }
                        ?>
                    </div>
                    <div class="col-contact">
                        <div class="contact-form">
                            <form id="form" role="form" action="<?php echo base_url(); ?>report-form-process" method="post" data-toggle="validator" onsubmit="return validate_captcha()" >
                                <input type="hidden" name="form_type" value="<?=@$form_type?>" />
                                <input type="hidden" name="rep_title" value="<?=@$report_data->rep_title?>" />
                                <input type="hidden" name="rd_url" value="<?=@$rd_url?>" />
                                <input type="hidden" name="report_id" value="<?=@$report_data->rep_id?>" />
                                <div class="row">
                                    <div class="form-group col-sm-5 col-sm-offset-1">
                                        <label class="control-label">Name <span class="star">*</span> :</label>
                                        <input class="form-control" name="name" id="name"  placeholder="Your Name" data-bv-field="full_name" type="text" value="<?=@$name?>" data-error="Please enter your name" required/>
                                        <div class="help-block with-errors"></div> 
                                    </div>
                                    <div class="form-group col-sm-5">
                                        <label class="control-label">Email <span class="star">*</span> :</label>
                                        <input class="form-control" name="email" id="email"  placeholder="Business Email" data-bv-field="email" type="email" value="<?=@$email?>" data-error="Please enter valid email address" required/>
                                        <div class="help-block with-errors"></div> 
                                    </div>

                                    <div class="form-group col-sm-5 col-sm-offset-1">
                                        <label class="control-label">Company <span class="star">*</span> :</label>
                                        <input class="form-control" name="company" id="company" placeholder="Company Name" data-bv-field="company" type="text" value="<?=@$company?>" data-error="Please enter company / organization name"   required/>
                                        <div class="help-block with-errors"></div> 
                                    </div>
                                    <div class="form-group col-sm-5">
                                        <label class="control-label">Job Role <span class="star">*</span> :</label>
                                        <input class="form-control" name="job_role" id="job_role" value="<?=@$job_role?>" placeholder="Job Role" data-bv-field="job_role" type="text" data-error="Please mention job role"  required/>
                                        <div class="help-block with-errors"></div> 
                                    </div>

                                    <div class="form-group col-sm-5 col-sm-offset-1">
                                        <label class="control-label">Country <span class="star">*</span> :</label>
                                        <select name="country" class="form-control selectpicker" data-bv-field="country" data-live-search="true" data-actions-box="true" data-error="Please select your country"  required>
                                            <?=getCountryDropdown()?>
                                        </select>
                                        <div class="help-block with-errors"></div> 
                                    </div>
                                    <div class="form-group col-sm-5">
                                        <label class="control-label">Contact Number <span class="star">*</span> :</label>
                                        <input class="form-control"  pattern="^[0-9]{10}$" name="phone" id="phone" value="<?=@$phone?>" maxlength="10" placeholder="Contact Number without country code" data-bv-field="phone" type="text" data-error="Please enter contact number" required/>
                                        <div class="help-block with-errors"></div> 
                                    </div>

                                    <div class="form-group col-sm-10 col-sm-offset-1">
                                        <label class="control-label">Message  :</label>
                                        <textarea  maxlength='200'  placeholder="Your Feedback" rows="5" class="form-control" name="message" id="message"><?=@$contact_msg?></textarea>
                                        <div class="help-block with-errors"></div> 
                                    </div>
                                    <div class="form-group col-sm-5 col-sm-offset-1">
                                        <label class="control-label" for="security_code">Security Code: <span class="star">*</span>: </label>
                                        <div class="input-group">
                                            <input data-error="Please enter security code" onkeyup="validate_captcha()" type="text" name="captcha_code" maxlength="6" placeholder="Security Code" class="form-control" id="security_code"  required/>
                                            <input type="text" id="txtCaptcha" readonly class="form-control" />
                                            <span class="input-group-append">
                                                <button title="Refresh Security Code" class="btn btn-captcha" type="button" onClick="DrawCaptcha();"> <span class="fa fa-refresh"></span> </button>
                                            </span> 
                                        </div>
                                        <p class="feedback" id="CaptchaError"></p>
                                        <div class="help-block with-errors"></div> 
                                    </div>

                                    <div class="col-sm-10 col-sm-offset-1">        
                                        <button title="<?=@$form_type?>" type="submit" class="btn btn-default btn-submit">
                                            <span class="fa fa-send-o"></span> <?=@$form_type?>
                                        </button>
                                    </div>
                                </div><br/>
                                <div class="col-sm-10 col-sm-offset-1 form-privacy">
                                    NOTE: Your personal data is secure with us. Please read our <a href="<?php echo base_url(); ?>privacy-policy" target="_blank">privacy policy</a>
                                </div>
                            </form><br/>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 xs-hidden sm-hidden"> 
                    <section class="prr-about-work-with-us bg10 p-t-20 p-b-0 sticky">                                      
                        <div class="txt-center">                    
                            <img class="img img-responsive report-img" style='width:auto;vertical-align:middle' src="<?php echo base_url(); ?>assets/images/category/<?php echo $report_data->category_image; ?>" alt="Report Details"/>
                        </div>
                        <div class="row">
                            <div class="feature-block">
                                <div class="feature-block-div"><span>ID</span> : PRR-<?=$report_data->rep_id?></div>
                                <div class="feature-block-div"><span>Pages</span> : <?=$report_data->rep_page?></div>
                                <div class="feature-block-div" title="Report is available in Excel, PDF, Powerpoint and Word Formats"><span>Format</span> : 
                                    <i class="fa fa-file-excel-o cl3"></i>&nbsp;
                                    <i class="fa fa-file-pdf-o cl3"></i>&nbsp;
                                    <i class="fa fa-file-powerpoint-o cl3"></i>&nbsp;
                                    <i class="fa fa-file-word-o cl3"></i>&nbsp;
                                </div>
                                <div class="feature-block-div"><span>Industry</span> : <?=categories()[@$report_data->rep_sub_cat_1_id]?></div>
                            </div>
                        </div> 
                    </section>
                </div>
            </div>
        </div>
        <hr class="contact-line"/>
        <section class="prr-about-work-with-us bg10 p-t-20 p-b-20">
            <div class="row landing-about-content">
                <div class="col-md-12" align='center'>
                    <h3 class="mtext-113 cl14 p-b-16">
                        Contact PRR
                    </h3>
                </div>
            </div><br/><br/>
            <div class="row p-b-20">
                <div class="col-md-4 landing-about-feature" align='center'>
                    <span class="about-feature-icon fa fa-map-marker fa-3x"></span>
                    <h5 class="about-feature-title">Location</h5>
                    <?php
                    $conf = $this->config->item('conf');
                    if (array_key_exists('international_address', $conf)){ ?>
                    <p class="stext-116 text-capitalize"><?=strtolower(@$conf["international_address"])?></p>
                    <?php } 
                    //if (array_key_exists('local_address', $conf)){
                    ?>
                    <!-- <p class="stext-116 text-capitalize"><b>India : </b><?=strtolower(@$conf["local_address"])?></p> -->
                    <?php //} ?>
                </div>
                <div class="col-md-4 landing-about-feature" align='center'>
                    <span class="about-feature-icon fa fa-phone fa-3x"></span>
                    <h5 class="about-feature-title">International</h5>
                    <p class="stext-116"><a href='tel:<?=@$this->config->item('conf')["contact_number"]?>'><?=@$this->config->item('conf')["contact_number"]?></a></p>
                </div>
                <div class="col-md-4 landing-about-feature" align='center'>
                    <span class="about-feature-icon fa fa-envelope fa-3x"></span>
                    <h5 class="about-feature-title">Email Us</h5>
                    <p class="stext-116" style="overflow-x: auto;"><a href='mailto:<?=@$this->config->item('conf')["email_address"]?>'><?=@$this->config->item('conf')["email_address"]?></a></p>
                </div>
            </div> 
        </section>
    </div>
</section>

<!--**********************    FOOTER OPEN      ***************************-->

<?php require_once 'layouts/footer.php'; ?>

<!--**********************    FOOTER CLOSE     ***************************-->
       