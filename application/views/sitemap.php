<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title><?=isset($meta->title) ? $meta->title : 'Sitemap | ProspectResearch Reports';?></title>
<!-- Basic -->
<meta charset="utf-8"/>
<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
<meta name="robots" content="index,follow"/>
<meta name="keywords" content="<?=isset($meta->keywords) ? $meta->keywords : 'ProspectResearch Reports Sitemap, Sitemap ProspectResearch Reports';?>" />
<meta name="description" content="<?=isset($meta->description) ? $meta->description : 'Sitemap for ProspectResearch Reports';?>"/>
<meta name="author" content="ProspectResearch Reports"/>


<!--**********************    HEADER OPEN      ***************************-->

<?php require_once 'layouts/header.php'; ?>

<!--**********************    HEADER CLOSE     ***************************-->

<style>
    p, h3{
        line-height: 1.8 !important;
    }
</style>

<ul class="breadcrumb m-b-0">
    <li class="breadcrumb-item"><a href="<?=base_url();?>"><img src="<?=base_url().'assets/images/home.png'?>" title='Go To Home' height="30px" width="30px" alt="Home Breadcrumb Icon" /></a></li>
    <li class="breadcrumb-item">Sitemap</li>
</ul>


<!-- Title page -->
<section class="bg10 txt-center p-lr-15 p-tb-50 ">
    <h2 class="ltext-103 cl5 txt-center">
    Sitemap
    </h2>
</section>	

<!-- Content page -->
<section class="bg0 p-t-20 p-b-60 prr-sitemap">
    <div class="container">
        <div class="row p-b-48">
            <div class="col-md-12 col-lg-12 p-b-30 sitemap-content">
                <div class="p-t-7 p-r-85 p-r-15-lg p-r-0-md sitemap-widget-text">
                    <h3 class="ltext-102 cl14 p-b-16">
                    Sitemap
                    </h3>
   
                    <p class="widget-content m-t-10"><a href="<?=base_url(); ?>"> Home</a></p>
                    <p class="widget-content m-t-10"><a href="<?=base_url(); ?>latest-reports"> Industry Reports </a></p>
                    
                    <div class="row">
                        <div class="col-md-4 sitemap-widget-text">
                            <h6 class="widget-title m-t-20">About</h6>
                            <p class="widget-content m-t-10"><a href="<?=base_url(); ?>about-us">About Us</a></p>
                            <p class="widget-content m-t-10"><a href="<?=base_url(); ?>research-methodology">Research Methodology</a></p>
                            <p class="widget-content m-t-10"><a href="<?=base_url(); ?>careers">Careers</a></p>
                        </div>
                        <div class="col-md-4 sitemap-widget-text">
                            <h6 class="widget-title m-t-20">Services</h6>
                            <p class="widget-content m-t-10"><a href="<?=base_url(); ?>syndicated-research">Syndicated Research</a></p>
                            <p class="widget-content m-t-10"><a href="<?=base_url(); ?>procurement-solutions">Procurement Solutions</a></p>
                            <p class="widget-content m-t-10"><a href="<?=base_url(); ?>consulting-services">Consulting Services</a></p>
                        </div>                        
                        <div class="col-md-4 sitemap-widget-text">
                        <h6 class="widget-title m-t-20">Contact</h6>
                        <p class="widget-content m-t-10"><a href="<?=base_url(); ?>contact-us">Contact Us</a></p>
                        <p class="widget-content m-t-10"><a href="<?=base_url(); ?>custom-research">Custom Research Service</a></p>
                        </div>
                    </div>

                    <h6 class="widget-title m-t-20">Industry Sectors</h6>
                    <div class="row">
                        <div class="col-md-4 sitemap-widget-text">
                            <p class="widget-content m-t-10"><a href="<?=base_url(); ?>category/information-and-communication-technology-and-media-market-report">ICT Media</a></p>
                            <p class="widget-content m-t-10"><a href="<?=base_url(); ?>category/semiconductor-and-electronics">Semiconductor and Electronics</a></p>
                            <p class="widget-content m-t-10"><a href="<?=base_url()?>category/agriculture">Agriculture</a></p>
                            <p class="widget-content m-t-10"><a href="<?=base_url()?>category/energy-and-power">Energy and Power</a></p>
                            <p class="widget-content m-t-10"><a href="<?=base_url()?>category/machinery-and-equipment">Machinery and Equipments</a></p>
                        </div>
                        <div class="col-md-4 sitemap-widget-text">
                            <p class="widget-content m-t-10"><a href="<?=base_url()?>category/medical-devices">Medical Devices</a></p>
                            <p class="widget-content m-t-10"><a href="<?=base_url()?>category/material-and-chemicals">Material and Chemicals</a></p>
                            <p class="widget-content m-t-10"><a href="<?=base_url()?>category/automotive-transportation">Automotive &amp; Transportation</a></p>
                            <p class="widget-content m-t-10"><a href="<?=base_url()?>category/aerospace-Defence">Aerospace and Defence</a></p>
                            <p class="widget-content m-t-10"><a href="<?=base_url()?>category/food-and-beverages">Food and Beverages</a></p>
                        </div>
                        <div class="col-md-4 sitemap-widget-text">
                            <p class="widget-content m-t-10"><a href="<?=base_url()?>category/pharmaceutical-and-healthcare">Pharmaceutical and Healthcare</a></p>
                            <p class="widget-content m-t-10"><a href="<?=base_url()?>category/consumer-goods-and-services">Consumer Goods and Services</a></p>
                            <p class="widget-content m-t-10"><a href="<?=base_url()?>category/company-reports">Company Reports</a></p>
                            <p class="widget-content m-t-10"><a href="<?=base_url()?>category/construction-manufacturing">Construction and Manufacturing</a></p>
                            <p class="widget-content m-t-10"><a href="<?=base_url()?>category/banking-financial-services-and-insurance">Banking, Financial Services &amp; Insurance</a></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>



<!--**********************    sitemap OPEN      ***************************-->

<?php require_once 'layouts/footer.php'; ?>

<!--**********************    sitemap CLOSE     ***************************-->


