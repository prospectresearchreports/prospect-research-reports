<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title><?php echo isset($meta->title) ? $meta->title : 'Return Policy | ProspectResearch Reports';?></title>
<!-- Basic -->
<meta charset="utf-8"/>
<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
<meta name="robots" content="index,follow"/>
<meta name="keywords" content="<?php echo isset($meta->keywords) ? $meta->keywords : 'Refund Policy,  return policy,Refund Policy ProspectResearch Reports';?>" />
<meta name="description" content="<?php echo isset($meta->description) ? $meta->description : 'ProspectResearch Reports Refund policy states that Due to the nature of the information being sold, we unfortunately cannot accept returns of products once they have been delivered and no refund will be processed under any circumstances.';?>"/>
<meta name="author" content="ProspectResearch Reports"/>

<!--**********************    HEADER OPEN      ***************************-->

<?php require_once 'layouts/header.php'; ?>

<!--**********************    HEADER CLOSE     ***************************-->
  
<style>
    p, h3{
        line-height: 1.8 !important;
    }
</style>

<ul class="breadcrumb m-b-0">
    <li class="breadcrumb-item"><a href="<?=base_url();?>"><img src="<?=base_url().'assets/images/home.png'?>" title='Go To Home' height="30px" width="30px" alt="Home Breadcrumb Icon" /></a></li>
    <li class="breadcrumb-item">Return Policy</li>
</ul>


<!-- Title page -->
<section class="bg10 txt-center p-lr-15 p-tb-50 ">
    <h2 class="ltext-103 cl5 txt-center">
        Return Policy
    </h2>
</section>	


<!-- Content page -->
<section class="bg0 p-t-50 p-b-30">
    <div class="container">
        <div class="row p-b-30">
            <div class="col-md-12 col-lg-12 p-b-30">
                <div class="p-t-7 p-r-85 p-r-15-lg p-r-0-md res-challenge-div">
                    <h3 class="mtext-113 cl14 p-b-30">
                    Return Policy
                    </h3>
                    <p class="stext-110 cl8 p-b-26">
                    We take payments in line with the procedure mentioned on our website only. we are going to not be to blame for sales, usage, and related taxes & customs. Once the payment is completed, it'll not be refunded. any quries regarding payment/ problems, contact the sales representative. However, if the user believes that there are some errors occurring during transactions, in this case, he/she should immediately contact our customer service team.
                    </p>
                    <p class="stext-110 cl8 p-b-26">
                        We don’t accept returns after delivery of products because of format & nature of knowledge and knowledge contained within the reports. However, the customization are provided through us just in case there's missing or incomplete data within the reports.
                    </p>
                    <p class="stext-110 cl8 p-b-26">
                        If you have any questions about a report&#39;s coverage or relevance, simply
                        <a href="<?=base_url()?>contact-us" class="bg7 cl0 mtext-103 p-t-10 p-r-10 p-b-10 p-l-10 prr-static-btn">Contact Us</a> 
                    </p>
                </div>
            </div>
        </div>
    </div>
</section>


<!--**********************    FOOTER OPEN      ***************************-->

<?php require_once 'layouts/footer.php'; ?>

<!--**********************    FOOTER CLOSE     ***************************-->
