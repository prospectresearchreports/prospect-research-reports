<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title><?php echo isset($meta->title) ? $meta->title : 'Categories | ProspectResearch Reports';?></title>
<!-- Basic -->
<meta charset="utf-8"/>
<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
<meta name="robots" content="index,follow"/>
<meta name="keywords" content="<?php echo isset($meta->keywords) ? $meta->keywords : 'Categories ProspectResearch Reports, Industry Verticals ,Reports ,Industry Reports, Business Research Process, Market News,  Research Report, Business Research, Industrial Analysis, Market Research Analysis,  USA Marketing Research Process, China Research Report, Japan Business Market Research Report, Europe Industrial Analysis,Industry Analysis Report';?>" />
<meta name="description" content="<?php echo isset($meta->description) ? $meta->description : 'ProspectResearch Reports Offers wide variety of various industry reports and company reports by category and sub category';?>"/>
<meta name="author" content="ProspectResearch Reports"/>

    <!--**********************    HEADER OPEN      ***************************-->

    <?php require_once 'layouts/header.php'; ?>

    <!--**********************    HEADER CLOSE     ***************************-->
  
<style>
    p, h3{
        line-height: 1.8 !important;
    }
</style>

<ul class="breadcrumb m-b-0">
    <li class="breadcrumb-item"><a href="<?=base_url();?>"><img src="<?=base_url().'assets/images/home.png'?>" title='Go To Home' height="30px" width="30px" alt="Home Breadcrumb Icon" /></a></li>
    <li class="breadcrumb-item">Industry Sectors</li>
</ul>

<!-- Title page -->
<section class="bg10 txt-center p-lr-15 p-tb-50">
    <h2 class="ltext-103 cl5 txt-center">
    Industry Sectors
    </h2>
</section>	

<!-- Content page -->
<section class="bg0 p-t-75 p-b-60">
    <div class="container">
        
      <div class="row industries">
        <?php 
            $categories = get_categories();
            foreach($categories->result() as $row):
        ?>
        <div class="col-sm-6 col-lg-4 m-lr-auto m-t-5 m-b-5 respon4">
        <!-- Block1 -->
          <div class="block1 wrap-pic-w">
            <div align='center'>
              <img src="<?=base_url()?>assets/images/category/<?=getCategoryIcon()[@$row->sc1_id]?>" alt="<?=$row->sc1_name?>" style="opacity:0.68;">
            </div>

            <a href="<?=base_url(); ?>category/<?=$row->sc1_url;?>" class="block1-txt ab-t-l s-full flex-col-l-sb p-lr-38 p-tb-34 trans-03 respon3">
              <div class="block1-txt-child1 flex-col-l">
                <span class="block1-name mtext-101 trans-04 p-b-8">
                  <?=str_ireplace('and','&amp;',$row->sc1_name)?>
                </span>
              </div>

              <div class="block1-txt-child2 p-b-4 trans-05">
                <div class="block1-link stext-101 cl0 trans-09">
                  View Reports
                </div>
              </div>
            </a>
          </div>
        </div>
        <?php endforeach;  ?>
      </div>
    </div>
</section>

<!--**********************    FOOTER OPEN      ***************************-->

<?php require_once 'layouts/footer.php'; ?>

<!--**********************    FOOTER CLOSE     ***************************-->
