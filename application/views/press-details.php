<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <title><?=$records->title?> | ProspectResearch Reports</title>
        <!-- Basic -->
        <meta charset="utf-8"/>
        <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
        <meta name="robots" content="index,follow"/>
        <meta name="keywords" content="ProspectResearch Reports Press Release, Press Release " />
        <meta name="description" content="Press Release- <?php echo $records->title; ?>"/>
        <meta name="author" content="ProspectResearch Reports"/>


        <!--**********************    HEADER OPEN      ***************************-->

        <?php require_once 'layouts/header.php'; ?>

        <!--**********************    HEADER CLOSE     ***************************-->

        <style>
            p, h3{
                line-height: 1.8 !important;
            }
        </style>

        <ul class="breadcrumb m-b-0">
            <li class="breadcrumb-item"><a href="<?=base_url();?>"><img src="<?=base_url().'assets/images/home.png'?>" title='Go To Home' height="30px" width="30px" alt="Home Breadcrumb Icon" /></a></li>
            <li class="breadcrumb-item"><a href="<?php echo base_url();?>press-release">Press Releases</a></li>
            <li class="breadcrumb-item"><?php echo $records->title ?></li>
        </ul>
        
    <main  class="bg0 p-t-40 p-b-60">
        <div class="container">
            <div class="row">
                <div class="col-md-8 blog-post-wrapper">
                    <div class="post-header"  align='center'>
                        <!-- <h1 class="post-title">Press Release</h1> -->
                        <div class="wrap-pic-w how-pos5-parent">
							<img style="max-height: 400px;" src="<?php echo base_url(); ?>assets/images/press/<?php echo $records->image ?>" alt="<?=$records->title ?>">

							<div class="flex-col-c-m size-123 bg9 how-pos5">
								<span class="ltext-107 cl14 txt-center">
									<?php 
                                    $date = date_create($records->published_on);
                                    echo date_format($date, 'd');
                                    ?>
								</span>

								<span class="stext-109 cl3 txt-center">
                                <?=date_format($date, 'M, Y')?>
								</span>
							</div>
						</div>
                        <div class="flex-w flex-sb-m p-t-18">
                            <span class="flex-w flex-m stext-111 cl14 p-r-30 m-tb-10">
                                <span><strong>
                                    <?=categories()[@$records->cat_id]?>  
                                </strong></span>
                            </span>
                        </div>
                    </div>
                    <div class="post-content">
                        <div class="p-t-30 p-b-30">
                            <h4 class="ltext-108 cl14 hov-cl1 trans-04">
                                <?=$records->title ?>
                            </h4>
                        </div>
                        <div class="press-content"><?=getPressContent($records->content); ?></div>
                    </div>
                </div>
                <div class="col-md-4"> 
                    <section class="prr-about-work-with-us bg10 p-t-20 p-b-0 sticky">
                        <div class="row landing-about-content">
                            <div class="col-md-12" align='center'>
                                <h3 class="mtext-113 cl14">
                                    Contact PRR
                                </h3>
                            </div>
                        </div><br/>
                        <div class="row p-b-0 press-fixed-sidebar">
                            <div class="col-md-12 landing-about-feature p-b-20 pr-contact" align='center'>
                                <span class="about-feature-icon fa fa-map-marker fa-3x"></span>
                                <h5 class="about-feature-title">Location</h5>
                                <p class="stext-116">71-75 Shelton Street, Covent Garden, London, WC2H 9JQ</p>
                            </div>
                            <div class="col-md-12 landing-about-feature p-b-20 pr-contact" align='center'>
                                <span class="about-feature-icon fa fa-phone fa-3x"></span>
                                <h5 class="about-feature-title">International</h5>
                                <p class="stext-116"><a href='tel:+44-20-3885-0387'>+44-20-3885-0387</a></p>
                            </div>
                            <div class="col-md-12 landing-about-feature" align='center'>
                                <span class="about-feature-icon fa fa-envelope fa-3x"></span>
                                <h5 class="about-feature-title">Email Us</h5>
                                <p class="stext-116" style="overflow-x: auto;"><a href='mailto:help@prospectresearchreports.com'>help@prospectresearchreports.com</a></p>
                            </div>
                        </div> 
                    </section>
                </div>
            </div>
        </div>
    </main>
    <script>
        $(function(){
            // var right_side_height=$('.home-contact-block').height();
            // var col_9_height=$('.col-9').height();
            // if(col_9_height < right_side_height){
            //     $('.col-9').css({ height : right_side_height});
            //     $('.press-desc').css({  'line-height' : '2' });
            // }                    

            // $('.press-desc').children('blockquote').attr('class','blockquote')
            // $(".press-desc").each(function(){
            //     console.log($(this).children('p').text().length);
            // })
            // if($('.press-desc p').text() === '&nbsp;'){
            //     console.log($('.press-desc p').text())
            // }
        })
    </script>

        

<!--**********************    FOOTER OPEN      ***************************-->

<?php require_once 'layouts/footer.php'; ?>

<!--**********************    FOOTER CLOSE     ***************************-->



