<!DOCTYPE html>
<html>
<head>
<!-- Basic -->
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>Checkout Link Expired | ProspectResearch Reports</title>
<meta name="keywords" content="Checkout Link Expired, Link Expired" />
<meta name="description" content="Your Payment has not successfully completed . Please visit our home Page.">
<meta name="author" content="ProspectResearch Reports"/>

<!--**********************    HEADER OPEN      ***************************-->

<?php require_once 'layouts/header.php'; ?>

<!--**********************    HEADER CLOSE     ***************************-->

<main class="cancel-page">
    <div class="container">
        <div class="row">
            <div class="col-md-12" align='center'>
                <article class="prr-cancel-block">
                    <h1 class="prr-cancel-title" style="text-transform: uppercase;">Checkout Link Expired! </h1>
                    <h2 class="fa fa-times error-cancel m-b-30"></h2>
                    <hr/>
                    <p class="prr-contact-para mtext-101">
                        Having trouble? <a class="contact-link" href="<?=base_url(); ?>contact-us"><span class='fa fa-send'></span> Contact us</a>
                    </p><br/>
                    <p class="prr-homepage-link lead">
                        <a class="btn btn-home" href="<?=base_url(); ?>" role="button">Continue To Homepage</a>
                    </p>
                </article>
            </div>
        </div>
    </div>
</div>

<!--**********************    FOOTER OPEN      ***************************-->

<?php require_once 'layouts/footer.php'; ?>

<!--**********************    FOOTER CLOSE     ***************************-->


