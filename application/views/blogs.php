<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <title>Blogs |  ProspectResearch Reports</title>
        <!-- Basic -->
        <meta charset="utf-8"/>
        <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
        <meta name="robots" content="index,follow"/>
        <meta name="keywords" content="ProspectResearch Reports Press Release, Press Release " />
        <meta name="description" content="ProspectResearch Reports&#039;s press release section features all the industry statistics cited by the global news publication sites."/>
        <meta name="author" content="ProspectResearch Reports"/>
        
        
        

        <style type="text/css">
            @media screen and  (min-width:440px) and (max-width:767px){
                .page-number{
                    margin-top:4%;
                }
            }
            @media screen and  (max-width:440px){
                .page-number{
                    margin-top:8%;
                }
            }
            @media screen and (min-width:767px){
                .page-number{
                    margin-top:2%;
                }
                .press-title > span> a{
                    font-size:1em;
                }
            } 
            @media screen and (min-width:1200px){
                .press-border{ 
                   height: 300px;
                }
            }
            @media screen and (min-width: 991px) and (max-width:1200px){
                .press-border{ 
                   height: 330px;
                }
                
            }
            @media screen and (min-width:991px){
                
                .col-1{
                    margin-top: 1%;
                    width: 49%;
                    float: left;
                }
                .press-block{ 
                    width: 49%;
                    float: left;
                    display: inline;
                    margin: 1% 0.5%;
                }
                
            }
            @media screen and (max-width:991px){
                .row > .col-md-2{
                    height: 60px;
                }
                .row > .col-md-10{
                    padding: 25px;
                }
            }
            @media screen and (max-width:767px){
                .container,.row,.col-md-12{
                    margin:0;
                    padding:0;
                }
                .text-category-banner-left > h1{
                    font-size: 1.3em;
                    text-align: center;
                }
                .press-border{
                    margin: 10px 0px;
                }
                
            }
            .press-border{ 
                background:#eee;
                border:1px solid lightgray;
            }
            button.btn-details{
                border-radius: 0px;
                font-size:1.2em;
                font-family: Poppins;
                font-weight: bold;
                background: #000;
                float: right;
                margin-right: 10px;
            }
            .press-title > span> a{
                font-weight: bold;
                color: #000;
                text-decoration: none;
            }
            div.press-desc p{
                font-size: 1.4em;
                line-height: 25px;
            }
            .calendar{
                border: 2px solid #fff;
                width: 95px;
                height: 70px;
                border-radius: 12px;
                font-size: 1.1em;
            }
            .calendar-top{
                height: 28px;
                background: #090d2b;
                border-radius: 12px 12px 0px 0px;
                padding: 5px;

            }
            .calendar-body{
                border-style: solid;
                border-width: 0px 5px 5px 5px;
                border-color: #090d2b;
                height:55px; 
                border-radius:  0px 0px 12px 12px;
                background: lightgrey;
                padding: 7px;
                
            }
            
           
        </style>



<!--**********************    HEADER OPEN      ***************************-->

<?php require_once 'layouts/header.php'; ?>

<!--**********************    HEADER CLOSE     ***************************-->

<script>
    $(document).ready(function(){
        var w=$(window).width();
        if(w > 991){
            $(".col-1").removeClass("press-block");
        }
    })
</script>



<div class="row">
    <div class="col-md-12" style="padding: 0px;"> 
        <img class="mega-banner-image" src="<?php echo base_url(); ?>assets/images/Blog.png" alt=""/>
        <div class="text-category-banner-left">
            <h1 style="text-transform: uppercase">Blogs</h1>
            <hr class="sm-hidden" style="border: 1px solid #000;"/><br/>            
        </div>  
    </div>    
</div>
<div class="jumbotron" style="background:#e8f2f1;">
    <div class="container-fluid">
        <div class="clearfix">
            <div style="display:inline-block"><?php if (isset($pagination)) echo $pagination; ?> </div>
            <div class="pull-right page-number"  style="display:inline-block">
            <?php if (isset($total_reports)) {if ($total_reports > 15) { ?> Page  <?php if (isset($page)) echo $page; ?> of <?php if (isset($total_pages)) echo $total_pages; } } ?></div>
        </div>
        <?php
        if ($total_reports <= 0) {
        ?>
            <article>
                <br/>
                <br/>
                <h2 style="color:#0465ac;text-align: center">Coming Soon.......</h2>
                <br/><br/><br/><br/>
            </article>
            
        <?php
        } else {
            if (isset($blog_list)) {
            $i=1;
            foreach ($blog_list->result() as $row) {
        ?>
        <div class="press-block col-<?php echo $i++; ?>">
            <div class="press-border" ><br/>
                <div itemscope itemtype="http://schema.org/Product" style="margin-bottom:5px">
                    <div class="row">
                        <div class="col-md-2" align="center">
                            <div class="calendar">
                                <div class="calendar-top">
                                    <span class="year" style="color:white"><?php $date=date_create($row->blog_date);  echo date_format($date, 'Y') ?></span>
                                </div>
                                <div class="calendar-body">                                    
                                    <span><?php $date=date_create($row->blog_date);  echo date_format($date, 'd') ?></span>
                                    <span><?php $date=date_create($row->blog_date);  echo date_format($date, 'F') ?></span>
                                </div>
                                <!--<div class="press-image">
                                    <img itemprop="image" class="img img-responsive" style="height:70px;width:70px;" src="<?php echo base_url(); ?>assets/images/press/calendar.png" alt=""/>
                                </div>
                                <div class="calendar" style="font-size:14px;font-weight: bold;">
                                    <span style="color:white"><?php $date=date_create($row->p_date);  echo date_format($date, 'Y') ?></span>
                                    <div class="calendar-partition">
                                        <span><?php $date=date_create($row->p_date);  echo date_format($date, 'd') ?></span>
                                        <span><?php $date=date_create($row->p_date);  echo date_format($date, 'F') ?></span>
                                    </div>
                                </div>-->
                            </div>
                        </div>
                        <div class="col-md-10">
                            <p class="press-title"><span itemprop="name"><a href="<?php echo base_url(); ?>blog/<?php echo $row->blog_url; ?>" rel="follow"><?php echo $row->blog_title; ?></a></span></p>
                            <div class="press-desc">
                                <h6><span itemprop="description"><?php $str = $row->blog_content; echo substr($str, 0, 350); ?></span></h6>
                            </div>
                            <button class="btn btn-primary btn-details" onclick="window.location.href='<?php echo base_url();?>blog/<?php echo $row->blog_url; ?>'">View Details</button>
                        </div>  
                    </div>
                </div>
            </div>
        </div>
        <?php  }  } } ?>
        <div class="clearfix">
            <div style="display:inline-block"><?php if (isset($pagination)) echo $pagination; ?> </div>
        </div>
    </div>
</div>
<br/>

<!--**********************    FOOTER OPEN      ***************************-->

<?php require_once 'layouts/footer.php'; ?>

<!--**********************    FOOTER CLOSE     ***************************-->





