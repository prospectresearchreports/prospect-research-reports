<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title><?=isset($meta->title) ? $meta->title : 'About Us | ProspectResearch Reports';?></title>
<!-- Basic -->
<meta charset="utf-8"/>
<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
<meta name="robots" content="index,follow"/>
<meta name="keywords" content="<?=(@$meta->keywords) ? @$meta->keywords : 'About ProspectResearch Reports'; ?>" />
<meta name="description" content="<?=(@$meta->description) ? @$meta->description : 'At ProspectResearch Reports we tend to aim to create it easier for decision makers to search out relevant info and find right research reports which might save their time and assist in what they are doing best, i.e. take time-critical choices.'; ?>"/>
<meta name="author" content="ProspectResearch Reports"/>

<!--**********************    HEADER OPEN      ***************************-->

<?php require_once 'layouts/header.php'; ?>

<!--**********************    HEADER CLOSE     ***************************-->

<style>
    p, h3{
        line-height: 1.8 !important;
    }
</style>

<ul class="breadcrumb m-b-0">
    <li class="breadcrumb-item"><a href="<?=base_url();?>"><img src="<?=base_url().'assets/images/home.png'?>" title='Go To Home' height="30px" width="30px" alt="Home Breadcrumb Icon" /></a></li>
    <li class="breadcrumb-item">About Us</li>
</ul>

<!-- Title page -->
<section class="bg10 txt-center p-lr-15 p-tb-50">
    <h2 class="ltext-103 cl5 txt-center">
        About Us
    </h2>
</section>	

	<!-- Content page -->
	<section class="bg0 p-t-75 p-b-60">
		<div class="container">
			<div class="row p-b-48">
				<div class="col-md-6 col-lg-7">
					<div class="p-t-7 p-r-85 p-r-15-lg p-r-0-md">
						<h3 class="mtext-111 cl14 p-b-16">
							Our Story
						</h3>

						<p class="stext-110 cl6 p-b-26">
                        A backstage artist of many popular shows, having their best hand in the success of the show", And this can be not what we claim, but this is often what our clients tell about us and our reports. In easy words, a research company, our reports and expertise into and out of doors your business circles is the bride to the success of your business.We are leaders in providing high quality marketing research, analysis and consulting services across India.
						</p>

						<p class="stext-110 cl6 p-b-26">
                        It is our pleasure to provide not only the best and most simplified research services possible, but also the best in personalized client care services to you. 
						</p>
					</div>
				</div>

				<div class="col-11 col-md-6 col-lg-5 m-lr-auto">
					<div class="how-bor1">
						<div class="hov-img0 txt-center">
							<img src="<?=base_url()?>assets/images/about-us.jpg" style='height:200px;width:auto;max-width:100%' alt="About Us">
						</div>
					</div>
				</div>
			</div>
			
			<div class="row">
				<div class="order-md-2 col-md-7 col-lg-8 p-b-30">
					<div class="p-t-7 p-l-85 p-l-15-lg p-l-0-md">
						<h3 class="mtext-111 cl14 p-b-16">
							Our Vision
						</h3>

                        <p class="stext-110 cl6 p-b-26">ProspectResearch Reports Insights guarantees best and optimum research solutions to enable you to achieve the following:</p>
                        <ul class="about-list">
                            <li>Estimate the necessities of clients.</li>
                            <li>Providing Transparent and market insights.</li>
                            <li>Having a correct strategies for growing organization.</li>
                            <li>Logical and Valuable decisions.</li>
                            <li>Potential customers are main picture of growing business.</li>
                        </ul>

					</div>
				</div>

				<div class="order-md-1 col-11 col-md-5 col-lg-4 m-lr-auto p-b-30">
					<div class="how-bor2">
						<div class="hov-img0">
							<img src="<?=base_url()?>assets/images/our-vision.jpg" alt="PRR Vision">
						</div>
					</div>
				</div>
			</div>
            
			
			<div class="row">
				<div class="order-md-1 col-md-7 col-lg-8 p-b-30">
					<div class="p-t-7 p-l-85 p-l-15-lg p-l-0-md">
						<h3 class="mtext-111 cl14 p-b-16">
							Our Mission
						</h3>

                        <p class="stext-110 cl6 p-b-26">We aim to provide the best quality of prospect research reports from the top most publishers of the world to you at the most affordable price you can ever get.</p>
                        <p class="stext-110 cl6 p-b-26"><b>Our secondary missions are:</b></p>
                        <ul class="about-list">
                            <li>To provide you the best value of your money.</li>
                            <li>To end your search of prospect research reports on our website.</li>
                            <li>Giving the best service to all our customers.</li>
                            <li>We aim to give that best service to you that you don'tgo unsatisfied from our website. </li>
                        </ul>

					</div>
				</div>

				<div class="order-md-2 col-11 col-md-5 col-lg-4 m-lr-auto p-b-30">
					<div class="how-bor2">
						<div class="hov-img0">
							<img src="<?=base_url()?>assets/images/our-mission.jpg" alt="PRR Mission">
						</div>
					</div>
				</div>
			</div>
            
			
			<div class="row">
				<div class="order-md-2 col-md-7 col-lg-8 p-b-30">
					<div class="p-t-7 p-l-85 p-l-15-lg p-l-0-md">
						<h3 class="mtext-111 cl14 p-b-16">
							Our Values
						</h3>

                        <ul class="about-list">
                            <li><b>Innovation – </b> To develop unique research methods supported advanced technologies and offer speed, flexibility and cost-effectiveness to our clients.</li>
                            <li><b> Reliability – </b> We'll never compromise in terms of information quality.</li>
                            <li><b>Consultancy – </b> We guarantee the best level of consulting services and in-depth insights.</li>
                            <li><b>Trust – </b> We are going to never give promises we cannot fulfill.</li>
                            <li><b>Social responsibility – </b> We believe better community which each contribution can make a change.</li>
                        </ul>

					</div>
				</div>

				<div class="order-md-1 col-11 col-md-5 col-lg-4 m-lr-auto p-b-30">
					<div class="how-bor2">
						<div class="hov-img0">
							<img src="<?=base_url()?>assets/images/our-values.jpg" alt="PRR Values">
						</div>
					</div>
				</div>
			</div>

            
			<div class="row p-t-40">
				<div class="order-md-1 col-md-7 col-lg-8 p-b-30">
					<div class="p-t-7 p-l-85 p-l-15-lg p-l-0-md">
						<h3 class="mtext-111 cl14 p-b-16">
							What We Do
						</h3>

                        <p class="stext-110 cl6 p-b-26">Business is never easy. And when you invest your time and money in your business, then definitely you don't want to face failure. But what can be the solution? Though your sales reports and those thick ledger books may help you in some way. But then what about your competitors? Nothing can match the quality of a well researched business intelligence report that not only knows you, but all your competitors.</p>
                        <p class="stext-110 cl6 p-b-26">That’s not all. What about the market trends? that may also affect your business in lot more ways than expected. This is why our reports help companies lay down the roadmap for the future carefully minding every trend/probability, that would create an opportunity for growth.</p>
					</div>
				</div>

				<div class="order-md-2 col-11 col-md-5 col-lg-4 m-lr-auto p-b-30">
					<div class="how-bor2">
						<div class="hov-img0">
							<img src="<?=base_url()?>assets/images/what-we-do.jpg" alt="What We Do">
						</div>
					</div>
				</div>
			</div>


            
        <section class="prr-about-work-with-us bg10 p-t-20 p-b-20">
            <div class="row landing-about-content">
                <div class="col-md-12" align='center'>
                    <h3 class="mtext-113 cl14 p-b-16">
                        Why PRR?
                    </h3>
                </div>
            </div><br/><br/>
            <div class="row p-b-20">
                <div class="col-md-6 landing-about-feature" align='center'>
                    <span class="about-feature-icon fa fa-gears fa-3x"></span>
                    <h5 class="about-feature-title">Reliable & Protected</h5>
                    <p class="stext-116">Your sensitive information and data <strong>are secure</strong> with us.</p>
                </div>
                <div class="col-md-6 landing-about-feature" align='center'>
                    <span class="about-feature-icon fa fa-info-circle fa-3x"></span>
                    <h5 class="about-feature-title">Global Choice</h5>
                    <p class="stext-116">Gain access to over 1M+ Prospect <strong>Research Reports.</strong></p>
                </div>
            </div>
            <div class="row p-t-20">
                <div class="col-md-6 landing-about-feature" align='center'>
                    <span class="about-feature-icon fa fa-user-plus fa-3x"></span>
                    <h5 class="about-feature-title">Customer Oriented</h5>
                    <p class="stext-116">We are available <strong>24*7</strong> for <strong>365 days</strong> in a year.</p>
                </div>
                <div class="col-md-6 landing-about-feature" align='center'>
                    <span class="about-feature-icon fa fa-hand-grab-o fa-3x"></span>
                    <h5 class="about-feature-title">Trusted By All</h5>
                    <p class="stext-116"><strong>500+ Contented Clients</strong> Globally.</p>
                </div>
            </div>
        </section>

		</div>
	</section>	

<!--**********************    FOOTER OPEN      ***************************-->

<?php require_once 'layouts/footer.php'; ?>

<!--**********************    FOOTER CLOSE     ***************************-->