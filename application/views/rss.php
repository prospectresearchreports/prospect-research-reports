<?php  echo '<?xml version="1.0" encoding="' . $encoding . '"?>' . "\n"; ?>
<rss version="2.0"
    xmlns:dc="http://purl.org/dc/elements/1.1/"
    xmlns:sy="http://purl.org/rss/1.0/modules/syndication/"
    xmlns:admin="http://webns.net/mvcb/"
    xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
    xmlns:content="http://purl.org/rss/1.0/modules/content/">
 
    <channel>
     
    <title><?php echo $feed_name; ?></title>
 
    <link><?php echo $feed_url; ?></link>
    <description><?php echo $page_description; ?></description>
    <dc:language><?php echo $page_language; ?></dc:language>
 
    <dc:rights>Copyright <?php echo gmdate("Y", time()); ?></dc:rights>
 
    <?php foreach($report_data->result() as $post): ?>
     
        <report>
            <title><?php echo character_limiter(xml_convert($post->rep_title),100); ?></title>
            <link><?php echo site_url('reports/' . $post->rep_url.'/'. $post->rep_url) ?></link>
            <description> <?php echo character_limiter($post->rep_descrip, 150); ?> ></description>
            <reportCategory><?php echo $post->sc1_name; ?></reportCategory>
            <price><?php echo $post->li_value; ?></price>
            <pubDate><?php echo $post->rep_date; ?></pubDate>
        </report>
 
         
    <?php endforeach; ?>
     
    </channel>
</rss>