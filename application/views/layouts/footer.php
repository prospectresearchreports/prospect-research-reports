
  <!-- ======= Footer ======= -->

  <!-- Footer -->
	<footer class="bg3 p-t-30 p-b-10">
		<div class="container">
			<div class="row">
				<div class="col-sm-6 col-lg-4 p-b-50">
					<h4 class="stext-301 cl0 p-t-9 p-r-9 p-b-9 m-b-30 txt-center footer-h4">
						<a class="trans-04" href="<?=base_url()?>"><img class="footer-logo" src="<?=base_url()?>assets/images/prospect-research-reports-logo-trans.png" alt="ProspectResearch Reports Logo"></a>
					</h4>

					<p class="stext-107 cl7 size-201"><strong> We are Global Leader In <span class="cl0 mtext-102">Market Research Reports.</span></strong></p>
					<br/>
					<p class="mtext-110 cl0 size-201"><strong> Address</strong></p>
					<br/>
            		<p class="stext-107 cl7 size-201"><b>International : </b> <?=@$this->config->item('conf')["international_address"]?></p>
            		<p class="stext-107 cl7 size-201"><b>India : </b> <?=@$this->config->item('conf')["local_address"]?></p>

					<br/>
					<p class="mtext-110 cl0 size-201"><strong> Contact US</strong></p>
					<br/>
					<p class="stext-107 cl7 size-201"><a class='footer-link cl7 hov-cl13 trans-04' href="tel:<?=@$this->config->item('conf')["contact_number"]?>"><?=@$this->config->item('conf')["contact_number"]?></a></p>
					<p class="stext-107 cl7 size-201"><a class='footer-link cl7 hov-cl13 trans-04' href="mailto:<?=@$this->config->item('conf')["email_address"]?>"><?=@$this->config->item('conf')["email_address"]?></a></p>
              
					<div class="p-t-27">
						<a href="https://twitter.com/ProspectResear3" title="Twitter" class="twitter fs-18 cl7 hov-cl13 trans-04 m-r-16"><i class="fa fa-twitter"></i></a>
						<a href="https://medium.com/@prospectresearchreports" title="Medium" class="facebook fs-18 cl7 hov-cl13 trans-04 m-r-16"><i class="fa fa-medium"></i></a>
						<a href="https://www.blogger.com/u/6/blog/posts/6260696524988884741" title="Blogger" class="instagram fs-18 cl7 hov-cl13 trans-04 m-r-16"><i class="fa fa-instagram"></i></a>
						<a href="https://www.linkedin.com/company/105282178/" class="linkedin fs-18 cl7 hov-cl13 trans-04 m-r-16"><i class="fa fa-linkedin"></i></a>
						<a href="<?=base_url()?>sitemap.html" class="twitter fs-18 cl7 hov-cl13 trans-04 m-r-16"><i class="fa fa-medium"></i></a>
						
					</div>
				</div>
				<div class="col-sm-6 col-lg-4 p-b-50">
					<h4 class="stext-301 cl0 p-b-30">
						Quick Links
					</h4>

					<ul>
						<li class="p-b-10">
							<a class="stext-107 cl7 hov-cl13 trans-04" href="<?=base_url()?>about-us">About Us</a>
						</li>
						<li class="p-b-10">
							<a class="stext-107 cl7 hov-cl13 trans-04" href="<?=base_url()?>contact-us">Contact Us</a>
						</li>
						<li class="p-b-10">
							<a class="stext-107 cl7 hov-cl13 trans-04" href="<?=base_url()?>privacy-policy">Privacy Policy</a>
						</li>
						<li class="p-b-10">
							<a class="stext-107 cl7 hov-cl13 trans-04" href="<?=base_url(); ?>terms-and-conditions">Terms &amp; Conditions</a>
						</li>
						<li class="p-b-10">
							<a class="stext-107 cl7 hov-cl13 trans-04" href="<?=base_url()?>careers">Careers</a>
						</li>
						<li class="p-b-10">
							<a class="stext-107 cl7 hov-cl13 trans-04" href="<?=base_url()?>return-policy">Return Policy</a>
						</li>
						<li class="p-b-10">
							<a class="stext-107 cl7 hov-cl13 trans-04" href="<?=base_url()?>payment-mode">Payment Modes</a>
						</li>

					</ul>
				</div>

				<div class="col-sm-6 col-lg-4 p-b-50">
					<h4 class="stext-301 cl0 p-b-30">
						Products &amp; Services
					</h4>

					<ul>
						<li class="p-b-10">
							<a class="stext-107 cl7 hov-cl13 trans-04" href="<?=base_url()?>industry-sectors">Industry Sectors</a>
						</li>
						<li class="p-b-10">
							<a class="stext-107 cl7 hov-cl13 trans-04" href="<?=base_url()?>latest-reports">Industry Reports</a>
						</li>
						<li class="p-b-10">
							<a class="stext-107 cl7 hov-cl13 trans-04" href="<?=base_url()?>press-release">Press Releases</a>
						</li>
						<li class="p-b-10">
							<a class="stext-107 cl7 hov-cl13 trans-04" href="<?=base_url()?>custom-research">Custom Research</a>
						</li>
						<li class="p-b-10">
							<a class="stext-107 cl7 hov-cl13 trans-04" href="<?=base_url()?>research-methodology">Research Methodology</a>
						</li>
						<li class="p-b-10">
							<a class="stext-107 cl7 hov-cl13 trans-04" href="<?=base_url()?>syndicated-research">Syndicated Research</a>
						</li>
						<li class="p-b-10">
							<a class="stext-107 cl7 hov-cl13 trans-04" href="<?=base_url()?>procurement-solutions">Procurement Solutions</a>
						</li>
						<li class="p-b-10">
							<a class="stext-107 cl7 hov-cl13 trans-04" href="<?=base_url()?>consulting-services">Consulting Services</a>
						</li>
					</ul>
				</div>


				<!-- <div class="col-sm-6 col-lg-3 p-b-50">
					<h4 class="stext-301 cl0 p-b-30">
						Newsletter
					</h4>

					<form>
						<div class="wrap-input1 w-full p-b-4">
							<input class="input1 bg-none plh1 stext-107 cl7" type="text" name="email" placeholder="email@example.com">
							<div class="focus-input1 trans-04"></div>
						</div>

						<div class="p-t-18">
							<button class="flex-c-m stext-101 cl0 size-103 bg1 bor1 hov-btn2 p-lr-15 trans-04">
								Subscribe
							</button>
						</div>
					</form>
				</div> -->
			</div>

			<div class="p-t-0">
				<p class="stext-107 cl6 txt-center">
                    &copy; Copyright <strong><span><?=date('Y')?> ProspectResearch Reports</span></strong>. All Rights Reserved
				</p>
			</div>
		</div>
	</footer>


	<!-- Back to top -->
	<div class="btn-back-to-top" id="myBtn">
		<span class="symbol-btn-back-to-top">
			<i class="zmdi zmdi-chevron-up"></i>
		</span>
	</div>
  <!-- End Footer -->

  <!-- <a href="#" class="scroll-top d-flex align-items-center justify-content-center"><i class="bi bi-arrow-up-short"></i></a> -->


  <!-- <div id="preloader"></div> -->


<!-- Full screen search box -->
<!-- <div id="searchModal" class="search-modal">
    <button type="button" class="close" aria-label="Close" data-dismiss="searchModal">
        <span aria-hidden="true">&times;</span>
    </button>
    <form action="<?=base_url().'search-result'?>" method="get" class="prr-overlay-search-form">
        <label for="search" class="sr-only">Search</label>
        <input type="search" autocomplete="off" class="prr-overlay-search-input" value="<?=$search ?? '' ?>" id="search" name="search" placeholder="Search here">
    </form>
</div> -->
<?php if(!isset($_COOKIE['cookie_consent'])){ ?> 
    <div class="container">
        <div class="footer-fixed-block">
            <h3 class="footer-fixed-text">We use cookies to ensure that we give you the best experience on our website. If you continue to use this site we will assume that you are happy with it. Please read our <a class="blue" href="<?=base_url(); ?>privacy-policy" target="_blank">privacy policy</a><br class="md-hidden" /></h3>
            <div class="button-group">
                <button class="btn-fixed-block btn-accept"><span class="fa fa-check-circle"></span> Accept</button>
                <button class="btn-fixed-block btn-dismiss"><span class="fa fa-times-circle"></span> Decline</button>
            </div>
        </div>
    </div>
<?php } ?>

    <!-- Vendor JS Files -->

	
    <script src="<?=base_url()?>assets/vendor/jquery/jquery-3.2.1.min.js"></script>

	<script src="<?=base_url()?>assets/vendor/animsition/js/animsition.min.js"></script>

	<script src="<?=base_url()?>assets/vendor/bootstrap/js/popper.js"></script>
	<script src="<?=base_url()?>assets/vendor/bootstrap/js/bootstrap.min.js"></script>

	<script src="<?=base_url()?>assets/vendor/select2/select2.min.js"></script>
	<script>
		$(".js-select2").each(function(){
			$(this).select2({
				minimumResultsForSearch: 20,
				dropdownParent: $(this).next('.dropDownSelect2')
			});
		})
	</script>

	<script src="<?=base_url()?>assets/vendor/daterangepicker/moment.min.js"></script>
	<script src="<?=base_url()?>assets/vendor/daterangepicker/daterangepicker.js"></script>

	<script src="<?=base_url()?>assets/vendor/slick/slick.min.js"></script>
	<script src="<?=base_url()?>assets/js/slick-custom.js"></script>

	<script src="<?=base_url()?>assets/vendor/parallax100/parallax100.js"></script>
	<script>
        $('.parallax100').parallax100();
	</script>

	<script src="<?=base_url()?>assets/vendor/MagnificPopup/jquery.magnific-popup.min.js"></script>
	<script>
		$('.gallery-lb').each(function() { // the containers for all your galleries
			$(this).magnificPopup({
		        delegate: 'a', // the selector for gallery item
		        type: 'image',
		        gallery: {
		        	enabled:true
		        },
		        mainClass: 'mfp-fade'
		    });
		});
	</script>

	<script src="<?=base_url()?>assets/vendor/isotope/isotope.pkgd.min.js"></script>
	<script src="<?=base_url()?>assets/vendor/sweetalert/sweetalert.min.js"></script>
	<script src="<?=base_url()?>assets/vendor/wowjs/wow.min.js"></script>
	<script>
		new WOW({mobile: false}).init();
	</script>

	<script>
		$('.js-addwish-b2').on('click', function(e){
			e.preventDefault();
		});

		$('.js-addwish-b2').each(function(){
			var nameProduct = $(this).parent().parent().find('.js-name-b2').html();
			$(this).on('click', function(){
				swal(nameProduct, "is added to wishlist !", "success");

				$(this).addClass('js-addedwish-b2');
				$(this).off('click');
			});
		});

		$('.js-addwish-detail').each(function(){
			var nameProduct = $(this).parent().parent().parent().find('.js-name-detail').html();

			$(this).on('click', function(){
				swal(nameProduct, "is added to wishlist !", "success");

				$(this).addClass('js-addedwish-detail');
				$(this).off('click');
			});
		});

		/*---------------------------------------------*/

		$('.js-addcart-detail').each(function(){
			var nameProduct = $(this).parent().parent().parent().parent().find('.js-name-detail').html();
			$(this).on('click', function(){
				swal(nameProduct, "is added to cart !", "success");
			});
		});
	</script>

	<script src="<?=base_url()?>assets/vendor/perfect-scrollbar/perfect-scrollbar.min.js"></script>
	<script>
		$('.js-pscroll').each(function(){
			$(this).css('position','relative');
			$(this).css('overflow','hidden');
			var ps = new PerfectScrollbar(this, {
				wheelSpeed: 1,
				scrollingThreshold: 1000,
				wheelPropagation: false,
			});

			$(window).on('resize', function(){
				ps.update();
			})
		});
	</script>
	<script src="<?=base_url()?>assets/js/main.js"></script>

<script type="text/javascript">
    var path = "<?= strlen($this->uri->segment('1')) > 0 ? str_ireplace('/index.php', '', current_url()) : str_ireplace('index.php', '', current_url());?>";
    // console.log(path)
    var href=$(' a[href="'+path+'"]').addClass('active');


    // fade in #back-top
    $("#back-top").hide();

    $(function () {
        $(window).scroll(function () {
            if ($(this).scrollTop() > 100) {
                    $('#back-top').fadeIn();
            } else {
                    $('#back-top').fadeOut();
            }
        });
            // scroll body to 0px on click
        $('#back-top .btn-back').click(function () {
            $('body,html').animate({
                    scrollTop: 0
            }, 800);
            return false;
        });
    });
    <?php if(isset($_COOKIE["cookie_consent"])){
        echo "$('.footer-fixed-block').css({'display':'none'})";
    } ?>

    $(".btn-dismiss").click(function(){
        document.cookie = "cookie_consent=decline; path=/; max-age="+24*60*60;
        location.reload(true);           
    })
    $(".btn-accept").click(function(){
        document.cookie = "cookie_consent=accept; path=/; max-age="+365*24*60*60;
        location.reload(true);           
    })
</script>
</body>
</html>