    <script>
        const base_url = "<?=base_url(); ?>"
    </script>
    <link rel="shortcut icon" href="<?=base_url()?>assets/images/icons/logo-icon.png" type="image/x-icon" />
    <link rel="canonical" href="<?=str_ireplace('/index.php','',current_url())?>"/>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <!-- <link rel="stylesheet" href="<?=base_url()?>assets/vendor/animate/animate.min.css"> -->
    <link rel="stylesheet" href="<?=base_url()?>assets/css/font-awesome.css">
    <link rel="stylesheet" href="<?=base_url()?>assets/css/style.css">
    <script src="<?=base_url()?>assets/vendor/jquery/jquery-3.2.1.min.js"></script>

     <!-- Google Fonts -->
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Open+Sans:ital,wght@0,300;0,400;0,500;0,600;0,700;1,300;1,400;1,600;1,700&family=Montserrat:ital,wght@0,300;0,400;0,500;0,600;0,700;1,300;1,400;1,500;1,600;1,700&family=Raleway:ital,wght@0,300;0,400;0,500;0,600;0,700;1,300;1,400;1,500;1,600;1,700&display=swap" rel="stylesheet">
    <!-- Vendor CSS Files -->
    <link href="<?=base_url()?>assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/fonts/iconic/css/material-design-iconic-font.min.css">
  <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/fonts/linearicons-v1.0.0/icon-font.min.css">
  <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/vendor/animate/animate.css">
  <!--===============================================================================================-->	
    <link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/vendor/css-hamburgers/hamburgers.min.css">
  <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/vendor/animsition/css/animsition.min.css">
  <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/vendor/select2/select2.min.css">
  <!--===============================================================================================-->	
    <link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/vendor/daterangepicker/daterangepicker.css">
  <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/vendor/slick/slick.css">
  <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/vendor/MagnificPopup/magnific-popup.css">
  <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/vendor/perfect-scrollbar/perfect-scrollbar.css">
  <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/css/util.css">
    <link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/css/main.css">

    <!-- <script src="<?=base_url()?>assets/js/loader.js"></script> -->
    <!-- Google tag (gtag.js) -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=G-FVE5XKG93H"></script>
    <script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'G-FVE5XKG93H');
    </script>	
	
</head>
<body class="animsition">

	<!-- Header -->
	<header class="header-v2">
		<!-- Header desktop -->
		<div class="container-menu-desktop trans-03">
			<div class="wrap-menu-desktop">
				<nav class="limiter-menu-desktop p-l-45">
					
					<!-- Logo desktop -->		
					<a href="<?=base_url()?>" class="logo">
						<img src="<?=base_url()?>assets/images/prospect-research-reports-logo.png" alt="ProspectResearch Reports Logo">
					</a>
					<!-- Menu desktop -->
					<div class="menu-desktop">
						<ul class="main-menu">
							<li class="">
								<a href="<?=base_url()?>">Home</a>
							</li>
							<li>
								<a aria-disabled="true">About</a>
								<ul class="sub-menu">
									<li><a href="<?=base_url()?>about-us">About Us</a></a></li>
									<li><a href="<?=base_url()?>research-methodology">Research Methodology</a></li>
									<!-- <li><a href="<?=base_url()?>careers">Careers</a></li> -->
								</ul>
							</li>
							<!-- <li>
								<a aria-disabled="true">Contact</a>
								<ul class="sub-menu">
									<li><a href="<?//=base_url()?>contact-us">Contact Us</a></a></li>
									<li><a href="<?//=base_url()?>custom-research">Custom Research</a></li>
								</ul>
							</li> -->
							<li>
								<a href="<?=base_url()?>contact-us">Contact Us</a>
							</li>
							<li>
								<a href="<?=base_url()?>custom-research">Custom Research</a>
							</li>
							<li>
								<a href="<?=base_url()?>industry-sectors">Industry Sectors</a>
							</li>
							<li>
								<a href="<?=base_url()?>latest-reports">Industry Reports</a>
							</li>
							<li>
								<a href="<?=base_url()?>press-release">Press Releases</a>
							</li>
						</ul>
					</div>	

					<!-- Icon header -->
					<div class="wrap-icon-header flex-w flex-r-m h-full">
						<div class="flex-c-m h-full p-r-24">
							<div class="icon-header-item cl13 hov-cl13 trans-04 p-lr-11 js-show-modal-search">
								<i class="zmdi zmdi-search"></i>
							</div>
						</div>
							
						<!-- <div class="flex-c-m h-full p-l-18 p-r-25 bor5">
							<div class="icon-header-item cl14 hov-cl1 trans-04 p-lr-11 icon-header-noti js-show-cart" data-notify="2">
								<i class="zmdi zmdi-shopping-cart"></i>
							</div>
						</div>
							
						<div class="flex-c-m h-full p-lr-19">
							<div class="icon-header-item cl14 hov-cl1 trans-04 p-lr-11 js-show-sidebar">
								<i class="zmdi zmdi-menu"></i>
							</div>
						</div> -->
					</div>
				</nav>
			</div>	
		</div>

		<!-- Header Mobile -->
		<div class="wrap-header-mobile">
			<!-- Logo moblie -->		
			<div class="logo-mobile">
				<a href="<?=base_url()?>"><img src="<?=base_url()?>assets/images/prospect-research-reports-logo.png" alt="ProspectResearch Reports Logo"></a>
			</div>

			<!-- Icon header -->
			<div class="wrap-icon-header flex-w flex-r-m h-full m-r-15">
				<div class="flex-c-m h-full p-r-10">
					<div class="icon-header-item cl13 hov-cl13 trans-04 p-lr-11 js-show-modal-search">
						<i class="zmdi zmdi-search"></i>
					</div>
				</div>

				<!-- <div class="flex-c-m h-full p-lr-10 bor5">
					<div class="icon-header-item cl14 hov-cl1 trans-04 p-lr-11 icon-header-noti js-show-cart" data-notify="2">
						<i class="zmdi zmdi-shopping-cart"></i>
					</div>
				</div> -->
			</div>

			<!-- Button show menu -->
			<div class="btn-show-menu-mobile hamburger hamburger--squeeze">
				<span class="hamburger-box">
					<span class="hamburger-inner"></span>
				</span>
			</div>
		</div>


		<!-- Menu Mobile -->
		<div class="menu-mobile">
			<ul class="main-menu-m">
				<li class="">
					<a href="<?=base_url()?>">Home</a>
				</li>
				<li>
					<a aria-disabled="true">About</a>
					<ul class="sub-menu-m">
						<li><a href="<?=base_url()?>about-us">About Us</a></a></li>
						<li><a href="<?=base_url()?>research-methodology">Research Methodology</a></li>
					</ul>
					<span class="arrow-main-menu-m">
						<i class="fa fa-angle-right" aria-hidden="true"></i>
					</span>
				</li>
				<!-- <li>
					<a aria-disabled="true">Contact</a>
					<ul class="sub-menu-m">
						<li><a href="<?//=base_url()?>contact-us">Contact Us</a></a></li>
						<li><a href="<?//=base_url()?>custom-research">Custom Research</a></li>
					</ul>
					<span class="arrow-main-menu-m">
						<i class="fa fa-angle-right" aria-hidden="true"></i>
					</span>
				</li> -->
				<li>
					<a href="<?=base_url()?>contact-us">Contact Us</a>
				</li>
				<li>
					<a href="<?=base_url()?>custom-research">Custom Research</a>
				</li>
				<li>
					<a href="<?=base_url()?>industry-sectors">Industry Sectors</a>
				</li>
				<li>
					<a href="<?=base_url()?>latest-reports">Industry Reports</a>
				</li>
				<li>
					<a href="<?=base_url()?>press-release">Press Releases</a>
				</li>
			</ul>
		</div>

		<!-- Modal Search -->
		<div class="modal-search-header flex-c-m trans-04 js-hide-modal-search">
			<div class="container-search-header">
				<button class="flex-c-m btn-hide-modal-search trans-04 js-hide-modal-search">
					<img src="<?=base_url()?>assets/images/icons/icon-close2.png" alt="CLOSE">
				</button>

				<form action="<?=base_url().'search-result'?>" method="get" class="wrap-search-header flex-w p-l-15">
					<button class="flex-c-m trans-04">
						<i class="zmdi zmdi-search"></i>
					</button>
					<input class="plh3" type="search" autocomplete="off"  value="<?=$search ?? '' ?>" id="search" name="search" placeholder="Search...">
				</form>
			</div>
		</div>
	</header>
  
  <!-- End Header -->