
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title><?php echo isset($meta->title) ? $meta->title : 'Custom Research | ProspectResearch Reports';?></title>
<!-- Basic -->
<meta charset="utf-8"/>
<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
<meta name="robots" content="index,follow"/>
<meta name="keywords" content="<?php echo isset($meta->keywords) ? $meta->keywords : 'Custom Research Service ProspectResearch Reports, Custom Research Method in Market,  Custom Research';?>" />
<meta name="description" content="<?php echo isset($meta->description) ? $meta->description : 'We,at ProspectResearch Reports,are here to listen to all customers queries and offer them satisfactory outputs.Fill the form here and avail reports of your choice and budget from our custom research service.';?>"/>
<meta name="author" content="ProspectResearch Reports"/>

<!--**********************    HEADER OPEN      ***************************-->

<?php require_once 'layouts/header.php'; ?>

<!--**********************    HEADER CLOSE     ***************************-->

<script src="<?= base_url();?>assets/js/validator.min.js"></script>
<script src="<?= base_url();?>assets/js/captcha.js"></script>

<!-- *****************   BOOTSTRAP SELECT          ******************  -->
<!-- <link href="<?= base_url();?>assets/bootstrap-select/css/bootstrap-select.min.css" rel='stylesheet' />
<script src="<?= base_url();?>assets/bootstrap-select/js/bootstrap-select.min.js"></script> -->

<script type="text/javascript">
    // $(document).ready(function(){
    //     $(".selectpicker").selectpicker();    
    // });
</script>

<script type="text/javascript">
    
// Form validation 
    
    function validate_captcha()
    {
        var text=txtCaptcha.value;
        str = text.replace(/ +/g, "");
        var code=security_code.value;

        if(str !== code)
        {
           CaptchaError.innerHTML="Invalid Captcha Code";
           return false;
        }
        else
        {
            CaptchaError.innerHTML="";
            return true;
        }
    }
</script>  

<ul class="breadcrumb m-b-0">
    <li class="breadcrumb-item"><a href="<?=base_url();?>"><img src="<?=base_url().'assets/images/home.png'?>" title='Go To Home' height="30px" width="30px" alt="Home Breadcrumb Icon" /></a></li>
    <li class="breadcrumb-item">Custom Research</li>
</ul>

<!-- Title page -->
<section class="bg10 txt-center p-lr-15 p-tb-50">
    <h2 class="ltext-103 cl5 txt-center">
        Custom Research
    </h2>
</section>	

<section class="bg0 p-t-10 p-b-30">
    <div class="container">
        <div class="contact">
            <div class="row">                
                <div class="col-md-12 col-contact-9">
                    <div class=" form-side-block contact-info sticky" align="center">
                        <p class="form-help-text m-b-20">* If you have any questions or need assistance, please reach out to our team.</p>
                        <p class="fa fa-envelope fa-3x"></p>
                        <h4 class="form-title">Fill the form below to customize your research !</h4>
                    </div>
                </div>
                <div class="col-md-12 col-contact">
                    <?php
                        $invalid_message=(@$message) ? @$message : '';
                        if(!empty(@$invalid_message)){
                            echo "<div class='alert alert-danger alert-message'>".@$invalid_message."</div>";
                        }
                    ?>
                    <div class="contact-form">
                        <form id="form" role="form" action="<?php echo base_url(); ?>custom-research-process" method="post" data-toggle="validator" onsubmit="return validate_captcha()" >
                            <div class="row">
                                <div class="form-group col-sm-5 col-sm-offset-1">
                                    <label class="control-label">Name <span class="star">*</span> :</label>
                                    <input class="form-control" name="name" id="name"  placeholder="Your Name" data-bv-field="full_name" type="text" value="<?=@$name?>" data-error="Please enter your name" required/>
                                    <div class="help-block with-errors"></div> 
                                </div>
                                <div class="form-group col-sm-5">
                                    <label class="control-label">Email <span class="star">*</span> :</label>
                                    <input class="form-control" name="email" id="email"  placeholder="Business Email" data-bv-field="email" type="email" value="<?=@$email?>" data-error="Please enter valid email address" required/>
                                    <div class="help-block with-errors"></div> 
                                </div>

                                <div class="form-group col-sm-5 col-sm-offset-1">
                                    <label class="control-label">Company <span class="star">*</span> :</label>
                                    <input class="form-control" name="company" id="company" placeholder="Company Name" data-bv-field="company" type="text" value="<?=@$company?>" data-error="Please enter company / organization name"   required/>
                                    <div class="help-block with-errors"></div> 
                                </div>
                                <div class="form-group col-sm-5">
                                    <label class="control-label">Job Role <span class="star">*</span> :</label>
                                    <input class="form-control" name="job_role" id="job_role" value="<?=@$job_role?>" placeholder="Job Role" data-bv-field="job_role" type="text" data-error="Please mention job role"  required/>
                                    <div class="help-block with-errors"></div> 
                                </div>

                                <div class="form-group col-sm-5 col-sm-offset-1">
                                    <label class="control-label">Country <span class="star">*</span> :</label>
                                    <select name="country" class="form-control selectpicker" data-bv-field="country" data-live-search="true" data-actions-box="true" data-error="Please select your country"  required>
                                        <?=getCountryDropdown()?>
                                    </select>
                                    <div class="help-block with-errors"></div> 
                                </div>
                                <div class="form-group col-sm-5">
                                    <label class="control-label">Contact Number <span class="star">*</span> :</label>
                                    <input class="form-control"  pattern="^[0-9]{10}$" name="phone" id="phone" value="<?=@$phone?>" maxlength="10" placeholder="Contact Number without country code" data-bv-field="phone" type="text" data-error="Please enter contact number" required/>
                                    <div class="help-block with-errors"></div> 
                                </div>

                                <div class="form-group col-sm-10 col-sm-offset-1">
                                    <label class="control-label">Requirements :</label>
                                    <textarea data-error="Please give us feedback"  maxlength='200'  rows="5"  class="form-control" name="message" id="message" data-bv-field="message" ><?=@$contact_msg?></textarea>
                                    <i class="form-control-feedback" maxlength='200' data-bv-icon-for="message" style="display: none;"></i>
                                    <div class="help-block with-errors"></div> 
                                </div>
                                <div class="form-group col-sm-5 col-sm-offset-1">
                                    <label class="control-label">Budget <span class="star">*</span> :</label>
                                    <select data-error="Please specify Your Budget" rows="5"  class="form-control" name="budget" id="budget" data-bv-field="budget"  required>
                                        <option value='' <?=$budget == '' ? 'selected' : ''?>>Select</option>
                                        <option value='0-5000' <?=$budget == '0-5000' ? 'selected' : ''?> >$0 - $5000</option>
                                        <option value='5000-10000' <?=$budget == '5000-10000' ? 'selected' : ''?>>$5000 - $10000</option>
                                        <option value='10000-20000' <?=$budget == '10000-20000' ? 'selected' : ''?>>$10000 - $20000</option>
                                    </select> 
                                    <div class="help-block with-errors"></div>
                                </div>

                                <div class="form-group col-sm-5">
                                    <label class="control-label" for="security_code">Security Code: <span class="star">*</span>: </label>
                                    <div class="input-group">
                                        <input data-error="Please enter security code" onkeyup="validate_captcha()" type="text" name="captcha_code" maxlength="6" placeholder="Security Code" class="form-control" id="security_code"  required/>
                                        <input type="text" id="txtCaptcha" readonly class="form-control" />
                                        <span class="input-group-append">
                                            <button title="Refresh Security Code" class="btn btn-captcha" type="button" onClick="DrawCaptcha();"> <span class="fa fa-refresh"></span> </button>
                                        </span> 
                                    </div>
                                    <p class="feedback" id="CaptchaError"></p>
                                    <div class="help-block with-errors"></div> 
                                </div>

                                <div class="col-sm-10 col-sm-offset-1">        
                                    <button title="Customize Research" type="submit" class="btn btn-default btn-submit">
                                        <span class="fa fa-file-pdf-o"></span> Customize Research
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <hr class="contact-line"/>
        <section class="prr-about-work-with-us bg10 p-t-20 p-b-20">
            <div class="row landing-about-content">
                <div class="col-md-12" align='center'>
                    <h3 class="mtext-113 cl14 p-b-16">
                        Contact PRR
                    </h3>
                </div>
            </div><br/><br/>
            <div class="row p-b-20">
                <div class="col-md-4 landing-about-feature" align='center'>
                    <span class="about-feature-icon fa fa-map-marker fa-3x"></span>
                    <h5 class="about-feature-title">Location</h5>
                    <?php
                    $conf = $this->config->item('conf');
                    if (array_key_exists('international_address', $conf)){ ?>
                    <p class="stext-116 text-capitalize"><?=strtolower(@$conf["international_address"])?></p>
                    <?php } 
                    //if (array_key_exists('local_address', $conf)){
                    ?>
                    <!-- <p class="stext-116 text-capitalize"><b>India : </b><?=strtolower(@$conf["local_address"])?></p> -->
                    <?php //} ?>
                </div>
                <div class="col-md-4 landing-about-feature" align='center'>
                    <span class="about-feature-icon fa fa-phone fa-3x"></span>
                    <h5 class="about-feature-title">International</h5>
                    <p class="stext-116"><a href='tel:<?=@$this->config->item('conf')["contact_number"]?>'><?=@$this->config->item('conf')["contact_number"]?></a></p>
                </div>
                <div class="col-md-4 landing-about-feature" align='center'>
                    <span class="about-feature-icon fa fa-envelope fa-3x"></span>
                    <h5 class="about-feature-title">Email Us</h5>
                    <p class="stext-116" style="overflow-x: auto;"><a href='mailto:<?=@$this->config->item('conf')["email_address"]?>'><?=@$this->config->item('conf')["email_address"]?></a></p>
                </div>
            </div> 
        </section>
    </div>

</section>

<!--**********************    FOOTER OPEN      ***************************-->

<?php require_once 'layouts/footer.php'; ?>

<!--**********************    FOOTER CLOSE     ***************************-->
