<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title><?php echo isset($meta->title) ? $meta->title : 'My Cart | ProspectResearch Reports';?></title>
<!-- Basic -->
<meta charset="utf-8"/>
<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
<meta name="robots" content="index,follow"/>
<meta name="keywords" content="<?php echo isset($meta->keywords) ? $meta->keywords : 'Cart ProspectResearch Reports , ProspectResearch Reports reports in Cart,My Cart ProspectResearch Reports';?>" />
<meta name="description" content="<?php echo isset($meta->description) ? $meta->description : 'ProspectResearch Reports offers you a Cart User Interface option in which you can add reports of your choice and can proceed further for checkout';?>"/>
<meta name="author" content="ProspectResearch Reports"/>


<!--**********************    HEADER OPEN      ***************************-->
        
<?php require_once 'layouts/header.php'; ?>

<!--**********************    HEADER CLOSE     ***************************-->

<style>
    @media screen and (max-width:767px){
        .btn-order,.btn-shopping{
            text-align: center;
        }        
    }
    @media screen and (min-width:767px){
        .col-md-7{
            width: 55%;
        }  
        .price-block{
            width: 28%;
        }   
        .fa-minus-circle,.fa-plus-circle{
            margin-left: -5px;
        }    
    }
    .btn-order{
        float: right;
        background: orange;
    }
    .btn-shopping > .btn{
        background: #efc707;
        color: #fff;
    }
    .col-md-3 i{
        font-size:1.2em;
    }
    .fa-minus-circle,.fa-plus-circle{
        cursor: pointer;
    }
    .btn-cart-sign{
        border:0px;
        background: none;
        font-size: 0.8em;
    }
    .btn-cart-sign:focus{
        border:0px;
        background: none;
        padding: 0px;
    }
    .panel-default > .panel-heading{
        background: #efc707;
        color: #fff;
    }
</style>
<script>
    
    function addCartItem(id){
        var quantity=parseInt(document.getElementById("quantity"+id).innerHTML);
        var price=parseInt(document.getElementById("price"+id).innerHTML);
        var base_price=parseInt(document.getElementById("base_price"+id).innerHTML);
        var total=parseInt($(".total").html());
        var badge=parseInt($(".badge").html());
        var title=document.getElementById("title"+id).innerHTML;
        
        quantity+=1;
        badge+=1;
        total=total+base_price;
        price=price+base_price;
        //alert(quantity+"-"+price+"-"+total+"-"+title+"-"+id);
            
        $.ajax({
            beforeSend:function() { 
             $("<img src='http://localhost/ProspectResearch Reports/assets/images/loader.gif'  style='position:relative;top:-150px;left:10px;z-index:2000;height:100%;width:100%' class='loading' />").appendTo("div.price-structure");
            },
            complete:function() {
                $(".loading").remove();
            },
           type:'post',
           url:'<?php echo base_url(); ?>update-cart',
           data:{quantity:quantity, price:price, title:title},
           
           success:function(data){
               $(".total").html(total);
               $("#price"+id).html(price);
               $("#quantity"+id).html(quantity);
               $(".badge").html(badge);
               $(".minus-circle"+id).css({'color':'#333'}).removeClass("disabled");
               $("#btn"+id).attr('onclick','removeCartItem('+id+')');
           }
           
        });
    }
    function removeCartItem(id){
        if(!$(".minus-circle"+id).hasClass("disabled")){
            //alert("hi");
            var quantity=parseInt(document.getElementById("quantity"+id).innerHTML);
            var price=parseInt(document.getElementById("price"+id).innerHTML);
            var base_price=parseInt(document.getElementById("base_price"+id).innerHTML);
            var total=parseInt($(".total").html());
            var badge=parseInt($(".badge").html());
            var title=document.getElementById("title"+id).innerHTML;

            quantity-=1;
            badge-=1;
            total=total-base_price;
            price=price-base_price;
            //alert(quantity+"-"+price+"-"+total+"-"+title+"-"+id);

            $.ajax({
                beforeSend:function() { 
                 $("<img src='http://localhost/ProspectResearch Reports/assets/images/loader.gif'  style='position:relative;top:-150px;left:10px;z-index:2000;height:100%;width:100%' class='loading' />").appendTo("div.price-structure");
                },
                complete:function() {
                    $(".loading").remove();
                },
               type:'post',
               url:'<?php echo base_url(); ?>update-cart',
               data:{quantity:quantity, price:price, title:title},
               success:function(data){
                   $(".total").html(total);
                   $("#price"+id).html(price);
                   $("#quantity"+id).html(quantity);
                   $(".badge").html(badge);
                   if(quantity === 1){
                       $(".minus-circle"+id).css({'color':'darkgray'}).addClass("disabled");
                   }
               }
            });
        }
    }
    
    
    
    /*$(minus).click(function(){
        if($(minus).hasClass("disabled")){
            alert("disabled");
        }
    })
    $(plus).click(function(){
        quantity+=1;
        total=total+price;
        price*=2;
        alert(quantity+"-"+price+"-"+total+"-"+title+"-"+cart_id);
        $.ajax({
           type:'post',
           url:'<?php echo base_url(); ?>/update-cart'
           data:'quantity='+quantity+"&price="+price,
           success:function(data){
               $("#total").html(total);
               $("#price").html(price);
               $("#quantity").html(quantity);
           }
        });*/
</script>
<?php if(isset($message)){
    echo "<script>
        var key=confirm('".$message."');
        if(key === true){
            window.location.href='".base_url()."cart-item/".$badge_count."/".$rep_id."/yes-add';        
        }else{
            window.location.href='".base_url()."my-cart';        
        }
         </script>";
} ?>
<div class="container">
    <ul class="breadcrumb" style="margin-bottom:0">
        <li><a href="<?php echo base_url(); ?>" style="font-size:20px"><img src="<?php echo base_url();?>assets/images/home.jpg"  height="30px" width="40px"/></a></li>
        <li>My Cart</li>
    </ul>
    <br/>
    <div class="row">
        <div class="col-md-8">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4>MY CART (<?php if(isset($total_items)){ echo $total_items; } else { echo ""; } ?>)</h4>
                </div>
                <div class="panel-body">
                    <?php
                    if(isset($total_items)){
                       //echo "<script>alert('HI')</script>";
                            if($total_items <= 0)
                            {
                                echo "<div align='center'><img src='". base_url()."assets/images/cart-empty.gif' class='img img-responsive' height='300px' width='300px'/>";
                                echo "<br/><br/><h4 style='color:maroon;'>Your Cart is Empty.</h4></div>";
                                echo '<meta http-equiv="refresh" content="6,url=' . base_url() . '"/>';

                            }
                            else {  

                    if (isset($report_data)) {
                        foreach($report_data->result() as $row) {
                    ?>
                    <div class="row" style=";font-size: 1.1em">
                        <div class="col-md-2">
                            <img class="img-responsive img-thumbnail" style="height:70px;width:90px;" src="<?php echo base_url(); ?>assets/images/report/<?php echo $row->category_image; ?>" alt="<?php echo $row->rep_title; ?>"  />
                        </div>
                        <div class="col-md-7">
                            <div>
                                <span><b>Title</b>  &nbsp;<?php echo "<span id='title".$row->cart_id."'>".$row->rep_title."</span>";?></span>
                            </div><br/>
                            <div>
                                <span><b>Price</b> : &nbsp;<?php echo "$<span id='price".$row->cart_id."'>".$row->report_price."</span>"." <span><b> ( ".$row->license." ) </b></span>";?></span>
                            </div>

                        </div>
                        <div class="col-md-3 price-block">
                            <span><b>Quantity</b> : &nbsp;
                                <i>
                                    <?php if($row->quantity == 1){ ?>
                                    <button class="btn-cart-sign" id="btn<?php echo $row->cart_id; ?>"><span class="fa fa-minus-circle disabled minus-circle<?php echo $row->cart_id; ?>" style="color:darkgray;"></span></button>
                                    <?php } else {?>
                                    <button class="btn-cart-sign"  onclick="removeCartItem('<?php echo $row->cart_id; ?>')"><span class="fa fa-minus-circle minus-circle<?php echo $row->cart_id; ?>" ></span></button>
                                    <?php } echo "<span id='quantity".$row->cart_id."'>".$row->quantity."</span>";?>
                                    <button class="btn-cart-sign" onclick="addCartItem('<?php echo $row->cart_id; ?>')"><span class="fa fa-plus-circle"></span></button>
                                    <span id="cart_id" style="display:none;"><?php echo $row->cart_id; ?></span>
                                    <span id="base_price<?php echo $row->cart_id; ?>" style="display:none;"><?php echo $row->base_price; ?></span>
                                </i>
                            </span>
                        </div>
                    </div><br/>
                        <div>
                            <p><button class="btn btn-danger" data-toggle="modal" data-target="#removeMe">REMOVE ITEM <span class="fa fa-close"></span> </button></p>

                            <div id="removeMe" class="modal fade" role="dialog">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                            <h4 class="modal-title">Delete Confirmation</h4>
                                        </div>
                                        <div class="modal-body">
                                            <p style="font-size: 1.2em"><strong>Are you sure you want to remove this item from cart ?</strong></p>
                                        </div>
                                        <div class="modal-footer">
                                            <button class="btn btn-primary" onclick="window.location.href='<?php echo base_url();?>cart-item-remove/<?php echo $row->cart_id;?>'">Yes, Remove</button>
                                            <button type="button" class="btn btn-warning" data-dismiss="modal">Cancel</button>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    <hr/>
                    <?php } } } } ?>
                    <br/>
                    <div>
                        <h3 style="float: left" class="btn-shopping"><button class="btn btn-lg" onclick="window.location.href='<?php echo base_url();?>'" style="border-radius: 0px"><span class="fa fa-angle-left" style="font-size: 20px"></span> CONTINUE SHOPPING</button></h3>

                        <h3 class="btn-order"><button class="btn btn-warning btn-lg" onclick="placeOrder('<?php if(isset($row->cart_id)){echo $row->cart_id;}else{ echo 0; } ?>')"  style="border-radius: 0px"> PLACE ORDER  <span class="fa fa-angle-right" style="font-size: 20px"></span> </button></h3>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4>PRICE DETAILS</h4>
                </div>
                <div class="panel-body price-structure">
                    <div class="text-left">

                        <p>PRICE ( <?php if(isset($total_items)){ echo "<b>".$total_items."  item / s</b>"; } else { echo ""; } ?> )
                            <span style="float:right"><?php $rep_price=0;
                            if (isset($report_data)) {
                            foreach ($report_data->result() as $row) {

                                $price=$row->report_price; 
                                $rep_price=$price+$rep_price;
                         } } 
                        echo "$<span class='total'>".$rep_price."</span>";
                        ?>
                    </span>
                        </p><br/>
                        <p>DELIVERY CHARGES :<span style="float:right;color:green">FREE</span></p>

                        <hr  style="border:0.5px dashed darkslategray"/>
                        <p>
                            <strong>AMOUNT PAYABLE</strong>
                            <span style="float:right">
                                <?php $rep_price=0;
                            if (isset($report_data)) {
                            foreach ($report_data->result() as $row) {

                                $price=$row->report_price; 
                                $rep_price=$price+$rep_price;
                         } } 
                        echo "$<span class='total'>".$rep_price."</span>";
                        ?>
                            </span>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<br/><br/>



<script>
function placeOrder(cart_id){
    //alert(cart_id);
    if(cart_id == "0"){
        alert("Sorry ! Order can not be Placed !");
    }else{
        window.location.href='<?php echo base_url();?>cart-checkout/<?php if(isset($total_items)) { if($total_items == '1')  { echo $row->cart_id; } else  { echo $total_items; } }?>';
    }
}

</script>

        
<!--**********************    FOOTER OPEN      ***************************-->

<?php require_once 'layouts/footer.php'; ?>

<!--**********************    FOOTER CLOSE     ***************************-->


