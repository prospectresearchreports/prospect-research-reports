<?php 
require_once 'config.php';
require_once 'checksession.php';
?>

	<link rel="shortcut icon" href="<?php echo BASE_URL; ?>assets/images/prospect-research-reports-logo.jpg" type="image/x-icon" />
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<script type="text/javascript" src="<?php echo BASE_URL; ?>assets/js/jquery.min.js"></script>
    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
    <link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.css" type="text/css" />
	<script type="text/javascript" src="<?php echo BASE_URL; ?>assets/js/bootstrap.min.js"></script>
	<link rel="stylesheet" href="<?php echo BASE_URL; ?>assets/css/bootstrap.min.css" type="text/css"/>
	<link rel="stylesheet" href="<?php echo BASE_URL; ?>assets/css/font-awesome.css" type="text/css"/>
	<link href="<?php echo BASE_URL; ?>assets/css/header.css" rel="stylesheet" type="text/css"/>
	<link href="<?php echo BASE_URL; ?>assets/css/style.css" rel="stylesheet" type="text/css"/>

</head>
<script type="text/javascript">

    $(document).ready(function(){        
        $(".overlay").css({backgroundColor:  'rgb(0, 51, 102)'});
        $(".datepicker").datepicker({dateFormat:"yy-mm-dd",
            onSelect: function(datetext) {
                var d = new Date(); // for now

                var h = d.getHours();
                h = (h < 10) ? ("0" + h) : h ;

                var m = d.getMinutes();
                m = (m < 10) ? ("0" + m) : m ;

                var s = d.getSeconds();
                s = (s < 10) ? ("0" + s) : s ;

                datetext = datetext + " " + h + ":" + m + ":" + s;
             $('.datepicker').val(datetext)   
            }
        });

    });
    $(window).load(function () {
        $('.loader').fadeOut('slow');
    });
    // function changeBackground(color){
    //     //alert(color);
    //     $(".overlay,.footer").css({backgroundColor:  color});
    //     document.cookie = "color="+color+"; path=/; max-age="+365*24*60*60;
    //     location.reload(true);

    // }
    //******************  SIDENAV OPEN/ CLOSE  ********************//

    function openNav() {
        $("#myNav").css({'width': '100%'});
    }
    function closeNav() {
        $("#myNav").css({'width': '0%'});
    }
</script>

</head>
<?php 
    
    //*********************** CHECK FOR SESSION ****************************//

    $getUserInfo=$link->query("select * from mr_login where  email='".@$_SESSION["email"]."'  and user_type='".@$_SESSION["user_type"]."'  ");    
    
    $userInfo=$getUserInfo->fetch_assoc();

?>
<body>
    <div class="loader"></div>
    <div id="myNav" class="overlay">
        <a onclick="return false;" class='dropdown-link logo-text'>                
            <span class="fa fa-copyright"></span> <br/> <p>ProspectResearch Reports</p>
        </a>
        <a href="javascript:void(0)" class="closebtn md-hidden" onclick="closeNav()">&times;</a>
        <div class="overlay-content">
            <a class="dropdown-btn"><i class="fa fa-user"></i>  <?=@$_SESSION["name"]." (".@$USER_TYPES[$userInfo["user_type"]].") "?>
                <i class="fa fa-caret-down"></i>
            </a>
            <div class="dropdown-container">
                <a href='<?php echo BASE_URL.'logout'; ?>' ><span class="fa fa-sign-out text-danger"></span>  Logout</a>
            </div>
            <hr class="hr-line"/>
            <a href="<?php echo BASE_URL.'dashboard'; ?>" class='dropdown-link'><i class="fa fa-home"></i> Dashboard</a>

            <?php if(@$_SESSION["user_type"] == '1'){ ?>
                <a href="<?php echo BASE_URL.'lead-list?type=4';?>" class='dropdown-link'><i class="fa fa-bar-chart"></i> Leads</a>  
                <a href="<?php echo BASE_URL.'lead-list?type=8';?>" class='dropdown-link'><i class="fa fa-suitcase"></i> Checkouts </a>            
                <a href="<?php echo BASE_URL.'global-checkout-list';?>" class='dropdown-link'><i class="fa fa-suitcase"></i>Global Checkouts </a>            
                <a href="<?php echo BASE_URL.'followup-list';?>" class='dropdown-link'><i class="fa fa-envelope"></i>Followup Types </a>            
                <a href="<?php echo BASE_URL.'followed-list';?>" class='dropdown-link'><i class="fa fa-users"></i>Followed Leads </a>            
            <?php } ?>
        </div>
    </div>
    <div class="main-block">
        <a class="link-crm sm-hidden" href='<?=ADMIN_URL?>' target="_blank"><span class="fa fa-arrow-right"></span> Go To Admin Panel  </a>

        <div class="row">
            <div class="col-md-4 logo md-hidden">
                <img title="Go To Home" onclick="window.location.href='<?php echo BASE_URL; ?>'" src="<?php echo BASE_URL.'assets/images/prospect-research-reports-logo.jpg' ?>"/>
            </div>
            <div class="col-md-1 md-hidden">
                <span class="nav-btn" style="display: inline-block;" onclick="openNav()">&#9776;</span>
            </div>
            <div id="logMeOut" class="modal fade" role="dialog">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title">Logout Confirmation <span class="fa fa-sign-out"></span>  </h4>
                        </div>
                        <div class="modal-body">
                            <p style="font-size: 1.2em"><strong>Are you sure you want to make log out ?</strong></p>
                        </div>
                        <div class="modal-footer">
                            <button class="btn btn-primary btn-confirm" onclick="window.location.href='<?php echo BASE_URL.'logout'; ?>'">Yes, Log Out</button>
                            <button type="button" class="btn btn-warning btn-cancel" data-dismiss="modal">Cancel</button>
                        </div>
                    </div>
                </div>
            </div>
        </div><hr style="border-bottom: 1px solid #efefef">
        
