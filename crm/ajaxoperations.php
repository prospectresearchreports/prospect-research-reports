<?php require_once 'config.php';



    //********* Handle Interval Form Data **********/
    if(isset($_POST['interval_type']) && isset($_POST['followup_interval'])){
        $lead_contact_id = mysqli_real_escape_string($link,$_POST['contact_id']);
        $followup_interval = mysqli_real_escape_string($link,$_POST['followup_interval']);
        $interval_type = mysqli_real_escape_string($link,$_POST['interval_type']);
        $interval_set_date = date("Y-m-d H:i:s");
        $update_qyr = $link->query("update mr_form_contact set interval_set_date='".$interval_set_date."',interval_type='".$interval_type."', followup_interval='".$followup_interval."' where contact_id=".$lead_contact_id);
        if($update_qyr){
            echo "Success";
        }else{
            echo "Error";
        }
    }

// ***********************************  STATUS      ******************************************    


    if(isset($_POST["contact_id"]) && isset($_POST["followup_status"]) )
    {
        $contact_id=mysqli_real_escape_string($link,$_POST["contact_id"]);
        $followup_status=mysqli_real_escape_string($link,$_POST["followup_status"]);
        $result = 'Error';
        $interval_set_date = date("Y-m-d H:i:s");
        if($followup_status=="0")
        {            
            $query=mysqli_query($link,"update mr_form_contact set followup_status=1, interval_set_date = '".$interval_set_date."'  where contact_id=$contact_id");        
            if($query){
                $result = "Success";
            }
        }
        if($followup_status=="1")
        {
            $query=mysqli_query($link,"update  mr_form_contact set followup_status=0 , interval_set_date = '".$interval_set_date."' where contact_id=$contact_id");            
            if($query){
                $result = "Success";
            }
        }  
        echo $result;  
    }
    // ***********************************  STATUS TOGGLE  ***********************************    
    
        if(isset($_POST["table"]) && isset($_POST['status_value']) && isset($_POST['id']) && isset($_POST['to_be_changed']))
        {
            $table = mysqli_real_escape_string($link,$_POST["table"]);
            $id = mysqli_real_escape_string($link,$_POST["id"]);
            $to_be_changed = mysqli_real_escape_string($link,$_POST["to_be_changed"]);
            $status_value = mysqli_real_escape_string($link,$_POST["status_value"]);
                        
            $update_qyr=mysqli_query($link,"update ".@$STATUS_TABLES[$table]." set ".@$STATUS_TYPES[$to_be_changed]."=$status_value where id=$id");        
            if($update_qyr){
                echo "Success";
            }else{
                echo "Error";
            }  
        }
    
    //*************************************   DELETE    **********************************************//
    
    if(isset($_POST["type"]) && $_POST['type'] == 'delete' && isset($_POST["table"]) &&  isset($_POST['id']))
    {
        $table = mysqli_real_escape_string($link,$_POST["table"]);
        $id = mysqli_real_escape_string($link,$_POST["id"]);
        $where = " where id = $id";
        if($table == '8'){
            $where = " where contact_id = $id";
        }
        $query=mysqli_query($link,"delete from ".@$STATUS_TABLES[$table].@$where);
        if($query){
            echo "Success";
        }else{
            echo 'Error'.(ENVIRONMENT == 'development' ? mysqli_error($link) : '');
        }    
    }
    
?>

