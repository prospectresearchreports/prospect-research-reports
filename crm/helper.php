<?php
$STATUS = array(
	'0' => 'Inactive',
	'1' => 'Active'
);

$ARCHIVE_STATUS = array(
	'0' => 'Not Achived',
	'1' => 'Archived'
);

$USER_TYPES = array(
	'1' => 'Admin',
	'2' => 'Digital Marketing',
	'3' => 'Sales Manager', 
	'4' => 'PHP Developer'
);
$PAYMENT_MODES = array(
	'1' => 'Paypal',
	'2' => 'EazyPay',
	'0' => 'NA',
	'' => 'NA'
);
$RECORDS_PER_PAGE = array(
	'1' => '1',
	'5' => '5',
	'10' => '10',
	'15' => '15', 
	'25' => '25', 
	'50' => '50',
	'100' => '100',
	'250' => '250', 
	'500' => '500'	
);
$FORM_TYPES = array(
	'1' => 'Contact Query', 
	'2' => 'Custom Research Query',
	'3' => 'Search Query', 
	'4' => 'Request Sample Query', 
	'5' => 'Request Customization Query', 
	'6' => 'Enquire Before Purchase Query', 
	'7' => 'Request Discount Query', 
	'8' => 'Checkout Query',	 
	'9' => 'Report Details Query',	
);
$STATUS_TABLES = array(
	'1' => 'mr_blogs', 
	'2' => 'mr_press',
	'3' => 'mr_publisher', 
	'4' => 'mr_report', 
	'5' => 'mr_sub_cat_1', 
	'6' => 'mr_login', 
	'7' => 'mr_bannedwords',
	'8' => 'mr_form_contact',
	'9' => 'mr_global_checkout'	
);
$STATUS_TYPES = array(
	'1' => 'status', 
	'2' => 'archive_status'	
);
$FOLLOWUPS = array(
	'1' => 'First', 
	'2' => 'Second',
	'3' => 'Third', 
	'4' => 'Fourth', 
	'5' => 'Fifth', 
	'6' => 'Sixth', 
	'7' => 'Seventh', 
	'8' => 'Eighth'	
);
$FOLLOWUP_STATUS = array(
	'0' => 'Click Here To Resume Auto Followup', 
	'1' => 'Click Here To Stop Auto Followup',
);
function encrypt_decrypt($type,$text){

	$auth_iv = '1234567891011123';  // encryption/decryption iv (Initialization Vector) 
	$auth_key = "prospectresearchreports";    // encryption/decryption key 
	$ciphering = "AES-256-CBC";    // Store the cipher method 
	$iv_length = openssl_cipher_iv_length($ciphering);
	$options = 0;    // Store the cipher method 
	$key = hash('sha256', $auth_key);
	$iv = substr(hash('sha256', $auth_iv), 0, 16);



	$string='';
	if(@$type === 'encrypt'){
		$string=openssl_encrypt($text, $ciphering, 
            $key, $options, $iv); 
		$string = base64_encode($string);

	}elseif (@$type === 'decrypt') {
		$string=openssl_decrypt(base64_decode($text), $ciphering, 
            $key, $options, $iv); 
	}
	return $string;
}

function setTags($data){
	$Tags = array();
	if(!empty($data) && isset($data['rep_title']) && isset($data['email']) ){
		$Tags_1 = $Tags_2 = $Tags_3 = array();
		if(isset($data['rep_title']) && !empty(@$data['rep_title'])){			
			$Tags_1 = array(
				1 => array(
					'tag' => '{{rep_title}}',
					'value' => '<b>'.$data['rep_title'].'</b>',
				),
				2 => array(
					'tag' => '{{report_title}}',
					'value' => '<b>'.$data['rep_title'].'</b>',
				),
				3 => array(
					'tag' => '{{REP_TITLE}}',
					'value' => '<b>'.$data['rep_title'].'</b>',
				),
				4 => array(
					'tag' => '{{REPORT_TITLE}}',
					'value' => '<b>'.$data['rep_title'].'</b>',
				),
			);
		}
		if(isset($data['name']) && !empty(@$data['name'])){
			$Tags_2 = array(
				1 => array(
					'tag' => '{{name}}',
					'value' => @$data['name'],
				),
				2 => array(
					'tag' => '{{NAME}}',
					'value' => @$data['name'],
				)
			);
		}
		if(isset($data['email']) && !empty(@$data['email'])){
			$Tags_3 = array(
				1 => array(
					'tag' => '{{email}}',
					'value' => @$data['email'],
				),
				2 => array(
					'tag' => '{{EMAIL}}',
					'value' => @$data['email'],
				)
			);
		}
		$Tags = array_merge($Tags_1, $Tags_2, $Tags_3); // $Tags_1 + $Tags_2 + $Tag_3;
	}		
	return $Tags;	
}

function replaceTags($message='',  $tags = array()){
	if(!empty($tags)){
		foreach($tags as $k => $tag){
			$message = ($tag['value'] != '') ? preg_replace('/'.$tag['tag'].'/', $tag['value'], $message) : preg_replace('/'.$tag['tag'].'/', '', $message);
		}
	}
	$message = trim($message);
	$message = trim($message, ',');
	$message = trim($message);
	return $message;
}
function pagination($data = array()){
	$pagination = '';
	if(!empty($data)){
		$pagination = "<li><a href='".@$data['first_page']."'>First</a></li>";

		$prev_cls = (@$data['pageno'] <= 1) ?   'disabled' : '';
		$prev_link = @$data['pageno'] <= 1 ? '#' :  @$data['prev_page'];
		$next_cls = (@$data['pageno'] >= @$data['total_pages']) ? 'disabled' : '';
		$next_link = (@$data['pageno'] >= @$data['total_pages']) ? '#' : @$data['next_page'];
		$last_cls = (@$data['pageno'] == @$data['total_pages']) ?   'disabled' : '';
		$last_link = (@$data['pageno'] == @$data['total_pages']) ? '#' : @$data['last_page'];

		$pagination.="<li class='".$prev_cls."'>";
		$pagination.="<a href='".@$prev_link."'>Prev</a>";
		$pagination.="</li>";
		$pagination.="<li class='".$next_cls."'>";
		$pagination.="<a href='".$next_link."'>Next</a>";
		$pagination.="</li>";
		$pagination.="<li class='".$last_cls."'>";
		$pagination.="<a href='".$last_link."'>Last</a>";
		$pagination.="</li>";
	}
	return $pagination;
}
function create_links($GET, $link, $table, $where = '', $contact_form_type = '', $is_followed_list = false){
	$data = array();
	$pageno = 1;  
	$no_of_records_per_page = 10; 
	$page_query = ''; 
	if(!empty($contact_form_type)){
		$contact_form_type="&type=$contact_form_type";
	}
	if (isset($GET['pageno']) && $GET['pageno'] > 0) {
		$pageno = $GET['pageno'];
	} 
	if (isset($GET['query']) && !empty($GET['query'])) {
		$page_query = "&query=".urlencode($GET['query']);
	} 
	if (isset($GET['per_page'])) {
		$no_of_records_per_page = $GET['per_page'];
	}
	$data['offset'] = ($pageno-1) * $no_of_records_per_page;
	$data['no_of_records_per_page'] = $no_of_records_per_page;
	$data['pageno'] = $pageno;
	if($is_followed_list){
		$query="select count(distinct(A.contact_id)) from ".@$table.' B inner join mr_crm_followups A on B.contact_id = A.contact_id '.@$where;
	}else{
		$query="select count(*) from ".@$table.@$where;
	}
	// echo $query;die;

	$result = $link->query($query);
	$data['total_rows'] = $total_rows = mysqli_fetch_array(@$result)[0];
	$data['total_pages'] = $total_pages = ceil($total_rows / $no_of_records_per_page);


	// PAGINATION LINKS
	$per_page = "?per_page=$no_of_records_per_page";

	$data['first_page'] = "$per_page&pageno=1".$page_query.$contact_form_type;
	$data['last_page'] = "$per_page&pageno=".$total_pages.$page_query.$contact_form_type;
	$data['next_page'] = "$per_page&pageno=".($pageno + 1).$page_query.$contact_form_type;
	$data['prev_page'] = "$per_page&pageno=".($pageno - 1).$page_query.$contact_form_type;
	return $data;
}
function getStatus($id, $status, $STATUS = array(), $checked, $table, $to_be_changed){
	$status_res = '';
	if(@$id > 0 && @$status >= 0 && !empty($table)){
		$status_res.='<td>';
		$status_res.='<label data-toggle="tooltip" title="'.@$STATUS[@$status].'" class="switch" onchange="updateStatus('.$id.','.@$status.','.@$to_be_changed.','.@$table.')">';
		$status_res.='<input id="status_'.$id.'"  type="checkbox" '.@$checked.'>';
		$status_res.='<span class="slider round"></span>';
		$status_res.='</label>';
		$status_res.='</td>';
	}
	return $status_res;
}
 
function getArchiveStatus($id, $archive_status, $ARCHIVE_STATUS = array(), $checked, $table, $to_be_changed){
	$archive_status_res = '';
	if(@$id > 0 && @$archive_status >= 0 && !empty($table)){
		$archive_status_res.='<td>';
		$archive_status_res.='<label data-toggle="tooltip" title="'.@$ARCHIVE_STATUS[@$archive_status].'" class="switch" onchange="updateStatus('.$id.','.@$archive_status.','.@$to_be_changed.','.@$table.')">';
		$archive_status_res.='<input id="archive_status_'.$id.'"  type="checkbox" '.@$checked.'>';
		$archive_status_res.='<span class="slider round"></span>';
		$archive_status_res.='</label>';
		$archive_status_res.='</td>';
	}
	return $archive_status_res;
}
 
