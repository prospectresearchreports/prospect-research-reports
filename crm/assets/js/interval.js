    
$(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip();
});

$('#interval_type').change(function(){
    var interval_type = parseInt($("#interval_type").val());
    $('#followup_interval').attr('max','100');
    if(interval_type > 0){
        if([1,2].includes(interval_type)){
            $('#followup_interval').attr('max','59');
        }else if(interval_type === 3){
            $('#followup_interval').attr('max','24');
        }else if(interval_type === 4){
            $('#followup_interval').attr('max','30');
        }else if(interval_type === 5){
            $('#followup_interval').attr('max','52');
        }else if(interval_type === 6){
            $('#followup_interval').attr('max','12');
        }else if(interval_type === 5){
            $('#followup_interval').attr('max','100');
        }
    }
})
function setFollowupInterval(contact_id, base_url) {
    var followup_interval = $("#followup_interval_"+contact_id).val()
    var interval_type = $("#interval_type_"+contact_id).val()
    $("#followup_error_"+contact_id).html("").css({display : 'none'});
    $("#followup_success_"+contact_id).html("").css({display : 'none'});
    if(followup_interval !== '' && interval_type !== ''){
        var form = $("#interval_form_"+contact_id)
        $("#followup_error_"+contact_id).html("").css({display : 'none'});
        $.ajax({
            url : 'ajaxoperations.php',
            type : 'post',
            data : form.serialize(),
            async : true,
            success : function(data){
                let response = data.trim();
                if(response.length > 0 && response === 'Success'){
                    $("#followup_success_"+contact_id).html("Followup Interval Set Successfully <meta http-equiv='refresh' content='2, url="+base_url+"followed-list' />").css({display : 'block'});
                }else{
                    alert('Followup Interval Set Request Failed');
                }
            }
        })
    }else{
        $("#followup_error_"+contact_id).html("All Fields Marked with * Are Mandatory").css({display : 'block'});
    }
}
function updateLeadFollowupStatus(contact_id, followup_status){
    // console.log(contact_id,followup_status)
    if(contact_id > 0){
        var is_interval_set = parseInt($("#is_interval_set_"+contact_id).val())
        if(is_interval_set > 0){
            $.ajax({
                url : 'ajaxoperations.php',
                type : 'post',
                data : { contact_id : contact_id, followup_status : followup_status },
                async : true,
                success : function(data){
                    let response = data.trim();
                    if(response.length > 0 && response === 'Success'){
                        window.location.reload();
                    }else{
                        alert('Status Update Request Failed');
                    }
                }
            })
        }else{
            $("#followup_" + contact_id).removeAttr('checked');
            alert("Please Set Followup Interval for Lead Contact ID : " + contact_id);
        }
    }
}

function confirmDelete(id)
{
    var xhr=new XMLHttpRequest();
    xhr.onreadystatechange=function()
    {
        if(this.readyState===4 && this.status===200)
        {
            var response=this.responseText;
            if(response.trim()!==""){
                document.getElementById("deleteCount").innerHTML=this.responseText;
            }else{
                $("#leadDelete"+id).css({display:'none'});
            }
        }
    }
    xhr.open("get","ajaxoperations.php?leadDeleteId="+id,true);
    xhr.send();
}